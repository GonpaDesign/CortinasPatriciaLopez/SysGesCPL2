﻿Imports System.Data 'Importa libreria para manejo de datos
Imports System.Data.SqlClient
Imports System
Imports System.Configuration

Public Class DbProducto

    Public Function SelectProductosCategoriaYProveedor(UnProducto As Producto) As DataTable
        Dim UnaConexion As New SqlConnection
        UnaConexion.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("bdCPL-SQL").ConnectionString
        UnaConexion.Open()

        Dim UnComando As New SqlCommand
        UnComando.Connection = UnaConexion
        UnComando.CommandText = "SelectProductoPorCategoriaProveedor"
        UnComando.CommandType = CommandType.StoredProcedure
        UnComando.Parameters.AddWithValue("@IdCategoria", UnProducto.IdCategoria)
        UnComando.Parameters.AddWithValue("@IdProveedor", UnProducto.IdProveedor)
        UnComando.ExecuteNonQuery()

        Dim UnAdaptador As New SqlDataAdapter
        SelectProductosCategoriaYProveedor = New DataTable

        UnAdaptador.SelectCommand = UnComando
        UnAdaptador.Fill(SelectProductosCategoriaYProveedor)

        UnaConexion.Close()
        Return SelectProductosCategoriaYProveedor
    End Function
    Public Function SelectProductosPorCategoria(UnProducto As Producto) As DataTable
        Dim UnaConexion As New SqlConnection
        UnaConexion.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("bdCPL-SQL").ConnectionString
        UnaConexion.Open()

        Dim UnComando As New SqlCommand
        UnComando.Connection = UnaConexion
        UnComando.CommandText = "SelectProductoPorCategoria"
        UnComando.CommandType = CommandType.StoredProcedure
        UnComando.Parameters.AddWithValue("@IdCategoria", UnProducto.IdCategoria)
        UnComando.ExecuteNonQuery()

        Dim UnAdaptador As New SqlDataAdapter
        SelectProductosPorCategoria = New DataTable

        UnAdaptador.SelectCommand = UnComando
        UnAdaptador.Fill(SelectProductosPorCategoria)

        UnaConexion.Close()
        Return SelectProductosPorCategoria
    End Function
    Public Function SelectProductosPorProveedor(UnProducto As Producto) As DataTable
        Dim UnaConexion As New SqlConnection
        UnaConexion.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("bdCPL-SQL").ConnectionString
        UnaConexion.Open()

        Dim UnComando As New SqlCommand
        UnComando.Connection = UnaConexion
        UnComando.CommandText = "SelectProductoPorProveedor"
        UnComando.CommandType = CommandType.StoredProcedure
        UnComando.Parameters.AddWithValue("@IdProveedor", UnProducto.IdProveedor)
        UnComando.ExecuteNonQuery()

        Dim UnAdaptador As New SqlDataAdapter
        SelectProductosPorProveedor = New DataTable

        UnAdaptador.SelectCommand = UnComando
        UnAdaptador.Fill(SelectProductosPorProveedor)

        UnaConexion.Close()
        Return SelectProductosPorProveedor
    End Function
    Public Function SelectAllProductos() As DataTable
        Dim UnaConexion As New SqlConnection
        UnaConexion.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("bdCPL-SQL").ConnectionString
        UnaConexion.Open()

        Dim UnComando As New SqlCommand
        UnComando.Connection = UnaConexion
        UnComando.CommandText = "SelectAllProductos"
        UnComando.CommandType = CommandType.StoredProcedure
        UnComando.ExecuteNonQuery()

        Dim UnAdaptador As New SqlDataAdapter
        SelectAllProductos = New DataTable

        UnAdaptador.SelectCommand = UnComando
        UnAdaptador.Fill(SelectAllProductos)

        UnaConexion.Close()
    End Function
    Public Function SelectProductoPorId(UnProducto As Producto) As DataTable
        Dim UnaConexion As New SqlConnection
        UnaConexion.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("bdCPL-SQL").ConnectionString
        UnaConexion.Open()

        Dim UnComando As New SqlCommand
        UnComando.Connection = UnaConexion
        UnComando.CommandText = "SelectProductoPorId"
        UnComando.CommandType = CommandType.StoredProcedure
        UnComando.Parameters.AddWithValue("@IdProducto", UnProducto.IdProducto)
        UnComando.ExecuteNonQuery()

        Dim UnAdaptador As New SqlDataAdapter
        SelectProductoPorId = New DataTable

        UnAdaptador.SelectCommand = UnComando
        UnAdaptador.Fill(SelectProductoPorId)

        UnaConexion.Close()
        Return SelectProductoPorId
    End Function
    Public Function ObtenerNuevoId() As Integer
        Dim UnaConexion As New SqlConnection
        UnaConexion.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("bdCPL-SQL").ConnectionString
        UnaConexion.Open()

        Dim UnComando As New SqlCommand
        UnComando.Connection = UnaConexion
        UnComando.CommandText = "SelectMaxIdProducto"
        UnComando.CommandType = CommandType.StoredProcedure
        UnComando.ExecuteNonQuery()

        Dim UnaTabla As DataTable
        Dim UnAdaptador As New SqlDataAdapter
        UnaTabla = New DataTable

        UnAdaptador.SelectCommand = UnComando
        UnAdaptador.Fill(UnaTabla)

        'Devuelve un solo registro, el del maximo Id
        'Convierte un DataRow en un dato tipo integer
        ObtenerNuevoId = 0
        Try
            ObtenerNuevoId = DirectCast((UnaTabla.Rows(0)(0)), Integer) + 1
        Catch
        End Try

        UnaConexion.Close()

        'Si la tabla esta vacia asigna 1 como primer Id
        If ObtenerNuevoId < 1 Then
            ObtenerNuevoId = 1
        End If
        Return ObtenerNuevoId
    End Function
    Public Sub Insert(NuevoProducto As Producto)
        Dim UnaConexion As New SqlConnection
        UnaConexion.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("bdCPL-SQL").ConnectionString
        UnaConexion.Open()

        'Calcula nuevo Id y lo asigna a propiedad de objeto cliente
        NuevoProducto.IdProducto() = ObtenerNuevoId()

        Dim UnComando As New SqlCommand
        UnComando.Connection = UnaConexion
        UnComando.CommandText = "InsertProducto"
        UnComando.CommandType = CommandType.StoredProcedure
        'Pasaje de lista de parametros: se pasan las propiedades de objeto cliente
        UnComando.Parameters.AddWithValue("@IdProducto", NuevoProducto.IdProducto)
        UnComando.Parameters.AddWithValue("@Codigo", NuevoProducto.Codigo)
        UnComando.Parameters.AddWithValue("@Nombre", NuevoProducto.Nombre)
        UnComando.Parameters.AddWithValue("@Descripcion", NuevoProducto.Descripcion)
        UnComando.Parameters.AddWithValue("@IdCategoria", NuevoProducto.IdCategoria)
        UnComando.Parameters.AddWithValue("@Unidad", NuevoProducto.Unidad)
        UnComando.Parameters.AddWithValue("@IdProveedor", NuevoProducto.IdProveedor)
        UnComando.Parameters.AddWithValue("@Costo", NuevoProducto.Costo)
        UnComando.Parameters.AddWithValue("@Fecha", NuevoProducto.Fecha)
        UnComando.ExecuteNonQuery()

        UnaConexion.Close()
    End Sub
    Public Sub Update(UnProducto As Producto)
        'Instancio la conexion
        Dim UnaConexion As New SqlConnection
        UnaConexion.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("bdCPL-SQL").ConnectionString
        'Abrimos la conexion
        UnaConexion.Open()

        Dim UnComando As New SqlCommand
        UnComando.Connection = UnaConexion
        UnComando.CommandText = "UpdateProducto"
        UnComando.CommandType = CommandType.StoredProcedure
        'Pasaje de lista de parametros: se pasan las propiedades de objeto Producto
        UnComando.Parameters.AddWithValue("@Codigo", UnProducto.Codigo)
        UnComando.Parameters.AddWithValue("@Nombre", UnProducto.Nombre)
        UnComando.Parameters.AddWithValue("@Descripcion", UnProducto.Descripcion)
        UnComando.Parameters.AddWithValue("@IdCategoria", UnProducto.IdCategoria)
        UnComando.Parameters.AddWithValue("@Unidad", UnProducto.Unidad)
        UnComando.Parameters.AddWithValue("@IdProveedor", UnProducto.IdProveedor)
        UnComando.Parameters.AddWithValue("@Costo", UnProducto.Costo)
        UnComando.Parameters.AddWithValue("@Fecha", UnProducto.Fecha)
        UnComando.Parameters.AddWithValue("@IdProducto", UnProducto.IdProducto)
        'Ejecuta comando indicado
        UnComando.ExecuteNonQuery()
        'Cierra la Conexion
        UnaConexion.Close()
    End Sub
    Public Sub Delete(UnProducto As Producto)
        'Recibe un objeto cliente como parámetro para eliminar en base de datos (solo se carga IdCliente, el resto no importa)
        'Política de borrado: Borrado lógico

        'Instancio la conexion
        Dim UnaConexion As New SqlConnection
        UnaConexion.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("bdCPL-SQL").ConnectionString
        'Abrimos la conexion
        UnaConexion.Open()

        Dim UnComando As New SqlCommand
        UnComando.Connection = UnaConexion
        UnComando.CommandText = "DeleteProducto"
        UnComando.CommandType = CommandType.StoredProcedure
        UnComando.Parameters.AddWithValue("@IdProducto", UnProducto.IdProducto)
        'Ejecuta comando indicado
        UnComando.ExecuteNonQuery()
        'Cierra la conexion
        UnaConexion.Close()
    End Sub
End Class
