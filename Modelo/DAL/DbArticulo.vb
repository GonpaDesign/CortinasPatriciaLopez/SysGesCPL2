﻿Imports System.Data 'Importa libreria para manejo de datos
Imports System.Data.SqlClient
Imports System
Imports System.Configuration

Public Class DbArticulo

    Public Function SelectArticuloPorPresupuestoAndItem(UnArticulo As Articulo) As DataTable
        Dim UnaConexion As New SqlConnection
        UnaConexion.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("bdCPL-SQL").ConnectionString
        UnaConexion.Open()

        Dim UnComando As New SqlCommand
        UnComando.Connection = UnaConexion
        UnComando.CommandText = "SelectArticuloPorPresupuestoAndItem"
        UnComando.CommandType = CommandType.StoredProcedure
        UnComando.Parameters.AddWithValue("@IdPresupuesto", UnArticulo.IdPresupuesto)
        UnComando.Parameters.AddWithValue("@Item", UnArticulo.Item)
        UnComando.ExecuteNonQuery()

        Dim UnAdaptador As New SqlDataAdapter
        SelectArticuloPorPresupuestoAndItem = New DataTable

        UnAdaptador.SelectCommand = UnComando
        UnAdaptador.Fill(SelectArticuloPorPresupuestoAndItem)

        UnaConexion.Close()
        Return SelectArticuloPorPresupuestoAndItem
    End Function
    Public Function SelectArticuloPorId(UnArticulo As Articulo) As DataTable
        Dim UnaConexion As New SqlConnection
        UnaConexion.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("bdCPL-SQL").ConnectionString
        UnaConexion.Open()

        Dim UnComando As New SqlCommand
        UnComando.Connection = UnaConexion
        UnComando.CommandText = "SelectArticuloPorId"
        UnComando.CommandType = CommandType.StoredProcedure
        UnComando.Parameters.AddWithValue("@IdArticulo", UnArticulo.IdArticulo)
        UnComando.ExecuteNonQuery()

        Dim UnAdaptador As New SqlDataAdapter
        SelectArticuloPorId = New DataTable

        UnAdaptador.SelectCommand = UnComando
        UnAdaptador.Fill(SelectArticuloPorId)

        UnaConexion.Close()
        Return SelectArticuloPorId
    End Function
    Public Function ObtenerNuevoId() As Integer
        Dim UnaConexion As New SqlConnection
        UnaConexion.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("bdCPL-SQL").ConnectionString
        UnaConexion.Open()

        Dim UnComando As New SqlCommand
        UnComando.Connection = UnaConexion
        UnComando.CommandText = "SelectMaxIdArticulo"
        UnComando.CommandType = CommandType.StoredProcedure
        UnComando.ExecuteNonQuery()

        Dim UnaTabla As DataTable
        Dim UnAdaptador As New SqlDataAdapter
        UnaTabla = New DataTable

        UnAdaptador.SelectCommand = UnComando
        UnAdaptador.Fill(UnaTabla)

        'Devuelve un solo registro, el del maximo Id
        'Convierte un DataRow en un dato tipo integer
        ObtenerNuevoId = 0
        Try
            ObtenerNuevoId = DirectCast((UnaTabla.Rows(0)(0)), Integer) + 1
        Catch
        End Try

        UnaConexion.Close()

        'Si la tabla esta vacia asigna 1 como primer Id
        If ObtenerNuevoId < 1 Then
            ObtenerNuevoId = 1
        End If
        Return ObtenerNuevoId
    End Function
    Public Sub Insert(NuevoArticulo As Articulo)
        Dim UnaConexion As New SqlConnection
        UnaConexion.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("bdCPL-SQL").ConnectionString
        UnaConexion.Open()

        'Calcula nuevo Id y lo asigna a propiedad de objeto cliente
        NuevoArticulo.IdArticulo() = ObtenerNuevoId()

        Dim UnComando As New SqlCommand
        UnComando.Connection = UnaConexion
        UnComando.CommandText = "InsertArticulo"
        UnComando.CommandType = CommandType.StoredProcedure
        'Pasaje de lista de parametros: se pasan las propiedades de objeto cliente
        UnComando.Parameters.Add(New SqlParameter("@IdArticulo", NuevoArticulo.IdArticulo))
        UnComando.Parameters.Add(New SqlParameter("@IdPresupuesto", NuevoArticulo.IdPresupuesto))
        UnComando.Parameters.Add(New SqlParameter("@Item", NuevoArticulo.Item))
        UnComando.Parameters.Add(New SqlParameter("@Nombre", NuevoArticulo.Nombre))
        UnComando.Parameters.Add(New SqlParameter("@Observacion", NuevoArticulo.Observacion))
        UnComando.Parameters.Add(New SqlParameter("@Dim1", NuevoArticulo.Dim1))
        UnComando.Parameters.Add(New SqlParameter("@Dim2", NuevoArticulo.Dim2))
        UnComando.Parameters.Add(New SqlParameter("@Dim3", NuevoArticulo.Dim3))
        UnComando.Parameters.Add(New SqlParameter("@Costo", NuevoArticulo.Costo))
        UnComando.Parameters.Add(New SqlParameter("@Precio", NuevoArticulo.Precio))
        'Ejecuta comando indicado
        UnComando.ExecuteNonQuery()
        UnaConexion.Close()
    End Sub
    Public Sub Update(UnArticulo As Articulo)
        'Instancio la conexion
        Dim UnaConexion As New SqlConnection
        UnaConexion.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("bdCPL-SQL").ConnectionString
        'Abrimos la conexion
        UnaConexion.Open()

        Dim UnComando As New SqlCommand
        UnComando.Connection = UnaConexion
        UnComando.CommandText = "UpdateArticulo"
        UnComando.CommandType = CommandType.StoredProcedure
        'Pasaje de lista de parametros: se pasan las propiedades de objeto cliente
        UnComando.Parameters.AddWithValue("@Nombre", UnArticulo.Nombre)
        UnComando.Parameters.AddWithValue("@Observacion", UnArticulo.Observacion)
        UnComando.Parameters.AddWithValue("@Dim1", UnArticulo.Dim1)
        UnComando.Parameters.AddWithValue("@Dim2", UnArticulo.Dim2)
        UnComando.Parameters.AddWithValue("@Dim3", UnArticulo.Dim3)
        UnComando.Parameters.AddWithValue("@Costo", UnArticulo.Costo)
        UnComando.Parameters.AddWithValue("@Precio", UnArticulo.Precio)
        UnComando.Parameters.AddWithValue("@Eliminado", UnArticulo.Eliminado)
        UnComando.Parameters.AddWithValue("@IdPresupuesto", UnArticulo.IdPresupuesto)
        UnComando.Parameters.AddWithValue("@Item", UnArticulo.Item)
        'Ejecuta comando indicado
        UnComando.ExecuteNonQuery()
        'Cierra la Conexion
        UnaConexion.Close()
    End Sub
    Public Sub Delete(UnArticulo As Articulo)
        'Recibe un objeto cliente como parámetro para eliminar en base de datos (solo se carga IdCliente, el resto no importa)
        'Política de borrado: Borrado lógico

        'Instancio la conexion
        Dim UnaConexion As New SqlConnection
        UnaConexion.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("bdCPL-SQL").ConnectionString
        'Abrimos la conexion
        UnaConexion.Open()

        Dim UnComando As New SqlCommand
        UnComando.Connection = UnaConexion
        UnComando.CommandText = "DeleteArticulo"
        UnComando.CommandType = CommandType.StoredProcedure
        'Pasaje de lista de parametros: se pasan las propiedades de objeto cliente
        UnComando.Parameters.AddWithValue("@IdArticulo", UnArticulo.IdArticulo)
        'Ejecuta comando indicado
        UnComando.ExecuteNonQuery()
        UnaConexion.Close()
    End Sub
End Class
