﻿Imports System.Data 'Importa libreria para manejo de datos
Imports System.Data.SqlClient
Imports System
Imports System.Configuration

Public Class DbCategoria

    Public Function SelectCategoriaPorId(UnaCategoria As Categoria) As DataTable
        Dim UnaConexion As New SqlConnection
        UnaConexion.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("bdCPL-SQL").ConnectionString
        UnaConexion.Open()

        Dim UnComando As New SqlCommand
        UnComando.Connection = UnaConexion
        UnComando.CommandText = "SelectCategoriaPorId"
        UnComando.CommandType = CommandType.StoredProcedure
        UnComando.Parameters.AddWithValue("@IdCategoria", UnaCategoria.IdCategoria)
        UnComando.ExecuteNonQuery()

        Dim UnAdaptador As New SqlDataAdapter
        SelectCategoriaPorId = New DataTable

        UnAdaptador.SelectCommand = UnComando
        UnAdaptador.Fill(SelectCategoriaPorId)

        UnaConexion.Close()
        Return SelectCategoriaPorId
    End Function
    Public Function SelectAllCategorias() As DataTable
        Dim UnaConexion As New SqlConnection
        UnaConexion.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("bdCPL-SQL").ConnectionString
        UnaConexion.Open()

        Dim UnComando As New SqlCommand
        UnComando.Connection = UnaConexion
        UnComando.CommandText = "SelectAllCategoria"
        UnComando.CommandType = CommandType.StoredProcedure
        UnComando.ExecuteNonQuery()

        Dim UnAdaptador As New SqlDataAdapter
        SelectAllCategorias = New DataTable

        UnAdaptador.SelectCommand = UnComando
        UnAdaptador.Fill(SelectAllCategorias)

        UnaConexion.Close()
    End Function
    Public Function ObtenerNuevoId() As Integer
        Dim UnaConexion As New SqlConnection
        UnaConexion.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("bdCPL-SQL").ConnectionString
        UnaConexion.Open()

        Dim UnComando As New SqlCommand
        UnComando.Connection = UnaConexion
        UnComando.CommandText = "SelectMaxIdCategoria"
        UnComando.CommandType = CommandType.StoredProcedure
        UnComando.ExecuteNonQuery()

        Dim UnaTabla As DataTable
        Dim UnAdaptador As New SqlDataAdapter
        UnaTabla = New DataTable

        UnAdaptador.SelectCommand = UnComando
        UnAdaptador.Fill(UnaTabla)

        'Devuelve un solo registro, el del maximo Id
        'Convierte un DataRow en un dato tipo integer
        ObtenerNuevoId = 0
        Try
            ObtenerNuevoId = DirectCast((UnaTabla.Rows(0)(0)), Integer) + 1
        Catch
        End Try

        UnaConexion.Close()

        'Si la tabla esta vacia asigna 1 como primer Id
        If ObtenerNuevoId < 1 Then
            ObtenerNuevoId = 1
        End If
        Return ObtenerNuevoId
    End Function
    Public Sub Insert(UnaCategoria As Categoria)
        Dim UnaConexion As New SqlConnection
        UnaConexion.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("bdCPL-SQL").ConnectionString
        UnaConexion.Open()

        'Calcula nuevo Id y lo asigna a propiedad de objeto cliente
        UnaCategoria.IdCategoria() = ObtenerNuevoId()

        Dim UnComando As New SqlCommand
        UnComando.Connection = UnaConexion
        UnComando.CommandText = "InsertCategoria"
        UnComando.CommandType = CommandType.StoredProcedure

        'Pasaje de lista de parametros: se pasan las propiedades de objeto Ciudad
        UnComando.Parameters.AddWithValue("@IdCategoria", UnaCategoria.IdCategoria)
        UnComando.Parameters.AddWithValue("@Categoria", UnaCategoria.Categoria)
        'Ejecuta comando indicado
        UnComando.ExecuteNonQuery()
        UnaConexion.Close()
    End Sub
    Public Sub Update(UnaCategoria As Categoria)
        'Instancio la conexion
        Dim UnaConexion As New SqlConnection
        UnaConexion.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("bdCPL-SQL").ConnectionString
        'Abrimos la conexion
        UnaConexion.Open()

        Dim UnComando As New SqlCommand
        UnComando.Connection = UnaConexion
        UnComando.CommandText = "UpdateCategoria"
        UnComando.CommandType = CommandType.StoredProcedure
        'Pasaje de lista de parametros: se pasan las propiedades de objeto Ciudad
        UnComando.Parameters.AddWithValue("@IdCategoria", UnaCategoria.IdCategoria)
        UnComando.Parameters.AddWithValue("@Categoria", UnaCategoria.Categoria)
        'Ejecuta comando indicado
        UnComando.ExecuteNonQuery()
        UnaConexion.Close()
    End Sub
    Public Sub Delete(UnaCategoria As Categoria)
        'Recibe un objeto ciudad como parámetro para eliminar en base de datos (solo se carga IdCiudad, el resto no importa)
        'Política de borrado: Borrado lógico

        'Instancio la conexion
        Dim UnaConexion As New SqlConnection
        UnaConexion.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("bdCPL-SQL").ConnectionString
        'Abrimos la conexion
        UnaConexion.Open()

        Dim UnComando As New SqlCommand
        UnComando.Connection = UnaConexion
        UnComando.CommandText = "DeleteCategoria"
        UnComando.CommandType = CommandType.StoredProcedure
        UnComando.Parameters.AddWithValue("@IdCategoria", UnaCategoria.IdCategoria)
        'Ejecuta comando indicado
        UnComando.ExecuteNonQuery()
        'Cierra la conexion
        UnaConexion.Close()
    End Sub
End Class
