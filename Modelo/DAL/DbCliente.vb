﻿Imports System.Data 'Importa libreria para manejo de datos
Imports System.Data.SqlClient
Imports System
Imports System.Configuration

Public Class DbCliente
    Public Function SelectAllClientes() As DataTable
        Dim UnaConexion As New SqlConnection
        UnaConexion.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("bdCPL-SQL").ConnectionString
        UnaConexion.Open()

        Dim UnComando As New SqlCommand
        UnComando.Connection = UnaConexion
        UnComando.CommandText = "SelectAllClientes"
        UnComando.CommandType = CommandType.StoredProcedure
        UnComando.ExecuteNonQuery()

        Dim UnAdaptador As New SqlDataAdapter
        SelectAllClientes = New DataTable

        UnAdaptador.SelectCommand = UnComando
        UnAdaptador.Fill(SelectAllClientes)

        UnaConexion.Close()
    End Function
    Public Function SelectClientePorId(UnCliente As Cliente) As DataTable
        Dim UnaConexion As New SqlConnection
        UnaConexion.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("bdCPL-SQL").ConnectionString
        UnaConexion.Open()

        Dim UnComando As New SqlCommand
        UnComando.Connection = UnaConexion
        UnComando.CommandText = "SelectClientesPorId"
        UnComando.CommandType = CommandType.StoredProcedure
        UnComando.Parameters.AddWithValue("@IdCliente", UnCliente.IdCliente)
        UnComando.ExecuteNonQuery()

        Dim UnAdaptador As New SqlDataAdapter
        SelectClientePorId = New DataTable

        UnAdaptador.SelectCommand = UnComando
        UnAdaptador.Fill(SelectClientePorId)

        UnaConexion.Close()
        Return SelectClientePorId
    End Function
    Public Function ObtenerNuevoId() As Integer
        Dim UnaConexion As New SqlConnection
        UnaConexion.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("bdCPL-SQL").ConnectionString
        UnaConexion.Open()

        Dim UnComando As New SqlCommand
        UnComando.Connection = UnaConexion
        UnComando.CommandText = "SelectMaxIdCliente"
        UnComando.CommandType = CommandType.StoredProcedure
        UnComando.ExecuteNonQuery()

        Dim UnaTabla As DataTable
        Dim UnAdaptador As New SqlDataAdapter
        UnaTabla = New DataTable

        UnAdaptador.SelectCommand = UnComando
        UnAdaptador.Fill(UnaTabla)

        'Devuelve un solo registro, el del maximo Id
        'Convierte un DataRow en un dato tipo integer
        ObtenerNuevoId = 0
        Try
            ObtenerNuevoId = DirectCast((UnaTabla.Rows(0)(0)), Integer) + 1
        Catch
        End Try

        UnaConexion.Close()

        'Si la tabla esta vacia asigna 1 como primer Id
        If ObtenerNuevoId < 1 Then
            ObtenerNuevoId = 1
        End If
        Return ObtenerNuevoId
    End Function
    Public Sub Insert(UnCliente As Cliente)
        Dim UnaConexion As New SqlConnection
        UnaConexion.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("bdCPL-SQL").ConnectionString
        UnaConexion.Open()

        'Calcula nuevo Id y lo asigna a propiedad de objeto cliente
        UnCliente.IdCliente() = ObtenerNuevoId()

        Dim UnComando As New SqlCommand
        UnComando.Connection = UnaConexion
        UnComando.CommandText = "InsertCliente"
        UnComando.CommandType = CommandType.StoredProcedure
        UnComando.Parameters.Add(New SqlParameter("@IdCliente", UnCliente.IdCliente))
        UnComando.Parameters.Add(New SqlParameter("@Nombre", UnCliente.Nombre))
        UnComando.Parameters.Add(New SqlParameter("@RazonSocial", UnCliente.RazonSocial))
        UnComando.Parameters.Add(New SqlParameter("@Web", UnCliente.Web))
        UnComando.Parameters.Add(New SqlParameter("@Direccion", UnCliente.Direccion))
        UnComando.Parameters.Add(New SqlParameter("@IdCiudad", UnCliente.IdCiudad))
        UnComando.Parameters.Add(New SqlParameter("@CP", UnCliente.CP))
        UnComando.Parameters.Add(New SqlParameter("@CUIT", UnCliente.Cuit))
        UnComando.Parameters.Add(New SqlParameter("@DNI", UnCliente.DNI))
        UnComando.Parameters.Add(New SqlParameter("@FechaNac", UnCliente.FechaNac))
        UnComando.Parameters.Add(New SqlParameter("@Telefono1", UnCliente.Telefono1))
        UnComando.Parameters.Add(New SqlParameter("@Telefono2", UnCliente.Telefono2))
        UnComando.Parameters.Add(New SqlParameter("@Telefono3", UnCliente.Telefono3))
        UnComando.Parameters.Add(New SqlParameter("@Movil1", UnCliente.Movil1))
        UnComando.Parameters.Add(New SqlParameter("@Movil2", UnCliente.Movil2))
        UnComando.Parameters.Add(New SqlParameter("@Movil3", UnCliente.Movil3))
        UnComando.Parameters.Add(New SqlParameter("@Mail1", UnCliente.Mail1))
        UnComando.Parameters.Add(New SqlParameter("@Mail2", UnCliente.Mail2))
        UnComando.Parameters.Add(New SqlParameter("@Mail3", UnCliente.Mail3))
        UnComando.Parameters.Add(New SqlParameter("@Contacto1", UnCliente.Contacto1))
        UnComando.Parameters.Add(New SqlParameter("@Contacto2", UnCliente.Contacto2))
        UnComando.Parameters.Add(New SqlParameter("@Contacto3", UnCliente.Contacto3))
        UnComando.ExecuteNonQuery()

        UnaConexion.Close()
    End Sub
    Public Sub Update(UnCliente As Cliente)
        'Instancio la conexion
        Dim UnaConexion As New SqlConnection
        UnaConexion.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("bdCPL-SQL").ConnectionString
        'Abrimos la conexion
        UnaConexion.Open()

        Dim UnComando As New SqlCommand
        UnComando.Connection = UnaConexion
        UnComando.CommandText = "UpdateCliente"
        UnComando.CommandType = CommandType.StoredProcedure
        UnComando.Parameters.AddWithValue("@IdCliente", UnCliente.IdCliente)
        UnComando.Parameters.AddWithValue("@Nombre", UnCliente.Nombre)
        UnComando.Parameters.AddWithValue("@RazonSocial", UnCliente.RazonSocial)
        UnComando.Parameters.AddWithValue("@Web", UnCliente.Web)
        UnComando.Parameters.AddWithValue("@Direccion", UnCliente.Direccion)
        UnComando.Parameters.AddWithValue("@IdCiudad", UnCliente.IdCiudad)
        UnComando.Parameters.AddWithValue("@CP", UnCliente.CP)
        UnComando.Parameters.AddWithValue("@CUIT", UnCliente.Cuit)
        UnComando.Parameters.AddWithValue("@DNI", UnCliente.DNI)
        UnComando.Parameters.AddWithValue("@FechaNac", UnCliente.FechaNac)
        UnComando.Parameters.AddWithValue("@Telefono1", UnCliente.Telefono1)
        UnComando.Parameters.AddWithValue("@Telefono2", UnCliente.Telefono2)
        UnComando.Parameters.AddWithValue("@Telefono3", UnCliente.Telefono3)
        UnComando.Parameters.AddWithValue("@Movil1", UnCliente.Movil1)
        UnComando.Parameters.AddWithValue("@Movil2", UnCliente.Movil2)
        UnComando.Parameters.AddWithValue("@Movil3", UnCliente.Movil3)
        UnComando.Parameters.AddWithValue("@Mail1", UnCliente.Mail1)
        UnComando.Parameters.AddWithValue("@Mail2", UnCliente.Mail2)
        UnComando.Parameters.AddWithValue("@Mail3", UnCliente.Mail3)
        UnComando.Parameters.AddWithValue("@Contacto1", UnCliente.Contacto1)
        UnComando.Parameters.AddWithValue("@Contacto2", UnCliente.Contacto2)
        UnComando.Parameters.AddWithValue("@Contacto3", UnCliente.Contacto3)
        'Ejecuta comando indicado
        UnComando.ExecuteNonQuery()
        'Cierra la Conexion
        UnaConexion.Close()
    End Sub
    Public Sub Delete(UnCliente As Cliente)
        'Recibe un objeto cliente como parámetro para eliminar en base de datos (solo se carga IdCliente, el resto no importa)
        'Política de borrado: Borrado lógico

        'Instancio la conexion
        Dim UnaConexion As New SqlConnection
        UnaConexion.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("bdCPL-SQL").ConnectionString
        'Abrimos la conexion
        UnaConexion.Open()

        Dim UnComando As New SqlCommand
        UnComando.Connection = UnaConexion
        UnComando.CommandText = "DeleteCliente"
        UnComando.CommandType = CommandType.StoredProcedure
        UnComando.Parameters.AddWithValue("@IdCliente", UnCliente.IdCliente)
        'Ejecuta comando indicado
        UnComando.ExecuteNonQuery()
        'Cierra la conexion
        UnaConexion.Close()
    End Sub
End Class
