﻿Imports System.Data.OleDb 'Importa libreria para trabajar con BD Access
Imports System.Data 'Importa libreria para manejo de datos

Imports System
Imports System.Configuration
Public Class DBConfiguracion
    Public Function SelectAllConfiguracion(UnaConfiguracion As Configuracion) As DataTable
        'Abrir conexion con base datos
        Dim UnaConexion As New OleDbConnection
        UnaConexion.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("bdCPL-SQL").ConnectionString
        UnaConexion.Open()

        'Crear un comando con una consulta SQL
        Dim UnComando As New OleDbCommand("SELECT * FROM TblConfiguracion WHERE Descripcion=@Descripcion", UnaConexion)
        UnComando.Parameters.AddWithValue("@Descripcion", UnaConfiguracion.Descripcion)
        UnComando.CommandType = CommandType.Text
        'Ejecuta comando indicado
        UnComando.ExecuteNonQuery()

        'Llenar tabla con adaptador
        Dim UnAdaptador As New OleDbDataAdapter(UnComando)
        Dim UnaTabla As New DataTable
        UnAdaptador.Fill(UnaTabla)

        UnaConexion.Close()

        'Retornar tabla llena
        Return UnaTabla
    End Function
    Public Sub Update(UnaConfiguracion As Configuracion)
        'Recibe un objeto cliente como parámetro para modificar en base de datos

        'Abrir conexion con base datos
        Dim UnaConexion As New OleDbConnection
        UnaConexion.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("bdCPL-SQL").ConnectionString
        UnaConexion.Open()

        'Crear un comando con una consulta SQL con paso de parametros en linea
        Dim UnComando As New OleDbCommand("UPDATE TblConfiguracion SET Valor=@Valor WHERE Descripcion=@Descripcion", UnaConexion)

        'Pasaje de lista de parametros: se pasan las propiedades de objeto cliente
        UnComando.Parameters.AddWithValue("@Valor", UnaConfiguracion.Valor)
        UnComando.Parameters.AddWithValue("@Descripcion", UnaConfiguracion.Descripcion)
        UnComando.CommandType = CommandType.Text
        'Ejecuta comando indicado
        UnComando.ExecuteNonQuery()
        UnaConexion.Close()
    End Sub
End Class
