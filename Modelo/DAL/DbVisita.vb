﻿Imports System.Data 'Importa libreria para manejo de datos
Imports System.Data.SqlClient
Imports System
Imports System.Configuration

Public Class DbVisita

    Public Function SelectVisitaEntreFecha(UnaVisita As Visita, Dias As Integer) As DataTable
        Dim UnaConexion As New SqlConnection
        UnaConexion.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("bdCPL-SQL").ConnectionString
        UnaConexion.Open()

        Dim UnComando As New SqlCommand
        UnComando.Connection = UnaConexion
        UnComando.CommandText = "SelectVisitaEntreFechas"
        UnComando.CommandType = CommandType.StoredProcedure
        UnComando.Parameters.AddWithValue("@Fecha", UnaVisita.Fecha)
        UnComando.Parameters.AddWithValue("@Fecha1", DateAdd(DateInterval.Day, Dias, UnaVisita.Fecha))
        UnComando.ExecuteNonQuery()

        Dim UnAdaptador As New SqlDataAdapter
        SelectVisitaEntreFecha = New DataTable

        UnAdaptador.SelectCommand = UnComando
        UnAdaptador.Fill(SelectVisitaEntreFecha)

        UnaConexion.Close()
        Return SelectVisitaEntreFecha
    End Function
    Public Function ObtenerNuevoId() As Integer
        Dim UnaConexion As New SqlConnection
        UnaConexion.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("bdCPL-SQL").ConnectionString
        UnaConexion.Open()

        Dim UnComando As New SqlCommand
        UnComando.Connection = UnaConexion
        UnComando.CommandText = "SelectMaxIdVisita"
        UnComando.CommandType = CommandType.StoredProcedure
        UnComando.ExecuteNonQuery()

        Dim UnaTabla As DataTable
        Dim UnAdaptador As New SqlDataAdapter
        UnaTabla = New DataTable

        UnAdaptador.SelectCommand = UnComando
        UnAdaptador.Fill(UnaTabla)

        'Devuelve un solo registro, el del maximo Id
        'Convierte un DataRow en un dato tipo integer
        ObtenerNuevoId = 0
        Try
            ObtenerNuevoId = DirectCast((UnaTabla.Rows(0)(0)), Integer) + 1
        Catch
        End Try

        UnaConexion.Close()

        'Si la tabla esta vacia asigna 1 como primer Id
        If ObtenerNuevoId < 1 Then
            ObtenerNuevoId = 1
        End If
        Return ObtenerNuevoId
    End Function
    Public Sub Insert(NuevaVisita As Visita)
        Dim UnaConexion As New SqlConnection
        UnaConexion.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("bdCPL-SQL").ConnectionString
        UnaConexion.Open()

        'Calcula nuevo Id y lo asigna a propiedad de objeto cliente
        NuevaVisita.IdVisita() = ObtenerNuevoId()

        Dim UnComando As New SqlCommand
        UnComando.Connection = UnaConexion
        UnComando.CommandText = "InsertVisita"
        UnComando.CommandType = CommandType.StoredProcedure
        'Pasaje de lista de parametros: se pasan las propiedades de objeto Proveedor
        UnComando.Parameters.AddWithValue("@IdVisita", NuevaVisita.IdVisita)
        UnComando.Parameters.AddWithValue("@Cliente", NuevaVisita.IdCliente)
        UnComando.Parameters.AddWithValue("@Direccion", NuevaVisita.Direccion)
        UnComando.Parameters.AddWithValue("@Ciudad", NuevaVisita.IdCiudad)
        UnComando.Parameters.AddWithValue("@Observaciones", NuevaVisita.Observacion)
        UnComando.Parameters.AddWithValue("@FechaVisita", NuevaVisita.Fecha)
        'Ejecuta comando indicado
        UnComando.ExecuteNonQuery()
        'Cierra la Conexion
        UnaConexion.Close()
    End Sub
    Public Sub Update(UnVisita As Visita)
        'Instancio la conexion
        Dim UnaConexion As New SqlConnection
        UnaConexion.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("bdCPL-SQL").ConnectionString
        'Abrimos la conexion
        UnaConexion.Open()

        Dim UnComando As New SqlCommand
        UnComando.Connection = UnaConexion
        UnComando.CommandText = "UpdateVisita"
        UnComando.CommandType = CommandType.StoredProcedure
        'Pasaje de lista de parametros: se pasan las propiedades de objeto Visita
        UnComando.Parameters.AddWithValue("@IdCliente", UnVisita.IdCliente)
        UnComando.Parameters.AddWithValue("@Direccion", UnVisita.Direccion)
        UnComando.Parameters.AddWithValue("@IdCiudad", UnVisita.IdCiudad)
        UnComando.Parameters.AddWithValue("@Observacion", UnVisita.Observacion)
        UnComando.Parameters.AddWithValue("@Fecha", UnVisita.Fecha)
        UnComando.Parameters.AddWithValue("@IdVisita", UnVisita.IdVisita)
        'Ejecuta comando indicado
        UnComando.ExecuteNonQuery()
        'Cierra la Conexion
        UnaConexion.Close()
    End Sub
    Public Sub Delete(UnaVisita As Visita)
        'Recibe un objeto Visita como parámetro para eliminar en base de datos (solo se carga IdVisita, el resto no importa)
        'Política de borrado: Borrado lógico

        'Instancio la conexion
        Dim UnaConexion As New SqlConnection
        UnaConexion.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("bdCPL-SQL").ConnectionString
        'Abrimos la conexion
        UnaConexion.Open()

        Dim UnComando As New SqlCommand
        UnComando.Connection = UnaConexion
        UnComando.CommandText = "DeleteVisita"
        UnComando.CommandType = CommandType.StoredProcedure
        UnComando.Parameters.AddWithValue("@IdVisita", UnaVisita.IdVisita)
        'Ejecuta comando indicado
        UnComando.ExecuteNonQuery()
        'Cierra la conexion
        UnaConexion.Close()
    End Sub
End Class
