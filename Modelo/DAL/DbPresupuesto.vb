﻿Imports System.Data 'Importa libreria para manejo de datos
Imports System.Data.SqlClient
Imports System
Imports System.Configuration

Public Class DbPresupuesto

    '----- FUNCIONES DE FILTRADO ------
    Public Function SelectAllPresupuestos() As DataTable
        Dim UnaConexion As New SqlConnection
        UnaConexion.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("bdCPL-SQL").ConnectionString
        UnaConexion.Open()

        Dim UnComando As New SqlCommand
        UnComando.Connection = UnaConexion
        UnComando.CommandText = "SelectAllPresupuestos"
        UnComando.CommandType = CommandType.StoredProcedure
        UnComando.ExecuteNonQuery()

        Dim UnAdaptador As New SqlDataAdapter
        SelectAllPresupuestos = New DataTable

        UnAdaptador.SelectCommand = UnComando
        UnAdaptador.Fill(SelectAllPresupuestos)

        UnaConexion.Close()
    End Function
    Public Function SelectPresupuestoPorId(UnPresupuesto As Presupuesto) As DataTable
        Dim UnaConexion As New SqlConnection
        UnaConexion.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("bdCPL-SQL").ConnectionString
        UnaConexion.Open()

        Dim UnComando As New SqlCommand
        UnComando.Connection = UnaConexion
        UnComando.CommandText = "SelectPresupuestoPorId"
        UnComando.CommandType = CommandType.StoredProcedure
        UnComando.Parameters.AddWithValue("@IdPresupuesto", UnPresupuesto.IdPresupuesto)
        UnComando.ExecuteNonQuery()

        Dim UnAdaptador As New SqlDataAdapter
        SelectPresupuestoPorId = New DataTable

        UnAdaptador.SelectCommand = UnComando
        UnAdaptador.Fill(SelectPresupuestoPorId)

        UnaConexion.Close()
        Return SelectPresupuestoPorId
    End Function
    Public Function SelectPresupuestoPorColocacion(UnPresupuesto As Presupuesto) As DataTable
        Dim UnaConexion As New SqlConnection
        UnaConexion.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("bdCPL-SQL").ConnectionString
        UnaConexion.Open()

        Dim UnComando As New SqlCommand
        UnComando.Connection = UnaConexion
        UnComando.CommandText = "SelectPresupuestoPorFechaColocacion"
        UnComando.CommandType = CommandType.StoredProcedure
        UnComando.Parameters.AddWithValue("@FechaColocacion", UnPresupuesto.FechaColocacion)
        UnComando.ExecuteNonQuery()

        Dim UnAdaptador As New SqlDataAdapter
        SelectPresupuestoPorColocacion = New DataTable

        UnAdaptador.SelectCommand = UnComando
        UnAdaptador.Fill(SelectPresupuestoPorColocacion)

        UnaConexion.Close()
        Return SelectPresupuestoPorColocacion
    End Function
    Public Function SelectPresupuestoPorColocacionPendiente(UnPresupuesto As Presupuesto) As DataTable
        Dim UnaConexion As New SqlConnection
        UnaConexion.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("bdCPL-SQL").ConnectionString
        UnaConexion.Open()

        Dim UnComando As New SqlCommand
        UnComando.Connection = UnaConexion
        UnComando.CommandText = "SelectPresupuestoPorColocacionPendiente"
        UnComando.CommandType = CommandType.StoredProcedure
        UnComando.Parameters.AddWithValue("@FechaColocacion", UnPresupuesto.FechaColocacion)
        UnComando.Parameters.AddWithValue("@Sinprogramar", CDate("01/01/1900"))
        UnComando.ExecuteNonQuery()

        Dim UnAdaptador As New SqlDataAdapter
        SelectPresupuestoPorColocacionPendiente = New DataTable

        UnAdaptador.SelectCommand = UnComando
        UnAdaptador.Fill(SelectPresupuestoPorColocacionPendiente)

        UnaConexion.Close()
        Return SelectPresupuestoPorColocacionPendiente
    End Function
    Public Function SelectPresupuestoPorCliente(UnPresupuesto As Presupuesto) As DataTable
        Dim UnaConexion As New SqlConnection
        UnaConexion.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("bdCPL-SQL").ConnectionString
        UnaConexion.Open()

        Dim UnComando As New SqlCommand
        UnComando.Connection = UnaConexion
        UnComando.CommandText = "SelectPresupuestoPorCliente"
        UnComando.CommandType = CommandType.StoredProcedure
        UnComando.Parameters.AddWithValue("@IdCLiente", UnPresupuesto.IdCliente)
        UnComando.ExecuteNonQuery()

        Dim UnAdaptador As New SqlDataAdapter
        SelectPresupuestoPorCliente = New DataTable

        UnAdaptador.SelectCommand = UnComando
        UnAdaptador.Fill(SelectPresupuestoPorCliente)

        UnaConexion.Close()
        Return SelectPresupuestoPorCliente
    End Function

    '------ FUNCIONES ABM ------
    Public Function ObtenerNuevoId() As Integer
        Dim UnaConexion As New SqlConnection
        UnaConexion.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("bdCPL-SQL").ConnectionString
        UnaConexion.Open()

        Dim UnComando As New SqlCommand
        UnComando.Connection = UnaConexion
        UnComando.CommandText = "SelectMaxIdPresupuesto"
        UnComando.CommandType = CommandType.StoredProcedure
        UnComando.ExecuteNonQuery()

        Dim UnaTabla As DataTable
        Dim UnAdaptador As New SqlDataAdapter
        UnaTabla = New DataTable

        UnAdaptador.SelectCommand = UnComando
        UnAdaptador.Fill(UnaTabla)

        'Devuelve un solo registro, el del maximo Id
        'Convierte un DataRow en un dato tipo integer
        ObtenerNuevoId = 0
        Try
            ObtenerNuevoId = DirectCast((UnaTabla.Rows(0)(0)), Integer) + 1
        Catch
        End Try

        UnaConexion.Close()

        'Si la tabla esta vacia asigna 1 como primer Id
        If ObtenerNuevoId < 1 Then
            ObtenerNuevoId = 1
        End If
        Return ObtenerNuevoId
    End Function

    '------ EVENTOS ABM ------
    Public Sub Insert(NuevoPresupuesto As Presupuesto)
        Dim UnaConexion As New SqlConnection
        UnaConexion.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("bdCPL-SQL").ConnectionString
        UnaConexion.Open()

        'Calcula nuevo Id y lo asigna a propiedad de objeto cliente
        NuevoPresupuesto.IdPresupuesto() = ObtenerNuevoId()

        Dim UnComando As New SqlCommand
        UnComando.Connection = UnaConexion
        UnComando.CommandText = "InsertPresupuesto"
        UnComando.CommandType = CommandType.StoredProcedure
        'Pasaje de lista de parametros: se pasan las propiedades de objeto cliente
        UnComando.Parameters.AddWithValue("@IdPresupuesto", NuevoPresupuesto.IdPresupuesto)
        UnComando.Parameters.AddWithValue("@IdCliente", NuevoPresupuesto.IdCliente)
        UnComando.Parameters.AddWithValue("@FechaGenerado", NuevoPresupuesto.FechaGenerado)
        UnComando.Parameters.AddWithValue("@FechaEntrega", NuevoPresupuesto.FechaEntrega)
        UnComando.Parameters.AddWithValue("@FechaColocacion", NuevoPresupuesto.FechaColocacion)
        UnComando.Parameters.AddWithValue("@Vigencia", NuevoPresupuesto.Vigencia)
        UnComando.Parameters.AddWithValue("@Notas", NuevoPresupuesto.Notas)
        UnComando.Parameters.AddWithValue("@Colocacion", NuevoPresupuesto.Colocacion)
        UnComando.ExecuteNonQuery()

        UnaConexion.Close()
    End Sub
    Public Sub Update(UnPresupuesto As Presupuesto)
        'Instancio la conexion
        Dim UnaConexion As New SqlConnection
        UnaConexion.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("bdCPL-SQL").ConnectionString
        'Abrimos la conexion
        UnaConexion.Open()

        Dim UnComando As New SqlCommand
        UnComando.Connection = UnaConexion
        UnComando.CommandText = "UpdatePresupuesto"
        UnComando.CommandType = CommandType.StoredProcedure
        'Pasaje de lista de parametros: se pasan las propiedades de objeto cliente
        UnComando.Parameters.AddWithValue("@IdCliente", UnPresupuesto.IdCliente)
        UnComando.Parameters.AddWithValue("@FechaColocacion", UnPresupuesto.FechaColocacion)
        UnComando.Parameters.AddWithValue("@FechaEntrega", UnPresupuesto.FechaEntrega)
        UnComando.Parameters.AddWithValue("@Vigencia", UnPresupuesto.Vigencia)
        UnComando.Parameters.AddWithValue("@Notas", UnPresupuesto.Notas)
        UnComando.Parameters.AddWithValue("@Colocacion", UnPresupuesto.Colocacion)
        UnComando.Parameters.AddWithValue("@IdPresupuesto", UnPresupuesto.IdPresupuesto)
        'Ejecuta comando indicado
        UnComando.ExecuteNonQuery()
        UnaConexion.Close()
    End Sub
    Public Sub Delete(UnPresupuesto As Presupuesto)
        'Recibe un objeto cliente como parámetro para eliminar en base de datos (solo se carga IdCliente, el resto no importa)
        'Política de borrado: Borrado lógico

        'Instancio la conexion
        Dim UnaConexion As New SqlConnection
        UnaConexion.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("bdCPL-SQL").ConnectionString
        'Abrimos la conexion
        UnaConexion.Open()

        Dim UnComando As New SqlCommand
        UnComando.Connection = UnaConexion
        UnComando.CommandText = "DeletePresupuesto"
        UnComando.CommandType = CommandType.StoredProcedure
        UnComando.Parameters.AddWithValue("@IdPresupuesto", UnPresupuesto.IdPresupuesto)
        'Ejecuta comando indicado
        UnComando.ExecuteNonQuery()
        'Cierra la conexion
        UnaConexion.Close()
    End Sub

    Public Sub UpdateColocacion(UnPresupuesto As Presupuesto)
        'Recibe un objeto cliente como parámetro para eliminar en base de datos (solo se carga IdCliente, el resto no importa)
        'Política de borrado: Borrado lógico

        'Instancio la conexion
        Dim UnaConexion As New SqlConnection
        UnaConexion.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("bdCPL-SQL").ConnectionString
        'Abrimos la conexion
        UnaConexion.Open()

        Dim UnComando As New SqlCommand
        UnComando.Connection = UnaConexion
        UnComando.CommandText = "UpdatePresupuesto"
        UnComando.CommandType = CommandType.StoredProcedure
        'Pasaje de lista de parametros: se pasan las propiedades de objeto cliente
        UnComando.Parameters.AddWithValue("@FechaColocacion", UnPresupuesto.FechaColocacion)
        UnComando.Parameters.AddWithValue("@IdPresupuesto", UnPresupuesto.IdPresupuesto)
        'Ejecuta comando indicado
        UnComando.ExecuteNonQuery()
        UnaConexion.Close()
    End Sub
    Public Sub UpdateEnviar(UnPresupuesto As Presupuesto)
        'Recibe un objeto cliente como parámetro para eliminar en base de datos (solo se carga IdCliente, el resto no importa)
        'Política de borrado: Borrado lógico

        'Instancio la conexion
        Dim UnaConexion As New SqlConnection
        UnaConexion.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("bdCPL-SQL").ConnectionString
        'Abrimos la conexion
        UnaConexion.Open()

        Dim UnComando As New SqlCommand
        UnComando.Connection = UnaConexion
        UnComando.CommandText = "UpdateEnviar"
        UnComando.CommandType = CommandType.StoredProcedure
        'Pasaje de lista de parametros: se pasan las propiedades de objeto cliente
        UnComando.Parameters.AddWithValue("@IdPresupuesto", UnPresupuesto.IdPresupuesto)
        'Ejecuta comando indicado
        UnComando.ExecuteNonQuery()
        UnaConexion.Close()
    End Sub
    Public Sub Aprobar(UnPresupuesto As Presupuesto)
        'Recibe un objeto cliente como parámetro para eliminar en base de datos (solo se carga IdCliente, el resto no importa)
        'Política de borrado: Borrado lógico

        'Instancio la conexion
        Dim UnaConexion As New SqlConnection
        UnaConexion.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("bdCPL-SQL").ConnectionString
        'Abrimos la conexion
        UnaConexion.Open()

        Dim UnComando As New SqlCommand
        UnComando.Connection = UnaConexion
        UnComando.CommandText = "UpdateAprobar"
        UnComando.CommandType = CommandType.StoredProcedure
        'Pasaje de lista de parametros: se pasan las propiedades de objeto cliente
        UnComando.Parameters.AddWithValue("@IdPresupuesto", UnPresupuesto.IdPresupuesto)
        'Ejecuta comando indicado
        UnComando.ExecuteNonQuery()
        UnaConexion.Close()
    End Sub
    Public Sub Entregar(UnPresupuesto As Presupuesto)
        'Recibe un objeto cliente como parámetro para eliminar en base de datos (solo se carga IdCliente, el resto no importa)
        'Política de borrado: Borrado lógico

        'Instancio la conexion
        Dim UnaConexion As New SqlConnection
        UnaConexion.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("bdCPL-SQL").ConnectionString
        'Abrimos la conexion
        UnaConexion.Open()

        Dim UnComando As New SqlCommand
        UnComando.Connection = UnaConexion
        UnComando.CommandText = "UpdateEntregar"
        UnComando.CommandType = CommandType.StoredProcedure
        'Pasaje de lista de parametros: se pasan las propiedades de objeto cliente
        UnComando.Parameters.AddWithValue("@IdPresupuesto", UnPresupuesto.IdPresupuesto)
        'Ejecuta comando indicado
        UnComando.ExecuteNonQuery()
        UnaConexion.Close()
    End Sub
    Public Sub Colocar(UnPresupuesto As Presupuesto)
        'Recibe un objeto cliente como parámetro para eliminar en base de datos (solo se carga IdCliente, el resto no importa)
        'Política de borrado: Borrado lógico

        'Instancio la conexion
        Dim UnaConexion As New SqlConnection
        UnaConexion.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("bdCPL-SQL").ConnectionString
        'Abrimos la conexion
        UnaConexion.Open()

        Dim UnComando As New SqlCommand
        UnComando.Connection = UnaConexion
        UnComando.CommandText = "UpdateColocar"
        UnComando.CommandType = CommandType.StoredProcedure
        'Pasaje de lista de parametros: se pasan las propiedades de objeto cliente
        UnComando.Parameters.AddWithValue("@IdPresupuesto", UnPresupuesto.IdPresupuesto)
        'Ejecuta comando indicado
        UnComando.ExecuteNonQuery()
        UnaConexion.Close()
    End Sub
End Class
