﻿Imports System.Data 'Importa libreria para manejo de datos
Imports System.Data.SqlClient
Imports System
Imports System.Configuration

Public Class DbItem

    Public Function SelectMaxItemOPPresupuesto(UnItem As Item) As Integer
        Dim UnaConexion As New SqlConnection
        UnaConexion.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("bdCPL-SQL").ConnectionString
        UnaConexion.Open()

        Dim UnComando As New SqlCommand
        UnComando.Connection = UnaConexion
        UnComando.CommandText = "SelectMaxItemOPPresupuesto"
        UnComando.CommandType = CommandType.StoredProcedure
        UnComando.Parameters.AddWithValue("@IdPresupuesto", UnItem.IdPresupuesto)
        'Ejecuta comando indicado
        UnComando.ExecuteNonQuery()

        'Llenar tabla con adaptador
        Dim UnAdaptador As New SqlDataAdapter
        Dim UnaTabla As New DataTable
        UnAdaptador.Fill(UnaTabla)

        'Devuelve un solo registro, el del maximo Id
        'Convierte un DataRow en un dato tipo integer
        SelectMaxItemOPPresupuesto = 0
        Try
            SelectMaxItemOPPresupuesto = DirectCast((UnaTabla.Rows(0)(0)), Integer) + 1
        Catch
        End Try

        UnaConexion.Close()

        'Si la tabla esta vacia asigna 1 como primer Id
        If SelectMaxItemOPPresupuesto < 1 Then
            SelectMaxItemOPPresupuesto = 0
        End If
        Return SelectMaxItemOPPresupuesto
    End Function
    Public Function CalcularTotal(UnItem As Item) As Double
        Dim UnaConexion As New SqlConnection
        UnaConexion.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("bdCPL-SQL").ConnectionString
        UnaConexion.Open()

        Dim UnComando As New SqlCommand
        UnComando.Connection = UnaConexion
        UnComando.CommandText = "SumTotalItemPrespuesto"
        UnComando.CommandType = CommandType.StoredProcedure
        UnComando.Parameters.AddWithValue("@IdPresupuesto", UnItem.IdPresupuesto)
        'Ejecuta comando indicado
        Try
            CalcularTotal = CDbl(UnComando.ExecuteScalar())
        Catch
            CalcularTotal = 0
        End Try
        UnaConexion.Close()

        'Retornar tabla llena
        Return CalcularTotal
    End Function
    Public Function CalcularTotalOP(UnItem As Item) As Double
        Dim UnaConexion As New SqlConnection
        UnaConexion.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("bdCPL-SQL").ConnectionString
        UnaConexion.Open()

        Dim UnComando As New SqlCommand
        UnComando.Connection = UnaConexion
        UnComando.CommandText = "SumTotalItemOperacion"
        UnComando.CommandType = CommandType.StoredProcedure
        UnComando.Parameters.AddWithValue("@IdPresupuesto", UnItem.IdPresupuesto)
        UnComando.Parameters.AddWithValue("@Opcion", UnItem.Opcion)
        'Ejecuta comando indicado
        Try
            CalcularTotalOP = CDbl(UnComando.ExecuteScalar())
        Catch
            CalcularTotalOP = 0
        End Try
        UnaConexion.Close()

        'Retornar tabla llena
        Return CalcularTotalOP
    End Function
    Public Function SelectItemPorPresupuesto(UnItem As Item) As DataTable
        Dim UnaConexion As New SqlConnection
        UnaConexion.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("bdCPL-SQL").ConnectionString
        UnaConexion.Open()

        Dim UnComando As New SqlCommand
        UnComando.Connection = UnaConexion
        UnComando.CommandText = "SelectItemPorPresupuesto"
        UnComando.CommandType = CommandType.StoredProcedure
        UnComando.Parameters.AddWithValue("@IdPresupuesto", UnItem.IdPresupuesto)
        UnComando.ExecuteNonQuery()

        Dim UnAdaptador As New SqlDataAdapter
        SelectItemPorPresupuesto = New DataTable

        UnAdaptador.SelectCommand = UnComando
        UnAdaptador.Fill(SelectItemPorPresupuesto)

        UnaConexion.Close()
        Return SelectItemPorPresupuesto
    End Function
    Public Function SelectMaxItemPresupuesto(UnItem As Item) As Integer
        Dim UnaConexion As New SqlConnection
        UnaConexion.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("bdCPL-SQL").ConnectionString
        UnaConexion.Open()

        Dim UnComando As New SqlCommand
        UnComando.Connection = UnaConexion
        UnComando.CommandText = "SelectMaxItemPresupuesto"
        UnComando.CommandType = CommandType.StoredProcedure
        UnComando.Parameters.AddWithValue("@IdPresupuesto", UnItem.IdPresupuesto)
        UnComando.ExecuteNonQuery()

        Dim UnaTabla As DataTable
        Dim UnAdaptador As New SqlDataAdapter
        UnaTabla = New DataTable

        UnAdaptador.SelectCommand = UnComando
        UnAdaptador.Fill(UnaTabla)

        'Devuelve un solo registro, el del maximo Id
        'Convierte un DataRow en un dato tipo integer
        SelectMaxItemPresupuesto = 0
        Try
            SelectMaxItemPresupuesto = CInt(UnaTabla.Rows(0)(0).ToString) + 1
        Catch
        End Try

        UnaConexion.Close()

        'Si la tabla esta vacia asigna 1 como primer Id
        If SelectMaxItemPresupuesto < 1 Then
            SelectMaxItemPresupuesto = 1
        End If
        Return SelectMaxItemPresupuesto
    End Function

    Public Function ObtenerNuevoId() As Integer
        Dim UnaConexion As New SqlConnection
        UnaConexion.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("bdCPL-SQL").ConnectionString
        UnaConexion.Open()

        Dim UnComando As New SqlCommand
        UnComando.Connection = UnaConexion
        UnComando.CommandText = "SelectMaxIdItem"
        UnComando.CommandType = CommandType.StoredProcedure
        UnComando.ExecuteNonQuery()

        Dim UnaTabla As DataTable
        Dim UnAdaptador As New SqlDataAdapter
        UnaTabla = New DataTable

        UnAdaptador.SelectCommand = UnComando
        UnAdaptador.Fill(UnaTabla)

        'Devuelve un solo registro, el del maximo Id
        'Convierte un DataRow en un dato tipo integer
        ObtenerNuevoId = 0
        Try
            ObtenerNuevoId = DirectCast((UnaTabla.Rows(0)(0)), Integer) + 1
        Catch
        End Try

        UnaConexion.Close()

        'Si la tabla esta vacia asigna 1 como primer Id
        If ObtenerNuevoId < 1 Then
            ObtenerNuevoId = 1
        End If
        Return ObtenerNuevoId
    End Function
    Public Sub Insert(NuevoItem As Item)
        Dim UnaConexion As New SqlConnection
        UnaConexion.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("bdCPL-SQL").ConnectionString
        UnaConexion.Open()

        'Calcula nuevo Id y lo asigna a propiedad de objeto cliente
        NuevoItem.IdItem() = ObtenerNuevoId()

        Dim UnComando As New SqlCommand
        UnComando.Connection = UnaConexion
        UnComando.CommandText = "InsertItem"
        UnComando.CommandType = CommandType.StoredProcedure
        'Pasaje de lista de parametros: se pasan las propiedades de objeto cliente
        UnComando.Parameters.AddWithValue("@IdItem", NuevoItem.IdItem)
        UnComando.Parameters.AddWithValue("@IdPresupuesto", NuevoItem.IdPresupuesto)
        UnComando.Parameters.AddWithValue("@Item", NuevoItem.Item)
        UnComando.Parameters.AddWithValue("@Ubicacion", NuevoItem.Ubicacion)
        UnComando.Parameters.AddWithValue("@Descripcion", NuevoItem.Descripcion)
        UnComando.Parameters.AddWithValue("@Observacion", NuevoItem.Observacion)
        UnComando.Parameters.AddWithValue("@Precio", NuevoItem.Precio)
        UnComando.Parameters.AddWithValue("@Costo", NuevoItem.Costo)
        UnComando.Parameters.AddWithValue("@Cantidad", NuevoItem.Cantidad)
        UnComando.Parameters.AddWithValue("@IdCategoria", NuevoItem.IdCategoria)
        UnComando.Parameters.AddWithValue("@Opcion", NuevoItem.Opcion)
        'Ejecuta comando indicado
        UnComando.ExecuteNonQuery()
        UnaConexion.Close()
    End Sub
    Public Sub Update(UnItem As Item)
        Dim UnaConexion As New SqlConnection
        UnaConexion.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("bdCPL-SQL").ConnectionString
        UnaConexion.Open()

        'Calcula nuevo Id y lo asigna a propiedad de objeto cliente
        UnItem.IdItem() = ObtenerNuevoId()

        Dim UnComando As New SqlCommand
        UnComando.Connection = UnaConexion
        UnComando.CommandText = "UpdateItem"
        UnComando.CommandType = CommandType.StoredProcedure
        'Pasaje de lista de parametros: se pasan las propiedades de objeto cliente
        UnComando.Parameters.AddWithValue("@IdPresupuesto", UnItem.IdPresupuesto)
        UnComando.Parameters.AddWithValue("@Item", UnItem.Item)
        UnComando.Parameters.AddWithValue("@Ubicacion", UnItem.Ubicacion)
        UnComando.Parameters.AddWithValue("@Descripcion", UnItem.Descripcion)
        UnComando.Parameters.AddWithValue("@Observacion", UnItem.Observacion)
        UnComando.Parameters.AddWithValue("@Cantidad", UnItem.Cantidad)
        UnComando.Parameters.AddWithValue("@Precio", UnItem.Precio)
        UnComando.Parameters.AddWithValue("@Costo", UnItem.Costo)
        UnComando.Parameters.AddWithValue("@IdCategoria", UnItem.IdCategoria)
        UnComando.Parameters.AddWithValue("@Opcion", UnItem.Opcion)
        UnComando.Parameters.AddWithValue("@IdItem", UnItem.IdItem)
        'Ejecuta comando indicado
        UnComando.ExecuteNonQuery()
        UnaConexion.Close()
    End Sub
    Public Sub Delete(UnItem As Item)
        'Recibe un objeto cliente como parámetro para eliminar en base de datos (solo se carga IdCliente, el resto no importa)
        'Política de borrado: Borrado lógico

        'Instancio la conexion
        Dim UnaConexion As New SqlConnection
        UnaConexion.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("bdCPL-SQL").ConnectionString
        'Abrimos la conexion
        UnaConexion.Open()

        Dim UnComando As New SqlCommand
        UnComando.Connection = UnaConexion
        UnComando.CommandText = "DeleteItem"
        UnComando.CommandType = CommandType.StoredProcedure
        'Pasaje de lista de parametros: se pasan las propiedades de objeto cliente
        UnComando.Parameters.AddWithValue("@IdItems", UnItem.IdItem)
        'Ejecuta comando indicado
        UnComando.ExecuteNonQuery()
        UnaConexion.Close()
    End Sub
    Public Sub Aprobar(UnItem As Item)
        'Recibe un objeto cliente como parámetro para eliminar en base de datos (solo se carga IdCliente, el resto no importa)
        'Política de borrado: Borrado lógico

        'Instancio la conexion
        Dim UnaConexion As New SqlConnection
        UnaConexion.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("bdCPL-SQL").ConnectionString
        'Abrimos la conexion
        UnaConexion.Open()

        Dim UnComando As New SqlCommand
        UnComando.Connection = UnaConexion
        UnComando.CommandText = "SetItemAprobado"
        UnComando.CommandType = CommandType.StoredProcedure
        'Pasaje de lista de parametros: se pasan las propiedades de objeto cliente
        UnComando.Parameters.AddWithValue("@IdItems", UnItem.IdItem)
        'Ejecuta comando indicado
        UnComando.ExecuteNonQuery()
        UnaConexion.Close()
    End Sub
    Public Sub Desaprobar(UnItem As Item)
        'Recibe un objeto cliente como parámetro para eliminar en base de datos (solo se carga IdCliente, el resto no importa)
        'Política de borrado: Borrado lógico

        'Instancio la conexion
        Dim UnaConexion As New SqlConnection
        UnaConexion.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("bdCPL-SQL").ConnectionString
        'Abrimos la conexion
        UnaConexion.Open()

        Dim UnComando As New SqlCommand
        UnComando.Connection = UnaConexion
        UnComando.CommandText = "SetItemDesaprobado"
        UnComando.CommandType = CommandType.StoredProcedure
        'Pasaje de lista de parametros: se pasan las propiedades de objeto cliente
        UnComando.Parameters.AddWithValue("@IdItems", UnItem.IdItem)
        'Ejecuta comando indicado
        UnComando.ExecuteNonQuery()
        UnaConexion.Close()
    End Sub
End Class
