﻿Imports System.Data 'Importa libreria para manejo de datos
Imports System.Data.SqlClient
Imports System
Imports System.Configuration

Public Class DbCiudad
    Public Function SelectCiudadPorId(UnaCiudad As Ciudad) As DataTable
        Dim UnaConexion As New SqlConnection
        UnaConexion.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("bdCPL-SQL").ConnectionString
        UnaConexion.Open()

        Dim UnComando As New SqlCommand
        UnComando.Connection = UnaConexion
        UnComando.CommandText = "SelectCiudadPorId"
        UnComando.CommandType = CommandType.StoredProcedure
        UnComando.Parameters.AddWithValue("@IdCiudad", UnaCiudad.IdCiudad)
        UnComando.ExecuteNonQuery()

        Dim UnAdaptador As New SqlDataAdapter
        SelectCiudadPorId = New DataTable

        UnAdaptador.SelectCommand = UnComando
        UnAdaptador.Fill(SelectCiudadPorId)

        UnaConexion.Close()
        Return SelectCiudadPorId
    End Function
    Public Function SelectAllCiudades() As DataTable
        Dim UnaConexion As New SqlConnection
        UnaConexion.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("bdCPL-SQL").ConnectionString
        UnaConexion.Open()

        Dim UnComando As New SqlCommand
        UnComando.Connection = UnaConexion
        UnComando.CommandText = "SelectAllCiudad"
        UnComando.CommandType = CommandType.StoredProcedure
        UnComando.ExecuteNonQuery()

        Dim UnAdaptador As New SqlDataAdapter
        SelectAllCiudades = New DataTable

        UnAdaptador.SelectCommand = UnComando
        UnAdaptador.Fill(SelectAllCiudades)

        UnaConexion.Close()
    End Function
    Public Function ObtenerNuevoId() As Integer
        Dim UnaConexion As New SqlConnection
        UnaConexion.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("bdCPL-SQL").ConnectionString
        UnaConexion.Open()

        Dim UnComando As New SqlCommand
        UnComando.Connection = UnaConexion
        UnComando.CommandText = "SelectMaxIdCiudad"
        UnComando.CommandType = CommandType.StoredProcedure
        UnComando.ExecuteNonQuery()

        Dim UnaTabla As DataTable
        Dim UnAdaptador As New SqlDataAdapter
        UnaTabla = New DataTable

        UnAdaptador.SelectCommand = UnComando
        UnAdaptador.Fill(UnaTabla)

        'Devuelve un solo registro, el del maximo Id
        'Convierte un DataRow en un dato tipo integer
        ObtenerNuevoId = 0
        Try
            ObtenerNuevoId = DirectCast((UnaTabla.Rows(0)(0)), Integer) + 1
        Catch
        End Try

        UnaConexion.Close()

        'Si la tabla esta vacia asigna 1 como primer Id
        If ObtenerNuevoId < 1 Then
            ObtenerNuevoId = 1
        End If
        Return ObtenerNuevoId
    End Function
    Public Sub Insert(UnaCiudad As Ciudad)
        Dim UnaConexion As New SqlConnection
        UnaConexion.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("bdCPL-SQL").ConnectionString
        UnaConexion.Open()

        'Calcula nuevo Id y lo asigna a propiedad de objeto cliente
        UnaCiudad.IdCiudad() = ObtenerNuevoId()

        Dim UnComando As New SqlCommand
        UnComando.Connection = UnaConexion
        UnComando.CommandText = "InsertCiudad"
        UnComando.CommandType = CommandType.StoredProcedure

        'Pasaje de lista de parametros: se pasan las propiedades de objeto Ciudad
        UnComando.Parameters.AddWithValue("@IdCiudad", UnaCiudad.IdCiudad)
        UnComando.Parameters.AddWithValue("@Localidad", UnaCiudad.Localidad)
        UnComando.Parameters.AddWithValue("@Partido", UnaCiudad.Partido)
        'Ejecuta comando indicado
        UnComando.ExecuteNonQuery()
        UnaConexion.Close()
    End Sub
    Public Sub Update(UnaCiudad As Ciudad)
        'Instancio la conexion
        Dim UnaConexion As New SqlConnection
        UnaConexion.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("bdCPL-SQL").ConnectionString
        'Abrimos la conexion
        UnaConexion.Open()

        Dim UnComando As New SqlCommand
        UnComando.Connection = UnaConexion
        UnComando.CommandText = "UpdateCliente"
        UnComando.CommandType = CommandType.StoredProcedure
        'Pasaje de lista de parametros: se pasan las propiedades de objeto Ciudad
        UnComando.Parameters.AddWithValue("@Localidad", UnaCiudad.Localidad)
        UnComando.Parameters.AddWithValue("@Partido", UnaCiudad.Partido)
        UnComando.Parameters.AddWithValue("@IdCiudad", UnaCiudad.IdCiudad)
        'Ejecuta comando indicado
        UnComando.ExecuteNonQuery()
        UnaConexion.Close()
    End Sub
    Public Sub Delete(UnaCiudad As Ciudad)
        'Recibe un objeto ciudad como parámetro para eliminar en base de datos (solo se carga IdCiudad, el resto no importa)
        'Política de borrado: Borrado lógico

        'Instancio la conexion
        Dim UnaConexion As New SqlConnection
        UnaConexion.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("bdCPL-SQL").ConnectionString
        'Abrimos la conexion
        UnaConexion.Open()

        Dim UnComando As New SqlCommand
        UnComando.Connection = UnaConexion
        UnComando.CommandText = "DeleteCiudad"
        UnComando.CommandType = CommandType.StoredProcedure
        UnComando.Parameters.AddWithValue("@IdCiudad", UnaCiudad.IdCiudad)
        'Ejecuta comando indicado
        UnComando.ExecuteNonQuery()
        'Cierra la conexion
        UnaConexion.Close()
    End Sub
End Class
