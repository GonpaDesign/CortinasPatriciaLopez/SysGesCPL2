﻿Imports System.Data 'Importa libreria para manejo de datos
Imports System.Data.SqlClient
Imports System
Imports System.Configuration

Public Class DBProveedor

    Public Function SelectAllProveedores() As DataTable
        Dim UnaConexion As New SqlConnection
        UnaConexion.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("bdCPL-SQL").ConnectionString
        UnaConexion.Open()

        Dim UnComando As New SqlCommand
        UnComando.Connection = UnaConexion
        UnComando.CommandText = "SelectAllProveedores"
        UnComando.CommandType = CommandType.StoredProcedure
        UnComando.ExecuteNonQuery()

        Dim UnAdaptador As New SqlDataAdapter
        SelectAllProveedores = New DataTable

        UnAdaptador.SelectCommand = UnComando
        UnAdaptador.Fill(SelectAllProveedores)

        UnaConexion.Close()
    End Function
    Public Function SelectProveedorPorId(UnProveedor As Proveedor) As DataTable
        Dim UnaConexion As New SqlConnection
        UnaConexion.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("bdCPL-SQL").ConnectionString
        UnaConexion.Open()

        Dim UnComando As New SqlCommand
        UnComando.Connection = UnaConexion
        UnComando.CommandText = "SelectProveedorPorId"
        UnComando.CommandType = CommandType.StoredProcedure
        UnComando.Parameters.AddWithValue("@IdProveedor", UnProveedor.IdProveedor)
        UnComando.ExecuteNonQuery()

        Dim UnAdaptador As New SqlDataAdapter
        SelectProveedorPorId = New DataTable

        UnAdaptador.SelectCommand = UnComando
        UnAdaptador.Fill(SelectProveedorPorId)

        UnaConexion.Close()
        Return SelectProveedorPorId
    End Function
    Public Function ObtenerNuevoId() As Integer
        Dim UnaConexion As New SqlConnection
        UnaConexion.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("bdCPL-SQL").ConnectionString
        UnaConexion.Open()

        Dim UnComando As New SqlCommand
        UnComando.Connection = UnaConexion
        UnComando.CommandText = "SelectMaxIdProveedor"
        UnComando.CommandType = CommandType.StoredProcedure
        UnComando.ExecuteNonQuery()

        Dim UnaTabla As DataTable
        Dim UnAdaptador As New SqlDataAdapter
        UnaTabla = New DataTable

        UnAdaptador.SelectCommand = UnComando
        UnAdaptador.Fill(UnaTabla)

        'Devuelve un solo registro, el del maximo Id
        'Convierte un DataRow en un dato tipo integer
        ObtenerNuevoId = 0
        Try
            ObtenerNuevoId = DirectCast((UnaTabla.Rows(0)(0)), Integer) + 1
        Catch
        End Try

        UnaConexion.Close()

        'Si la tabla esta vacia asigna 1 como primer Id
        If ObtenerNuevoId < 1 Then
            ObtenerNuevoId = 1
        End If
        Return ObtenerNuevoId
    End Function
    Public Sub Insert(NuevoProveedor As Proveedor)
        Dim UnaConexion As New SqlConnection
        UnaConexion.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("bdCPL-SQL").ConnectionString
        UnaConexion.Open()

        'Calcula nuevo Id y lo asigna a propiedad de objeto cliente
        NuevoProveedor.IdProveedor() = ObtenerNuevoId()

        Dim UnComando As New SqlCommand
        UnComando.Connection = UnaConexion
        UnComando.CommandText = "InsertProveedor"
        UnComando.CommandType = CommandType.StoredProcedure
        'Pasaje de lista de parametros: se pasan las propiedades de objeto Proveedor
        UnComando.Parameters.AddWithValue("@IdProveedor", NuevoProveedor.IdProveedor)
        UnComando.Parameters.AddWithValue("@Nombre", NuevoProveedor.Nombre)
        UnComando.Parameters.AddWithValue("@Cuit", NuevoProveedor.CUIT)
        UnComando.Parameters.AddWithValue("@RazonSocial", NuevoProveedor.RazonSocial)
        UnComando.Parameters.AddWithValue("@Sucursal", NuevoProveedor.Sucursal)
        UnComando.Parameters.AddWithValue("@Web", NuevoProveedor.Web)
        UnComando.Parameters.AddWithValue("@Direccion", NuevoProveedor.Direccion)
        UnComando.Parameters.AddWithValue("@IdCiudad", NuevoProveedor.IdCiudad)
        UnComando.Parameters.AddWithValue("@CP", NuevoProveedor.CP)
        UnComando.Parameters.AddWithValue("@Mail1", NuevoProveedor.Mail1)
        UnComando.Parameters.AddWithValue("@Mail2", NuevoProveedor.Mail2)
        UnComando.Parameters.AddWithValue("@Mail3", NuevoProveedor.Mail3)
        UnComando.Parameters.AddWithValue("@Contacto1", NuevoProveedor.Contacto1)
        UnComando.Parameters.AddWithValue("@Contacto2", NuevoProveedor.Contacto2)
        UnComando.Parameters.AddWithValue("@Contacto3", NuevoProveedor.Contacto3)
        UnComando.Parameters.AddWithValue("@TelContacto1", NuevoProveedor.TelContacto1)
        UnComando.Parameters.AddWithValue("@TelContacto2", NuevoProveedor.TelContacto2)
        UnComando.Parameters.AddWithValue("@TelContacto3", NuevoProveedor.TelContacto3)
        'Ejecuta comando indicado
        UnComando.ExecuteNonQuery()
        'Cierra la Conexion
        UnaConexion.Close()
    End Sub
    Public Sub Update(UnProveedor As Proveedor)
        'Instancio la conexion
        Dim UnaConexion As New SqlConnection
        UnaConexion.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("bdCPL-SQL").ConnectionString
        'Abrimos la conexion
        UnaConexion.Open()

        Dim UnComando As New SqlCommand
        UnComando.Connection = UnaConexion
        UnComando.CommandText = "UpdateProveedor"
        UnComando.CommandType = CommandType.StoredProcedure
        'Pasaje de lista de parametros: se pasan las propiedades de objeto cliente
        UnComando.Parameters.AddWithValue("@Nombre", UnProveedor.Nombre)
        UnComando.Parameters.AddWithValue("@Cuit", UnProveedor.CUIT)
        UnComando.Parameters.AddWithValue("@RazonSocial", UnProveedor.RazonSocial)
        UnComando.Parameters.AddWithValue("@Sucursal", UnProveedor.Sucursal)
        UnComando.Parameters.AddWithValue("@Web", UnProveedor.Web)
        UnComando.Parameters.AddWithValue("@Direccion", UnProveedor.Direccion)
        UnComando.Parameters.AddWithValue("@IdCiudad", UnProveedor.IdCiudad)
        UnComando.Parameters.AddWithValue("@CP", UnProveedor.CP)
        UnComando.Parameters.AddWithValue("@Mail1", UnProveedor.Mail1)
        UnComando.Parameters.AddWithValue("@Mail2", UnProveedor.Mail2)
        UnComando.Parameters.AddWithValue("@Mail3", UnProveedor.Mail3)
        UnComando.Parameters.AddWithValue("@Contacto1", UnProveedor.Contacto1)
        UnComando.Parameters.AddWithValue("@Contacto2", UnProveedor.Contacto2)
        UnComando.Parameters.AddWithValue("@Contacto3", UnProveedor.Contacto3)
        UnComando.Parameters.AddWithValue("@TelContacto1", UnProveedor.TelContacto1)
        UnComando.Parameters.AddWithValue("@TelContacto2", UnProveedor.TelContacto2)
        UnComando.Parameters.AddWithValue("@TelContacto3", UnProveedor.TelContacto3)
        UnComando.Parameters.AddWithValue("@IdProveedor", UnProveedor.IdProveedor)
        'Ejecuta comando indicado
        UnComando.ExecuteNonQuery()
        'Cierra la Conexion
        UnaConexion.Close()
    End Sub
    Public Sub Delete(UnProveedor As Proveedor)
        'Recibe un objeto cliente como parámetro para eliminar en base de datos (solo se carga IdCliente, el resto no importa)
        'Política de borrado: Borrado lógico

        'Instancio la conexion
        Dim UnaConexion As New SqlConnection
        UnaConexion.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("bdCPL-SQL").ConnectionString
        'Abrimos la conexion
        UnaConexion.Open()

        Dim UnComando As New SqlCommand
        UnComando.Connection = UnaConexion
        UnComando.CommandText = "DeleteProveedor"
        UnComando.CommandType = CommandType.StoredProcedure
        UnComando.Parameters.AddWithValue("@IdProveedor", UnProveedor.IdProveedor)
        'Ejecuta comando indicado
        UnComando.ExecuteNonQuery()
        'Cierra la conexion
        UnaConexion.Close()
    End Sub
End Class
