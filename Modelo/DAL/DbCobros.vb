﻿Imports System.Data.OleDb 'Importa libreria para trabajar con BD Access
Imports System.Data 'Importa libreria para manejo de datos

Imports System
Imports System.Configuration
Public Class DbCobros
    Public Function CalcularCobrado(UnPresupuesto As Presupuesto) As Double
        'Abrir conexion con base datos
        Dim UnaConexion As New OleDbConnection
        UnaConexion.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("bdCPL-SQL").ConnectionString
        UnaConexion.Open()

        'Crear un comando con una consulta SQL
        Dim UnComando As New OleDbCommand("SELECT SUM (Monto) FROM TblCobros WHERE IdPresupuesto=@IdPresupuesto AND Eliminado=false", UnaConexion)
        UnComando.Parameters.AddWithValue("@IdPresupuesto", UnPresupuesto.IdPresupuesto)
        UnComando.CommandType = CommandType.Text
        'Ejecuta comando indicado
        Try
            CalcularCobrado = CDbl(UnComando.ExecuteScalar())
        Catch
            CalcularCobrado = 0
        End Try
        UnaConexion.Close()

        'Retornar tabla llena
        Return CalcularCobrado
    End Function
    Public Function ObtenerNuevoId() As Integer
        'Abrir conexion con base datos
        Dim UnaConexion As New OleDbConnection
        UnaConexion.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("bdCPL-SQL").ConnectionString
        UnaConexion.Open()

        'Crear un comando con una consulta SQL
        Dim UnComando As New OleDbCommand("SELECT MAX(IdCobro) FROM TblCobros", UnaConexion)
        UnComando.CommandType = CommandType.Text
        'Ejecuta comando indicado
        UnComando.ExecuteNonQuery()

        'Llenar tabla con adaptador
        Dim UnAdaptador As New OleDbDataAdapter(UnComando)
        Dim UnaTabla As New DataTable
        UnAdaptador.Fill(UnaTabla)

        'Devuelve un solo registro, el del maximo Id
        'Convierte un DataRow en un dato tipo integer
        ObtenerNuevoId = 0
        Try
            ObtenerNuevoId = DirectCast((UnaTabla.Rows(0)(0)), Integer) + 1
        Catch
        End Try

        UnaConexion.Close()

        'Si la tabla esta vacia asigna 1 como primer Id
        If ObtenerNuevoId < 1 Then
            ObtenerNuevoId = 1
        End If
        Return ObtenerNuevoId
    End Function
    Public Sub Insert(NuevoCobro As Cobro)
        'Recibe un objeto cliente como parámetro

        'Abrir conexion con base datos
        Dim UnaConexion As New OleDbConnection
        UnaConexion.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("bdCPL-SQL").ConnectionString
        UnaConexion.Open()

        'Calcula nuevo Id y lo asigna a propiedad de objeto cliente
        NuevoCobro.IdCobro = ObtenerNuevoId()

        'Crear un comando con una consulta SQL con paso de parametros en linea
        Dim UnComando As New OleDbCommand("INSERT INTO TblArticulos(IdCobro, IdPresupuesto, Tipo, IdCheque, IdTransferencia, Monto, Fecha, Observaciones, Eliminado) VALUES (@IdCobro, @IdPresupuesto, @Tipo, @IdCheque, @IdTransferencia, @Monto, @Fecha, @Observaciones, false)", UnaConexion)

        'Pasaje de lista de parametros: se pasan las propiedades de objeto cliente
        UnComando.Parameters.AddWithValue("@IdCobro", NuevoCobro.IdCobro)
        UnComando.Parameters.AddWithValue("@IdPresupuesto", NuevoCobro.IdPresupuesto)
        UnComando.Parameters.AddWithValue("@Tipo", NuevoCobro.Tipo)
        UnComando.Parameters.AddWithValue("@IdCheque", NuevoCobro.IdCheque)
        UnComando.Parameters.AddWithValue("@IdTransferencia", NuevoCobro.IdTransferencia)
        UnComando.Parameters.AddWithValue("@Monto", NuevoCobro.Monto)
        UnComando.Parameters.AddWithValue("@Fecha", NuevoCobro.Fecha)
        UnComando.Parameters.AddWithValue("@Observaciones", NuevoCobro.Observaciones)

        UnComando.CommandType = CommandType.Text
        'Ejecuta comando indicado
        UnComando.ExecuteNonQuery()
        UnaConexion.Close()
    End Sub
    Public Sub Update(UnCobro As Cobro)
        'Recibe un objeto cliente como parámetro para modificar en base de datos

        'Abrir conexion con base datos
        Dim UnaConexion As New OleDbConnection
        UnaConexion.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("bdCPL-SQL").ConnectionString
        UnaConexion.Open()

        'Crear un comando con una consulta SQL con paso de parametros en linea
        Dim UnComando As New OleDbCommand("UPDATE TblCobros SET IdPresupuesto=@IdPresupuesto, Tipo=@Tipo, IdCheque=@IdCheque, IdTransferencia=@IdTransferencia, Monto=@Monto, Fecha=@Fecha, Observaciones=@Observaciones WHERE IdCobro=@IdCobro", UnaConexion)

        'Pasaje de lista de parametros: se pasan las propiedades de objeto cliente
        UnComando.Parameters.AddWithValue("@IdPresupuesto", UnCobro.IdPresupuesto)
        UnComando.Parameters.AddWithValue("@Tipo", UnCobro.Tipo)
        UnComando.Parameters.AddWithValue("@IdCheque", UnCobro.IdCheque)
        UnComando.Parameters.AddWithValue("@IdTransferencia", UnCobro.IdTransferencia)
        UnComando.Parameters.AddWithValue("@Monto", UnCobro.Monto)
        UnComando.Parameters.AddWithValue("@Fecha", UnCobro.Fecha)
        UnComando.Parameters.AddWithValue("@Observaciones", UnCobro.Observaciones)
        UnComando.Parameters.AddWithValue("@IdCobro", UnCobro.IdCobro)
        UnComando.CommandType = CommandType.Text
        'Ejecuta comando indicado
        UnComando.ExecuteNonQuery()
        UnaConexion.Close()
    End Sub
    Public Sub Delete(UnCobro As Cobro)
        'Recibe un objeto cliente como parámetro para eliminar en base de datos (solo se carga IdCobro, el resto no importa)
        'Política de borrado: Borrado lógico

        'Abrir conexion con base datos
        Dim UnaConexion As New OleDbConnection
        UnaConexion.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("bdCPL-SQL").ConnectionString
        UnaConexion.Open()

        'Crear un comando con una consulta SQL con paso de parametros en linea
        Dim UnComando As New OleDbCommand("UPDATE TblCobros SET Eliminado=true WHERE IdCobro=@IdCobro", UnaConexion)

        'Pasaje de lista de parametros: se pasan las propiedades de objeto cliente
        UnComando.Parameters.AddWithValue("@IdCobro", UnCobro.IdCobro)
        UnComando.CommandType = CommandType.Text
        'Ejecuta comando indicado
        UnComando.ExecuteNonQuery()
        UnaConexion.Close()
    End Sub
End Class
