﻿Public Class GestorProducto
    Public Function BuscarPorId(UnProducto As Producto) As Producto
        Dim AccesoProducto As New DbProducto 'Instancio clase DbCliente de DAL

        Dim UnaTabla As DataTable
        UnaTabla = AccesoProducto.SelectProductoPorId(UnProducto)
        Dim Registro As DataRow = UnaTabla.Rows(0)

        Dim ProductoCompleto As New Producto

        ProductoCompleto.IdProducto = Registro("IdProducto")


        ProductoCompleto.Nombre = Registro("Nombre")

        Try
            ProductoCompleto.Codigo = Registro("Codigo")
        Catch
        End Try
        Try
            ProductoCompleto.Descripcion = Registro("Descripcion")
        Catch
        End Try
        Try
            ProductoCompleto.IdCategoria = Registro("IdCategoria")
        Catch
        End Try
        Try
            ProductoCompleto.Unidad = Registro("Unidad")
        Catch
        End Try
        Try
            ProductoCompleto.IdProveedor = Registro("IdProveedor")
        Catch
        End Try
        Try
            ProductoCompleto.Costo = Registro("Costo")
        Catch
        End Try
        Try
            ProductoCompleto.Fecha = Registro("Fecha")
        Catch
        End Try
        
        Return ProductoCompleto
    End Function
    Public Function ListarTodosNombre() As List(Of Producto)
        Dim UnaListaDeProductos As New List(Of Producto) 'Lista de objetos producto
        Dim AccesoProducto As New DbProducto 'Instancio clase DbProducto de DAL

        Dim UnaTabla As DataTable
        UnaTabla = AccesoProducto.SelectAllProductos() 'Pido a capa de datos la tabla productos

        'Recorro DataTable por cada fila agrego un producto a la lista de clientes
        For Each Registro In UnaTabla.Rows
            Dim u As New Producto

            u.IdProducto = Registro("IdProducto")
            u.Nombre = Registro("Nombre")
            UnaListaDeProductos.Add(u)
        Next

        'Retorno lista de objetos cliente para consumir desde formulario
        Return UnaListaDeProductos
    End Function
    Public Function ListarProductosPorCategoriaYProveedor(UnProducto As Producto) As List(Of Producto)
        Dim UnaListaDeProductos As New List(Of Producto) 'Lista de objetos producto
        Dim AccesoProducto As New DbProducto 'Instancio clase DbProducto de DAL

        Dim UnaTabla As DataTable
        UnaTabla = AccesoProducto.SelectProductosCategoriaYProveedor(UnProducto) 'Pido a capa de datos la tabla productos

        'Recorro DataTable por cada fila agrego un producto a la lista de clientes
        For Each Registro In UnaTabla.Rows
            Dim u As New Producto

            u.IdProducto = Registro("IdProducto")
            u.Nombre = Registro("Nombre")
            u.Descripcion = Registro("Descripcion")
            u.Unidad = Registro("Unidad")
            u.Costo = Registro("Costo")
            u.Fecha = Registro("Fecha")
            UnaListaDeProductos.Add(u)
        Next

        'Retorno lista de objetos cliente para consumir desde formulario
        Return UnaListaDeProductos
    End Function
    Public Function ListarProductosPorCategoria(UnProducto As Producto) As List(Of Producto)
        Dim UnaListaDeProductos As New List(Of Producto) 'Lista de objetos producto
        Dim AccesoProducto As New DbProducto 'Instancio clase DbProducto de DAL

        Dim UnaTabla As DataTable
        UnaTabla = AccesoProducto.SelectProductosPorCategoria(UnProducto) 'Pido a capa de datos la tabla productos

        'Recorro DataTable por cada fila agrego un producto a la lista de clientes
        For Each Registro In UnaTabla.Rows
            Dim u As New Producto

            u.IdProducto = Registro("IdProducto")
            u.Nombre = Registro("Nombre")
            UnaListaDeProductos.Add(u)
        Next

        'Retorno lista de objetos cliente para consumir desde formulario
        Return UnaListaDeProductos
    End Function
    Public Function ListarProductosPorProveedor(UnProducto As Producto) As List(Of Producto)
        Dim UnaListaDeProductos As New List(Of Producto) 'Lista de objetos producto
        Dim AccesoProducto As New DbProducto 'Instancio clase DbProducto de DAL

        Dim UnaTabla As DataTable
        UnaTabla = AccesoProducto.SelectProductosPorProveedor(UnProducto) 'Pido a capa de datos la tabla productos

        'Recorro DataTable por cada fila agrego un producto a la lista de clientes
        For Each Registro In UnaTabla.Rows
            Dim u As New Producto

            u.IdProducto = Registro("IdProducto")
            u.Nombre = Registro("Nombre")
            UnaListaDeProductos.Add(u)
        Next

        'Retorno lista de objetos cliente para consumir desde formulario
        Return UnaListaDeProductos
    End Function
    Public Sub Nuevo(NuevoProducto As Producto)
        Dim AccesoProducto As New DbProducto 'Instancio clase DbProducto de DAL
        AccesoProducto.Insert(NuevoProducto) 'Pido a capa de datos que guarde el producto que le mando
    End Sub
    Public Sub Modificar(UnProducto As Producto)
        Dim AccesoProducto As New DbProducto 'Instancio clase DbProducto de DAL
        AccesoProducto.Update(UnProducto) 'Pido a capa de datos que modifique producto que le mando
    End Sub
    Public Sub Eliminar(UnProducto As Producto)
        Dim AccesoProducto As New DbProducto 'Instancio clase DbProducto de DAL
        AccesoProducto.Delete(UnProducto) 'Pido a capa de datos que "elimine" producto que le mando
    End Sub
End Class
