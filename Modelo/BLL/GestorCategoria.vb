﻿Public Class GestorCategoria
    Public Function BuscarPorId(UnaCategoria As Categoria) As Categoria
        Dim AccesoCategoria As New DbCategoria 'Instancio clase DbCiudad de DAL

        Dim UnaTabla As DataTable
        UnaTabla = AccesoCategoria.SelectCategoriaPorId(UnaCategoria)
        Dim Registro As DataRow = UnaTabla.Rows(0)

        Dim CategoriaCompleta As New Categoria

        CategoriaCompleta.IdCategoria = Registro("IdCategoria")

        CategoriaCompleta.Categoria = Registro("Categoria")

        Return CategoriaCompleta
    End Function
    Public Function ListarTodosNombre() As List(Of Categoria)
        Dim UnaListaDeCategorias As New List(Of Categoria) 'Lista de objetos producto
        Dim AccesoCategoria As New DbCategoria 'Instancio clase DbProducto de DAL

        Dim UnaTabla As DataTable
        UnaTabla = AccesoCategoria.SelectAllCategorias() 'Pido a capa de datos la tabla productos

        'Recorro DataTable por cada fila agrego un producto a la lista de clientes
        For Each Registro In UnaTabla.Rows
            Dim u As New Categoria

            u.IdCategoria = Registro("IdCategoria")
            u.Categoria = Registro("Categoria")
            UnaListaDeCategorias.Add(u)
        Next

        'Retorno lista de objetos cliente para consumir desde formulario
        Return UnaListaDeCategorias
    End Function
End Class
