﻿Public Class Cliente
    Private _IdCliente As Integer
    Private _Nombre As String
    Private _RazonSocial As String
    Private _Direccion As String
    Private _IdCiudad As Integer
    Private _CP As String
    Private _Web As String
    Private _Cuit As Long
    Private _DNI As Integer
    Private _Telefono1 As Long
    Private _Telefono2 As Long
    Private _Telefono3 As Long
    Private _Movil1 As Long
    Private _Movil2 As Long
    Private _Movil3 As Long
    Private _Mail1 As String
    Private _Mail2 As String
    Private _Mail3 As String
    Private _FechaNac As Date
    Private _Contacto1 As String
    Private _Contacto2 As String
    Private _Contacto3 As String

    Public Property IdCliente() As Integer
        Get
            Return _IdCliente
        End Get
        Set(ByVal value As Integer)
            _IdCliente = value
        End Set
    End Property
    Public Property Nombre() As String
        Get
            Return _Nombre
        End Get
        Set(ByVal value As String)
            _Nombre = value
        End Set
    End Property
    Public Property RazonSocial() As String
        Get
            Return _RazonSocial
        End Get
        Set(ByVal value As String)
            _RazonSocial = value
        End Set
    End Property
    Public Property Direccion() As String
        Get
            Return _Direccion
        End Get
        Set(ByVal value As String)
            _Direccion = value
        End Set
    End Property
    Public Property IdCiudad() As Integer
        Get
            Return _IdCiudad
        End Get
        Set(ByVal value As Integer)
            _IdCiudad = value
        End Set
    End Property
    Public Property CP() As String
        Get
            Return _CP
        End Get
        Set(ByVal value As String)
            _CP = value
        End Set
    End Property
    Public Property Web() As String
        Get
            Return _Web
        End Get
        Set(ByVal value As String)
            _Web = value
        End Set
    End Property
    Public Property Cuit() As Long
        Get
            Return _Cuit
        End Get
        Set(ByVal value As Long)
            _Cuit = value
        End Set
    End Property
    Public Property DNI() As Integer
        Get
            Return _DNI
        End Get
        Set(ByVal value As Integer)
            _DNI = value
        End Set
    End Property
    Public Property Telefono1() As Long
        Get
            Return _Telefono1
        End Get
        Set(ByVal value As Long)
            _Telefono1 = value
        End Set
    End Property
    Public Property Telefono2() As Long
        Get
            Return _Telefono2
        End Get
        Set(ByVal value As Long)
            _Telefono2 = value
        End Set
    End Property
    Public Property Telefono3() As Long
        Get
            Return _Telefono3
        End Get
        Set(ByVal value As Long)
            _Telefono3 = value
        End Set
    End Property
    Public Property Movil1() As Long
        Get
            Return _Movil1
        End Get
        Set(ByVal value As Long)
            _Movil1 = value
        End Set
    End Property
    Public Property Movil2() As Long
        Get
            Return _Movil2
        End Get
        Set(ByVal value As Long)
            _Movil2 = value
        End Set
    End Property
    Public Property Movil3() As Long
        Get
            Return _Movil3
        End Get
        Set(ByVal value As Long)
            _Movil3 = value
        End Set
    End Property
    Public Property Mail1() As String
        Get
            Return _Mail1
        End Get
        Set(ByVal value As String)
            _Mail1 = value
        End Set
    End Property
    Public Property Mail2() As String
        Get
            Return _Mail2
        End Get
        Set(ByVal value As String)
            _Mail2 = value
        End Set
    End Property
    Public Property Mail3() As String
        Get
            Return _Mail3
        End Get
        Set(ByVal value As String)
            _Mail3 = value
        End Set
    End Property
    Public Property FechaNac() As Date
        Get
            Return _FechaNac
        End Get
        Set(ByVal value As Date)
            _FechaNac = value
        End Set
    End Property
    Public Property Contacto1() As String
        Get
            Return _Contacto1
        End Get
        Set(ByVal value As String)
            _Contacto1 = value
        End Set
    End Property
    Public Property Contacto2() As String
        Get
            Return _Contacto2
        End Get
        Set(ByVal value As String)
            _Contacto2 = value
        End Set
    End Property
    Public Property Contacto3() As String
        Get
            Return _Contacto3
        End Get
        Set(ByVal value As String)
            _Contacto3 = value
        End Set
    End Property
End Class