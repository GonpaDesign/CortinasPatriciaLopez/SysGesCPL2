﻿Public Class Articulo
    Private _IdArticulo As Integer
    Private _IdPresupuesto As Integer
    Private _Item As Double
    Private _Nombre As String
    Private _Dim1 As Double
    Private _Dim2 As Double
    Private _Dim3 As Double
    Private _Costo As Double
    Private _Precio As Double
    Private _Observacion As String
    Private _Eliminado As Boolean
    Public Property IdArticulo() As Integer
        Get
            Return _IdArticulo
        End Get
        Set(ByVal value As Integer)
            _IdArticulo = value
        End Set
    End Property
    Public Property IdPresupuesto() As Integer
        Get
            Return _IdPresupuesto
        End Get
        Set(ByVal value As Integer)
            _IdPresupuesto = value
        End Set
    End Property
    Public Property Item() As Double
        Get
            Return _Item
        End Get
        Set(ByVal value As Double)
            _Item = value
        End Set
    End Property
    Public Property Nombre() As String
        Get
            Return _Nombre
        End Get
        Set(ByVal value As String)
            _Nombre = value
        End Set
    End Property
    Public Property Dim1() As Double
        Get
            Return _Dim1
        End Get
        Set(ByVal value As Double)
            _Dim1 = value
        End Set
    End Property
    Public Property Dim2() As Double
        Get
            Return _Dim2
        End Get
        Set(ByVal value As Double)
            _Dim2 = value
        End Set
    End Property
    Public Property Dim3() As Double
        Get
            Return _Dim3
        End Get
        Set(ByVal value As Double)
            _Dim3 = value
        End Set
    End Property
    Public Property Costo() As Double
        Get
            Return _Costo
        End Get
        Set(ByVal value As Double)
            _Costo = value
        End Set
    End Property
    Public Property Precio() As Double
        Get
            Return _Precio
        End Get
        Set(ByVal value As Double)
            _Precio = value
        End Set
    End Property
    Public Property Observacion() As String
        Get
            Return _Observacion
        End Get
        Set(ByVal value As String)
            _Observacion = value
        End Set
    End Property
    Public Property Eliminado() As Boolean
        Get
            Return _Eliminado
        End Get
        Set(ByVal value As Boolean)
            _Eliminado = value
        End Set
    End Property
End Class
