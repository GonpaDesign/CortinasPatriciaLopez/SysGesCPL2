﻿Public Class GestorProveedor

    Public Function BuscarPorId(UnProveedor As Proveedor) As Proveedor
        Dim AccesoProveedores As New DBProveedor 'Instancio clase DbProveedor de DAL

        Dim UnaTabla As DataTable
        UnaTabla = AccesoProveedores.SelectProveedorPorId(UnProveedor)
        Dim Registro As DataRow = UnaTabla.Rows(0)

        Dim ProveedorCompleto As New Proveedor

        ProveedorCompleto.IdProveedor = Registro("IdProveedor")

        ProveedorCompleto.Nombre = Registro("Nombre")

        Try
            ProveedorCompleto.Direccion = Registro("Direccion")
        Catch
        End Try
        Try
            ProveedorCompleto.IdCiudad = Registro("IdCiudad")
        Catch
        End Try
        Try
            ProveedorCompleto.CP = Registro("CP")
        Catch
        End Try
        Try
            ProveedorCompleto.CUIT = Registro("Cuit")
        Catch
        End Try
        Try
            ProveedorCompleto.RazonSocial = Registro("RazonSocial")
        Catch
        End Try
        Try
            ProveedorCompleto.Web = Registro("Web")
        Catch
        End Try
        Try
            ProveedorCompleto.Sucursal = Registro("Sucursal")
        Catch
        End Try
        Try
            ProveedorCompleto.Telefono1 = Registro("Telefono1")
        Catch
        End Try
        Try
            ProveedorCompleto.Telefono2 = Registro("Telefono2")
        Catch
        End Try
        Try
            ProveedorCompleto.Telefono3 = Registro("Telefono3")
        Catch
        End Try
        Try
            ProveedorCompleto.Mail1 = Registro("Mail1")
        Catch
        End Try
        Try
            ProveedorCompleto.Mail2 = Registro("Mail2")
        Catch
        End Try
        Try
            ProveedorCompleto.Mail3 = Registro("Mail3")
        Catch
        End Try
        Try
            ProveedorCompleto.Contacto1 = Registro("Contacto1")
        Catch
        End Try
        Try
            ProveedorCompleto.Contacto2 = Registro("Contacto2")
        Catch
        End Try
        Try
            ProveedorCompleto.Contacto3 = Registro("Contacto3")
        Catch
        End Try
        Try
            ProveedorCompleto.TelContacto1 = Registro("TelContacto1")
        Catch
        End Try
        Try
            ProveedorCompleto.TelContacto2 = Registro("TelContacto2")
        Catch
        End Try
        Try
            ProveedorCompleto.TelContacto3 = Registro("TelContacto3")
        Catch
        End Try

        Return ProveedorCompleto
    End Function
    Public Function ListarTodosNombre() As List(Of Proveedor)
        Dim UnaListaDeProveedores As New List(Of Proveedor) 'Lista de objetos producto
        Dim AccesoProveedor As New DBProveedor 'Instancio clase DbProducto de DAL

        Dim UnaTabla As DataTable
        UnaTabla = AccesoProveedor.SelectAllProveedores() 'Pido a capa de datos la tabla productos

        'Recorro DataTable por cada fila agrego un producto a la lista de clientes
        For Each Registro In UnaTabla.Rows
            Dim u As New Proveedor

            u.IdProveedor = Registro("IdProveedor")
            u.Nombre = Registro("Nombre")
            UnaListaDeProveedores.Add(u)
        Next

        'Retorno lista de objetos cliente para consumir desde formulario
        Return UnaListaDeProveedores
    End Function
    Public Sub Nuevo(NuevoProveedor As Proveedor)
        Dim AccesoProveedores As New DBProveedor 'Instancio clase DbCliente de DAL
        AccesoProveedores.Insert(NuevoProveedor) 'Pido a capa de datos que guarde el cliente que le mando
    End Sub
    Public Sub Modificar(UnProveedor As Proveedor)
        Dim AccesoProveedores As New DBProveedor 'Instancio clase DbCliente de DAL
        AccesoProveedores.Update(UnProveedor) 'Pido a capa de datos que modifique cliente que le mando
    End Sub
    Public Sub Eliminar(UnProveedor As Proveedor)
        Dim AccesoProveedores As New DBProveedor  'Instancio clase DbCliente de DAL
        AccesoProveedores.Delete(UnProveedor) 'Pido a capa de datos que "elimine" cliente que le mando
    End Sub
End Class
