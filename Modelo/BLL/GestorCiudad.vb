﻿Public Class GestorCiudad
    Public Function BuscarPorId(UnaCiudad As Ciudad) As Ciudad
        Dim AccesoCiudad As New DbCiudad 'Instancio clase DbCiudad de DAL

        Dim UnaTabla As DataTable
        UnaTabla = AccesoCiudad.SelectCiudadPorId(UnaCiudad)
        Dim Registro As DataRow = UnaTabla.Rows(0)

        Dim CiudadCompleta As New Modelo.Ciudad

        CiudadCompleta.IdCiudad = Registro("IdCiudad")

        CiudadCompleta.Localidad = Registro("Ciudad")

        CiudadCompleta.Partido = Registro("Partido")

        Return CiudadCompleta
    End Function
    Public Function ListarTodasCiudades() As List(Of Ciudad)
        Dim UnaListaDeCiudades As New List(Of Ciudad) 'Lista de objetos Ciudad
        Dim AccesoCiudad As New DbCiudad 'Instancio clase DbCiudad de DAL

        Dim UnaTabla As DataTable
        UnaTabla = AccesoCiudad.SelectAllCiudades() 'Pido a capa de datos la tabla Ciudads

        'Recorro DataTable por cada fila agrego un Ciudad a la lista de Ciudads
        For Each Registro In UnaTabla.Rows
            Dim u As New Ciudad

            u.IdCiudad = Registro("IdCiudad")
            u.Localidad = Registro("Ciudad")
            UnaListaDeCiudades.Add(u)
        Next

        'Retorno lista de objetos Ciudad para consumir desde formulario
        Return UnaListaDeCiudades
    End Function
End Class
