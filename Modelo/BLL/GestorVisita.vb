﻿Public Class GestorVisita

    Public Sub Nuevo(NuevoVisita As Visita)
        Dim AccesoVisita As New DbVisita 'Instancio clase DbVisita de DAL
        AccesoVisita.Insert(NuevoVisita) 'Pido a capa de datos que guarde el Visita que le mando
    End Sub

    Public Sub Modificar(UnVisita As Visita)
        Dim AccesoVisita As New DbVisita 'Instancio clase DbVisita de DAL
        AccesoVisita.Update(UnVisita) 'Pido a capa de datos que modifique Visita que le mando
    End Sub

    Public Sub Eliminar(UnVisita As Visita)
        Dim AccesoVisita As New DbVisita 'Instancio clase DbVisita de DAL
        AccesoVisita.Delete(UnVisita) 'Pido a capa de datos que "elimine" Visita que le mando
    End Sub
End Class
