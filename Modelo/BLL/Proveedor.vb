﻿Public Class Proveedor
    Private _IdProveedor As Integer
    Private _Nombre As String
    Private _RazonSocial As String
    Private _CUIT As Long
    Private _Web As String
    Private _Direccion As String
    Private _IdCiudad As Integer
    Private _CP As String
    Private _Sucursal As String
    Private _Mail1 As String
    Private _Mail2 As String
    Private _Mail3 As String
    Private _Telefono1 As Long
    Private _Telefono2 As Long
    Private _Telefono3 As Long
    Private _Contacto1 As String
    Private _Contacto2 As String
    Private _Contacto3 As String
    Private _TelContacto1 As Long
    Private _TelContacto2 As Long
    Private _TelContacto3 As Long


    Public Property IdProveedor() As Integer
        Get
            Return _IdProveedor
        End Get
        Set(ByVal value As Integer)
            _IdProveedor = value
        End Set
    End Property
    Public Property Nombre() As String
        Get
            Return _Nombre
        End Get
        Set(ByVal value As String)
            _Nombre = value
        End Set
    End Property
    Public Property RazonSocial() As String
        Get
            Return _RazonSocial
        End Get
        Set(ByVal value As String)
            _RazonSocial = value
        End Set
    End Property
    Public Property CUIT() As Long
        Get
            Return _CUIT
        End Get
        Set(ByVal value As Long)
            _CUIT = value
        End Set
    End Property
    Public Property Web() As String
        Get
            Return _Web
        End Get
        Set(ByVal value As String)
            _Web = value
        End Set
    End Property
    Public Property Direccion() As String
        Get
            Return _Direccion
        End Get
        Set(ByVal value As String)
            _Direccion = value
        End Set
    End Property
    Public Property IdCiudad() As Integer
        Get
            Return _IdCiudad
        End Get
        Set(ByVal value As Integer)
            _IdCiudad = value
        End Set
    End Property
    Public Property CP() As String
        Get
            Return _CP
        End Get
        Set(ByVal value As String)
            _CP = value
        End Set
    End Property
    Public Property Sucursal() As String
        Get
            Return _Sucursal
        End Get
        Set(ByVal value As String)
            _Sucursal = value
        End Set
    End Property
    Public Property Mail1() As String
        Get
            Return _Mail1
        End Get
        Set(ByVal value As String)
            _Mail1 = value
        End Set
    End Property
    Public Property Mail2() As String
        Get
            Return _Mail2
        End Get
        Set(ByVal value As String)
            _Mail2 = value
        End Set
    End Property
    Public Property Mail3() As String
        Get
            Return _Mail3
        End Get
        Set(ByVal value As String)
            _Mail3 = value
        End Set
    End Property
    Public Property Telefono1() As Long
        Get
            Return _Telefono1
        End Get
        Set(ByVal value As Long)
            _Telefono1 = value
        End Set
    End Property
    Public Property Telefono2() As Long
        Get
            Return _Telefono2
        End Get
        Set(ByVal value As Long)
            _Telefono2 = value
        End Set
    End Property
    Public Property Telefono3() As Long
        Get
            Return _Telefono3
        End Get
        Set(ByVal value As Long)
            _Telefono3 = value
        End Set
    End Property
    Public Property Contacto1() As String
        Get
            Return _Contacto1
        End Get
        Set(ByVal value As String)
            _Contacto1 = value
        End Set
    End Property
    Public Property Contacto2() As String
        Get
            Return _Contacto2
        End Get
        Set(ByVal value As String)
            _Contacto2 = value
        End Set
    End Property
    Public Property Contacto3() As String
        Get
            Return _Contacto3
        End Get
        Set(ByVal value As String)
            _Contacto3 = value
        End Set
    End Property
    Public Property TelContacto1() As Long
        Get
            Return _TelContacto1
        End Get
        Set(ByVal value As Long)
            _TelContacto1 = value
        End Set
    End Property
    Public Property TelContacto2() As Long
        Get
            Return _TelContacto2
        End Get
        Set(ByVal value As Long)
            _TelContacto2 = value
        End Set
    End Property
    Public Property TelContacto3() As Long
        Get
            Return _TelContacto3
        End Get
        Set(ByVal value As Long)
            _TelContacto3 = value
        End Set
    End Property

End Class
