﻿Public Class Item
    Private _IdItem As Integer
    Private _IdPresupuesto As Integer
    Private _Item As Integer
    Private _Ubicacion As String
    Private _Descripcion As String
    Private _Observacion As String
    Private _Precio As Double
    Private _Costo As Double
    Private _Cantidad As Integer
    Private _IdCategoria As Integer
    Private _Opcion As Integer
    Private _Aprobado As Boolean
    Public Property IdItem() As Integer
        Get
            Return _IdItem
        End Get
        Set(ByVal value As Integer)
            _IdItem = value
        End Set
    End Property
    Public Property IdPresupuesto() As Integer
        Get
            Return _IdPresupuesto
        End Get
        Set(ByVal value As Integer)
            _IdPresupuesto = value
        End Set
    End Property
        Public Property Item() As Double
            Get
                Return _Item
            End Get
            Set(ByVal value As Double)
                _Item = value
            End Set
    End Property
    Public Property Ubicacion() As String
        Get
            Return _Ubicacion
        End Get
        Set(ByVal value As String)
            _Ubicacion = value
        End Set
    End Property
    Public Property Descripcion() As String
        Get
            Return _Descripcion
        End Get
        Set(ByVal value As String)
            _Descripcion = value
        End Set
    End Property
    Public Property Observacion() As String
        Get
            Return _Observacion
        End Get
        Set(ByVal value As String)
            _Observacion = value
        End Set
    End Property
    Public Property Precio() As Double
        Get
            Return _Precio
        End Get
        Set(ByVal value As Double)
            _Precio = value
        End Set
    End Property
    Public Property Costo() As Double
        Get
            Return _Costo
        End Get
        Set(ByVal value As Double)
            _Costo = value
        End Set
    End Property
    Public Property IdCategoria() As Integer
        Get
            Return _IdCategoria
        End Get
        Set(ByVal value As Integer)
            _IdCategoria = value
        End Set
    End Property
    Public Property Opcion() As Integer
        Get
            Return _Opcion
        End Get
        Set(ByVal value As Integer)
            _Opcion = value
        End Set
    End Property
    Public Property Cantidad() As Integer
        Get
            Return _Cantidad
        End Get
        Set(ByVal value As Integer)
            _Cantidad = value
        End Set
    End Property
    Public Property Aprobado() As Boolean
        Get
            Return _Aprobado
        End Get
        Set(ByVal value As Boolean)
            _Aprobado = value
        End Set
    End Property
End Class
