﻿Public Class GestorListaPrecio
    Public Sub Nuevo(NuevaLista As ListaPrecio)
        Dim AccesoItem As New DbListaPrecio 'Instancio clase DbListaPrecio de DAL
        AccesoItem.Insert(NuevaLista) 'Pido a capa de datos que guarde la lista que le mando
    End Sub
    Public Sub Modificar(UnaLista As ListaPrecio)
        Dim AccesoItem As New DbListaPrecio 'Instancio clase DbListaPrecio de DAL
        AccesoItem.Update(UnaLista) 'Pido a capa de datos que modifique la lista que le mando
    End Sub
    Public Sub Eliminar(UnaLista As ListaPrecio)
        Dim AccesoItem As New DbListaPrecio 'Instancio clase DbListaPrecio de DAL
        AccesoItem.Delete(UnaLista) 'Pido a capa de datos que "elimine" la lista que le mando
    End Sub
    Public Function Fecha(UnaLista As ListaPrecio)
        Dim AccesoListaPrecio As New DbListaPrecio
        Fecha = AccesoListaPrecio.SelectLista(UnaLista)
        Return Fecha
    End Function
End Class
