﻿Public Class ListaPrecio
    Private _IdListaPrecio As Integer
    Private _IdProveedor As Integer
    Private _Fecha As Date
    Public Property IdListaPrecio() As Integer
        Get
            Return _IdListaPrecio
        End Get
        Set(ByVal value As Integer)
            _IdListaPrecio = value
        End Set
    End Property
    Public Property IdProveedor() As Integer
        Get
            Return _IdProveedor
        End Get
        Set(ByVal value As Integer)
            _IdProveedor = value
        End Set
    End Property
    Public Property Fecha() As Date
        Get
            Return _Fecha
        End Get
        Set(ByVal value As Date)
            _Fecha = value
        End Set
    End Property
End Class
