﻿Public Class GestorClientes
    Public Function BuscarPorId(UnCliente As Cliente) As Cliente
        Dim AccesoClientes As New DbCliente 'Instancio clase DbCliente de DAL

        Dim UnaTabla As DataTable
        UnaTabla = AccesoClientes.SelectClientePorId(UnCliente)
        Dim Registro As DataRow = UnaTabla.Rows(0)

        Dim ClienteCompleto As New Cliente

        ClienteCompleto.IdCliente = Registro("IdCliente")

        ClienteCompleto.Nombre = Registro("Nombre")

        Try
            ClienteCompleto.Direccion = Registro("Direccion")
        Catch
        End Try
        Try
            ClienteCompleto.IdCiudad = Registro("IdCiudad")
        Catch
        End Try
        Try
            ClienteCompleto.CP = Registro("CP")
        Catch
        End Try
        Try
            ClienteCompleto.DNI = Registro("DNI")
        Catch
        End Try
        Try
            ClienteCompleto.Cuit = Registro("Cuit")
        Catch
        End Try
        Try
            ClienteCompleto.RazonSocial = Registro("RazonSocial")
        Catch
        End Try
        Try
            ClienteCompleto.Web = Registro("Web")
        Catch
        End Try
        Try
            ClienteCompleto.FechaNac = Registro("FechaNac")
        Catch
        End Try
        Try
            ClienteCompleto.Telefono1 = Registro("Telefono1")
        Catch
        End Try
        Try
            ClienteCompleto.Telefono2 = Registro("Telefono2")
        Catch
        End Try
        Try
            ClienteCompleto.Telefono3 = Registro("Telefono3")
        Catch
        End Try
        Try
            ClienteCompleto.Movil1 = Registro("Movil1")
        Catch
        End Try
        Try
            ClienteCompleto.Movil2 = Registro("Movil2")
        Catch
        End Try
        Try
            ClienteCompleto.Movil3 = Registro("Movil3")
        Catch
        End Try
        Try
            ClienteCompleto.Mail1 = Registro("Mail1")
        Catch
        End Try
        Try
            ClienteCompleto.Mail2 = Registro("Mail2")
        Catch
        End Try
        Try
            ClienteCompleto.Mail3 = Registro("Mail3")
        Catch
        End Try
        Try
            ClienteCompleto.Contacto1 = Registro("Contacto1")
        Catch
        End Try
        Try
            ClienteCompleto.Contacto2 = Registro("Contacto2")
        Catch
        End Try
        Try
            ClienteCompleto.Contacto3 = Registro("Contacto3")
        Catch
        End Try


        Return ClienteCompleto
    End Function
    Public Function ListarTodosNombre() As List(Of Cliente)
        Dim UnaListaClientes As New List(Of Cliente)
        Dim AccesoClientes As New DbCliente 'Instancio clase DbCliente de DAL

        Dim UnaTabla As DataTable
        UnaTabla = AccesoClientes.SelectAllClientes()

        'Recorro DataTable por cada fila agrego un producto a la lista de clientes
        For Each Registro In UnaTabla.Rows
            Dim u As New Cliente

            u.IdCliente = Registro("IdCliente")
            u.Nombre = Registro("Nombre")
            UnaListaClientes.Add(u)
        Next

        Return UnaListaClientes 'Pido a capa de datos la tabla clientes
    End Function
    Public Sub Nuevo(NuevoCliente As Cliente)
        Dim AccesoClientes As New DbCliente 'Instancio clase DbCliente de DAL
        AccesoClientes.Insert(NuevoCliente) 'Pido a capa de datos que guarde el cliente que le mando
    End Sub
    Public Sub Modificar(UnCliente As Cliente)
        Dim AccesoClientes As New DbCliente 'Instancio clase DbCliente de DAL
        AccesoClientes.Update(UnCliente) 'Pido a capa de datos que modifique cliente que le mando
    End Sub
    Public Sub Eliminar(UnCliente As Cliente)
        Dim AccesoClientes As New DbCliente 'Instancio clase DbCliente de DAL
        AccesoClientes.Delete(UnCliente) 'Pido a capa de datos que "elimine" cliente que le mando
    End Sub
End Class
