﻿Public Class GestorArticulo

    '------ FUNCIONES FILTRO ------
    Public Function BuscarPorItem(UnArticulo As Articulo) As Articulo
        Dim AccesoArticulo As New DbArticulo 'Instancio clase DbArticulo de DAL

        Dim UnaTabla As DataTable
        UnaTabla = AccesoArticulo.SelectArticuloPorPresupuestoAndItem(UnArticulo)
        Dim Registro As DataRow = UnaTabla.Rows(0)

        Dim ArticuloCompleto As New Articulo

        Try
            ArticuloCompleto.IdArticulo = Registro("IdArticulo")
        Catch
        End Try
        Try
            ArticuloCompleto.IdPresupuesto = Registro("IdPresupuesto")
        Catch
        End Try
        Try
            ArticuloCompleto.Item = Registro("Item")
        Catch
        End Try
        Try
            ArticuloCompleto.Nombre = Registro("Nombre")
        Catch
        End Try
        Try
            ArticuloCompleto.Observacion = Registro("Obvservacion")
        Catch
        End Try
        Try
            ArticuloCompleto.Costo = Registro("Costo")
        Catch
        End Try
        Try
            ArticuloCompleto.Precio = Registro("Precio")
        Catch
        End Try
        Try
            ArticuloCompleto.Dim1 = Registro("Dim1")
        Catch
        End Try
        Try
            ArticuloCompleto.Dim2 = Registro("Dim2")
        Catch
        End Try
        Try
            ArticuloCompleto.Dim3 = Registro("Dim3")
        Catch
        End Try

        Return ArticuloCompleto
    End Function

    '------ FUNCIONES CALCULOS ------
    Public Function CalcularPrecioGeneroCortina(UnProducto As Modelo.Producto, Genero As String, Ancho As String, Largo As String)
        Dim Precio As Double
        Try
            If Genero = "" Or Largo = "" Then
            Else
                If Ancho = UnProducto.Unidad Then
                    Precio = CDbl(Largo) * CDbl(UnProducto.Costo)
                Else
                    Precio = CDbl(Ancho) * CDbl(UnProducto.Costo)
                End If
            End If
        Catch
        End Try
        Return Precio
    End Function

    '------ EVENTOS ABM ------
    Public Sub Nuevo(NuevoArticulo As Articulo)
        Dim AccesoArticulo As New DbArticulo 'Instancio clase DbArticulo de DAL
        AccesoArticulo.Insert(NuevoArticulo) 'Pido a capa de datos que guarde el Articulo que le mando
    End Sub
    Public Sub Modificar(UnArticulo As Articulo)
        Dim AccesoArticulo As New DbArticulo 'Instancio clase DbArticulo de DAL
        AccesoArticulo.Update(UnArticulo) 'Pido a capa de datos que modifique Articulo que le mando
    End Sub
    Public Sub Eliminar(UnArticulo As Articulo)
        Dim AccesoArticulo As New DbArticulo 'Instancio clase DbArticulo de DAL
        AccesoArticulo.Delete(UnArticulo) 'Pido a capa de datos que "elimine" Articulo que le mando
    End Sub
End Class
