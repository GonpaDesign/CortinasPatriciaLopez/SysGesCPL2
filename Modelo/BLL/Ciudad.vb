﻿Public Class Ciudad
    Private _IdCiudad As Integer
    Private _Localidad As String
    Private _Partido As String
    Public Property IdCiudad() As Integer
        Get
            Return _IdCiudad
        End Get
        Set(ByVal value As Integer)
            _IdCiudad = value
        End Set
    End Property
    Public Property Localidad() As String
        Get
            Return _Localidad
        End Get
        Set(ByVal value As String)
            _Localidad = value
        End Set
    End Property
    Public Property Partido() As String
        Get
            Return _Partido
        End Get
        Set(ByVal value As String)
            _Partido = value
        End Set
    End Property
End Class
