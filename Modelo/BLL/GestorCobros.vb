﻿Public Class GestorCobros
    Public Function CalcularCobrado(UnPresupuesto As Presupuesto) As Double
        Dim AccesoCobros As New DbCobros 'Instancio clase DbCliente de DAL
        CalcularCobrado = AccesoCobros.CalcularCobrado(UnPresupuesto) 'Pido a capa de datos que guarde el cliente que le mando
    End Function
    Public Sub Nuevo(NuevoCobro As Cobro)
        Dim AccesoCobros As New DbCobros 'Instancio clase DbCliente de DAL
        AccesoCobros.Insert(NuevoCobro) 'Pido a capa de datos que guarde el cliente que le mando
    End Sub

    Public Sub Modificar(UnCobro As Cobro)
        Dim AccesoCobros As New DbCobros 'Instancio clase DbCobro de DAL
        AccesoCobros.Update(UnCobro) 'Pido a capa de datos que modifique Cobro que le mando
    End Sub

    Public Sub Eliminar(UnCobro As Cobro)
        Dim AccesoCobros As New DbCobros 'Instancio clase DbCobro de DAL
        AccesoCobros.Delete(UnCobro) 'Pido a capa de datos que "elimine" Cobro que le mando
    End Sub
End Class
