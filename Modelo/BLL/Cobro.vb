﻿Public Class Cobro
    Private _IdCobro As Integer
    Private _IdPresupuesto As Integer
    Private _Tipo As String
    Private _IdCheque As Integer
    Private _IdTransferencia As Integer
    Private _Monto As Double
    Private _Fecha As Date
    Private _Observaciones As String

    Public Property IdCobro() As Integer
        Get
            Return _IdCobro
        End Get
        Set(ByVal value As Integer)
            _IdCobro = value
        End Set
    End Property
    Public Property IdPresupuesto() As Integer
        Get
            Return _IdPresupuesto
        End Get
        Set(ByVal value As Integer)
            _IdPresupuesto = value
        End Set
    End Property
    Public Property Tipo() As String
        Get
            Return _Tipo
        End Get
        Set(ByVal value As String)
            _Tipo = value
        End Set
    End Property
    Public Property IdCheque() As Integer
        Get
            Return _IdCheque
        End Get
        Set(ByVal value As Integer)
            _IdCheque = value
        End Set
    End Property
    Public Property IdTransferencia() As Integer
        Get
            Return _IdTransferencia
        End Get
        Set(ByVal value As Integer)
            _IdTransferencia = value
        End Set
    End Property
    Public Property Monto() As Double
        Get
            Return _Monto
        End Get
        Set(ByVal value As Double)
            _Monto = value
        End Set
    End Property
    Public Property Fecha() As Date
        Get
            Return _Fecha
        End Get
        Set(ByVal value As Date)
            _Fecha = value
        End Set
    End Property
    Public Property Observaciones() As String
        Get
            Return _Observaciones
        End Get
        Set(ByVal value As String)
            _Observaciones = value
        End Set
    End Property

End Class
