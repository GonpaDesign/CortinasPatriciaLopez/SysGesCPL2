﻿Public Class Presupuesto
    Private _IdPresupuesto As Integer
    Private _IdCliente As Integer
    Private _FechaGenerado As Date
    Private _FechaColocacion As Date
    Private _Vigencia As Integer
    Private _FechaEntrega As Date
    Private _Notas As String
    Private _Colocacion As Boolean
    Private _Colocado As Boolean
    Private _Entregado As Boolean
    Private _Aprobado As Boolean

    Public Property IdPresupuesto() As Integer
        Get
            Return _IdPresupuesto
        End Get
        Set(ByVal value As Integer)
            _IdPresupuesto = value
        End Set
    End Property
    Public Property IdCliente() As Integer
        Get
            Return _IdCliente
        End Get
        Set(ByVal value As Integer)
            _IdCliente = value
        End Set
    End Property
    Public Property FechaGenerado() As Date
        Get
            Return _FechaGenerado
        End Get
        Set(ByVal value As Date)
            _FechaGenerado = value
        End Set
    End Property
    Public Property FechaColocacion() As Date
        Get
            Return _FechaColocacion
        End Get
        Set(ByVal value As Date)
            _FechaColocacion = value
        End Set
    End Property
    Public Property Vigencia() As Integer
        Get
            Return _Vigencia
        End Get
        Set(ByVal value As Integer)
            _Vigencia = value
        End Set
    End Property
    Public Property FechaEntrega() As Date
        Get
            Return _FechaEntrega
        End Get
        Set(ByVal value As Date)
            _FechaEntrega = value
        End Set
    End Property
    Public Property Notas() As String
        Get
            Return _Notas
        End Get
        Set(ByVal value As String)
            _Notas = value
        End Set
    End Property
    Public Property Colocacion() As Boolean
        Get
            Return _Colocacion
        End Get
        Set(ByVal value As Boolean)
            _Colocacion = value
        End Set
    End Property
    Public Property Entregado() As Boolean
        Get
            Return _Entregado
        End Get
        Set(ByVal value As Boolean)
            _Entregado = value
        End Set
    End Property
    Public Property Colocado() As Boolean
        Get
            Return _Colocado
        End Get
        Set(ByVal value As Boolean)
            _Colocado = value
        End Set
    End Property
    Public Property Aprobado() As Boolean
        Get
            Return _Aprobado
        End Get
        Set(ByVal value As Boolean)
            _Aprobado = value
        End Set
    End Property
End Class
