﻿Public Class Configuracion
    Private _IdConfiguracion As Integer
    Private _Descripcion As String
    Private _Valor As Double
    Public Property IdConfiguracion() As Integer
        Get
            Return _IdConfiguracion
        End Get
        Set(ByVal value As Integer)
            _IdConfiguracion = value
        End Set
    End Property
    Public Property Descripcion() As String
        Get
            Return _Descripcion
        End Get
        Set(ByVal value As String)
            _Descripcion = value
        End Set
    End Property
    Public Property Valor() As Double
        Get
            Return _Valor
        End Get
        Set(ByVal value As Double)
            _Valor = value
        End Set
    End Property
End Class
