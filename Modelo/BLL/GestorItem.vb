﻿Public Class GestorItem

    '------ FUNCIONES ------

    Public Function SelectItemPorPresupuesto(UnItem As Item) As List(Of Item)
        Dim UnaListaDeItems As New List(Of Item) 'Lista de objetos producto
        Dim AccesoItem As New DbItem 'Instancio clase DbProducto de DAL

        Dim UnaTabla As DataTable
        UnaTabla = AccesoItem.SelectItemPorPresupuesto(UnItem) 'Pido a capa de datos la tabla productos

        'Recorro DataTable por cada fila agrego un producto a la lista de clientes
        For Each Registro In UnaTabla.Rows
            Dim u As New Item

            u.Item = Registro("Item")
            u.IdCategoria = Registro("IdCategoria")
            u.Ubicacion = Registro("Ubicacion")
            u.Descripcion = Registro("Descripcion")
            u.Observacion = Registro("Observacion")
            u.Cantidad = Registro("Cantidad")
            u.Precio = Registro("Precio")
            u.Costo = Registro("Costo")
            u.Opcion = Registro("Opcion")
            u.Aprobado = Registro("Aprobado")
            UnaListaDeItems.Add(u)
        Next

        'Retorno lista de objetos cliente para consumir desde formulario
        Return UnaListaDeItems
    End Function
    Public Function ObtenerItemMax(UnItem As Item) As Integer
        Dim AccesoItem As New DbItem 'Instancio clase DbItem de DAL
        ObtenerItemMax = AccesoItem.SelectMaxItemPresupuesto(UnItem) 'Pido a capa de datos que guarde el Item que le mando
        Return ObtenerItemMax
    End Function
    Public Function CalcularTotal(UnItem As Item)
        Dim AccesoItem As New DbItem 'Instancio clase DbItem de DAL
        CalcularTotal = AccesoItem.CalcularTotal(UnItem) 'Pido a capa de datos que guarde el Item que le mando
        Return CalcularTotal
    End Function
    Public Function CalcularPrecioOpcion(Presupuesto As Integer, OP As Integer)
        Dim UnItem As Item = New Item() With {.IdPresupuesto = Presupuesto, .Opcion = OP}
        Dim AccesoItem As New DbItem 'Instancio clase DbItem de DAL
        CalcularPrecioOpcion = AccesoItem.CalcularTotalOP(UnItem) 'Pido a capa de datos que guarde el Item que le mando
        Return CalcularPrecioOpcion
    End Function

    '------ ABM ------

    Public Sub Desaprobar(UnItem As Item)
        Dim AccesoItem As New DbItem 'Instancio clase DbItem de DAL
        AccesoItem.Desaprobar(UnItem) 'Pido a capa de datos que guarde el Item que le mando
    End Sub
    Public Sub Aprobar(UnItem As Item)
        Dim AccesoItem As New DbItem 'Instancio clase DbItem de DAL
        AccesoItem.Aprobar(UnItem) 'Pido a capa de datos que guarde el Item que le mando
    End Sub
    Public Sub Nuevo(NuevoItem As Item)
        Dim AccesoItem As New DbItem 'Instancio clase DbItem de DAL
        AccesoItem.Insert(NuevoItem) 'Pido a capa de datos que guarde el Item que le mando
    End Sub
    Public Sub Modificar(UnItem As Item)
        Dim AccesoItem As New DbItem 'Instancio clase DbItem de DAL
        AccesoItem.Update(UnItem) 'Pido a capa de datos que modifique Item que le mando
    End Sub
    Public Sub Eliminar(UnItem As Item)
        Dim AccesoItem As New DbItem 'Instancio clase DbItem de DAL
        AccesoItem.Delete(UnItem) 'Pido a capa de datos que "elimine" Item que le mando
    End Sub
End Class
