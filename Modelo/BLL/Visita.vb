﻿Public Class Visita
    Private _IdVisita As Integer
    Private _IdCliente As Integer
    Private _Direccion As String
    Private _IdCiudad As Integer
    Private _Fecha As Date
    Private _Observacion As String
    Public Property IdVisita() As Integer
        Get
            Return _IdVisita
        End Get
        Set(ByVal value As Integer)
            _IdVisita = value
        End Set
    End Property
    Public Property IdCliente() As Integer
        Get
            Return _IdCliente
        End Get
        Set(ByVal value As Integer)
            _IdCliente = value
        End Set
    End Property
    Public Property Direccion() As String
        Get
            Return _Direccion
        End Get
        Set(ByVal value As String)
            _Direccion = value
        End Set
    End Property
    Public Property IdCiudad() As Integer
        Get
            Return _IdCiudad
        End Get
        Set(ByVal value As Integer)
            _IdCiudad = value
        End Set
    End Property
    Public Property Observacion() As String
        Get
            Return _Observacion
        End Get
        Set(ByVal value As String)
            _Observacion = value
        End Set
    End Property
    Public Property Fecha() As Date
        Get
            Return _Fecha
        End Get
        Set(ByVal value As Date)
            _Fecha = value
        End Set
    End Property
End Class
