﻿Public Class Categoria
    Private _IdCategoria As Integer
    Private _Categoria As String
    Public Property IdCategoria() As Integer
        Get
            Return _IdCategoria
        End Get
        Set(ByVal value As Integer)
            _IdCategoria = value
        End Set
    End Property
    Public Property Categoria() As String
        Get
            Return _Categoria
        End Get
        Set(ByVal value As String)
            _Categoria = value
        End Set
    End Property
End Class
