﻿Public Class GestorPresupuesto

    '------ FUNCIONES DE FILTRADO ------
    Public Function BuscarPorId(UnPresupuesto As Presupuesto) As Presupuesto
        Dim AccesoPresupuesto As New DbPresupuesto 'Instancio clase DbPresupuesto de DAL

        Dim UnaTabla As DataTable
        UnaTabla = AccesoPresupuesto.SelectPresupuestoPorId(UnPresupuesto)
        Dim Registro As DataRow = UnaTabla.Rows(0)

        Dim PresupuestoCompleto As New Presupuesto

        PresupuestoCompleto.IdPresupuesto = Registro("IdPresupuesto")


        PresupuestoCompleto.IdCliente = Registro("IdCliente")

        Try
            PresupuestoCompleto.FechaGenerado = Registro("FechaGenerado")
        Catch
        End Try
        Try
            PresupuestoCompleto.FechaColocacion = Registro("FechaColocacion")
        Catch
        End Try
        Try
            PresupuestoCompleto.Vigencia = Registro("Vigencia")
        Catch
        End Try
        Try
            PresupuestoCompleto.FechaEntrega = Registro("FechaEntrega")
        Catch
        End Try
        Try
            PresupuestoCompleto.Notas = Registro("Notas")
        Catch
        End Try
        Try
            PresupuestoCompleto.Colocacion = Registro("Colocacion")
        Catch
        End Try


        Return PresupuestoCompleto
    End Function
    Public Function ListarTodosNombre() As List(Of Presupuesto)
        Dim UnaListaDePresupuestos As New List(Of Presupuesto) 'Lista de objetos producto
        Dim AccesoPresupuesto As New DbPresupuesto 'Instancio clase DbProducto de DAL

        Dim UnaTabla As DataTable
        UnaTabla = AccesoPresupuesto.SelectAllPresupuestos() 'Pido a capa de datos la tabla productos

        'Recorro DataTable por cada fila agrego un producto a la lista de clientes
        For Each Registro In UnaTabla.Rows
            Dim u As New Presupuesto

            u.IdPresupuesto = Registro("IdPresupuesto")
            u.IdCliente = Registro("IdCliente")
            UnaListaDePresupuestos.Add(u)
        Next

        'Retorno lista de objetos cliente para consumir desde formulario
        Return UnaListaDePresupuestos
    End Function
    Public Function MaxId() As Integer
        Dim AccesoPresupuesto As New DbPresupuesto

        MaxId = AccesoPresupuesto.ObtenerNuevoId()
        MaxId = MaxId - 1
        Return MaxId
    End Function

    '------ EVENTOS ABM ------
    Public Sub Nuevo(NuevoPresupuesto As Presupuesto)
        Dim AccesoPresupuesto As New DbPresupuesto 'Instancio clase DbProducto de DAL
        AccesoPresupuesto.Insert(NuevoPresupuesto) 'Pido a capa de datos que guarde el producto que le mando
    End Sub
    Public Sub Modificar(UnPresupuesto As Presupuesto)
        Dim AccesoPresupuesto As New DbPresupuesto 'Instancio clase DbProducto de DAL
        AccesoPresupuesto.Update(UnPresupuesto) 'Pido a capa de datos que modifique producto que le mando
    End Sub
    Public Sub ModificarColocacion(UnPresupuesto As Presupuesto)
        Dim AccesoPresupuesto As New DbPresupuesto 'Instancio clase DbProducto de DAL
        AccesoPresupuesto.UpdateColocacion(UnPresupuesto) 'Pido a capa de datos que modifique producto que le mando
    End Sub
    Public Sub Eliminar(UnPresupuesto As Presupuesto)
        Dim AccesoPresupuesto As New DbPresupuesto 'Instancio clase DbProducto de DAL
        AccesoPresupuesto.Delete(UnPresupuesto) 'Pido a capa de datos que "elimine" producto que le mando
    End Sub
    Public Sub Aprobar(UnPresupuesto As Presupuesto)
        Dim AccesoPresupuesto As New DbPresupuesto 'Instancio clase DbProducto de DAL
        AccesoPresupuesto.Aprobar(UnPresupuesto) 'Pido a capa de datos que "Apruebe" producto que le mando
    End Sub
    Public Sub Colocar(UnPresupuesto As Presupuesto)
        Dim AccesoPresupuesto As New DbPresupuesto 'Instancio clase DbProducto de DAL
        AccesoPresupuesto.Colocar(UnPresupuesto) 'Pido a capa de datos que "Coloque" producto que le mando
    End Sub
    Public Sub Entregar(UnPresupuesto As Presupuesto)
        Dim AccesoPresupuesto As New DbPresupuesto 'Instancio clase DbProducto de DAL
        AccesoPresupuesto.Entregar(UnPresupuesto) 'Pido a capa de datos que "Entregue" producto que le mando
    End Sub
End Class

