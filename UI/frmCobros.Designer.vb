﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCobros
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.btnEliminar = New DevExpress.XtraEditors.SimpleButton()
        Me.btnSalir = New DevExpress.XtraEditors.SimpleButton()
        Me.btnCancelar = New DevExpress.XtraEditors.SimpleButton()
        Me.gcBuscar = New DevExpress.XtraEditors.PanelControl()
        Me.btnGuardar = New DevExpress.XtraEditors.SimpleButton()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.cmbBuscar = New System.Windows.Forms.ComboBox()
        Me.cmbIdPresupuesto = New System.Windows.Forms.ComboBox()
        Me.lblPresupuesto = New DevExpress.XtraEditors.LabelControl()
        Me.gcHistorial = New DevExpress.XtraEditors.GroupControl()
        Me.dgvHistorial = New System.Windows.Forms.DataGridView()
        Me.gcPagosNuevo = New DevExpress.XtraEditors.GroupControl()
        Me.dtpFechaNac = New System.Windows.Forms.DateTimePicker()
        Me.txtWeb = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.txtCUIT = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtNombre = New System.Windows.Forms.TextBox()
        Me.txtEmpresa = New System.Windows.Forms.TextBox()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.txtId = New System.Windows.Forms.TextBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.gcEstadoCobro = New DevExpress.XtraEditors.GroupControl()
        Me.cmbCiudad = New System.Windows.Forms.ComboBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtDireccion = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtCP = New System.Windows.Forms.TextBox()
        Me.SimpleButton1 = New DevExpress.XtraEditors.SimpleButton()
        CType(Me.gcBuscar, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gcBuscar.SuspendLayout()
        CType(Me.gcHistorial, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gcHistorial.SuspendLayout()
        CType(Me.dgvHistorial, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gcPagosNuevo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gcPagosNuevo.SuspendLayout()
        CType(Me.gcEstadoCobro, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gcEstadoCobro.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnEliminar
        '
        Me.btnEliminar.Appearance.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold)
        Me.btnEliminar.Appearance.Options.UseFont = True
        Me.btnEliminar.Location = New System.Drawing.Point(1033, 596)
        Me.btnEliminar.LookAndFeel.SkinName = "Visual Studio 2013 Dark"
        Me.btnEliminar.LookAndFeel.UseDefaultLookAndFeel = False
        Me.btnEliminar.Name = "btnEliminar"
        Me.btnEliminar.Size = New System.Drawing.Size(75, 27)
        Me.btnEliminar.TabIndex = 61
        Me.btnEliminar.Text = "Eliminar"
        '
        'btnSalir
        '
        Me.btnSalir.Appearance.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold)
        Me.btnSalir.Appearance.Options.UseFont = True
        Me.btnSalir.Location = New System.Drawing.Point(1033, 662)
        Me.btnSalir.LookAndFeel.SkinName = "Visual Studio 2013 Dark"
        Me.btnSalir.LookAndFeel.UseDefaultLookAndFeel = False
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(75, 27)
        Me.btnSalir.TabIndex = 60
        Me.btnSalir.Text = "Cerrar"
        '
        'btnCancelar
        '
        Me.btnCancelar.Appearance.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold)
        Me.btnCancelar.Appearance.Options.UseFont = True
        Me.btnCancelar.Cursor = System.Windows.Forms.Cursors.Default
        Me.btnCancelar.Location = New System.Drawing.Point(1033, 629)
        Me.btnCancelar.LookAndFeel.SkinName = "Visual Studio 2013 Dark"
        Me.btnCancelar.LookAndFeel.UseDefaultLookAndFeel = False
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 27)
        Me.btnCancelar.TabIndex = 59
        Me.btnCancelar.Text = "Cancelar"
        '
        'gcBuscar
        '
        Me.gcBuscar.Controls.Add(Me.cmbIdPresupuesto)
        Me.gcBuscar.Controls.Add(Me.lblPresupuesto)
        Me.gcBuscar.Controls.Add(Me.btnGuardar)
        Me.gcBuscar.Controls.Add(Me.Label8)
        Me.gcBuscar.Controls.Add(Me.cmbBuscar)
        Me.gcBuscar.Location = New System.Drawing.Point(12, 12)
        Me.gcBuscar.LookAndFeel.SkinName = "Visual Studio 2013 Dark"
        Me.gcBuscar.LookAndFeel.UseDefaultLookAndFeel = False
        Me.gcBuscar.Name = "gcBuscar"
        Me.gcBuscar.Size = New System.Drawing.Size(1090, 37)
        Me.gcBuscar.TabIndex = 62
        '
        'btnGuardar
        '
        Me.btnGuardar.Appearance.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold)
        Me.btnGuardar.Appearance.Options.UseFont = True
        Me.btnGuardar.Cursor = System.Windows.Forms.Cursors.Default
        Me.btnGuardar.Location = New System.Drawing.Point(1009, 5)
        Me.btnGuardar.LookAndFeel.SkinName = "Visual Studio 2013 Dark"
        Me.btnGuardar.LookAndFeel.UseDefaultLookAndFeel = False
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(75, 27)
        Me.btnGuardar.TabIndex = 58
        Me.btnGuardar.Text = "Guardar"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.BackColor = System.Drawing.Color.Transparent
        Me.Label8.Font = New System.Drawing.Font("Cambria", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Label8.Location = New System.Drawing.Point(92, 8)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(71, 19)
        Me.Label8.TabIndex = 54
        Me.Label8.Text = "Nombre:"
        '
        'cmbBuscar
        '
        Me.cmbBuscar.AllowDrop = True
        Me.cmbBuscar.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cmbBuscar.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cmbBuscar.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbBuscar.Font = New System.Drawing.Font("Calibri", 12.0!)
        Me.cmbBuscar.IntegralHeight = False
        Me.cmbBuscar.Location = New System.Drawing.Point(169, 5)
        Me.cmbBuscar.Name = "cmbBuscar"
        Me.cmbBuscar.Size = New System.Drawing.Size(592, 27)
        Me.cmbBuscar.TabIndex = 53
        '
        'cmbIdPresupuesto
        '
        Me.cmbIdPresupuesto.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbIdPresupuesto.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbIdPresupuesto.FormattingEnabled = True
        Me.cmbIdPresupuesto.Location = New System.Drawing.Point(882, 6)
        Me.cmbIdPresupuesto.Name = "cmbIdPresupuesto"
        Me.cmbIdPresupuesto.Size = New System.Drawing.Size(121, 26)
        Me.cmbIdPresupuesto.TabIndex = 63
        '
        'lblPresupuesto
        '
        Me.lblPresupuesto.Appearance.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold)
        Me.lblPresupuesto.Location = New System.Drawing.Point(767, 8)
        Me.lblPresupuesto.Name = "lblPresupuesto"
        Me.lblPresupuesto.Size = New System.Drawing.Size(109, 19)
        Me.lblPresupuesto.TabIndex = 62
        Me.lblPresupuesto.Text = "Presupuesto N°:"
        '
        'gcHistorial
        '
        Me.gcHistorial.Controls.Add(Me.dgvHistorial)
        Me.gcHistorial.Location = New System.Drawing.Point(12, 245)
        Me.gcHistorial.LookAndFeel.SkinName = "Visual Studio 2013 Dark"
        Me.gcHistorial.LookAndFeel.UseDefaultLookAndFeel = False
        Me.gcHistorial.Name = "gcHistorial"
        Me.gcHistorial.Size = New System.Drawing.Size(1009, 444)
        Me.gcHistorial.TabIndex = 63
        Me.gcHistorial.Text = "Historial"
        '
        'dgvHistorial
        '
        Me.dgvHistorial.AllowUserToAddRows = False
        Me.dgvHistorial.AllowUserToDeleteRows = False
        Me.dgvHistorial.AllowUserToResizeColumns = False
        Me.dgvHistorial.AllowUserToResizeRows = False
        Me.dgvHistorial.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(32, Byte), Integer), CType(CType(31, Byte), Integer), CType(CType(53, Byte), Integer))
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvHistorial.DefaultCellStyle = DataGridViewCellStyle1
        Me.dgvHistorial.Location = New System.Drawing.Point(95, 27)
        Me.dgvHistorial.Margin = New System.Windows.Forms.Padding(3, 6, 3, 6)
        Me.dgvHistorial.MultiSelect = False
        Me.dgvHistorial.Name = "dgvHistorial"
        Me.dgvHistorial.ReadOnly = True
        Me.dgvHistorial.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvHistorial.RowTemplate.ReadOnly = True
        Me.dgvHistorial.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvHistorial.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvHistorial.Size = New System.Drawing.Size(903, 409)
        Me.dgvHistorial.TabIndex = 38
        '
        'gcPagosNuevo
        '
        Me.gcPagosNuevo.Controls.Add(Me.SimpleButton1)
        Me.gcPagosNuevo.Controls.Add(Me.dtpFechaNac)
        Me.gcPagosNuevo.Controls.Add(Me.txtWeb)
        Me.gcPagosNuevo.Controls.Add(Me.Label5)
        Me.gcPagosNuevo.Controls.Add(Me.Label20)
        Me.gcPagosNuevo.Controls.Add(Me.txtCUIT)
        Me.gcPagosNuevo.Controls.Add(Me.Label1)
        Me.gcPagosNuevo.Controls.Add(Me.txtNombre)
        Me.gcPagosNuevo.Controls.Add(Me.txtEmpresa)
        Me.gcPagosNuevo.Controls.Add(Me.Label19)
        Me.gcPagosNuevo.Controls.Add(Me.Label12)
        Me.gcPagosNuevo.Controls.Add(Me.txtId)
        Me.gcPagosNuevo.Controls.Add(Me.Label15)
        Me.gcPagosNuevo.Location = New System.Drawing.Point(12, 117)
        Me.gcPagosNuevo.LookAndFeel.SkinName = "Visual Studio 2013 Dark"
        Me.gcPagosNuevo.LookAndFeel.UseDefaultLookAndFeel = False
        Me.gcPagosNuevo.Name = "gcPagosNuevo"
        Me.gcPagosNuevo.Size = New System.Drawing.Size(1090, 122)
        Me.gcPagosNuevo.TabIndex = 64
        Me.gcPagosNuevo.Text = "Registro de Cobro"
        '
        'dtpFechaNac
        '
        Me.dtpFechaNac.CustomFormat = "dd/MM/yyyy"
        Me.dtpFechaNac.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpFechaNac.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpFechaNac.Location = New System.Drawing.Point(839, 24)
        Me.dtpFechaNac.MaxDate = New Date(2050, 12, 31, 0, 0, 0, 0)
        Me.dtpFechaNac.MinDate = New Date(1900, 1, 1, 0, 0, 0, 0)
        Me.dtpFechaNac.Name = "dtpFechaNac"
        Me.dtpFechaNac.Size = New System.Drawing.Size(117, 26)
        Me.dtpFechaNac.TabIndex = 59
        '
        'txtWeb
        '
        Me.txtWeb.Font = New System.Drawing.Font("Calibri", 12.0!)
        Me.txtWeb.Location = New System.Drawing.Point(169, 90)
        Me.txtWeb.Name = "txtWeb"
        Me.txtWeb.Size = New System.Drawing.Size(684, 27)
        Me.txtWeb.TabIndex = 49
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.BackColor = System.Drawing.Color.Transparent
        Me.Label5.Font = New System.Drawing.Font("Cambria", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Label5.Location = New System.Drawing.Point(64, 93)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(99, 19)
        Me.Label5.TabIndex = 48
        Me.Label5.Text = "Página Web:"
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.BackColor = System.Drawing.Color.Transparent
        Me.Label20.Font = New System.Drawing.Font("Cambria", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Label20.Location = New System.Drawing.Point(860, 93)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(49, 19)
        Me.Label20.TabIndex = 44
        Me.Label20.Text = "CUIT:"
        '
        'txtCUIT
        '
        Me.txtCUIT.Font = New System.Drawing.Font("Calibri", 12.0!)
        Me.txtCUIT.Location = New System.Drawing.Point(913, 90)
        Me.txtCUIT.Name = "txtCUIT"
        Me.txtCUIT.Size = New System.Drawing.Size(172, 27)
        Me.txtCUIT.TabIndex = 46
        Me.txtCUIT.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Cambria", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Label1.Location = New System.Drawing.Point(92, 27)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(71, 19)
        Me.Label1.TabIndex = 7
        Me.Label1.Text = "Nombre:"
        '
        'txtNombre
        '
        Me.txtNombre.Font = New System.Drawing.Font("Calibri", 12.0!)
        Me.txtNombre.Location = New System.Drawing.Point(169, 24)
        Me.txtNombre.Name = "txtNombre"
        Me.txtNombre.Size = New System.Drawing.Size(513, 27)
        Me.txtNombre.TabIndex = 29
        '
        'txtEmpresa
        '
        Me.txtEmpresa.Font = New System.Drawing.Font("Calibri", 12.0!)
        Me.txtEmpresa.Location = New System.Drawing.Point(169, 57)
        Me.txtEmpresa.Name = "txtEmpresa"
        Me.txtEmpresa.Size = New System.Drawing.Size(684, 27)
        Me.txtEmpresa.TabIndex = 43
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.BackColor = System.Drawing.Color.Transparent
        Me.Label19.Font = New System.Drawing.Font("Cambria", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Label19.Location = New System.Drawing.Point(55, 60)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(108, 19)
        Me.Label19.TabIndex = 42
        Me.Label19.Text = "Razón Social:"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.BackColor = System.Drawing.Color.Transparent
        Me.Label12.Font = New System.Drawing.Font("Cambria", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Label12.Location = New System.Drawing.Point(962, 63)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(29, 19)
        Me.Label12.TabIndex = 25
        Me.Label12.Text = "Id:"
        '
        'txtId
        '
        Me.txtId.Font = New System.Drawing.Font("Calibri", 12.0!)
        Me.txtId.Location = New System.Drawing.Point(997, 60)
        Me.txtId.Name = "txtId"
        Me.txtId.Size = New System.Drawing.Size(87, 27)
        Me.txtId.TabIndex = 26
        Me.txtId.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.BackColor = System.Drawing.Color.Transparent
        Me.Label15.Font = New System.Drawing.Font("Cambria", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Label15.Location = New System.Drawing.Point(688, 27)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(145, 19)
        Me.Label15.TabIndex = 40
        Me.Label15.Text = "Fecha Nacimiento:"
        '
        'gcEstadoCobro
        '
        Me.gcEstadoCobro.Controls.Add(Me.cmbCiudad)
        Me.gcEstadoCobro.Controls.Add(Me.Label3)
        Me.gcEstadoCobro.Controls.Add(Me.Label2)
        Me.gcEstadoCobro.Controls.Add(Me.txtDireccion)
        Me.gcEstadoCobro.Controls.Add(Me.Label4)
        Me.gcEstadoCobro.Controls.Add(Me.txtCP)
        Me.gcEstadoCobro.Location = New System.Drawing.Point(12, 55)
        Me.gcEstadoCobro.LookAndFeel.SkinName = "Visual Studio 2013 Dark"
        Me.gcEstadoCobro.LookAndFeel.UseDefaultLookAndFeel = False
        Me.gcEstadoCobro.Name = "gcEstadoCobro"
        Me.gcEstadoCobro.Size = New System.Drawing.Size(1090, 56)
        Me.gcEstadoCobro.TabIndex = 65
        Me.gcEstadoCobro.Text = "Estado de Cobros"
        '
        'cmbCiudad
        '
        Me.cmbCiudad.AllowDrop = True
        Me.cmbCiudad.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cmbCiudad.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cmbCiudad.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbCiudad.Font = New System.Drawing.Font("Calibri", 12.0!)
        Me.cmbCiudad.IntegralHeight = False
        Me.cmbCiudad.Location = New System.Drawing.Point(694, 24)
        Me.cmbCiudad.Name = "cmbCiudad"
        Me.cmbCiudad.Size = New System.Drawing.Size(266, 27)
        Me.cmbCiudad.TabIndex = 59
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.Label3.Font = New System.Drawing.Font("Cambria", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Label3.Location = New System.Drawing.Point(622, 27)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(66, 19)
        Me.Label3.TabIndex = 3
        Me.Label3.Text = "Ciudad:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.Font = New System.Drawing.Font("Cambria", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Label2.Location = New System.Drawing.Point(79, 27)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(84, 19)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Dirección:"
        '
        'txtDireccion
        '
        Me.txtDireccion.Font = New System.Drawing.Font("Calibri", 12.0!)
        Me.txtDireccion.Location = New System.Drawing.Point(169, 24)
        Me.txtDireccion.Name = "txtDireccion"
        Me.txtDireccion.Size = New System.Drawing.Size(449, 27)
        Me.txtDireccion.TabIndex = 14
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.BackColor = System.Drawing.Color.Transparent
        Me.Label4.Font = New System.Drawing.Font("Cambria", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Label4.Location = New System.Drawing.Point(966, 27)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(32, 19)
        Me.Label4.TabIndex = 4
        Me.Label4.Text = "CP:"
        '
        'txtCP
        '
        Me.txtCP.Font = New System.Drawing.Font("Calibri", 12.0!)
        Me.txtCP.Location = New System.Drawing.Point(1004, 24)
        Me.txtCP.Name = "txtCP"
        Me.txtCP.Size = New System.Drawing.Size(80, 27)
        Me.txtCP.TabIndex = 16
        Me.txtCP.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'SimpleButton1
        '
        Me.SimpleButton1.Appearance.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold)
        Me.SimpleButton1.Appearance.Options.UseFont = True
        Me.SimpleButton1.Cursor = System.Windows.Forms.Cursors.Default
        Me.SimpleButton1.Location = New System.Drawing.Point(1009, 24)
        Me.SimpleButton1.LookAndFeel.SkinName = "Visual Studio 2013 Dark"
        Me.SimpleButton1.LookAndFeel.UseDefaultLookAndFeel = False
        Me.SimpleButton1.Name = "SimpleButton1"
        Me.SimpleButton1.Size = New System.Drawing.Size(75, 27)
        Me.SimpleButton1.TabIndex = 60
        Me.SimpleButton1.Text = "Guardar"
        '
        'frmCobros
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoSize = True
        Me.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.BackColor = System.Drawing.Color.Black
        Me.ClientSize = New System.Drawing.Size(1120, 701)
        Me.ControlBox = False
        Me.Controls.Add(Me.gcEstadoCobro)
        Me.Controls.Add(Me.gcPagosNuevo)
        Me.Controls.Add(Me.gcHistorial)
        Me.Controls.Add(Me.gcBuscar)
        Me.Controls.Add(Me.btnEliminar)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(Me.btnCancelar)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.MaximumSize = New System.Drawing.Size(1366, 701)
        Me.MinimumSize = New System.Drawing.Size(1120, 701)
        Me.Name = "frmCobros"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Tag = "Cobranzas"
        Me.Text = "Cobranzas"
        CType(Me.gcBuscar, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gcBuscar.ResumeLayout(False)
        Me.gcBuscar.PerformLayout()
        CType(Me.gcHistorial, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gcHistorial.ResumeLayout(False)
        CType(Me.dgvHistorial, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gcPagosNuevo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gcPagosNuevo.ResumeLayout(False)
        Me.gcPagosNuevo.PerformLayout()
        CType(Me.gcEstadoCobro, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gcEstadoCobro.ResumeLayout(False)
        Me.gcEstadoCobro.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnEliminar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnSalir As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnCancelar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents gcBuscar As DevExpress.XtraEditors.PanelControl
    Friend WithEvents btnGuardar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents cmbBuscar As System.Windows.Forms.ComboBox
    Friend WithEvents cmbIdPresupuesto As System.Windows.Forms.ComboBox
    Friend WithEvents lblPresupuesto As DevExpress.XtraEditors.LabelControl
    Friend WithEvents gcHistorial As DevExpress.XtraEditors.GroupControl
    Friend WithEvents dgvHistorial As System.Windows.Forms.DataGridView
    Friend WithEvents gcPagosNuevo As DevExpress.XtraEditors.GroupControl
    Friend WithEvents dtpFechaNac As System.Windows.Forms.DateTimePicker
    Friend WithEvents txtWeb As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents txtCUIT As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtNombre As System.Windows.Forms.TextBox
    Friend WithEvents txtEmpresa As System.Windows.Forms.TextBox
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents txtId As System.Windows.Forms.TextBox
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents gcEstadoCobro As DevExpress.XtraEditors.GroupControl
    Friend WithEvents cmbCiudad As System.Windows.Forms.ComboBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtDireccion As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtCP As System.Windows.Forms.TextBox
    Friend WithEvents SimpleButton1 As DevExpress.XtraEditors.SimpleButton
End Class
