﻿Public Class frmEntregas


    '------ EVENTOS DE FORMULARIO ------
    Private Sub frmEntregas_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        ConfigurarForm()
        ConfigurarDataGridView()
        ConfigurarTextBox()
    End Sub

    '------ CONFIGURACIONES ------
    Public Sub ConfigurarForm()
        Me.Dock = DockStyle.None
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.ShowIcon = False
        Me.MaximumSize = New Size(frmPrincipal.ClientSize.Width - 4, frmPrincipal.ClientSize.Height - 4)
        Me.MinimumSize = Me.MaximumSize
        Me.Dock = DockStyle.Fill
        Dim Separacion As Integer = (frmPrincipal.ClientSize.Width - 1090 + 150) / 2
        gcBuscar.Location = New Point(Separacion, 45)
        gcCliente.Location = New Point(Separacion, 88)
        gcFiltros.Location = New Point(Separacion + 913, 88)
        gcEntregas.Size = New Size(907, frmPrincipal.ClientSize.Height - (218 + 10))
        gcEntregas.Location = New Point(Separacion, 216)
        lblTitulo.Location = New Point((frmPrincipal.ClientSize.Width - 398 + 150) / 2, 4)
        Dim Separacionbtn As Integer = (frmPrincipal.ClientSize.Width - Separacion + 75)
        btnSalir.Location = New Point(Separacionbtn, frmPrincipal.ClientSize.Height - 37)
    End Sub
    Public Sub ConfigurarTextBox()
        'Configuro la cantidad de cararcteres maximos
        txtNombre.MaxLength = 200
        txtDireccion.MaxLength = 150
        'Configuro alineacion del texto
        txtClienteId.TextAlign = HorizontalAlignment.Center
        txtCiudad.TextAlign = HorizontalAlignment.Center
        txtTelefono1.TextAlign = HorizontalAlignment.Center
        txtMovil1.TextAlign = HorizontalAlignment.Center
        txtMovil2.TextAlign = HorizontalAlignment.Center
        'Configuro los que seran siempre solo lectura
        txtCiudad.ReadOnly = True
        txtClienteId.ReadOnly = True
        txtNombre.ReadOnly = True
        txtTelefono1.ReadOnly = True
        txtMovil1.ReadOnly = True
        txtMovil2.ReadOnly = True
    End Sub
    Private Sub ConfigurarDataGridView()
        dgvEntregas.Location = New Point(121, 27)
        dgvEntregas.Size = New Size(778, gcEntregas.ClientSize.Height - 33)
    End Sub

    '------ EVENTOS DE BOTONES ------
    Private Sub btnSalir_Click(sender As System.Object, e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

End Class