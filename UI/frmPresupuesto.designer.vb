﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPresupuesto
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.gcBuscar = New DevExpress.XtraEditors.PanelControl()
        Me.cmbIdPresupuesto = New System.Windows.Forms.ComboBox()
        Me.btnNuevo = New DevExpress.XtraEditors.SimpleButton()
        Me.lblPresupuesto = New DevExpress.XtraEditors.LabelControl()
        Me.btnEliminar = New DevExpress.XtraEditors.SimpleButton()
        Me.gcPresupuesto = New DevExpress.XtraEditors.GroupControl()
        Me.btnAprobar = New DevExpress.XtraEditors.SimpleButton()
        Me.cmbCliente = New System.Windows.Forms.ComboBox()
        Me.btnGuardar = New DevExpress.XtraEditors.SimpleButton()
        Me.txtIdCliente = New System.Windows.Forms.TextBox()
        Me.lblCliente = New DevExpress.XtraEditors.LabelControl()
        Me.lblIdCliente = New DevExpress.XtraEditors.LabelControl()
        Me.gcInfoAdicional = New DevExpress.XtraEditors.GroupControl()
        Me.chkColocacion = New System.Windows.Forms.CheckBox()
        Me.txtVigencia = New System.Windows.Forms.TextBox()
        Me.txtNotas = New System.Windows.Forms.TextBox()
        Me.dtpEntrega = New System.Windows.Forms.DateTimePicker()
        Me.lblFechaEntrega = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
        Me.lblLugarEntrega = New DevExpress.XtraEditors.LabelControl()
        Me.txtTotal = New System.Windows.Forms.TextBox()
        Me.lblTotal = New DevExpress.XtraEditors.LabelControl()
        Me.dgvItems = New System.Windows.Forms.DataGridView()
        Me.gcNuevoItem = New DevExpress.XtraEditors.GroupControl()
        Me.btnSillones = New DevExpress.XtraEditors.SimpleButton()
        Me.btnFundas = New DevExpress.XtraEditors.SimpleButton()
        Me.btnTela = New DevExpress.XtraEditors.SimpleButton()
        Me.btnCortina = New DevExpress.XtraEditors.SimpleButton()
        Me.btnAprobarItem = New DevExpress.XtraEditors.SimpleButton()
        Me.btnSalir = New DevExpress.XtraEditors.SimpleButton()
        Me.btnPresPdf = New DevExpress.XtraEditors.SimpleButton()
        Me.btnCancelar = New DevExpress.XtraEditors.SimpleButton()
        Me.GroupControl4 = New DevExpress.XtraEditors.GroupControl()
        Me.btnTallerPDF = New DevExpress.XtraEditors.SimpleButton()
        Me.gcItems = New DevExpress.XtraEditors.GroupControl()
        Me.lblTitulo = New System.Windows.Forms.Label()
        CType(Me.gcBuscar, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gcBuscar.SuspendLayout()
        CType(Me.gcPresupuesto, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gcPresupuesto.SuspendLayout()
        CType(Me.gcInfoAdicional, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gcInfoAdicional.SuspendLayout()
        CType(Me.dgvItems, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gcNuevoItem, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gcNuevoItem.SuspendLayout()
        CType(Me.GroupControl4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl4.SuspendLayout()
        CType(Me.gcItems, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gcItems.SuspendLayout()
        Me.SuspendLayout()
        '
        'gcBuscar
        '
        Me.gcBuscar.Controls.Add(Me.cmbIdPresupuesto)
        Me.gcBuscar.Controls.Add(Me.btnNuevo)
        Me.gcBuscar.Controls.Add(Me.lblPresupuesto)
        Me.gcBuscar.Controls.Add(Me.btnEliminar)
        Me.gcBuscar.Location = New System.Drawing.Point(12, 45)
        Me.gcBuscar.LookAndFeel.SkinName = "Visual Studio 2013 Dark"
        Me.gcBuscar.LookAndFeel.UseDefaultLookAndFeel = False
        Me.gcBuscar.Name = "gcBuscar"
        Me.gcBuscar.Size = New System.Drawing.Size(1090, 37)
        Me.gcBuscar.TabIndex = 0
        '
        'cmbIdPresupuesto
        '
        Me.cmbIdPresupuesto.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbIdPresupuesto.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbIdPresupuesto.FormattingEnabled = True
        Me.cmbIdPresupuesto.Location = New System.Drawing.Point(124, 5)
        Me.cmbIdPresupuesto.Name = "cmbIdPresupuesto"
        Me.cmbIdPresupuesto.Size = New System.Drawing.Size(121, 26)
        Me.cmbIdPresupuesto.TabIndex = 61
        '
        'btnNuevo
        '
        Me.btnNuevo.Appearance.Font = New System.Drawing.Font("Cambria", 11.0!, System.Drawing.FontStyle.Bold)
        Me.btnNuevo.Appearance.Options.UseFont = True
        Me.btnNuevo.Location = New System.Drawing.Point(1006, 5)
        Me.btnNuevo.LookAndFeel.SkinName = "Visual Studio 2013 Dark"
        Me.btnNuevo.LookAndFeel.UseDefaultLookAndFeel = False
        Me.btnNuevo.Name = "btnNuevo"
        Me.btnNuevo.Size = New System.Drawing.Size(75, 27)
        Me.btnNuevo.TabIndex = 13
        Me.btnNuevo.Text = "Nuevo"
        '
        'lblPresupuesto
        '
        Me.lblPresupuesto.Appearance.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold)
        Me.lblPresupuesto.Location = New System.Drawing.Point(9, 7)
        Me.lblPresupuesto.Name = "lblPresupuesto"
        Me.lblPresupuesto.Size = New System.Drawing.Size(109, 19)
        Me.lblPresupuesto.TabIndex = 3
        Me.lblPresupuesto.Text = "Presupuesto N°:"
        '
        'btnEliminar
        '
        Me.btnEliminar.Appearance.Font = New System.Drawing.Font("Cambria", 11.0!, System.Drawing.FontStyle.Bold)
        Me.btnEliminar.Appearance.Options.UseFont = True
        Me.btnEliminar.Location = New System.Drawing.Point(925, 5)
        Me.btnEliminar.LookAndFeel.SkinName = "Visual Studio 2013 Dark"
        Me.btnEliminar.LookAndFeel.UseDefaultLookAndFeel = False
        Me.btnEliminar.Name = "btnEliminar"
        Me.btnEliminar.Size = New System.Drawing.Size(75, 27)
        Me.btnEliminar.TabIndex = 18
        Me.btnEliminar.Text = "Eliminar"
        '
        'gcPresupuesto
        '
        Me.gcPresupuesto.Controls.Add(Me.btnAprobar)
        Me.gcPresupuesto.Controls.Add(Me.cmbCliente)
        Me.gcPresupuesto.Controls.Add(Me.btnGuardar)
        Me.gcPresupuesto.Controls.Add(Me.txtIdCliente)
        Me.gcPresupuesto.Controls.Add(Me.lblCliente)
        Me.gcPresupuesto.Controls.Add(Me.lblIdCliente)
        Me.gcPresupuesto.Location = New System.Drawing.Point(12, 88)
        Me.gcPresupuesto.LookAndFeel.SkinName = "Visual Studio 2013 Dark"
        Me.gcPresupuesto.LookAndFeel.UseDefaultLookAndFeel = False
        Me.gcPresupuesto.Name = "gcPresupuesto"
        Me.gcPresupuesto.Size = New System.Drawing.Size(1090, 56)
        Me.gcPresupuesto.TabIndex = 1
        Me.gcPresupuesto.Text = "Presupuesto"
        '
        'btnAprobar
        '
        Me.btnAprobar.Appearance.Font = New System.Drawing.Font("Cambria", 11.0!, System.Drawing.FontStyle.Bold)
        Me.btnAprobar.Appearance.Options.UseFont = True
        Me.btnAprobar.Location = New System.Drawing.Point(924, 24)
        Me.btnAprobar.LookAndFeel.SkinName = "Visual Studio 2013 Dark"
        Me.btnAprobar.LookAndFeel.UseDefaultLookAndFeel = False
        Me.btnAprobar.Name = "btnAprobar"
        Me.btnAprobar.Size = New System.Drawing.Size(75, 27)
        Me.btnAprobar.TabIndex = 19
        Me.btnAprobar.Text = "Aprobar"
        '
        'cmbCliente
        '
        Me.cmbCliente.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cmbCliente.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cmbCliente.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbCliente.DropDownWidth = 835
        Me.cmbCliente.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbCliente.IntegralHeight = False
        Me.cmbCliente.Location = New System.Drawing.Point(276, 24)
        Me.cmbCliente.Name = "cmbCliente"
        Me.cmbCliente.Size = New System.Drawing.Size(642, 27)
        Me.cmbCliente.TabIndex = 19
        '
        'btnGuardar
        '
        Me.btnGuardar.Appearance.Font = New System.Drawing.Font("Cambria", 11.0!, System.Drawing.FontStyle.Bold)
        Me.btnGuardar.Appearance.Options.UseFont = True
        Me.btnGuardar.Location = New System.Drawing.Point(1006, 24)
        Me.btnGuardar.LookAndFeel.SkinName = "Visual Studio 2013 Dark"
        Me.btnGuardar.LookAndFeel.UseDefaultLookAndFeel = False
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(75, 27)
        Me.btnGuardar.TabIndex = 16
        Me.btnGuardar.Text = "Guardar"
        '
        'txtIdCliente
        '
        Me.txtIdCliente.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtIdCliente.Location = New System.Drawing.Point(124, 24)
        Me.txtIdCliente.Name = "txtIdCliente"
        Me.txtIdCliente.Size = New System.Drawing.Size(78, 27)
        Me.txtIdCliente.TabIndex = 10
        Me.txtIdCliente.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'lblCliente
        '
        Me.lblCliente.Appearance.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold)
        Me.lblCliente.Location = New System.Drawing.Point(210, 27)
        Me.lblCliente.Name = "lblCliente"
        Me.lblCliente.Size = New System.Drawing.Size(60, 19)
        Me.lblCliente.TabIndex = 8
        Me.lblCliente.Text = "Nombre:"
        '
        'lblIdCliente
        '
        Me.lblIdCliente.Appearance.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold)
        Me.lblIdCliente.Location = New System.Drawing.Point(29, 27)
        Me.lblIdCliente.Name = "lblIdCliente"
        Me.lblIdCliente.Size = New System.Drawing.Size(89, 19)
        Me.lblIdCliente.TabIndex = 7
        Me.lblIdCliente.Text = "Id de Cliente:"
        '
        'gcInfoAdicional
        '
        Me.gcInfoAdicional.Controls.Add(Me.chkColocacion)
        Me.gcInfoAdicional.Controls.Add(Me.txtVigencia)
        Me.gcInfoAdicional.Controls.Add(Me.txtNotas)
        Me.gcInfoAdicional.Controls.Add(Me.dtpEntrega)
        Me.gcInfoAdicional.Controls.Add(Me.lblFechaEntrega)
        Me.gcInfoAdicional.Controls.Add(Me.LabelControl9)
        Me.gcInfoAdicional.Controls.Add(Me.lblLugarEntrega)
        Me.gcInfoAdicional.Location = New System.Drawing.Point(12, 150)
        Me.gcInfoAdicional.LookAndFeel.SkinName = "Visual Studio 2013 Dark"
        Me.gcInfoAdicional.LookAndFeel.UseDefaultLookAndFeel = False
        Me.gcInfoAdicional.Name = "gcInfoAdicional"
        Me.gcInfoAdicional.Size = New System.Drawing.Size(1090, 119)
        Me.gcInfoAdicional.TabIndex = 10
        Me.gcInfoAdicional.Text = "Información Adicional"
        '
        'chkColocacion
        '
        Me.chkColocacion.AutoSize = True
        Me.chkColocacion.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkColocacion.Location = New System.Drawing.Point(949, 86)
        Me.chkColocacion.Name = "chkColocacion"
        Me.chkColocacion.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.chkColocacion.Size = New System.Drawing.Size(132, 23)
        Me.chkColocacion.TabIndex = 64
        Me.chkColocacion.Text = "Con Colocación"
        Me.chkColocacion.UseVisualStyleBackColor = True
        '
        'txtVigencia
        '
        Me.txtVigencia.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtVigencia.Location = New System.Drawing.Point(964, 53)
        Me.txtVigencia.Name = "txtVigencia"
        Me.txtVigencia.Size = New System.Drawing.Size(117, 27)
        Me.txtVigencia.TabIndex = 63
        Me.txtVigencia.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtNotas
        '
        Me.txtNotas.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNotas.Location = New System.Drawing.Point(124, 24)
        Me.txtNotas.Multiline = True
        Me.txtNotas.Name = "txtNotas"
        Me.txtNotas.Size = New System.Drawing.Size(767, 83)
        Me.txtNotas.TabIndex = 62
        '
        'dtpEntrega
        '
        Me.dtpEntrega.CustomFormat = "dd/MM/yyyy"
        Me.dtpEntrega.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpEntrega.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpEntrega.Location = New System.Drawing.Point(964, 24)
        Me.dtpEntrega.MaxDate = New Date(2050, 12, 31, 0, 0, 0, 0)
        Me.dtpEntrega.MinDate = New Date(1900, 1, 1, 0, 0, 0, 0)
        Me.dtpEntrega.Name = "dtpEntrega"
        Me.dtpEntrega.Size = New System.Drawing.Size(117, 26)
        Me.dtpEntrega.TabIndex = 61
        '
        'lblFechaEntrega
        '
        Me.lblFechaEntrega.Appearance.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold)
        Me.lblFechaEntrega.Location = New System.Drawing.Point(901, 26)
        Me.lblFechaEntrega.Name = "lblFechaEntrega"
        Me.lblFechaEntrega.Size = New System.Drawing.Size(57, 19)
        Me.lblFechaEntrega.TabIndex = 19
        Me.lblFechaEntrega.Text = "Entrega:"
        '
        'LabelControl9
        '
        Me.LabelControl9.Appearance.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold)
        Me.LabelControl9.Location = New System.Drawing.Point(897, 56)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(61, 19)
        Me.LabelControl9.TabIndex = 17
        Me.LabelControl9.Text = "Vigencia:"
        '
        'lblLugarEntrega
        '
        Me.lblLugarEntrega.Appearance.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold)
        Me.lblLugarEntrega.Location = New System.Drawing.Point(15, 27)
        Me.lblLugarEntrega.Name = "lblLugarEntrega"
        Me.lblLugarEntrega.Size = New System.Drawing.Size(103, 19)
        Me.lblLugarEntrega.TabIndex = 11
        Me.lblLugarEntrega.Text = "Observaciones:"
        '
        'txtTotal
        '
        Me.txtTotal.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTotal.Location = New System.Drawing.Point(1006, 316)
        Me.txtTotal.Name = "txtTotal"
        Me.txtTotal.Size = New System.Drawing.Size(76, 27)
        Me.txtTotal.TabIndex = 69
        Me.txtTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'lblTotal
        '
        Me.lblTotal.Appearance.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold)
        Me.lblTotal.Location = New System.Drawing.Point(961, 319)
        Me.lblTotal.Name = "lblTotal"
        Me.lblTotal.Size = New System.Drawing.Size(39, 19)
        Me.lblTotal.TabIndex = 68
        Me.lblTotal.Text = "Total:"
        '
        'dgvItems
        '
        Me.dgvItems.AllowUserToAddRows = False
        Me.dgvItems.AllowUserToDeleteRows = False
        Me.dgvItems.AllowUserToOrderColumns = True
        Me.dgvItems.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvItems.Location = New System.Drawing.Point(124, 27)
        Me.dgvItems.Margin = New System.Windows.Forms.Padding(6)
        Me.dgvItems.Name = "dgvItems"
        Me.dgvItems.ReadOnly = True
        Me.dgvItems.RowTemplate.DefaultCellStyle.Font = New System.Drawing.Font("SansSerif", 8.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dgvItems.RowTemplate.DefaultCellStyle.ForeColor = System.Drawing.Color.Black
        Me.dgvItems.Size = New System.Drawing.Size(958, 280)
        Me.dgvItems.TabIndex = 0
        '
        'gcNuevoItem
        '
        Me.gcNuevoItem.Controls.Add(Me.btnSillones)
        Me.gcNuevoItem.Controls.Add(Me.btnFundas)
        Me.gcNuevoItem.Controls.Add(Me.btnTela)
        Me.gcNuevoItem.Controls.Add(Me.btnCortina)
        Me.gcNuevoItem.Location = New System.Drawing.Point(12, 629)
        Me.gcNuevoItem.LookAndFeel.SkinName = "Visual Studio 2013 Dark"
        Me.gcNuevoItem.LookAndFeel.UseDefaultLookAndFeel = False
        Me.gcNuevoItem.Name = "gcNuevoItem"
        Me.gcNuevoItem.Size = New System.Drawing.Size(1009, 60)
        Me.gcNuevoItem.TabIndex = 12
        Me.gcNuevoItem.Text = "Nuevo Item"
        '
        'btnSillones
        '
        Me.btnSillones.Appearance.Font = New System.Drawing.Font("Cambria", 12.0!, System.Drawing.FontStyle.Bold)
        Me.btnSillones.Appearance.Options.UseFont = True
        Me.btnSillones.Location = New System.Drawing.Point(757, 28)
        Me.btnSillones.LookAndFeel.SkinName = "Visual Studio 2013 Dark"
        Me.btnSillones.LookAndFeel.UseDefaultLookAndFeel = False
        Me.btnSillones.Name = "btnSillones"
        Me.btnSillones.Size = New System.Drawing.Size(244, 27)
        Me.btnSillones.TabIndex = 17
        Me.btnSillones.Text = "Sillones"
        '
        'btnFundas
        '
        Me.btnFundas.Appearance.Font = New System.Drawing.Font("Cambria", 12.0!, System.Drawing.FontStyle.Bold)
        Me.btnFundas.Appearance.Options.UseFont = True
        Me.btnFundas.Location = New System.Drawing.Point(506, 28)
        Me.btnFundas.LookAndFeel.SkinName = "Visual Studio 2013 Dark"
        Me.btnFundas.LookAndFeel.UseDefaultLookAndFeel = False
        Me.btnFundas.Name = "btnFundas"
        Me.btnFundas.Size = New System.Drawing.Size(245, 27)
        Me.btnFundas.TabIndex = 16
        Me.btnFundas.Text = "Fundas y almohadones"
        '
        'btnTela
        '
        Me.btnTela.Appearance.Font = New System.Drawing.Font("Cambria", 12.0!, System.Drawing.FontStyle.Bold)
        Me.btnTela.Appearance.Options.UseFont = True
        Me.btnTela.Location = New System.Drawing.Point(255, 28)
        Me.btnTela.LookAndFeel.SkinName = "Visual Studio 2013 Dark"
        Me.btnTela.LookAndFeel.UseDefaultLookAndFeel = False
        Me.btnTela.Name = "btnTela"
        Me.btnTela.Size = New System.Drawing.Size(245, 27)
        Me.btnTela.TabIndex = 15
        Me.btnTela.Text = "Telas"
        '
        'btnCortina
        '
        Me.btnCortina.Appearance.Font = New System.Drawing.Font("Cambria", 12.0!, System.Drawing.FontStyle.Bold)
        Me.btnCortina.Appearance.Options.UseFont = True
        Me.btnCortina.Location = New System.Drawing.Point(5, 28)
        Me.btnCortina.LookAndFeel.SkinName = "Visual Studio 2013 Dark"
        Me.btnCortina.LookAndFeel.UseDefaultLookAndFeel = False
        Me.btnCortina.Name = "btnCortina"
        Me.btnCortina.Size = New System.Drawing.Size(244, 27)
        Me.btnCortina.TabIndex = 13
        Me.btnCortina.Text = "Cortinas y Sistemas"
        '
        'btnAprobarItem
        '
        Me.btnAprobarItem.Appearance.Font = New System.Drawing.Font("Cambria", 11.0!, System.Drawing.FontStyle.Bold)
        Me.btnAprobarItem.Appearance.Options.UseFont = True
        Me.btnAprobarItem.Location = New System.Drawing.Point(124, 316)
        Me.btnAprobarItem.LookAndFeel.SkinName = "Visual Studio 2013 Dark"
        Me.btnAprobarItem.LookAndFeel.UseDefaultLookAndFeel = False
        Me.btnAprobarItem.Name = "btnAprobarItem"
        Me.btnAprobarItem.Size = New System.Drawing.Size(150, 27)
        Me.btnAprobarItem.TabIndex = 14
        Me.btnAprobarItem.Text = "Aprobar Items"
        '
        'btnSalir
        '
        Me.btnSalir.Appearance.Font = New System.Drawing.Font("Cambria", 11.0!, System.Drawing.FontStyle.Bold)
        Me.btnSalir.Appearance.Options.UseFont = True
        Me.btnSalir.Location = New System.Drawing.Point(1027, 662)
        Me.btnSalir.LookAndFeel.SkinName = "Visual Studio 2013 Dark"
        Me.btnSalir.LookAndFeel.UseDefaultLookAndFeel = False
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(75, 27)
        Me.btnSalir.TabIndex = 15
        Me.btnSalir.Text = "Cerrar"
        '
        'btnPresPdf
        '
        Me.btnPresPdf.Appearance.Font = New System.Drawing.Font("Cambria", 11.0!, System.Drawing.FontStyle.Bold)
        Me.btnPresPdf.Appearance.Options.UseFont = True
        Me.btnPresPdf.Location = New System.Drawing.Point(5, 23)
        Me.btnPresPdf.LookAndFeel.SkinName = "Visual Studio 2013 Dark"
        Me.btnPresPdf.LookAndFeel.UseDefaultLookAndFeel = False
        Me.btnPresPdf.Name = "btnPresPdf"
        Me.btnPresPdf.Size = New System.Drawing.Size(75, 27)
        Me.btnPresPdf.TabIndex = 17
        Me.btnPresPdf.Text = "Cliente"
        '
        'btnCancelar
        '
        Me.btnCancelar.Appearance.Font = New System.Drawing.Font("Cambria", 11.0!, System.Drawing.FontStyle.Bold)
        Me.btnCancelar.Appearance.Options.UseFont = True
        Me.btnCancelar.Location = New System.Drawing.Point(1027, 629)
        Me.btnCancelar.LookAndFeel.SkinName = "Visual Studio 2013 Dark"
        Me.btnCancelar.LookAndFeel.UseDefaultLookAndFeel = False
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 27)
        Me.btnCancelar.TabIndex = 19
        Me.btnCancelar.Text = "Cancelar"
        '
        'GroupControl4
        '
        Me.GroupControl4.Controls.Add(Me.btnTallerPDF)
        Me.GroupControl4.Controls.Add(Me.btnPresPdf)
        Me.GroupControl4.Location = New System.Drawing.Point(15, 27)
        Me.GroupControl4.LookAndFeel.SkinName = "Visual Studio 2013 Dark"
        Me.GroupControl4.LookAndFeel.UseDefaultLookAndFeel = False
        Me.GroupControl4.Name = "GroupControl4"
        Me.GroupControl4.Size = New System.Drawing.Size(86, 89)
        Me.GroupControl4.TabIndex = 23
        Me.GroupControl4.Text = "Exportar"
        '
        'btnTallerPDF
        '
        Me.btnTallerPDF.Appearance.Font = New System.Drawing.Font("Cambria", 11.0!, System.Drawing.FontStyle.Bold)
        Me.btnTallerPDF.Appearance.Options.UseFont = True
        Me.btnTallerPDF.Location = New System.Drawing.Point(5, 58)
        Me.btnTallerPDF.LookAndFeel.SkinName = "Visual Studio 2013 Dark"
        Me.btnTallerPDF.LookAndFeel.UseDefaultLookAndFeel = False
        Me.btnTallerPDF.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.btnTallerPDF.Name = "btnTallerPDF"
        Me.btnTallerPDF.Size = New System.Drawing.Size(75, 27)
        Me.btnTallerPDF.TabIndex = 19
        Me.btnTallerPDF.Text = "Taller"
        '
        'gcItems
        '
        Me.gcItems.Controls.Add(Me.btnAprobarItem)
        Me.gcItems.Controls.Add(Me.GroupControl4)
        Me.gcItems.Controls.Add(Me.txtTotal)
        Me.gcItems.Controls.Add(Me.lblTotal)
        Me.gcItems.Controls.Add(Me.dgvItems)
        Me.gcItems.Location = New System.Drawing.Point(12, 275)
        Me.gcItems.LookAndFeel.SkinName = "Visual Studio 2013 Dark"
        Me.gcItems.LookAndFeel.UseDefaultLookAndFeel = False
        Me.gcItems.Name = "gcItems"
        Me.gcItems.Size = New System.Drawing.Size(1090, 348)
        Me.gcItems.TabIndex = 25
        Me.gcItems.Text = "Items"
        '
        'lblTitulo
        '
        Me.lblTitulo.AccessibleRole = System.Windows.Forms.AccessibleRole.TitleBar
        Me.lblTitulo.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.lblTitulo.AutoSize = True
        Me.lblTitulo.BackColor = System.Drawing.Color.Transparent
        Me.lblTitulo.Font = New System.Drawing.Font("Microsoft Sans Serif", 24.0!)
        Me.lblTitulo.ForeColor = System.Drawing.Color.White
        Me.lblTitulo.Location = New System.Drawing.Point(337, 4)
        Me.lblTitulo.Name = "lblTitulo"
        Me.lblTitulo.Size = New System.Drawing.Size(465, 37)
        Me.lblTitulo.TabIndex = 26
        Me.lblTitulo.Text = "Administrador de Presupuestos"
        '
        'frmPresupuesto
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoSize = True
        Me.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.BackColor = System.Drawing.Color.Black
        Me.ClientSize = New System.Drawing.Size(1120, 701)
        Me.Controls.Add(Me.lblTitulo)
        Me.Controls.Add(Me.gcItems)
        Me.Controls.Add(Me.btnCancelar)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(Me.gcNuevoItem)
        Me.Controls.Add(Me.gcInfoAdicional)
        Me.Controls.Add(Me.gcPresupuesto)
        Me.Controls.Add(Me.gcBuscar)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.MaximumSize = New System.Drawing.Size(1366, 701)
        Me.MinimizeBox = False
        Me.MinimumSize = New System.Drawing.Size(1120, 701)
        Me.Name = "frmPresupuesto"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Tag = "Presupuestos"
        Me.Text = "Presupuestos"
        CType(Me.gcBuscar, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gcBuscar.ResumeLayout(False)
        Me.gcBuscar.PerformLayout()
        CType(Me.gcPresupuesto, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gcPresupuesto.ResumeLayout(False)
        Me.gcPresupuesto.PerformLayout()
        CType(Me.gcInfoAdicional, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gcInfoAdicional.ResumeLayout(False)
        Me.gcInfoAdicional.PerformLayout()
        CType(Me.dgvItems, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gcNuevoItem, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gcNuevoItem.ResumeLayout(False)
        CType(Me.GroupControl4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl4.ResumeLayout(False)
        CType(Me.gcItems, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gcItems.ResumeLayout(False)
        Me.gcItems.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents gcBuscar As DevExpress.XtraEditors.PanelControl
    Friend WithEvents gcPresupuesto As DevExpress.XtraEditors.GroupControl
    Friend WithEvents lblCliente As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblIdCliente As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblPresupuesto As DevExpress.XtraEditors.LabelControl
    Friend WithEvents gcInfoAdicional As DevExpress.XtraEditors.GroupControl
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblLugarEntrega As DevExpress.XtraEditors.LabelControl
    Friend WithEvents dgvItems As System.Windows.Forms.DataGridView
    Friend WithEvents gcNuevoItem As DevExpress.XtraEditors.GroupControl
    Friend WithEvents btnSillones As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnFundas As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnTela As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnCortina As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents lblFechaEntrega As DevExpress.XtraEditors.LabelControl
    Friend WithEvents btnNuevo As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnAprobarItem As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnSalir As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents txtIdCliente As System.Windows.Forms.TextBox
    Friend WithEvents btnGuardar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmbCliente As System.Windows.Forms.ComboBox
    Friend WithEvents btnPresPdf As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnEliminar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents dtpEntrega As System.Windows.Forms.DateTimePicker
    Friend WithEvents txtVigencia As System.Windows.Forms.TextBox
    Friend WithEvents txtNotas As System.Windows.Forms.TextBox
    Friend WithEvents cmbIdPresupuesto As System.Windows.Forms.ComboBox
    Friend WithEvents btnCancelar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents chkColocacion As System.Windows.Forms.CheckBox
    Friend WithEvents txtTotal As System.Windows.Forms.TextBox
    Friend WithEvents lblTotal As DevExpress.XtraEditors.LabelControl
    Friend WithEvents btnAprobar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents GroupControl4 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents btnTallerPDF As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents gcItems As DevExpress.XtraEditors.GroupControl
    Friend WithEvents lblTitulo As System.Windows.Forms.Label
End Class
