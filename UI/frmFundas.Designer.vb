﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmFundas
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnGuardar = New DevExpress.XtraEditors.SimpleButton()
        Me.btnSalir = New DevExpress.XtraEditors.SimpleButton()
        Me.gcFundaAlmohadon = New DevExpress.XtraEditors.GroupControl()
        Me.cmbFATipo = New System.Windows.Forms.ComboBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.cmbFATipoE = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cmbFATipoSub = New System.Windows.Forms.TextBox()
        Me.cmbFATipoL = New System.Windows.Forms.TextBox()
        Me.Label29 = New System.Windows.Forms.Label()
        Me.cmbFATipoA = New System.Windows.Forms.TextBox()
        Me.cmbFAGeneroA = New System.Windows.Forms.TextBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.txtFAObs = New System.Windows.Forms.TextBox()
        Me.Label51 = New System.Windows.Forms.Label()
        Me.cmbFATipoP = New System.Windows.Forms.TextBox()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.cmbFAGeneroP = New System.Windows.Forms.TextBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.cmbFAGeneroL = New System.Windows.Forms.TextBox()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.cmbFAGenero = New System.Windows.Forms.ComboBox()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.gcRellenoAlmohadon = New DevExpress.XtraEditors.GroupControl()
        Me.txtRASub = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.cmbRAC3E = New System.Windows.Forms.TextBox()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.cmbRAC2E = New System.Windows.Forms.TextBox()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.cmbRAC1E = New System.Windows.Forms.TextBox()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.cmbRAC3P = New System.Windows.Forms.TextBox()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.cmbRAC2P = New System.Windows.Forms.TextBox()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.cmbRAC3 = New System.Windows.Forms.ComboBox()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.cmbRAC2 = New System.Windows.Forms.ComboBox()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.cmbRAC1 = New System.Windows.Forms.ComboBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.txtRATipoA = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtRAObs = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.cmbRAC1P = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.GroupControl5 = New DevExpress.XtraEditors.GroupControl()
        Me.txtRAL = New System.Windows.Forms.TextBox()
        Me.txtRAA = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txtRATipoP = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.txtRATipoL = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.cmbRATipo = New System.Windows.Forms.ComboBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.gcItem = New DevExpress.XtraEditors.GroupControl()
        Me.chkRellenoAl = New System.Windows.Forms.CheckBox()
        Me.chkFundaAl = New System.Windows.Forms.CheckBox()
        Me.chkFundaSillon = New System.Windows.Forms.CheckBox()
        Me.Label56 = New System.Windows.Forms.Label()
        Me.SpinEdit1 = New DevExpress.XtraEditors.SpinEdit()
        Me.txtItemDesc = New System.Windows.Forms.TextBox()
        Me.Label55 = New System.Windows.Forms.Label()
        Me.cmbOpcion = New System.Windows.Forms.ComboBox()
        Me.Label54 = New System.Windows.Forms.Label()
        Me.txtObservaciones = New System.Windows.Forms.TextBox()
        Me.Label48 = New System.Windows.Forms.Label()
        Me.txtUbicacion = New System.Windows.Forms.TextBox()
        Me.Label47 = New System.Windows.Forms.Label()
        Me.gcFundaSillon = New DevExpress.XtraEditors.GroupControl()
        Me.cmbFSTipo = New System.Windows.Forms.ComboBox()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.cmbFSTipoE = New System.Windows.Forms.TextBox()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.cmbFSTipoSub = New System.Windows.Forms.TextBox()
        Me.cmbFSTipoL = New System.Windows.Forms.TextBox()
        Me.Label30 = New System.Windows.Forms.Label()
        Me.cmbFSTipoA = New System.Windows.Forms.TextBox()
        Me.cmbFSGeneroA = New System.Windows.Forms.TextBox()
        Me.Label31 = New System.Windows.Forms.Label()
        Me.Label32 = New System.Windows.Forms.Label()
        Me.Label33 = New System.Windows.Forms.Label()
        Me.txtFSObs = New System.Windows.Forms.TextBox()
        Me.Label34 = New System.Windows.Forms.Label()
        Me.cmbFSTipoP = New System.Windows.Forms.TextBox()
        Me.Label35 = New System.Windows.Forms.Label()
        Me.cmbFSGeneroP = New System.Windows.Forms.TextBox()
        Me.Label36 = New System.Windows.Forms.Label()
        Me.cmbFSGeneroL = New System.Windows.Forms.TextBox()
        Me.Label37 = New System.Windows.Forms.Label()
        Me.cmbFSGenero = New System.Windows.Forms.ComboBox()
        Me.Label38 = New System.Windows.Forms.Label()
        Me.gcTotal = New DevExpress.XtraEditors.GroupControl()
        Me.txtFinal = New System.Windows.Forms.TextBox()
        Me.Label50 = New System.Windows.Forms.Label()
        Me.txtIVA = New System.Windows.Forms.TextBox()
        Me.Label49 = New System.Windows.Forms.Label()
        Me.txtTotalP = New System.Windows.Forms.TextBox()
        Me.Label46 = New System.Windows.Forms.Label()
        CType(Me.gcFundaAlmohadon, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gcFundaAlmohadon.SuspendLayout()
        CType(Me.gcRellenoAlmohadon, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gcRellenoAlmohadon.SuspendLayout()
        CType(Me.GroupControl5, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl5.SuspendLayout()
        CType(Me.gcItem, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gcItem.SuspendLayout()
        CType(Me.SpinEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gcFundaSillon, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gcFundaSillon.SuspendLayout()
        CType(Me.gcTotal, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gcTotal.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnGuardar
        '
        Me.btnGuardar.Appearance.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold)
        Me.btnGuardar.Appearance.Options.UseFont = True
        Me.btnGuardar.Cursor = System.Windows.Forms.Cursors.Default
        Me.btnGuardar.Location = New System.Drawing.Point(946, 662)
        Me.btnGuardar.LookAndFeel.SkinName = "Visual Studio 2013 Dark"
        Me.btnGuardar.LookAndFeel.UseDefaultLookAndFeel = False
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(75, 27)
        Me.btnGuardar.TabIndex = 66
        Me.btnGuardar.Text = "Guardar"
        '
        'btnSalir
        '
        Me.btnSalir.Appearance.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold)
        Me.btnSalir.Appearance.Options.UseFont = True
        Me.btnSalir.Location = New System.Drawing.Point(1027, 662)
        Me.btnSalir.LookAndFeel.SkinName = "Visual Studio 2013 Dark"
        Me.btnSalir.LookAndFeel.UseDefaultLookAndFeel = False
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(75, 27)
        Me.btnSalir.TabIndex = 65
        Me.btnSalir.Text = "Cerrar"
        '
        'gcFundaAlmohadon
        '
        Me.gcFundaAlmohadon.Controls.Add(Me.cmbFATipo)
        Me.gcFundaAlmohadon.Controls.Add(Me.Label7)
        Me.gcFundaAlmohadon.Controls.Add(Me.cmbFATipoE)
        Me.gcFundaAlmohadon.Controls.Add(Me.Label1)
        Me.gcFundaAlmohadon.Controls.Add(Me.cmbFATipoSub)
        Me.gcFundaAlmohadon.Controls.Add(Me.cmbFATipoL)
        Me.gcFundaAlmohadon.Controls.Add(Me.Label29)
        Me.gcFundaAlmohadon.Controls.Add(Me.cmbFATipoA)
        Me.gcFundaAlmohadon.Controls.Add(Me.cmbFAGeneroA)
        Me.gcFundaAlmohadon.Controls.Add(Me.Label14)
        Me.gcFundaAlmohadon.Controls.Add(Me.Label2)
        Me.gcFundaAlmohadon.Controls.Add(Me.Label13)
        Me.gcFundaAlmohadon.Controls.Add(Me.txtFAObs)
        Me.gcFundaAlmohadon.Controls.Add(Me.Label51)
        Me.gcFundaAlmohadon.Controls.Add(Me.cmbFATipoP)
        Me.gcFundaAlmohadon.Controls.Add(Me.Label26)
        Me.gcFundaAlmohadon.Controls.Add(Me.cmbFAGeneroP)
        Me.gcFundaAlmohadon.Controls.Add(Me.Label16)
        Me.gcFundaAlmohadon.Controls.Add(Me.cmbFAGeneroL)
        Me.gcFundaAlmohadon.Controls.Add(Me.Label17)
        Me.gcFundaAlmohadon.Controls.Add(Me.cmbFAGenero)
        Me.gcFundaAlmohadon.Controls.Add(Me.Label23)
        Me.gcFundaAlmohadon.Location = New System.Drawing.Point(12, 238)
        Me.gcFundaAlmohadon.LookAndFeel.SkinName = "Visual Studio 2013 Dark"
        Me.gcFundaAlmohadon.LookAndFeel.UseDefaultLookAndFeel = False
        Me.gcFundaAlmohadon.Name = "gcFundaAlmohadon"
        Me.gcFundaAlmohadon.Size = New System.Drawing.Size(1090, 106)
        Me.gcFundaAlmohadon.TabIndex = 68
        Me.gcFundaAlmohadon.Text = "Funda de Almohadon"
        '
        'cmbFATipo
        '
        Me.cmbFATipo.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbFATipo.FormattingEnabled = True
        Me.cmbFATipo.Location = New System.Drawing.Point(99, 52)
        Me.cmbFATipo.Name = "cmbFATipo"
        Me.cmbFATipo.Size = New System.Drawing.Size(491, 22)
        Me.cmbFATipo.TabIndex = 42
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(59, 56)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(34, 15)
        Me.Label7.TabIndex = 41
        Me.Label7.Text = "Tipo:"
        '
        'cmbFATipoE
        '
        Me.cmbFATipoE.Font = New System.Drawing.Font("SansSerif", 9.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbFATipoE.Location = New System.Drawing.Point(911, 51)
        Me.cmbFATipoE.MaxLength = 5
        Me.cmbFATipoE.Name = "cmbFATipoE"
        Me.cmbFATipoE.Size = New System.Drawing.Size(35, 22)
        Me.cmbFATipoE.TabIndex = 5
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(826, 56)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(79, 15)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "Espesor (m):"
        '
        'cmbFATipoSub
        '
        Me.cmbFATipoSub.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbFATipoSub.Location = New System.Drawing.Point(1005, 80)
        Me.cmbFATipoSub.Name = "cmbFATipoSub"
        Me.cmbFATipoSub.Size = New System.Drawing.Size(80, 21)
        Me.cmbFATipoSub.TabIndex = 28
        '
        'cmbFATipoL
        '
        Me.cmbFATipoL.Font = New System.Drawing.Font("SansSerif", 9.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbFATipoL.Location = New System.Drawing.Point(785, 51)
        Me.cmbFATipoL.MaxLength = 5
        Me.cmbFATipoL.Name = "cmbFATipoL"
        Me.cmbFATipoL.Size = New System.Drawing.Size(35, 22)
        Me.cmbFATipoL.TabIndex = 3
        '
        'Label29
        '
        Me.Label29.AutoSize = True
        Me.Label29.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label29.Location = New System.Drawing.Point(940, 83)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(59, 15)
        Me.Label29.TabIndex = 27
        Me.Label29.Text = "SubTotal:"
        '
        'cmbFATipoA
        '
        Me.cmbFATipoA.Font = New System.Drawing.Font("SansSerif", 9.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbFATipoA.Location = New System.Drawing.Point(670, 52)
        Me.cmbFATipoA.MaxLength = 5
        Me.cmbFATipoA.Name = "cmbFATipoA"
        Me.cmbFATipoA.Size = New System.Drawing.Size(35, 22)
        Me.cmbFATipoA.TabIndex = 2
        '
        'cmbFAGeneroA
        '
        Me.cmbFAGeneroA.Font = New System.Drawing.Font("SansSerif", 9.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbFAGeneroA.Location = New System.Drawing.Point(785, 23)
        Me.cmbFAGeneroA.MaxLength = 5
        Me.cmbFAGeneroA.Name = "cmbFAGeneroA"
        Me.cmbFAGeneroA.Size = New System.Drawing.Size(35, 22)
        Me.cmbFAGeneroA.TabIndex = 39
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.Location = New System.Drawing.Point(714, 56)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(65, 15)
        Me.Label14.TabIndex = 1
        Me.Label14.Text = "Largo (m):"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(711, 28)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(68, 15)
        Me.Label2.TabIndex = 40
        Me.Label2.Text = "Ancho (m):"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.Location = New System.Drawing.Point(596, 55)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(68, 15)
        Me.Label13.TabIndex = 0
        Me.Label13.Text = "Ancho (m):"
        '
        'txtFAObs
        '
        Me.txtFAObs.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFAObs.Location = New System.Drawing.Point(99, 80)
        Me.txtFAObs.Name = "txtFAObs"
        Me.txtFAObs.Size = New System.Drawing.Size(835, 21)
        Me.txtFAObs.TabIndex = 37
        '
        'Label51
        '
        Me.Label51.AutoSize = True
        Me.Label51.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label51.Location = New System.Drawing.Point(11, 83)
        Me.Label51.Name = "Label51"
        Me.Label51.Size = New System.Drawing.Size(82, 15)
        Me.Label51.TabIndex = 38
        Me.Label51.Text = "Observación:"
        '
        'cmbFATipoP
        '
        Me.cmbFATipoP.Font = New System.Drawing.Font("SansSerif", 9.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbFATipoP.Location = New System.Drawing.Point(1005, 52)
        Me.cmbFATipoP.Name = "cmbFATipoP"
        Me.cmbFATipoP.Size = New System.Drawing.Size(80, 22)
        Me.cmbFATipoP.TabIndex = 32
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label26.Location = New System.Drawing.Point(952, 56)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(47, 15)
        Me.Label26.TabIndex = 31
        Me.Label26.Text = "Precio:"
        '
        'cmbFAGeneroP
        '
        Me.cmbFAGeneroP.Font = New System.Drawing.Font("SansSerif", 9.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbFAGeneroP.Location = New System.Drawing.Point(1005, 24)
        Me.cmbFAGeneroP.Name = "cmbFAGeneroP"
        Me.cmbFAGeneroP.Size = New System.Drawing.Size(80, 22)
        Me.cmbFAGeneroP.TabIndex = 1
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.Location = New System.Drawing.Point(952, 28)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(47, 15)
        Me.Label16.TabIndex = 1
        Me.Label16.Text = "Precio:"
        '
        'cmbFAGeneroL
        '
        Me.cmbFAGeneroL.Font = New System.Drawing.Font("SansSerif", 9.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbFAGeneroL.Location = New System.Drawing.Point(911, 23)
        Me.cmbFAGeneroL.MaxLength = 5
        Me.cmbFAGeneroL.Name = "cmbFAGeneroL"
        Me.cmbFAGeneroL.Size = New System.Drawing.Size(35, 22)
        Me.cmbFAGeneroL.TabIndex = 1
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.Location = New System.Drawing.Point(840, 28)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(65, 15)
        Me.Label17.TabIndex = 20
        Me.Label17.Text = "Largo (m):"
        '
        'cmbFAGenero
        '
        Me.cmbFAGenero.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbFAGenero.FormattingEnabled = True
        Me.cmbFAGenero.Location = New System.Drawing.Point(99, 24)
        Me.cmbFAGenero.Name = "cmbFAGenero"
        Me.cmbFAGenero.Size = New System.Drawing.Size(606, 22)
        Me.cmbFAGenero.TabIndex = 12
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label23.Location = New System.Drawing.Point(44, 28)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(51, 15)
        Me.Label23.TabIndex = 2
        Me.Label23.Text = "Genero:"
        '
        'gcRellenoAlmohadon
        '
        Me.gcRellenoAlmohadon.Controls.Add(Me.txtRASub)
        Me.gcRellenoAlmohadon.Controls.Add(Me.Label5)
        Me.gcRellenoAlmohadon.Controls.Add(Me.cmbRAC3E)
        Me.gcRellenoAlmohadon.Controls.Add(Me.Label25)
        Me.gcRellenoAlmohadon.Controls.Add(Me.cmbRAC2E)
        Me.gcRellenoAlmohadon.Controls.Add(Me.Label24)
        Me.gcRellenoAlmohadon.Controls.Add(Me.cmbRAC1E)
        Me.gcRellenoAlmohadon.Controls.Add(Me.Label22)
        Me.gcRellenoAlmohadon.Controls.Add(Me.cmbRAC3P)
        Me.gcRellenoAlmohadon.Controls.Add(Me.Label20)
        Me.gcRellenoAlmohadon.Controls.Add(Me.cmbRAC2P)
        Me.gcRellenoAlmohadon.Controls.Add(Me.Label21)
        Me.gcRellenoAlmohadon.Controls.Add(Me.cmbRAC3)
        Me.gcRellenoAlmohadon.Controls.Add(Me.Label19)
        Me.gcRellenoAlmohadon.Controls.Add(Me.cmbRAC2)
        Me.gcRellenoAlmohadon.Controls.Add(Me.Label18)
        Me.gcRellenoAlmohadon.Controls.Add(Me.cmbRAC1)
        Me.gcRellenoAlmohadon.Controls.Add(Me.Label15)
        Me.gcRellenoAlmohadon.Controls.Add(Me.txtRATipoA)
        Me.gcRellenoAlmohadon.Controls.Add(Me.Label3)
        Me.gcRellenoAlmohadon.Controls.Add(Me.txtRAObs)
        Me.gcRellenoAlmohadon.Controls.Add(Me.Label4)
        Me.gcRellenoAlmohadon.Controls.Add(Me.cmbRAC1P)
        Me.gcRellenoAlmohadon.Controls.Add(Me.Label6)
        Me.gcRellenoAlmohadon.Controls.Add(Me.GroupControl5)
        Me.gcRellenoAlmohadon.Controls.Add(Me.txtRATipoP)
        Me.gcRellenoAlmohadon.Controls.Add(Me.Label10)
        Me.gcRellenoAlmohadon.Controls.Add(Me.txtRATipoL)
        Me.gcRellenoAlmohadon.Controls.Add(Me.Label11)
        Me.gcRellenoAlmohadon.Controls.Add(Me.cmbRATipo)
        Me.gcRellenoAlmohadon.Controls.Add(Me.Label12)
        Me.gcRellenoAlmohadon.Location = New System.Drawing.Point(12, 350)
        Me.gcRellenoAlmohadon.LookAndFeel.SkinName = "Visual Studio 2013 Dark"
        Me.gcRellenoAlmohadon.LookAndFeel.UseDefaultLookAndFeel = False
        Me.gcRellenoAlmohadon.Name = "gcRellenoAlmohadon"
        Me.gcRellenoAlmohadon.Size = New System.Drawing.Size(1090, 162)
        Me.gcRellenoAlmohadon.TabIndex = 69
        Me.gcRellenoAlmohadon.Text = "Relleno de Almohadon"
        '
        'txtRASub
        '
        Me.txtRASub.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRASub.Location = New System.Drawing.Point(1005, 136)
        Me.txtRASub.Name = "txtRASub"
        Me.txtRASub.Size = New System.Drawing.Size(80, 21)
        Me.txtRASub.TabIndex = 28
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(940, 139)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(59, 15)
        Me.Label5.TabIndex = 27
        Me.Label5.Text = "SubTotal:"
        '
        'cmbRAC3E
        '
        Me.cmbRAC3E.Font = New System.Drawing.Font("SansSerif", 9.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbRAC3E.Location = New System.Drawing.Point(564, 107)
        Me.cmbRAC3E.MaxLength = 5
        Me.cmbRAC3E.Name = "cmbRAC3E"
        Me.cmbRAC3E.Size = New System.Drawing.Size(40, 22)
        Me.cmbRAC3E.TabIndex = 55
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label25.Location = New System.Drawing.Point(479, 111)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(79, 15)
        Me.Label25.TabIndex = 56
        Me.Label25.Text = "Espesor (m):"
        '
        'cmbRAC2E
        '
        Me.cmbRAC2E.Font = New System.Drawing.Font("SansSerif", 9.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbRAC2E.Location = New System.Drawing.Point(564, 79)
        Me.cmbRAC2E.MaxLength = 5
        Me.cmbRAC2E.Name = "cmbRAC2E"
        Me.cmbRAC2E.Size = New System.Drawing.Size(40, 22)
        Me.cmbRAC2E.TabIndex = 53
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label24.Location = New System.Drawing.Point(479, 83)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(79, 15)
        Me.Label24.TabIndex = 54
        Me.Label24.Text = "Espesor (m):"
        '
        'cmbRAC1E
        '
        Me.cmbRAC1E.Font = New System.Drawing.Font("SansSerif", 9.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbRAC1E.Location = New System.Drawing.Point(564, 52)
        Me.cmbRAC1E.MaxLength = 5
        Me.cmbRAC1E.Name = "cmbRAC1E"
        Me.cmbRAC1E.Size = New System.Drawing.Size(40, 22)
        Me.cmbRAC1E.TabIndex = 51
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label22.Location = New System.Drawing.Point(479, 55)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(79, 15)
        Me.Label22.TabIndex = 52
        Me.Label22.Text = "Espesor (m):"
        '
        'cmbRAC3P
        '
        Me.cmbRAC3P.Font = New System.Drawing.Font("SansSerif", 9.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbRAC3P.Location = New System.Drawing.Point(780, 108)
        Me.cmbRAC3P.Name = "cmbRAC3P"
        Me.cmbRAC3P.Size = New System.Drawing.Size(55, 22)
        Me.cmbRAC3P.TabIndex = 50
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label20.Location = New System.Drawing.Point(727, 112)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(47, 15)
        Me.Label20.TabIndex = 49
        Me.Label20.Text = "Precio:"
        '
        'cmbRAC2P
        '
        Me.cmbRAC2P.Font = New System.Drawing.Font("SansSerif", 9.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbRAC2P.Location = New System.Drawing.Point(780, 80)
        Me.cmbRAC2P.Name = "cmbRAC2P"
        Me.cmbRAC2P.Size = New System.Drawing.Size(55, 22)
        Me.cmbRAC2P.TabIndex = 47
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label21.Location = New System.Drawing.Point(727, 84)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(47, 15)
        Me.Label21.TabIndex = 48
        Me.Label21.Text = "Precio:"
        '
        'cmbRAC3
        '
        Me.cmbRAC3.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbRAC3.FormattingEnabled = True
        Me.cmbRAC3.Location = New System.Drawing.Point(99, 108)
        Me.cmbRAC3.Name = "cmbRAC3"
        Me.cmbRAC3.Size = New System.Drawing.Size(375, 22)
        Me.cmbRAC3.TabIndex = 46
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.Location = New System.Drawing.Point(44, 111)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(49, 15)
        Me.Label19.TabIndex = 45
        Me.Label19.Text = "Capa 3:"
        '
        'cmbRAC2
        '
        Me.cmbRAC2.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbRAC2.FormattingEnabled = True
        Me.cmbRAC2.Location = New System.Drawing.Point(99, 80)
        Me.cmbRAC2.Name = "cmbRAC2"
        Me.cmbRAC2.Size = New System.Drawing.Size(374, 22)
        Me.cmbRAC2.TabIndex = 44
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.Location = New System.Drawing.Point(44, 83)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(49, 15)
        Me.Label18.TabIndex = 43
        Me.Label18.Text = "Capa 2:"
        '
        'cmbRAC1
        '
        Me.cmbRAC1.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbRAC1.FormattingEnabled = True
        Me.cmbRAC1.Location = New System.Drawing.Point(99, 52)
        Me.cmbRAC1.Name = "cmbRAC1"
        Me.cmbRAC1.Size = New System.Drawing.Size(374, 22)
        Me.cmbRAC1.TabIndex = 42
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.Location = New System.Drawing.Point(44, 55)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(49, 15)
        Me.Label15.TabIndex = 41
        Me.Label15.Text = "Capa 1:"
        '
        'txtRATipoA
        '
        Me.txtRATipoA.Font = New System.Drawing.Font("SansSerif", 9.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRATipoA.Location = New System.Drawing.Point(564, 23)
        Me.txtRATipoA.MaxLength = 5
        Me.txtRATipoA.Name = "txtRATipoA"
        Me.txtRATipoA.Size = New System.Drawing.Size(40, 22)
        Me.txtRATipoA.TabIndex = 39
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(490, 27)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(68, 15)
        Me.Label3.TabIndex = 40
        Me.Label3.Text = "Ancho (m):"
        '
        'txtRAObs
        '
        Me.txtRAObs.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRAObs.Location = New System.Drawing.Point(99, 136)
        Me.txtRAObs.Name = "txtRAObs"
        Me.txtRAObs.Size = New System.Drawing.Size(835, 21)
        Me.txtRAObs.TabIndex = 37
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(11, 139)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(82, 15)
        Me.Label4.TabIndex = 38
        Me.Label4.Text = "Observación:"
        '
        'cmbRAC1P
        '
        Me.cmbRAC1P.Font = New System.Drawing.Font("SansSerif", 9.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbRAC1P.Location = New System.Drawing.Point(780, 52)
        Me.cmbRAC1P.Name = "cmbRAC1P"
        Me.cmbRAC1P.Size = New System.Drawing.Size(55, 22)
        Me.cmbRAC1P.TabIndex = 32
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(727, 57)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(47, 15)
        Me.Label6.TabIndex = 31
        Me.Label6.Text = "Precio:"
        '
        'GroupControl5
        '
        Me.GroupControl5.Controls.Add(Me.txtRAL)
        Me.GroupControl5.Controls.Add(Me.txtRAA)
        Me.GroupControl5.Controls.Add(Me.Label9)
        Me.GroupControl5.Controls.Add(Me.Label8)
        Me.GroupControl5.Location = New System.Drawing.Point(841, 24)
        Me.GroupControl5.LookAndFeel.SkinName = "Visual Studio 2013 Dark"
        Me.GroupControl5.LookAndFeel.UseDefaultLookAndFeel = False
        Me.GroupControl5.Name = "GroupControl5"
        Me.GroupControl5.Size = New System.Drawing.Size(244, 56)
        Me.GroupControl5.TabIndex = 21
        Me.GroupControl5.Text = "Dimensiones:"
        '
        'txtRAL
        '
        Me.txtRAL.Font = New System.Drawing.Font("SansSerif", 9.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRAL.Location = New System.Drawing.Point(196, 24)
        Me.txtRAL.MaxLength = 5
        Me.txtRAL.Name = "txtRAL"
        Me.txtRAL.Size = New System.Drawing.Size(40, 22)
        Me.txtRAL.TabIndex = 3
        '
        'txtRAA
        '
        Me.txtRAA.Font = New System.Drawing.Font("SansSerif", 9.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRAA.Location = New System.Drawing.Point(79, 24)
        Me.txtRAA.MaxLength = 5
        Me.txtRAA.Name = "txtRAA"
        Me.txtRAA.Size = New System.Drawing.Size(40, 22)
        Me.txtRAA.TabIndex = 2
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(5, 28)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(68, 15)
        Me.Label9.TabIndex = 0
        Me.Label9.Text = "Ancho (m):"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(125, 28)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(65, 15)
        Me.Label8.TabIndex = 1
        Me.Label8.Text = "Largo (m):"
        '
        'txtRATipoP
        '
        Me.txtRATipoP.Font = New System.Drawing.Font("SansSerif", 9.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRATipoP.Location = New System.Drawing.Point(780, 24)
        Me.txtRATipoP.Name = "txtRATipoP"
        Me.txtRATipoP.Size = New System.Drawing.Size(55, 22)
        Me.txtRATipoP.TabIndex = 1
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(727, 28)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(47, 15)
        Me.Label10.TabIndex = 1
        Me.Label10.Text = "Precio:"
        '
        'txtRATipoL
        '
        Me.txtRATipoL.Font = New System.Drawing.Font("SansSerif", 9.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRATipoL.Location = New System.Drawing.Point(681, 24)
        Me.txtRATipoL.MaxLength = 5
        Me.txtRATipoL.Name = "txtRATipoL"
        Me.txtRATipoL.Size = New System.Drawing.Size(40, 22)
        Me.txtRATipoL.TabIndex = 1
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(610, 27)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(65, 15)
        Me.Label11.TabIndex = 20
        Me.Label11.Text = "Largo (m):"
        '
        'cmbRATipo
        '
        Me.cmbRATipo.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbRATipo.FormattingEnabled = True
        Me.cmbRATipo.Location = New System.Drawing.Point(99, 24)
        Me.cmbRATipo.Name = "cmbRATipo"
        Me.cmbRATipo.Size = New System.Drawing.Size(374, 22)
        Me.cmbRATipo.TabIndex = 12
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(59, 27)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(34, 15)
        Me.Label12.TabIndex = 2
        Me.Label12.Text = "Tipo:"
        '
        'gcItem
        '
        Me.gcItem.Controls.Add(Me.chkRellenoAl)
        Me.gcItem.Controls.Add(Me.chkFundaAl)
        Me.gcItem.Controls.Add(Me.chkFundaSillon)
        Me.gcItem.Controls.Add(Me.Label56)
        Me.gcItem.Controls.Add(Me.SpinEdit1)
        Me.gcItem.Controls.Add(Me.txtItemDesc)
        Me.gcItem.Controls.Add(Me.Label55)
        Me.gcItem.Controls.Add(Me.cmbOpcion)
        Me.gcItem.Controls.Add(Me.Label54)
        Me.gcItem.Controls.Add(Me.txtObservaciones)
        Me.gcItem.Controls.Add(Me.Label48)
        Me.gcItem.Controls.Add(Me.txtUbicacion)
        Me.gcItem.Controls.Add(Me.Label47)
        Me.gcItem.Location = New System.Drawing.Point(12, 12)
        Me.gcItem.LookAndFeel.SkinName = "Visual Studio 2013 Dark"
        Me.gcItem.LookAndFeel.UseDefaultLookAndFeel = False
        Me.gcItem.Name = "gcItem"
        Me.gcItem.Size = New System.Drawing.Size(1090, 108)
        Me.gcItem.TabIndex = 71
        Me.gcItem.Text = "Datos Item:"
        '
        'chkRellenoAl
        '
        Me.chkRellenoAl.AutoSize = True
        Me.chkRellenoAl.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkRellenoAl.Location = New System.Drawing.Point(950, 74)
        Me.chkRellenoAl.Name = "chkRellenoAl"
        Me.chkRellenoAl.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.chkRellenoAl.Size = New System.Drawing.Size(135, 19)
        Me.chkRellenoAl.TabIndex = 67
        Me.chkRellenoAl.Text = "Relleno Almohadon"
        Me.chkRellenoAl.UseVisualStyleBackColor = True
        '
        'chkFundaAl
        '
        Me.chkFundaAl.AutoSize = True
        Me.chkFundaAl.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkFundaAl.Location = New System.Drawing.Point(958, 49)
        Me.chkFundaAl.Name = "chkFundaAl"
        Me.chkFundaAl.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.chkFundaAl.Size = New System.Drawing.Size(127, 19)
        Me.chkFundaAl.TabIndex = 66
        Me.chkFundaAl.Text = "Funda Almohadon"
        Me.chkFundaAl.UseVisualStyleBackColor = True
        '
        'chkFundaSillon
        '
        Me.chkFundaSillon.AutoSize = True
        Me.chkFundaSillon.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkFundaSillon.Location = New System.Drawing.Point(991, 24)
        Me.chkFundaSillon.Name = "chkFundaSillon"
        Me.chkFundaSillon.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.chkFundaSillon.Size = New System.Drawing.Size(94, 19)
        Me.chkFundaSillon.TabIndex = 65
        Me.chkFundaSillon.Text = "Funda Sillon"
        Me.chkFundaSillon.UseVisualStyleBackColor = True
        '
        'Label56
        '
        Me.Label56.AutoSize = True
        Me.Label56.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label56.Location = New System.Drawing.Point(814, 85)
        Me.Label56.Name = "Label56"
        Me.Label56.Size = New System.Drawing.Size(60, 15)
        Me.Label56.TabIndex = 64
        Me.Label56.Text = "Cantidad:"
        '
        'SpinEdit1
        '
        Me.SpinEdit1.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.SpinEdit1.Location = New System.Drawing.Point(880, 81)
        Me.SpinEdit1.Name = "SpinEdit1"
        Me.SpinEdit1.Properties.Appearance.Font = New System.Drawing.Font("SansSerif", 9.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SpinEdit1.Properties.Appearance.Options.UseFont = True
        Me.SpinEdit1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.SpinEdit1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.SpinEdit1.Properties.MaxValue = New Decimal(New Integer() {20, 0, 0, 0})
        Me.SpinEdit1.Size = New System.Drawing.Size(64, 22)
        Me.SpinEdit1.TabIndex = 63
        '
        'txtItemDesc
        '
        Me.txtItemDesc.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtItemDesc.Location = New System.Drawing.Point(99, 25)
        Me.txtItemDesc.Multiline = True
        Me.txtItemDesc.Name = "txtItemDesc"
        Me.txtItemDesc.Size = New System.Drawing.Size(546, 23)
        Me.txtItemDesc.TabIndex = 62
        '
        'Label55
        '
        Me.Label55.AutoSize = True
        Me.Label55.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label55.Location = New System.Drawing.Point(15, 28)
        Me.Label55.Name = "Label55"
        Me.Label55.Size = New System.Drawing.Size(78, 15)
        Me.Label55.TabIndex = 61
        Me.Label55.Text = "Descripción:"
        '
        'cmbOpcion
        '
        Me.cmbOpcion.DisplayMember = "0"
        Me.cmbOpcion.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbOpcion.FormattingEnabled = True
        Me.cmbOpcion.Items.AddRange(New Object() {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9"})
        Me.cmbOpcion.Location = New System.Drawing.Point(883, 53)
        Me.cmbOpcion.Name = "cmbOpcion"
        Me.cmbOpcion.Size = New System.Drawing.Size(61, 22)
        Me.cmbOpcion.TabIndex = 60
        '
        'Label54
        '
        Me.Label54.AutoSize = True
        Me.Label54.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label54.Location = New System.Drawing.Point(851, 56)
        Me.Label54.Name = "Label54"
        Me.Label54.Size = New System.Drawing.Size(26, 15)
        Me.Label54.TabIndex = 31
        Me.Label54.Text = "Op:"
        '
        'txtObservaciones
        '
        Me.txtObservaciones.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtObservaciones.Location = New System.Drawing.Point(99, 53)
        Me.txtObservaciones.Multiline = True
        Me.txtObservaciones.Name = "txtObservaciones"
        Me.txtObservaciones.Size = New System.Drawing.Size(709, 50)
        Me.txtObservaciones.TabIndex = 30
        '
        'Label48
        '
        Me.Label48.AutoSize = True
        Me.Label48.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label48.Location = New System.Drawing.Point(11, 55)
        Me.Label48.Name = "Label48"
        Me.Label48.Size = New System.Drawing.Size(82, 15)
        Me.Label48.TabIndex = 29
        Me.Label48.Text = "Observación:"
        '
        'txtUbicacion
        '
        Me.txtUbicacion.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtUbicacion.Location = New System.Drawing.Point(723, 24)
        Me.txtUbicacion.Multiline = True
        Me.txtUbicacion.Name = "txtUbicacion"
        Me.txtUbicacion.Size = New System.Drawing.Size(221, 23)
        Me.txtUbicacion.TabIndex = 28
        '
        'Label47
        '
        Me.Label47.AutoSize = True
        Me.Label47.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label47.Location = New System.Drawing.Point(651, 28)
        Me.Label47.Name = "Label47"
        Me.Label47.Size = New System.Drawing.Size(66, 15)
        Me.Label47.TabIndex = 27
        Me.Label47.Text = "Ubicación:"
        '
        'gcFundaSillon
        '
        Me.gcFundaSillon.Controls.Add(Me.cmbFSTipo)
        Me.gcFundaSillon.Controls.Add(Me.Label27)
        Me.gcFundaSillon.Controls.Add(Me.cmbFSTipoE)
        Me.gcFundaSillon.Controls.Add(Me.Label28)
        Me.gcFundaSillon.Controls.Add(Me.cmbFSTipoSub)
        Me.gcFundaSillon.Controls.Add(Me.cmbFSTipoL)
        Me.gcFundaSillon.Controls.Add(Me.Label30)
        Me.gcFundaSillon.Controls.Add(Me.cmbFSTipoA)
        Me.gcFundaSillon.Controls.Add(Me.cmbFSGeneroA)
        Me.gcFundaSillon.Controls.Add(Me.Label31)
        Me.gcFundaSillon.Controls.Add(Me.Label32)
        Me.gcFundaSillon.Controls.Add(Me.Label33)
        Me.gcFundaSillon.Controls.Add(Me.txtFSObs)
        Me.gcFundaSillon.Controls.Add(Me.Label34)
        Me.gcFundaSillon.Controls.Add(Me.cmbFSTipoP)
        Me.gcFundaSillon.Controls.Add(Me.Label35)
        Me.gcFundaSillon.Controls.Add(Me.cmbFSGeneroP)
        Me.gcFundaSillon.Controls.Add(Me.Label36)
        Me.gcFundaSillon.Controls.Add(Me.cmbFSGeneroL)
        Me.gcFundaSillon.Controls.Add(Me.Label37)
        Me.gcFundaSillon.Controls.Add(Me.cmbFSGenero)
        Me.gcFundaSillon.Controls.Add(Me.Label38)
        Me.gcFundaSillon.Location = New System.Drawing.Point(12, 126)
        Me.gcFundaSillon.LookAndFeel.SkinName = "Visual Studio 2013 Dark"
        Me.gcFundaSillon.LookAndFeel.UseDefaultLookAndFeel = False
        Me.gcFundaSillon.Name = "gcFundaSillon"
        Me.gcFundaSillon.Size = New System.Drawing.Size(1090, 106)
        Me.gcFundaSillon.TabIndex = 69
        Me.gcFundaSillon.Text = "Funda de Sillon"
        '
        'cmbFSTipo
        '
        Me.cmbFSTipo.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbFSTipo.FormattingEnabled = True
        Me.cmbFSTipo.Location = New System.Drawing.Point(99, 52)
        Me.cmbFSTipo.Name = "cmbFSTipo"
        Me.cmbFSTipo.Size = New System.Drawing.Size(491, 22)
        Me.cmbFSTipo.TabIndex = 42
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label27.Location = New System.Drawing.Point(59, 56)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(34, 15)
        Me.Label27.TabIndex = 41
        Me.Label27.Text = "Tipo:"
        '
        'cmbFSTipoE
        '
        Me.cmbFSTipoE.Font = New System.Drawing.Font("SansSerif", 9.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbFSTipoE.Location = New System.Drawing.Point(911, 51)
        Me.cmbFSTipoE.MaxLength = 5
        Me.cmbFSTipoE.Name = "cmbFSTipoE"
        Me.cmbFSTipoE.Size = New System.Drawing.Size(35, 22)
        Me.cmbFSTipoE.TabIndex = 5
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label28.Location = New System.Drawing.Point(826, 56)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(79, 15)
        Me.Label28.TabIndex = 4
        Me.Label28.Text = "Espesor (m):"
        '
        'cmbFSTipoSub
        '
        Me.cmbFSTipoSub.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbFSTipoSub.Location = New System.Drawing.Point(1005, 80)
        Me.cmbFSTipoSub.Name = "cmbFSTipoSub"
        Me.cmbFSTipoSub.Size = New System.Drawing.Size(80, 21)
        Me.cmbFSTipoSub.TabIndex = 28
        '
        'cmbFSTipoL
        '
        Me.cmbFSTipoL.Font = New System.Drawing.Font("SansSerif", 9.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbFSTipoL.Location = New System.Drawing.Point(785, 51)
        Me.cmbFSTipoL.MaxLength = 5
        Me.cmbFSTipoL.Name = "cmbFSTipoL"
        Me.cmbFSTipoL.Size = New System.Drawing.Size(35, 22)
        Me.cmbFSTipoL.TabIndex = 3
        '
        'Label30
        '
        Me.Label30.AutoSize = True
        Me.Label30.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label30.Location = New System.Drawing.Point(940, 83)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(59, 15)
        Me.Label30.TabIndex = 27
        Me.Label30.Text = "SubTotal:"
        '
        'cmbFSTipoA
        '
        Me.cmbFSTipoA.Font = New System.Drawing.Font("SansSerif", 9.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbFSTipoA.Location = New System.Drawing.Point(670, 52)
        Me.cmbFSTipoA.MaxLength = 5
        Me.cmbFSTipoA.Name = "cmbFSTipoA"
        Me.cmbFSTipoA.Size = New System.Drawing.Size(35, 22)
        Me.cmbFSTipoA.TabIndex = 2
        '
        'cmbFSGeneroA
        '
        Me.cmbFSGeneroA.Font = New System.Drawing.Font("SansSerif", 9.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbFSGeneroA.Location = New System.Drawing.Point(785, 23)
        Me.cmbFSGeneroA.MaxLength = 5
        Me.cmbFSGeneroA.Name = "cmbFSGeneroA"
        Me.cmbFSGeneroA.Size = New System.Drawing.Size(35, 22)
        Me.cmbFSGeneroA.TabIndex = 39
        '
        'Label31
        '
        Me.Label31.AutoSize = True
        Me.Label31.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label31.Location = New System.Drawing.Point(714, 56)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(65, 15)
        Me.Label31.TabIndex = 1
        Me.Label31.Text = "Largo (m):"
        '
        'Label32
        '
        Me.Label32.AutoSize = True
        Me.Label32.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label32.Location = New System.Drawing.Point(711, 28)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(68, 15)
        Me.Label32.TabIndex = 40
        Me.Label32.Text = "Ancho (m):"
        '
        'Label33
        '
        Me.Label33.AutoSize = True
        Me.Label33.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label33.Location = New System.Drawing.Point(596, 55)
        Me.Label33.Name = "Label33"
        Me.Label33.Size = New System.Drawing.Size(68, 15)
        Me.Label33.TabIndex = 0
        Me.Label33.Text = "Ancho (m):"
        '
        'txtFSObs
        '
        Me.txtFSObs.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFSObs.Location = New System.Drawing.Point(99, 80)
        Me.txtFSObs.Name = "txtFSObs"
        Me.txtFSObs.Size = New System.Drawing.Size(835, 21)
        Me.txtFSObs.TabIndex = 37
        '
        'Label34
        '
        Me.Label34.AutoSize = True
        Me.Label34.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label34.Location = New System.Drawing.Point(11, 83)
        Me.Label34.Name = "Label34"
        Me.Label34.Size = New System.Drawing.Size(82, 15)
        Me.Label34.TabIndex = 38
        Me.Label34.Text = "Observación:"
        '
        'cmbFSTipoP
        '
        Me.cmbFSTipoP.Font = New System.Drawing.Font("SansSerif", 9.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbFSTipoP.Location = New System.Drawing.Point(1005, 52)
        Me.cmbFSTipoP.Name = "cmbFSTipoP"
        Me.cmbFSTipoP.Size = New System.Drawing.Size(80, 22)
        Me.cmbFSTipoP.TabIndex = 32
        '
        'Label35
        '
        Me.Label35.AutoSize = True
        Me.Label35.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label35.Location = New System.Drawing.Point(952, 56)
        Me.Label35.Name = "Label35"
        Me.Label35.Size = New System.Drawing.Size(47, 15)
        Me.Label35.TabIndex = 31
        Me.Label35.Text = "Precio:"
        '
        'cmbFSGeneroP
        '
        Me.cmbFSGeneroP.Font = New System.Drawing.Font("SansSerif", 9.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbFSGeneroP.Location = New System.Drawing.Point(1005, 24)
        Me.cmbFSGeneroP.Name = "cmbFSGeneroP"
        Me.cmbFSGeneroP.Size = New System.Drawing.Size(80, 22)
        Me.cmbFSGeneroP.TabIndex = 1
        '
        'Label36
        '
        Me.Label36.AutoSize = True
        Me.Label36.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label36.Location = New System.Drawing.Point(952, 28)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(47, 15)
        Me.Label36.TabIndex = 1
        Me.Label36.Text = "Precio:"
        '
        'cmbFSGeneroL
        '
        Me.cmbFSGeneroL.Font = New System.Drawing.Font("SansSerif", 9.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbFSGeneroL.Location = New System.Drawing.Point(911, 23)
        Me.cmbFSGeneroL.MaxLength = 5
        Me.cmbFSGeneroL.Name = "cmbFSGeneroL"
        Me.cmbFSGeneroL.Size = New System.Drawing.Size(35, 22)
        Me.cmbFSGeneroL.TabIndex = 1
        '
        'Label37
        '
        Me.Label37.AutoSize = True
        Me.Label37.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label37.Location = New System.Drawing.Point(840, 28)
        Me.Label37.Name = "Label37"
        Me.Label37.Size = New System.Drawing.Size(65, 15)
        Me.Label37.TabIndex = 20
        Me.Label37.Text = "Largo (m):"
        '
        'cmbFSGenero
        '
        Me.cmbFSGenero.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbFSGenero.FormattingEnabled = True
        Me.cmbFSGenero.Location = New System.Drawing.Point(99, 24)
        Me.cmbFSGenero.Name = "cmbFSGenero"
        Me.cmbFSGenero.Size = New System.Drawing.Size(606, 22)
        Me.cmbFSGenero.TabIndex = 12
        '
        'Label38
        '
        Me.Label38.AutoSize = True
        Me.Label38.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label38.Location = New System.Drawing.Point(44, 28)
        Me.Label38.Name = "Label38"
        Me.Label38.Size = New System.Drawing.Size(51, 15)
        Me.Label38.TabIndex = 2
        Me.Label38.Text = "Genero:"
        '
        'gcTotal
        '
        Me.gcTotal.Controls.Add(Me.txtFinal)
        Me.gcTotal.Controls.Add(Me.Label50)
        Me.gcTotal.Controls.Add(Me.txtIVA)
        Me.gcTotal.Controls.Add(Me.Label49)
        Me.gcTotal.Controls.Add(Me.txtTotalP)
        Me.gcTotal.Controls.Add(Me.Label46)
        Me.gcTotal.Location = New System.Drawing.Point(343, 639)
        Me.gcTotal.LookAndFeel.SkinName = "Visual Studio 2013 Dark"
        Me.gcTotal.LookAndFeel.UseDefaultLookAndFeel = False
        Me.gcTotal.Name = "gcTotal"
        Me.gcTotal.Size = New System.Drawing.Size(448, 50)
        Me.gcTotal.TabIndex = 43
        Me.gcTotal.Text = "Total:"
        '
        'txtFinal
        '
        Me.txtFinal.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.txtFinal.Location = New System.Drawing.Point(340, 24)
        Me.txtFinal.Name = "txtFinal"
        Me.txtFinal.Size = New System.Drawing.Size(90, 21)
        Me.txtFinal.TabIndex = 32
        '
        'Label50
        '
        Me.Label50.AutoSize = True
        Me.Label50.Font = New System.Drawing.Font("SansSerif", 8.999999!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.Label50.Location = New System.Drawing.Point(296, 27)
        Me.Label50.Name = "Label50"
        Me.Label50.Size = New System.Drawing.Size(38, 14)
        Me.Label50.TabIndex = 31
        Me.Label50.Text = "Final:"
        '
        'txtIVA
        '
        Me.txtIVA.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.txtIVA.Location = New System.Drawing.Point(196, 24)
        Me.txtIVA.Name = "txtIVA"
        Me.txtIVA.Size = New System.Drawing.Size(90, 21)
        Me.txtIVA.TabIndex = 30
        '
        'Label49
        '
        Me.Label49.AutoSize = True
        Me.Label49.Font = New System.Drawing.Font("SansSerif", 8.999999!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.Label49.Location = New System.Drawing.Point(159, 27)
        Me.Label49.Name = "Label49"
        Me.Label49.Size = New System.Drawing.Size(31, 14)
        Me.Label49.TabIndex = 29
        Me.Label49.Text = "IVA:"
        '
        'txtTotalP
        '
        Me.txtTotalP.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.txtTotalP.Location = New System.Drawing.Point(58, 24)
        Me.txtTotalP.Name = "txtTotalP"
        Me.txtTotalP.Size = New System.Drawing.Size(90, 21)
        Me.txtTotalP.TabIndex = 28
        '
        'Label46
        '
        Me.Label46.AutoSize = True
        Me.Label46.Font = New System.Drawing.Font("SansSerif", 8.999999!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.Label46.Location = New System.Drawing.Point(5, 27)
        Me.Label46.Name = "Label46"
        Me.Label46.Size = New System.Drawing.Size(48, 14)
        Me.Label46.TabIndex = 27
        Me.Label46.Text = "Precio:"
        '
        'frmFundas
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoSize = True
        Me.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.BackColor = System.Drawing.Color.Black
        Me.ClientSize = New System.Drawing.Size(1120, 701)
        Me.ControlBox = False
        Me.Controls.Add(Me.gcTotal)
        Me.Controls.Add(Me.gcFundaSillon)
        Me.Controls.Add(Me.gcItem)
        Me.Controls.Add(Me.gcRellenoAlmohadon)
        Me.Controls.Add(Me.gcFundaAlmohadon)
        Me.Controls.Add(Me.btnGuardar)
        Me.Controls.Add(Me.btnSalir)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.MaximumSize = New System.Drawing.Size(1366, 701)
        Me.MinimumSize = New System.Drawing.Size(1120, 701)
        Me.Name = "frmFundas"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Fundas Y Almohadones"
        CType(Me.gcFundaAlmohadon, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gcFundaAlmohadon.ResumeLayout(False)
        Me.gcFundaAlmohadon.PerformLayout()
        CType(Me.gcRellenoAlmohadon, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gcRellenoAlmohadon.ResumeLayout(False)
        Me.gcRellenoAlmohadon.PerformLayout()
        CType(Me.GroupControl5, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl5.ResumeLayout(False)
        Me.GroupControl5.PerformLayout()
        CType(Me.gcItem, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gcItem.ResumeLayout(False)
        Me.gcItem.PerformLayout()
        CType(Me.SpinEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gcFundaSillon, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gcFundaSillon.ResumeLayout(False)
        Me.gcFundaSillon.PerformLayout()
        CType(Me.gcTotal, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gcTotal.ResumeLayout(False)
        Me.gcTotal.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnGuardar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnSalir As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents gcFundaAlmohadon As DevExpress.XtraEditors.GroupControl
    Friend WithEvents txtFAObs As System.Windows.Forms.TextBox
    Friend WithEvents Label51 As System.Windows.Forms.Label
    Friend WithEvents cmbFATipoSub As System.Windows.Forms.TextBox
    Friend WithEvents Label29 As System.Windows.Forms.Label
    Friend WithEvents cmbFATipoP As System.Windows.Forms.TextBox
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents cmbFATipoE As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cmbFATipoL As System.Windows.Forms.TextBox
    Friend WithEvents cmbFATipoA As System.Windows.Forms.TextBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents cmbFAGeneroP As System.Windows.Forms.TextBox
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents cmbFAGeneroL As System.Windows.Forms.TextBox
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents cmbFAGenero As System.Windows.Forms.ComboBox
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents cmbFAGeneroA As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents gcRellenoAlmohadon As DevExpress.XtraEditors.GroupControl
    Friend WithEvents cmbRAC3E As System.Windows.Forms.TextBox
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents cmbRAC2E As System.Windows.Forms.TextBox
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents cmbRAC1E As System.Windows.Forms.TextBox
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents cmbRAC3P As System.Windows.Forms.TextBox
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents cmbRAC2P As System.Windows.Forms.TextBox
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents cmbRAC3 As System.Windows.Forms.ComboBox
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents cmbRAC2 As System.Windows.Forms.ComboBox
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents cmbRAC1 As System.Windows.Forms.ComboBox
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents txtRATipoA As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtRAObs As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtRASub As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents cmbRAC1P As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents GroupControl5 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents txtRAL As System.Windows.Forms.TextBox
    Friend WithEvents txtRAA As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents txtRATipoP As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents txtRATipoL As System.Windows.Forms.TextBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents cmbRATipo As System.Windows.Forms.ComboBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents cmbFATipo As System.Windows.Forms.ComboBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents gcItem As DevExpress.XtraEditors.GroupControl
    Friend WithEvents chkRellenoAl As System.Windows.Forms.CheckBox
    Friend WithEvents chkFundaAl As System.Windows.Forms.CheckBox
    Friend WithEvents chkFundaSillon As System.Windows.Forms.CheckBox
    Friend WithEvents Label56 As System.Windows.Forms.Label
    Friend WithEvents SpinEdit1 As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents txtItemDesc As System.Windows.Forms.TextBox
    Friend WithEvents Label55 As System.Windows.Forms.Label
    Friend WithEvents cmbOpcion As System.Windows.Forms.ComboBox
    Friend WithEvents Label54 As System.Windows.Forms.Label
    Friend WithEvents txtObservaciones As System.Windows.Forms.TextBox
    Friend WithEvents Label48 As System.Windows.Forms.Label
    Friend WithEvents txtUbicacion As System.Windows.Forms.TextBox
    Friend WithEvents Label47 As System.Windows.Forms.Label
    Friend WithEvents gcFundaSillon As DevExpress.XtraEditors.GroupControl
    Friend WithEvents cmbFSTipo As System.Windows.Forms.ComboBox
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents cmbFSTipoE As System.Windows.Forms.TextBox
    Friend WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents cmbFSTipoSub As System.Windows.Forms.TextBox
    Friend WithEvents cmbFSTipoL As System.Windows.Forms.TextBox
    Friend WithEvents Label30 As System.Windows.Forms.Label
    Friend WithEvents cmbFSTipoA As System.Windows.Forms.TextBox
    Friend WithEvents cmbFSGeneroA As System.Windows.Forms.TextBox
    Friend WithEvents Label31 As System.Windows.Forms.Label
    Friend WithEvents Label32 As System.Windows.Forms.Label
    Friend WithEvents Label33 As System.Windows.Forms.Label
    Friend WithEvents txtFSObs As System.Windows.Forms.TextBox
    Friend WithEvents Label34 As System.Windows.Forms.Label
    Friend WithEvents cmbFSTipoP As System.Windows.Forms.TextBox
    Friend WithEvents Label35 As System.Windows.Forms.Label
    Friend WithEvents cmbFSGeneroP As System.Windows.Forms.TextBox
    Friend WithEvents Label36 As System.Windows.Forms.Label
    Friend WithEvents cmbFSGeneroL As System.Windows.Forms.TextBox
    Friend WithEvents Label37 As System.Windows.Forms.Label
    Friend WithEvents cmbFSGenero As System.Windows.Forms.ComboBox
    Friend WithEvents Label38 As System.Windows.Forms.Label
    Friend WithEvents gcTotal As DevExpress.XtraEditors.GroupControl
    Friend WithEvents txtFinal As System.Windows.Forms.TextBox
    Friend WithEvents Label50 As System.Windows.Forms.Label
    Friend WithEvents txtIVA As System.Windows.Forms.TextBox
    Friend WithEvents Label49 As System.Windows.Forms.Label
    Friend WithEvents txtTotalP As System.Windows.Forms.TextBox
    Friend WithEvents Label46 As System.Windows.Forms.Label
End Class
