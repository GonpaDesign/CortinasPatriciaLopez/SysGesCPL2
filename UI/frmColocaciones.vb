﻿Imports System.Xml
Public Class frmColocaciones
    Private Funciones As New Funciones

    '------ EVENTOS DE FORMULARIO ------
    Private Sub frmColocaciones_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        ConfigurarForm()
        ConfigurarTextBox()
        ConfigurarDataGridView()

        LimpiarCasilleros()

        btnRealizado.Enabled = False
    End Sub
    Private Sub frmColocaciones_GotFocus(sender As Object, e As EventArgs) Handles MyBase.Click, MyBase.GotFocus
        Me.SendToBack()
    End Sub

    '------ CONFIGURACIONES ------
    Public Sub ConfigurarForm()
        Me.Dock = DockStyle.None
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.ShowIcon = False
        Me.MaximumSize = New Size(frmPrincipal.ClientSize.Width - 4, frmPrincipal.ClientSize.Height - 4)
        Me.MinimumSize = Me.MaximumSize
        Me.Dock = DockStyle.Fill
        Dim Separacion As Integer = (frmPrincipal.ClientSize.Width - 1090 + 150) / 2
        gcBuscar.Location = New Point(Separacion, 45)
        gcCliente.Location = New Point(Separacion, 88)
        gcFecha.Location = New Point(Separacion + 913, 88)
        gcFiltros.Location = New Point(Separacion + 913, 218)
        gcPagos.Location = New Point(Separacion + 913, 363)
        gcColocaciones.Size = New Size(907, frmPrincipal.ClientSize.Height - (218 + 10))
        gcColocaciones.Location = New Point(Separacion, 218)
        lblTitulo.Location = New Point((frmPrincipal.ClientSize.Width - 462 + 150) / 2, 4)
        Dim Separacionbtn As Integer = (frmPrincipal.ClientSize.Width - Separacion + 75)
        btnRealizado.Location = New Point(Separacionbtn, frmPrincipal.ClientSize.Height - 70)
        btnSalir.Location = New Point(Separacionbtn, frmPrincipal.ClientSize.Height - 37)
    End Sub
    Public Sub ConfigurarTextBox()
        'Configuro la cantidad de cararcteres maximos
        txtNombre.MaxLength = 200
        txtDireccion.MaxLength = 150
        'Configuro alineacion del texto
        txtClienteId.TextAlign = HorizontalAlignment.Center
        txtCiudad.TextAlign = HorizontalAlignment.Center
        txtTelefono1.TextAlign = HorizontalAlignment.Center
        txtMovil1.TextAlign = HorizontalAlignment.Center
        txtMovil2.TextAlign = HorizontalAlignment.Center
        txtPresupuestoId.TextAlign = HorizontalAlignment.Center
        txtCobrado.TextAlign = HorizontalAlignment.Center
        txtTotal.TextAlign = HorizontalAlignment.Center
        txtPendiente.TextAlign = HorizontalAlignment.Center
        'Configuro los que seran siempre solo lectura
        txtCiudad.ReadOnly = True
        txtClienteId.ReadOnly = True
        txtNombre.ReadOnly = True
        txtTelefono1.ReadOnly = True
        txtMovil1.ReadOnly = True
        txtMovil2.ReadOnly = True
        txtPresupuestoId.ReadOnly = True
        txtCobrado.ReadOnly = True
        txtTotal.ReadOnly = True
        txtPendiente.ReadOnly = True
    End Sub
    Private Sub ConfigurarDataGridView()
        dgvColocaciones.Location = New Point(121, 27)
        dgvColocaciones.Size = New Size(778, gcColocaciones.ClientSize.Height - 33)
    End Sub

    '------ EVENTOS DE BOTONES ------
    Private Sub btnSalir_Click(sender As System.Object, e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub
    Private Sub btnGuardar_Click(sender As Object, e As EventArgs) Handles btnGuardar.Click
        Dim UnPresupuesto As New Modelo.Presupuesto
        Dim UnGestorPresupuesto As New Modelo.GestorPresupuesto
        UnPresupuesto.FechaEntrega = CDate(dtpHora.Value)
        UnPresupuesto.IdPresupuesto = CInt(dgvColocaciones.CurrentRow.Cells(0).Value)
        UnGestorPresupuesto.ModificarColocacion(UnPresupuesto)
        btnActualizar.PerformClick()
    End Sub
    Private Sub btnActualizar_Click(sender As Object, e As EventArgs) Handles btnActualizar.Click
        Cargardgv()
    End Sub
    Private Sub btnRealizado_Click(sender As Object, e As EventArgs) Handles btnRealizado.Click
        Dim UnPresupuesto As New Modelo.Presupuesto
        Dim UnGestorPresupuesto As New Modelo.GestorPresupuesto
        UnPresupuesto.IdPresupuesto = CInt(dgvColocaciones.CurrentRow.Cells(0).Value)
        UnGestorPresupuesto.Colocar(UnPresupuesto)
        btnActualizar.PerformClick()
    End Sub

    '------ EVENTOS DE DATAGRIDVIEW ------
    Private Sub Cargardgv()
        Dim UnPresupuesto As New Modelo.Presupuesto
        If rbSinProgramar.Checked = True Then
            UnPresupuesto.FechaColocacion = CDate("1 / 1 / 1900")
        Else
            UnPresupuesto.FechaColocacion = dtpFechaSelect.Value.Date
        End If
        Dim unAccesoPresupuesto As New Modelo.DbPresupuesto
        dgvColocaciones.DataSource = unAccesoPresupuesto.SelectPresupuestoPorColocacion(UnPresupuesto)

        configdgv()
    End Sub
    Private Sub Configdgv()
        dgvColocaciones.Columns(2).Visible = False
        dgvColocaciones.Columns(3).Visible = False
        dgvColocaciones.Columns(6).Visible = False
        dgvColocaciones.Columns(7).Visible = False
        dgvColocaciones.Columns(8).Visible = False
        dgvColocaciones.Columns(9).Visible = False

        'dgvColocaciones.Columns(2).Width = 35
        dgvColocaciones.SelectionMode = DataGridViewSelectionMode.FullRowSelect
    End Sub
    Private Sub dgvColocaciones_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvColocaciones.CellDoubleClick
        Cursor = Cursors.WaitCursor
        Dim UnCliente As New Modelo.Cliente
        Dim UnGestorCliente As New Modelo.GestorClientes
        UnCliente.IdCliente = CInt(dgvColocaciones.CurrentRow.Cells(1).Value)
        Dim ClienteSeleccionado As New Modelo.Cliente
        ClienteSeleccionado = UnGestorCliente.BuscarPorId(UnCliente)
        CargarCliente(ClienteSeleccionado)
        If dgvColocaciones.CurrentRow.Cells(10).Value = CDate("1 / 1 / 1900") Then
            dtpFecha.Value = Date.Today
        Else
            dtpFecha.Value = dgvColocaciones.CurrentRow.Cells(10).Value
        End If
        btnGuardar.Enabled = True

        Dim UnItem As New Modelo.Item
        Dim UnGestorItem As New Modelo.GestorItem
        UnItem.IdPresupuesto = CInt(dgvColocaciones.CurrentRow.Cells(0).Value)
        txtPresupuestoId.Text = dgvColocaciones.CurrentRow.Cells(0).Value
        'Dim PresupuestoSeleccionado As New Modelo.Presupuesto
        'PresupuestoSeleccionado = UnGestorPresupuesto.BuscarPorId(UnPresupuesto)
        'txtCobrado.Text = UnGestorCobros.CalcularCobrado(UnPresupuesto)
        txtTotal.Text = UnGestorItem.CalcularTotal(UnItem)
        'txtPendiente.Text = UnGestorItem.CalcularTotal(UnPresupuesto.IdPresupuesto) - UnGestorCobros.CalcularCobrado(UnPresupuesto)
        Cursor = Cursors.Default
    End Sub
    Private Sub dgvColocaciones_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvColocaciones.CellClick
        btnGuardar.Enabled = False
        btnRealizado.Enabled = True
    End Sub

    '------ FUNCIONES PARA TEXTBOX ------
    Private Sub CargarCliente(UnCliente As Modelo.Cliente)
        txtNombre.Text = UnCliente.Nombre
        txtDireccion.Text = UnCliente.Direccion
        txtTelefono1.Text = Funciones.Telefono(CStr(UnCliente.Telefono1))
        txtMovil1.Text = Funciones.Telefono(CStr(UnCliente.Movil1))
        txtMovil2.Text = Funciones.Telefono(CStr(UnCliente.Movil2))
        Dim UnaCiudad As New Modelo.Ciudad
        Dim UnGestorCiudad As New Modelo.GestorCiudad
        UnaCiudad.IdCiudad = UnCliente.IdCiudad
        txtCiudad.Text = UnGestorCiudad.BuscarPorId(UnaCiudad).Localidad
    End Sub
    Private Sub LimpiarCasilleros()
        txtClienteId.Text = ""
        txtNombre.Text = ""
        txtDireccion.Text = ""
        txtCiudad.Text = ""
        txtTelefono1.Text = ""
        txtMovil1.Text = ""
        txtMovil2.Text = ""
        txtPresupuestoId.Text = ""
        txtCobrado.Text = ""
        txtTotal.Text = ""
        txtPendiente.Text = ""
    End Sub

    '------ EVENTOS DE DATETIMEPICKER ------
    Private Sub dtpFecha_ValueChanged(sender As Object, e As EventArgs) Handles dtpFecha.ValueChanged
        dtpHora.Value = dtpFecha.Value
    End Sub
    Private Sub dtpHora_ValueChanged(sender As Object, e As EventArgs) Handles dtpHora.ValueChanged
        If dtpHora.Value < Date.Today Then
            btnRealizado.Enabled = True
        Else
            btnRealizado.Enabled = False
        End If
    End Sub
End Class