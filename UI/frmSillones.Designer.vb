﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSillones
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnGuardar = New DevExpress.XtraEditors.SimpleButton()
        Me.btnSalir = New DevExpress.XtraEditors.SimpleButton()
        Me.gcItem = New DevExpress.XtraEditors.GroupControl()
        Me.TextBox5 = New System.Windows.Forms.TextBox()
        Me.Label55 = New System.Windows.Forms.Label()
        Me.cmbOpcion = New System.Windows.Forms.ComboBox()
        Me.Label54 = New System.Windows.Forms.Label()
        Me.txtObservaciones = New System.Windows.Forms.TextBox()
        Me.Label48 = New System.Windows.Forms.Label()
        Me.txtUbicacion = New System.Windows.Forms.TextBox()
        Me.Label47 = New System.Windows.Forms.Label()
        Me.gcTotal = New DevExpress.XtraEditors.GroupControl()
        Me.txtFinal = New System.Windows.Forms.TextBox()
        Me.Label50 = New System.Windows.Forms.Label()
        Me.txtIVA = New System.Windows.Forms.TextBox()
        Me.Label49 = New System.Windows.Forms.Label()
        Me.txtTotalP = New System.Windows.Forms.TextBox()
        Me.Label46 = New System.Windows.Forms.Label()
        CType(Me.gcItem, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gcItem.SuspendLayout()
        CType(Me.gcTotal, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gcTotal.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnGuardar
        '
        Me.btnGuardar.Appearance.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold)
        Me.btnGuardar.Appearance.Options.UseFont = True
        Me.btnGuardar.Cursor = System.Windows.Forms.Cursors.Default
        Me.btnGuardar.Location = New System.Drawing.Point(946, 662)
        Me.btnGuardar.LookAndFeel.SkinName = "Visual Studio 2013 Dark"
        Me.btnGuardar.LookAndFeel.UseDefaultLookAndFeel = False
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(75, 27)
        Me.btnGuardar.TabIndex = 66
        Me.btnGuardar.Text = "Guardar"
        '
        'btnSalir
        '
        Me.btnSalir.Appearance.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold)
        Me.btnSalir.Appearance.Options.UseFont = True
        Me.btnSalir.Location = New System.Drawing.Point(1027, 662)
        Me.btnSalir.LookAndFeel.SkinName = "Visual Studio 2013 Dark"
        Me.btnSalir.LookAndFeel.UseDefaultLookAndFeel = False
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(75, 27)
        Me.btnSalir.TabIndex = 65
        Me.btnSalir.Text = "Cerrar"
        '
        'gcItem
        '
        Me.gcItem.Controls.Add(Me.TextBox5)
        Me.gcItem.Controls.Add(Me.Label55)
        Me.gcItem.Controls.Add(Me.cmbOpcion)
        Me.gcItem.Controls.Add(Me.Label54)
        Me.gcItem.Controls.Add(Me.txtObservaciones)
        Me.gcItem.Controls.Add(Me.Label48)
        Me.gcItem.Controls.Add(Me.txtUbicacion)
        Me.gcItem.Controls.Add(Me.Label47)
        Me.gcItem.Location = New System.Drawing.Point(12, 12)
        Me.gcItem.LookAndFeel.SkinName = "Visual Studio 2013 Dark"
        Me.gcItem.LookAndFeel.UseDefaultLookAndFeel = False
        Me.gcItem.Name = "gcItem"
        Me.gcItem.Size = New System.Drawing.Size(1090, 88)
        Me.gcItem.TabIndex = 68
        Me.gcItem.Text = "Datos Item:"
        '
        'TextBox5
        '
        Me.TextBox5.Font = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox5.Location = New System.Drawing.Point(99, 24)
        Me.TextBox5.Multiline = True
        Me.TextBox5.Name = "TextBox5"
        Me.TextBox5.Size = New System.Drawing.Size(433, 25)
        Me.TextBox5.TabIndex = 62
        '
        'Label55
        '
        Me.Label55.AutoSize = True
        Me.Label55.Font = New System.Drawing.Font("Cambria", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label55.Location = New System.Drawing.Point(6, 29)
        Me.Label55.Name = "Label55"
        Me.Label55.Size = New System.Drawing.Size(87, 16)
        Me.Label55.TabIndex = 61
        Me.Label55.Text = "Descripción:"
        '
        'cmbOpcion
        '
        Me.cmbOpcion.DisplayMember = "0"
        Me.cmbOpcion.Font = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbOpcion.FormattingEnabled = True
        Me.cmbOpcion.Items.AddRange(New Object() {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9"})
        Me.cmbOpcion.Location = New System.Drawing.Point(574, 23)
        Me.cmbOpcion.Name = "cmbOpcion"
        Me.cmbOpcion.Size = New System.Drawing.Size(61, 26)
        Me.cmbOpcion.TabIndex = 60
        '
        'Label54
        '
        Me.Label54.AutoSize = True
        Me.Label54.Font = New System.Drawing.Font("Cambria", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label54.Location = New System.Drawing.Point(538, 29)
        Me.Label54.Name = "Label54"
        Me.Label54.Size = New System.Drawing.Size(30, 16)
        Me.Label54.TabIndex = 31
        Me.Label54.Text = "Op:"
        '
        'txtObservaciones
        '
        Me.txtObservaciones.Font = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtObservaciones.Location = New System.Drawing.Point(99, 55)
        Me.txtObservaciones.Multiline = True
        Me.txtObservaciones.Name = "txtObservaciones"
        Me.txtObservaciones.Size = New System.Drawing.Size(986, 26)
        Me.txtObservaciones.TabIndex = 30
        '
        'Label48
        '
        Me.Label48.AutoSize = True
        Me.Label48.Font = New System.Drawing.Font("Cambria", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label48.Location = New System.Drawing.Point(3, 61)
        Me.Label48.Name = "Label48"
        Me.Label48.Size = New System.Drawing.Size(90, 16)
        Me.Label48.TabIndex = 29
        Me.Label48.Text = "Observación:"
        '
        'txtUbicacion
        '
        Me.txtUbicacion.Font = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtUbicacion.Location = New System.Drawing.Point(721, 24)
        Me.txtUbicacion.Multiline = True
        Me.txtUbicacion.Name = "txtUbicacion"
        Me.txtUbicacion.Size = New System.Drawing.Size(364, 25)
        Me.txtUbicacion.TabIndex = 28
        '
        'Label47
        '
        Me.Label47.AutoSize = True
        Me.Label47.Font = New System.Drawing.Font("Cambria", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label47.Location = New System.Drawing.Point(641, 29)
        Me.Label47.Name = "Label47"
        Me.Label47.Size = New System.Drawing.Size(74, 16)
        Me.Label47.TabIndex = 27
        Me.Label47.Text = "Ubicación:"
        '
        'gcTotal
        '
        Me.gcTotal.Controls.Add(Me.txtFinal)
        Me.gcTotal.Controls.Add(Me.Label50)
        Me.gcTotal.Controls.Add(Me.txtIVA)
        Me.gcTotal.Controls.Add(Me.Label49)
        Me.gcTotal.Controls.Add(Me.txtTotalP)
        Me.gcTotal.Controls.Add(Me.Label46)
        Me.gcTotal.Location = New System.Drawing.Point(338, 633)
        Me.gcTotal.LookAndFeel.SkinName = "Visual Studio 2013 Dark"
        Me.gcTotal.LookAndFeel.UseDefaultLookAndFeel = False
        Me.gcTotal.Name = "gcTotal"
        Me.gcTotal.Size = New System.Drawing.Size(448, 56)
        Me.gcTotal.TabIndex = 67
        Me.gcTotal.Text = "Total:"
        '
        'txtFinal
        '
        Me.txtFinal.Location = New System.Drawing.Point(340, 24)
        Me.txtFinal.Name = "txtFinal"
        Me.txtFinal.Size = New System.Drawing.Size(90, 20)
        Me.txtFinal.TabIndex = 32
        '
        'Label50
        '
        Me.Label50.AutoSize = True
        Me.Label50.Font = New System.Drawing.Font("Cambria", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label50.Location = New System.Drawing.Point(292, 24)
        Me.Label50.Name = "Label50"
        Me.Label50.Size = New System.Drawing.Size(43, 16)
        Me.Label50.TabIndex = 31
        Me.Label50.Text = "Final:"
        '
        'txtIVA
        '
        Me.txtIVA.Location = New System.Drawing.Point(196, 24)
        Me.txtIVA.Name = "txtIVA"
        Me.txtIVA.Size = New System.Drawing.Size(90, 20)
        Me.txtIVA.TabIndex = 30
        '
        'Label49
        '
        Me.Label49.AutoSize = True
        Me.Label49.Font = New System.Drawing.Font("Cambria", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label49.Location = New System.Drawing.Point(157, 24)
        Me.Label49.Name = "Label49"
        Me.Label49.Size = New System.Drawing.Size(34, 16)
        Me.Label49.TabIndex = 29
        Me.Label49.Text = "IVA:"
        '
        'txtTotalP
        '
        Me.txtTotalP.Location = New System.Drawing.Point(61, 24)
        Me.txtTotalP.Name = "txtTotalP"
        Me.txtTotalP.Size = New System.Drawing.Size(90, 20)
        Me.txtTotalP.TabIndex = 28
        '
        'Label46
        '
        Me.Label46.AutoSize = True
        Me.Label46.Font = New System.Drawing.Font("Cambria", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label46.Location = New System.Drawing.Point(4, 24)
        Me.Label46.Name = "Label46"
        Me.Label46.Size = New System.Drawing.Size(53, 16)
        Me.Label46.TabIndex = 27
        Me.Label46.Text = "Precio:"
        '
        'frmSillones
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Black
        Me.ClientSize = New System.Drawing.Size(1120, 701)
        Me.Controls.Add(Me.gcItem)
        Me.Controls.Add(Me.gcTotal)
        Me.Controls.Add(Me.btnGuardar)
        Me.Controls.Add(Me.btnSalir)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "frmSillones"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "frmSillones"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.gcItem, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gcItem.ResumeLayout(False)
        Me.gcItem.PerformLayout()
        CType(Me.gcTotal, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gcTotal.ResumeLayout(False)
        Me.gcTotal.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnGuardar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnSalir As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents gcItem As DevExpress.XtraEditors.GroupControl
    Friend WithEvents TextBox5 As System.Windows.Forms.TextBox
    Friend WithEvents Label55 As System.Windows.Forms.Label
    Friend WithEvents cmbOpcion As System.Windows.Forms.ComboBox
    Friend WithEvents Label54 As System.Windows.Forms.Label
    Friend WithEvents txtObservaciones As System.Windows.Forms.TextBox
    Friend WithEvents Label48 As System.Windows.Forms.Label
    Friend WithEvents txtUbicacion As System.Windows.Forms.TextBox
    Friend WithEvents Label47 As System.Windows.Forms.Label
    Friend WithEvents gcTotal As DevExpress.XtraEditors.GroupControl
    Friend WithEvents txtFinal As System.Windows.Forms.TextBox
    Friend WithEvents Label50 As System.Windows.Forms.Label
    Friend WithEvents txtIVA As System.Windows.Forms.TextBox
    Friend WithEvents Label49 As System.Windows.Forms.Label
    Friend WithEvents txtTotalP As System.Windows.Forms.TextBox
    Friend WithEvents Label46 As System.Windows.Forms.Label
End Class
