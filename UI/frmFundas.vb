﻿Public Class frmFundas
    Private UnGestorProducto As New Modelo.GestorProducto
    Private UnGestorArticulo As New Modelo.GestorArticulo
    Private UnGestorItem As New Modelo.GestorItem

    Public Operacion As Integer = 0 ' 0=Alta, 1=Modificacion
    Private Const Categoria As Integer = 3 'Cortinas y sistemas = 1, Telas = 2, Fundas y almohadones = 3, Sillones = 4

    '------ EVENTOS DE FORMULARIOS ------
    Private Sub frmFundas_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        ConfigurarForm()
        ConfigurarTextBox()

        gcFundaSillon.Enabled = False
        gcFundaSillon.Hide()
        gcFundaAlmohadon.Enabled = False
        gcFundaAlmohadon.Hide()
        gcRellenoAlmohadon.Enabled = False
        gcRellenoAlmohadon.Hide()

        chkFundaSillon.Checked = False
        chkFundaAl.Checked = False
        chkRellenoAl.Checked = False

        LimpiarCasilleros()
    End Sub

    '------ CONFIGURACIONES ------
    Public Sub ConfigurarForm()
        Me.Dock = DockStyle.None
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.ShowIcon = False
        Me.MaximumSize = New Size(frmPrincipal.ClientSize.Width - 4, frmPrincipal.ClientSize.Height - 4)
        Me.MinimumSize = Me.MaximumSize
        Me.Dock = DockStyle.Fill
        Dim Separacion As Integer = (frmPrincipal.ClientSize.Width - 1090 + 150) / 2
        gcItem.Location = New Point(Separacion, 12)
        ReconfigurarGroupControl(0)
        'lblTitulo.Location = New Point((frmPrincipal.ClientSize.Width - 384 + 150) / 2, 4)
        Dim Separacionbtn As Integer = (frmPrincipal.ClientSize.Width - Separacion + 75)
        gcTotal.Location = New Point((frmPrincipal.ClientSize.Width - 448) / 2, frmPrincipal.ClientSize.Height - 66)
        btnGuardar.Location = New Point(Separacionbtn - 81, frmPrincipal.ClientSize.Height - 37)
        btnSalir.Location = New Point(Separacionbtn, frmPrincipal.ClientSize.Height - 37)
    End Sub
    Public Sub ReconfigurarGroupControl(Posicion As Integer)
        Dim Separacion As Integer = (frmPrincipal.ClientSize.Width - 1090 + 150) / 2
        gcFundaSillon.Location = New Point(Separacion, 126 - Posicion)
        gcFundaAlmohadon.Location = New Point(Separacion, 238 - Posicion)
        gcRellenoAlmohadon.Location = New Point(Separacion, 350 - Posicion)
    End Sub
    Public Sub ConfigurarTextBox()
        'Configuro la cantidad de cararcteres maximos
        'txtNombre.MaxLength = 200
        'txtDireccion.MaxLength = 150
        'txtCP.MaxLength = 8
        'txtEmpresa.MaxLength = 100
        'txtWeb.MaxLength = 75
        'txtMail1.MaxLength = 50
        'txtMail2.MaxLength = 50
        'txtMail3.MaxLength = 50
        'Configuro alineacion del texto
        'txtTotalP.TextAlign = HorizontalAlignment.Center
        'txtSistemaP.TextAlign = HorizontalAlignment.Center
        'txtSoportesP.TextAlign = HorizontalAlignment.Center
        'txtArgollasP.TextAlign = HorizontalAlignment.Center
        'txtTerminalesP.TextAlign = HorizontalAlignment.Center
        'txtCabezal1P.TextAlign = HorizontalAlignment.Center
        'txtGenero1P.TextAlign = HorizontalAlignment.Center
        'txtForro1P.TextAlign = HorizontalAlignment.Center
        'txtAnchoI1.TextAlign = HorizontalAlignment.Center
        'txtLargoI1.TextAlign = HorizontalAlignment.Center
        'txtAnchoD1.TextAlign = HorizontalAlignment.Center
        'txtLargoD1.TextAlign = HorizontalAlignment.Center
        'txtCabezal2P.TextAlign = HorizontalAlignment.Center
        'txtGenero2P.TextAlign = HorizontalAlignment.Center
        'txtForro2P.TextAlign = HorizontalAlignment.Center
        'txtAnchoI2.TextAlign = HorizontalAlignment.Center
        'txtLargoI2.TextAlign = HorizontalAlignment.Center
        'txtAnchoD2.TextAlign = HorizontalAlignment.Center
        'txtLargoD2.TextAlign = HorizontalAlignment.Center
        'txtGeneroQL1.TextAlign = HorizontalAlignment.Center
        'txtForroQL1.TextAlign = HorizontalAlignment.Center
        'txtGeneroQL2.TextAlign = HorizontalAlignment.Center
        'txtForroQL2.TextAlign = HorizontalAlignment.Center
        'txtIVA.TextAlign = HorizontalAlignment.Center
        'txtFinal.TextAlign = HorizontalAlignment.Center
        'txtForroQA1.TextAlign = HorizontalAlignment.Center
        'txtForroQA2.TextAlign = HorizontalAlignment.Center
        'txtGeneroQA1.TextAlign = HorizontalAlignment.Center
        'txtGeneroQA2.TextAlign = HorizontalAlignment.Center
        'txtCabezal1Q.TextAlign = HorizontalAlignment.Center
        'txtCabezal2Q.TextAlign = HorizontalAlignment.Center
        'txtSistemaQ.TextAlign = HorizontalAlignment.Center
        'txtSubSistP.TextAlign = HorizontalAlignment.Center
        'txtSubCort1P.TextAlign = HorizontalAlignment.Center
        'txtSubCort2P.TextAlign = HorizontalAlignment.Center
        'Configuro los que seran siempre solo lectura
        'txtCabezal1Q.ReadOnly = True
        'txtCabezal2Q.ReadOnly = True
        'txtGeneroQA1.ReadOnly = True
        'txtForroQA1.ReadOnly = True
        'txtGeneroQA2.ReadOnly = True
        'txtForroQA2.ReadOnly = True
    End Sub
    Private Sub LimpiarCasilleros()
        'cmbSistema.SelectedIndex = -1
        'cmbSoportes.SelectedIndex = -1
        'cmbArgollas.SelectedIndex = -1
        'cmbTerminales.SelectedIndex = -1
        'cmbCabezal1.SelectedIndex = -1
        'cmbGenero1.SelectedIndex = -1
        'cmbForro1.SelectedIndex = -1
        'cmbCabezal2.SelectedIndex = -1
        'cmbGenero2.SelectedIndex = -1
        'cmbForro2.SelectedIndex = -1

        'txtSistemaP.Text = Funciones.Precio("0")
        'txtSoportesP.Text = Funciones.Precio("0")
        'txtArgollasP.Text = Funciones.Precio("0")
        'txtTerminalesP.Text = Funciones.Precio("0")
        'txtSubSistP.Text = Funciones.Precio("0")
        'txtCabezal1P.Text = Funciones.Precio("0")
        'txtGenero1P.Text = Funciones.Precio("0")
        'txtForro1P.Text = Funciones.Precio("0")
        'txtAnchoI1.Text = "0"
        'txtLargoI1.Text = "0"
        'txtAnchoD1.Text = "0"
        'txtLargoD1.Text = "0"
        'txtGeneroQL1.Text = "0"
        'txtForroQL1.Text = "0"
        'txtSubCort1P.Text = Funciones.Precio("0")
        'txtCabezal2P.Text = Funciones.Precio("0")
        'txtGenero2P.Text = Funciones.Precio("0")
        'txtForro2P.Text = Funciones.Precio("0")
        'txtAnchoI2.Text = "0"
        'txtLargoI2.Text = "0"
        'txtAnchoD2.Text = "0"
        'txtLargoD2.Text = "0"
        'txtGeneroQL2.Text = "0"
        'txtForroQL2.Text = "0"
        'txtSubCort2P.Text = Funciones.Precio("0")
        'txtTotalP.Text = Funciones.Precio("0")
        'txtIVA.Text = Funciones.Precio(txtIVA.Text)
        'txtFinal.Text = Funciones.Precio("0")
    End Sub

    '------ EVENTOS DE BOTONES ------
    Private Sub btnSalir_Click(sender As Object, e As EventArgs) Handles btnSalir.Click
        Me.Close()
        frmPresupuesto.Show()
        frmPresupuesto.SendToBack()
        frmDockBotones.BringToFront()
    End Sub
    Private Sub btnGuardar_Click(sender As Object, e As EventArgs) Handles btnGuardar.Click
        Dim UnItem As New Modelo.Item
        'Guardado del Item
        UnItem.IdPresupuesto = frmPresupuesto.PresupuestoId
        UnItem.Item = frmPresupuesto.Item
        'UnItem.Descripcion = txtDescripcion.Text
        UnItem.Observacion = txtObservaciones.Text
        UnItem.Precio = txtTotalP.Text
        UnItem.IdCategoria = Categoria
        UnItem.Ubicacion = txtUbicacion.Text
        UnItem.Opcion = cmbOpcion.Text
        Select Case Operacion
            Case 0 'Alta
                UnGestorItem.Nuevo(UnItem)
            Case 1 'Modificacion
                UnItem.IdItem = Double.Parse(frmPresupuesto.Item)
                UnGestorItem.Modificar(UnItem)
        End Select
        'Guardado de los Articulos
        Dim Items As Double = frmPresupuesto.Item

        'If txtGeneroQL1.Text <> "0" Then
        'GrabarArticulo(CDbl(Items + 0.01), CStr(cmbGenero1.Text), CDbl(txtGeneroQA1.Text), CDbl(txtGeneroQL1.Text), 0, CostoGenero1, CDbl(txtGenero1P.Text), CStr(txtGenero1O.Text)) 'GENERO1
        'End If
        frmPresupuesto.ActualizarListaItems()
        Operacion = 0
        Close()
        frmPresupuesto.Dock = DockStyle.Fill
        frmPresupuesto.Show()
    End Sub

    '---- EVENTOS DE CHECKBOX ------
    Private Sub chkFundaSillon_CheckStateChanged(sender As Object, e As EventArgs) Handles chkFundaSillon.CheckStateChanged
        If chkFundaSillon.Checked = True Then
            ReconfigurarGroupControl(0)
            gcFundaSillon.Enabled = True
            gcFundaSillon.Show()
            'CargarcmbSistema()
            'CargarcmbArgollas()
            'CargarcmbSoportes()
            'CargarcmbTerminales()
        Else
            gcFundaSillon.Enabled = False
            gcFundaSillon.Hide()
            ReconfigurarGroupControl(112)
        End If
    End Sub
    Private Sub chkFundaAl_CheckStateChanged(sender As Object, e As EventArgs) Handles chkFundaAl.CheckStateChanged
        If chkFundaAl.Checked = True Then
            If chkFundaSillon.Checked = True Then
            Else
                ReconfigurarGroupControl(112)
            End If
            gcFundaAlmohadon.Enabled = True
            gcFundaAlmohadon.Show()
            'CargarcmbCabezal1()
            'CargarcmbGenero1()
            chkRellenoAl.Enabled = True
        Else
            chkRellenoAl.CheckState = CheckState.Unchecked
            gcFundaAlmohadon.Enabled = False
            gcFundaAlmohadon.Hide()
            chkRellenoAl.Enabled = False
        End If
    End Sub
    Private Sub chkRellenoAl_CheckStateChanged(sender As Object, e As EventArgs) Handles chkRellenoAl.CheckStateChanged
        If chkRellenoAl.Checked = True Then
            gcRellenoAlmohadon.Enabled = True
            gcRellenoAlmohadon.Show()
            'CargarcmbCabezal2()
            'CargarcmbGenero2()
        Else
            gcRellenoAlmohadon.Enabled = False
            gcRellenoAlmohadon.Hide()
        End If
    End Sub

    '------ EVENTOS GENERALES ------
    Private Sub GrabarArticulo(Item As Double, Nom As String, Dm1 As Double, Dm2 As Double, Dm3 As Double, Cst As Double, Precio As Double, Obs As String)

        Dim UnArticulo As New Modelo.Articulo       'Guardado del Articulo
        UnArticulo.IdPresupuesto = frmPresupuesto.PresupuestoId
        UnArticulo.Item = Item
        UnArticulo.Nombre = Nom
        UnArticulo.Dim1 = Dm1
        UnArticulo.Dim2 = Dm2
        UnArticulo.Dim3 = Dm3
        UnArticulo.Costo = Cst
        UnArticulo.Precio = Precio
        UnArticulo.Observacion = Obs


        Select Case Operacion
            Case 0 'Alta
                UnGestorArticulo.Nuevo(UnArticulo)
            Case 1 'Modificacion
                UnArticulo.Item = Double.Parse(frmPresupuesto.Item)
                UnArticulo.IdPresupuesto = frmPresupuesto.PresupuestoId
                UnGestorArticulo.Modificar(UnArticulo)
        End Select
    End Sub
End Class