﻿Public Class frmClientes
    Private Funciones As New Funciones
    Private Operacion As Integer = 0 ' 0=Alta, 1=Modificacion
    Private ABM As Boolean = False 'True=Alta, False=Modificacion

    '------ EVENTOS DE FORMULARIOS ------
    Private Sub Clientes_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        ConfigurarForm()
        ConfigurarTextBox()
        ConfigurarDataGridView()

        ActualizarCmbBuscar()
        ActualizarCmbciudad()

        LimpiarCasilleros()

        ModoNormal()
        ModoReadOnly()

        cmbBuscar.SelectedIndex = -1
        cmbBuscar.Focus()
    End Sub
    Private Sub frmClientes_Click(sender As Object, e As EventArgs) Handles MyBase.Click, MyBase.GotFocus
        Me.SendToBack()
    End Sub

    '------ EVENTOS DE BOTONOES ------
    Private Sub btnNuevo_Click(sender As System.Object, e As System.EventArgs) Handles btnNuevo.Click
        Operacion = 0
        ModoAbm()
        ModoEditar()
        LimpiarCasilleros()
        txtNombre.Focus()
    End Sub
    Private Sub btnEliminar_Click(sender As Object, e As EventArgs) Handles btnEliminar.Click
        If txtId.Text = "" Then
        Else
            Dim UnCliente As New Modelo.Cliente
            UnCliente.IdCliente = Integer.Parse(txtId.Text)
            If MsgBox(String.Format("¿Desea eliminar el cliente {0}?", txtNombre.Text), vbOKCancel, "Eliminar Cliente") = vbOK Then
                Dim UnGestorCliente As New Modelo.GestorClientes
                UnGestorCliente.Eliminar(UnCliente)
                LimpiarCasilleros()
                ModoNormal()
                ModoReadOnly()
            End If
        End If
        Operacion = 1
    End Sub
    Private Sub btnSalir_Click(sender As Object, e As EventArgs) Handles btnSalir.Click
        ModoReadOnly()
        Operacion = 0
        Me.Close()
    End Sub
    Private Sub btnGuardar_Click(sender As Object, e As EventArgs) Handles btnGuardar.Click
        Dim UnGestorCliente As New Modelo.GestorClientes
        Dim UnCliente As New Modelo.Cliente

        UnCliente.Nombre = txtNombre.Text
        UnCliente.Direccion = txtDireccion.Text
        UnCliente.IdCiudad = cmbCiudad.SelectedIndex
        UnCliente.CP = txtCP.Text
        UnCliente.Telefono1 = Funciones.SoloNumeros(txtTelefono1.Text)
        UnCliente.Telefono2 = Funciones.SoloNumeros(txtTelefono2.Text)
        UnCliente.Telefono3 = Funciones.SoloNumeros(txtTelefono3.Text)
        UnCliente.Movil1 = Funciones.SoloNumeros(txtMovil1.Text)
        UnCliente.Movil2 = Funciones.SoloNumeros(txtMovil2.Text)
        UnCliente.Movil3 = Funciones.SoloNumeros(txtMovil3.Text)
        UnCliente.Mail1 = txtMail1.Text
        UnCliente.Mail2 = txtMail2.Text
        UnCliente.Mail3 = txtMail3.Text
        UnCliente.RazonSocial = txtEmpresa.Text
        UnCliente.Cuit = Funciones.SoloNumeros(txtCUIT.Text)
        UnCliente.DNI = Funciones.SoloNumeros(txtDNI.Text)
        UnCliente.Web = txtWeb.Text
        UnCliente.Contacto1 = txtContacto1.Text
        UnCliente.Contacto2 = txtContacto2.Text
        UnCliente.Contacto3 = txtContacto3.Text
        UnCliente.FechaNac = dtpFechaNac.Text

        Select Case Operacion
            Case 0 ' ALTA
                UnGestorCliente.Nuevo(UnCliente)
            Case 1
                ' MODIFICACION
                UnCliente.IdCliente = Integer.Parse(txtId.Text)
                UnGestorCliente.Modificar(UnCliente)
        End Select

        ModoNormal()
        ModoReadOnly()
        ActualizarCmbBuscar()

        Operacion = 1
    End Sub
    Private Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        Try
            Dim ClienteSeleccionado As New Modelo.Cliente
            Dim UnGestorCliente As New Modelo.GestorClientes
            ClienteSeleccionado.IdCliente = txtId.Text
            ClienteSeleccionado = UnGestorCliente.BuscarPorId(ClienteSeleccionado)
            MostrarDetalleDeCliente(ClienteSeleccionado)
        Catch
        End Try

        ModoNormal()
        ModoReadOnly()
        Operacion = 1
    End Sub

    '------ EVENTOS DE COMBOBOX ------
    Private Sub cmbBuscar_SelectionChangeCommitted(sender As Object, e As EventArgs) Handles cmbBuscar.SelectionChangeCommitted
        Dim ClienteSeleccionado As New Modelo.Cliente
        Dim UnGestorCliente As New Modelo.GestorClientes
        Dim Index As Integer

        Index = cmbBuscar.SelectedIndex
        ClienteSeleccionado = UnGestorCliente.BuscarPorId(cmbBuscar.SelectedItem)
        MostrarDetalleDeCliente(ClienteSeleccionado)

        ModoNormal()
        ModoReadOnly()

        Operacion = 1
        cmbBuscar.SelectedIndex = Index
    End Sub

    '------ FUNCIONES PARA COMBOBOX ------
    Private Sub ActualizarCmbBuscar()
        Dim UnGestorCliente As New Modelo.GestorClientes
        cmbBuscar.DataSource = UnGestorCliente.ListarTodosNombre()
        cmbBuscar.DisplayMember = "Nombre"
    End Sub
    Private Sub ActualizarCmbciudad()
        Dim UnGestorCiudad As New Modelo.GestorCiudad
        cmbCiudad.DataSource = UnGestorCiudad.ListarTodasCiudades
        cmbCiudad.DisplayMember = "Localidad"
    End Sub

    '------ FUNCIONES GENERALES ------
    Public Sub MostrarDetalleDeCliente(UnCliente As Modelo.Cliente)
        Try
            txtId.Text = UnCliente.IdCliente
            txtNombre.Text = UnCliente.Nombre
            txtDireccion.Text = UnCliente.Direccion
            If UnCliente.IdCiudad = -1 Then
                cmbCiudad.SelectedIndex = -1
            Else
                Dim UnaCiudad As New Modelo.Ciudad With {.IdCiudad = UnCliente.IdCiudad}
                Dim UnGestorCiudad As New Modelo.GestorCiudad
                cmbCiudad.Text = UnGestorCiudad.BuscarPorId(UnaCiudad).Localidad
            End If
            txtCP.Text = UnCliente.CP
            txtTelefono1.Text = Funciones.Telefono(CStr(UnCliente.Telefono1))
            txtTelefono2.Text = Funciones.Telefono(CStr(UnCliente.Telefono2))
            txtTelefono3.Text = Funciones.Telefono(CStr(UnCliente.Telefono3))
            txtMovil1.Text = Funciones.Telefono(CStr(UnCliente.Movil1))
            txtMovil2.Text = Funciones.Telefono(CStr(UnCliente.Movil2))
            txtMovil3.Text = Funciones.Telefono(CStr(UnCliente.Movil3))
            txtMail1.Text = UnCliente.Mail1
            txtMail2.Text = UnCliente.Mail2
            txtMail3.Text = UnCliente.Mail3
            txtEmpresa.Text = UnCliente.RazonSocial
            txtCUIT.Text = Funciones.CUIT(CStr(UnCliente.Cuit))
            txtDNI.Text = Funciones.DNI(CStr(UnCliente.DNI))
            txtWeb.Text = UnCliente.Web
            txtContacto1.Text = UnCliente.Contacto1
            txtContacto2.Text = UnCliente.Contacto2
            txtContacto3.Text = UnCliente.Contacto3
            dtpFechaNac.Text = UnCliente.FechaNac
        Catch
            MsgBox("Se ha producido un error al intentar cargar el cliente", MsgBoxStyle.OkOnly)
        End Try

        Try
            Dim unAccesoPresupuesto As New Modelo.DbPresupuesto
            dgvHistorial.DataSource = unAccesoPresupuesto.SelectPresupuestoPorCliente(cmbBuscar.SelectedItem)
            dgvHistorial.Columns(1).Visible = False
            dgvHistorial.Columns(3).Visible = False
        Catch
        End Try

    End Sub

    '------ FUNCIONES DE TEXTBOX ------
    Private Sub LimpiarCasilleros()
        cmbCiudad.SelectedIndex = -1
        txtId.Text = ""
        txtNombre.Text = ""
        txtDireccion.Text = ""
        txtCP.Text = ""
        txtTelefono1.Text = ""
        txtTelefono2.Text = ""
        txtTelefono3.Text = ""
        txtMovil1.Text = ""
        txtMovil2.Text = ""
        txtMovil3.Text = ""
        txtMail1.Text = ""
        txtMail2.Text = ""
        txtMail3.Text = ""
        txtEmpresa.Text = ""
        txtCUIT.Text = ""
        txtDNI.Text = ""
        txtWeb.Text = ""
        dgvHistorial.Text = ""
        txtContacto2.Text = ""
        txtContacto3.Text = ""
        dtpFechaNac.Text = "1/1/1900"
    End Sub
    Private Sub Solonumeros(sender As Object, e As KeyPressEventArgs) Handles txtDNI.KeyPress, txtCUIT.KeyPress, txtMovil1.KeyPress, txtMovil2.KeyPress, txtMovil3.KeyPress
        If Not (Char.IsNumber(e.KeyChar) Or Char.IsControl(e.KeyChar)) Then e.Handled = True
    End Sub
    Private Sub txtCUIT_Leave(sender As Object, e As EventArgs) Handles txtCUIT.Leave
        txtCUIT.Text = Funciones.CUIT(txtCUIT.Text)
    End Sub
    Private Sub txtDNI_Leave(sender As Object, e As EventArgs) Handles txtDNI.Leave
        txtDNI.Text = Funciones.DNI(txtDNI.Text)
    End Sub
    Private Sub txtTelefono1_Leave(sender As Object, e As EventArgs)
        txtTelefono1.Text = Funciones.Telefono(txtTelefono1.Text)
    End Sub
    Private Sub txtTelefono2_Leave(sender As Object, e As EventArgs)
        txtTelefono2.Text = Funciones.Telefono(txtTelefono2.Text)
    End Sub
    Private Sub txtTelefono3_Leave(sender As Object, e As EventArgs)
        txtTelefono3.Text = Funciones.Telefono(txtTelefono3.Text)
    End Sub
    Private Sub txtMovil1_Leave(sender As Object, e As EventArgs) Handles txtMovil1.Leave
        txtMovil1.Text = Funciones.Telefono(txtMovil1.Text)
    End Sub
    Private Sub txtMovil2_Leave(sender As Object, e As EventArgs) Handles txtMovil2.Leave
        txtMovil2.Text = Funciones.Telefono(txtMovil2.Text)
    End Sub
    Private Sub txtMovil3_Leave(sender As Object, e As EventArgs) Handles txtMovil3.Leave
        txtMovil3.Text = Funciones.Telefono(txtMovil3.Text)
    End Sub

    '------ FUNCIONES DE FUNCIONAMIENTO ------
    Public Sub ModoNormal()
        Dim UnGestorCliente As New Modelo.GestorClientes
        ActualizarCmbBuscar()

        If txtId.Text <> "" Then
            Dim UnCliente As New Modelo.Cliente
            UnCliente.IdCliente = CInt(txtId.Text)
            Dim ClienteSeleccionado As New Modelo.Cliente
            ClienteSeleccionado = UnGestorCliente.BuscarPorId(UnCliente)
            MostrarDetalleDeCliente(ClienteSeleccionado)
        End If

        btnNuevo.Enabled = True
        btnGuardar.Enabled = False
        btnEliminar.Enabled = True
        btnCancelar.Enabled = False
    End Sub
    Public Sub ModoReadOnly()
        cmbBuscar.Enabled = True
        cmbCiudad.Enabled = False

        dtpFechaNac.Enabled = False

        txtId.ReadOnly = True
        txtNombre.ReadOnly = True
        txtDireccion.ReadOnly = True
        txtCP.ReadOnly = True
        txtTelefono1.ReadOnly = True
        txtTelefono2.ReadOnly = True
        txtTelefono3.ReadOnly = True
        txtMovil1.ReadOnly = True
        txtMovil2.ReadOnly = True
        txtMovil3.ReadOnly = True
        txtMail1.ReadOnly = True
        txtMail2.ReadOnly = True
        txtMail3.ReadOnly = True
        txtEmpresa.ReadOnly = True
        txtCUIT.ReadOnly = True
        txtDNI.ReadOnly = True
        txtWeb.ReadOnly = True
        dgvHistorial.ReadOnly = True
        txtContacto2.ReadOnly = True
        txtContacto3.ReadOnly = True

        ABM = False
    End Sub
    Public Sub ModoAbm()
        btnNuevo.Enabled = False
        btnGuardar.Enabled = True
        btnEliminar.Enabled = False
        btnCancelar.Enabled = True
    End Sub
    Public Sub ModoEditar()
        cmbCiudad.Enabled = True
        cmbBuscar.Enabled = False

        dtpFechaNac.Enabled = True

        txtNombre.ReadOnly = False
        txtDireccion.ReadOnly = False
        txtCP.ReadOnly = False
        txtTelefono1.ReadOnly = False
        txtTelefono2.ReadOnly = False
        txtTelefono3.ReadOnly = False
        txtMovil1.ReadOnly = False
        txtMovil2.ReadOnly = False
        txtMovil3.ReadOnly = False
        txtMail1.ReadOnly = False
        txtMail2.ReadOnly = False
        txtMail3.ReadOnly = False
        txtEmpresa.ReadOnly = False
        txtCUIT.ReadOnly = False
        txtDNI.ReadOnly = False
        txtWeb.ReadOnly = False
        dgvHistorial.ReadOnly = False
        txtContacto2.ReadOnly = False
        txtContacto3.ReadOnly = False

        ABM = True
    End Sub

    '------ CONFIGURACIONES ------
    Public Sub ConfigurarForm()
        Me.Dock = DockStyle.None
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.ShowIcon = False
        Me.MaximumSize = New Size(frmPrincipal.ClientSize.Width - 4, frmPrincipal.ClientSize.Height - 4)
        Me.MinimumSize = Me.MaximumSize
        Me.Dock = DockStyle.Fill
        Dim Separacion As Integer = (frmPrincipal.ClientSize.Width - 1090 + 150) / 2
        gcBuscar.Location = New Point(Separacion, 45)
        gcInfoGral.Location = New Point(Separacion, 88)
        gcDomicilio.Location = New Point(Separacion, 216)
        gcContacto.Location = New Point(Separacion, 278)
        gcHistorial.Size = New Size(1009, frmPrincipal.ClientSize.Height - (464 + 10))
        gcHistorial.Location = New Point(Separacion, 464)
        lblTitulo.Location = New Point((frmPrincipal.ClientSize.Width - 384 + 150) / 2, 4)
        Dim Separacionbtn As Integer = (frmPrincipal.ClientSize.Width - Separacion + 75)
        btnCancelar.Location = New Point(Separacionbtn, frmPrincipal.ClientSize.Height - 103)
        btnEliminar.Location = New Point(Separacionbtn, frmPrincipal.ClientSize.Height - 70)
        btnSalir.Location = New Point(Separacionbtn, frmPrincipal.ClientSize.Height - 37)
    End Sub
    Public Sub ConfigurarTextBox()
        'Configuro la cantidad de cararcteres maximos
        txtNombre.MaxLength = 200
        txtDireccion.MaxLength = 150
        txtCP.MaxLength = 8
        txtEmpresa.MaxLength = 100
        txtWeb.MaxLength = 75
        txtMail1.MaxLength = 50
        txtMail2.MaxLength = 50
        txtMail3.MaxLength = 50
        txtContacto1.MaxLength = 50
        txtContacto2.MaxLength = 50
        txtContacto3.MaxLength = 50
        'Configuro alineacion del texto
        txtId.TextAlign = HorizontalAlignment.Center
        txtCP.TextAlign = HorizontalAlignment.Center
        txtTelefono1.TextAlign = HorizontalAlignment.Center
        txtTelefono2.TextAlign = HorizontalAlignment.Center
        txtTelefono3.TextAlign = HorizontalAlignment.Center
        txtMovil1.TextAlign = HorizontalAlignment.Center
        txtMovil2.TextAlign = HorizontalAlignment.Center
        txtMovil3.TextAlign = HorizontalAlignment.Center
        txtMail1.TextAlign = HorizontalAlignment.Center
        txtMail2.TextAlign = HorizontalAlignment.Center
        txtMail3.TextAlign = HorizontalAlignment.Center
        txtContacto1.TextAlign = HorizontalAlignment.Center
        txtContacto2.TextAlign = HorizontalAlignment.Center
        txtContacto3.TextAlign = HorizontalAlignment.Center
        txtCUIT.TextAlign = HorizontalAlignment.Center
        txtDNI.TextAlign = HorizontalAlignment.Center
        'Configuro los que seran siempre solo lectura
        txtId.ReadOnly = True
    End Sub
    Private Sub ConfigurarDataGridView()
        dgvHistorial.Location = New Point(82, 27)
        dgvHistorial.Size = New Size(916, gcHistorial.ClientSize.Height - 33)
    End Sub

    '------ EVENTOS QUE LLAMAN FUNCIONES ------
    Private Sub txtTextChanged(sender As Object, e As EventArgs) Handles txtNombre.Click, txtDireccion.Click, txtCP.Click, txtMovil1.Click, txtMovil2.Click, txtMovil3.Click, txtEmpresa.Click, txtCUIT.Click, txtDNI.Click, txtWeb.Click, dtpFechaNac.ValueChanged, cmbCiudad.Click, txtContacto1.Click, txtContacto2.Click, txtContacto3.Click
        If ABM = True Then
        Else
            ModoAbm()
            ModoEditar()
        End If
        If Operacion = 0 Then
        Else
            Operacion = 1
        End If
    End Sub

    '------ EVENTOS DE DATAGRIDVIEW ------
    Private Sub dgvClientes_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvHistorial.CellDoubleClick
        Cursor = Cursors.WaitCursor
        Dim UnPresupuesto As New Modelo.Presupuesto
        UnPresupuesto.IdPresupuesto = CInt(dgvHistorial.CurrentRow.Cells(0).Value)
        frmPresupuesto.Show()
        frmPresupuesto.CargarPresupuesto(UnPresupuesto)
        Me.Hide()
        Cursor = Cursors.Default
    End Sub

    '------ Importacion de datos ------
    Private Sub btnImportar_Click(sender As Object, e As EventArgs) Handles btnImportar.Click
        Cursor = System.Windows.Forms.Cursors.WaitCursor
        Dim DGBox As New OpenFileDialog
        DGBox.Filter = "Archivos de texto plano (.txt)|*.txt"
        DGBox.Multiselect = False
        DGBox.ShowDialog()
        If DGBox.FileName.Length > 0 Then
            Using Archivo As New System.IO.StreamReader(DGBox.FileName)
                While Not Archivo.EndOfStream
                    Dim Texto As String
                    Dim text As String()
                    Texto = Archivo.ReadLine
                    text = Texto.Split(";")
                    btnNuevo.PerformClick()
                    txtNombre.Text = CStr(text(1))
                    txtEmpresa.Text = CStr(text(2))
                    txtWeb.Text = CStr(text(3))
                    txtDireccion.Text = CStr(text(4))
                    cmbCiudad.SelectedIndex = CInt(text(5))
                    txtCP.Text = CStr(text(6))
                    txtCUIT.Text = Funciones.SoloNumeros(CStr(text(7)))
                    txtDNI.Text = Funciones.SoloNumeros(CStr(text(8)))
                    If text(9) = "" Then
                        dtpFechaNac.Text = CDate("1/1/1900")
                    Else
                        dtpFechaNac.Text = CStr(text(9))
                    End If
                    txtTelefono1.Text = Funciones.SoloNumeros(CStr(text(10)))
                    txtTelefono2.Text = Funciones.SoloNumeros(CStr(text(11)))
                    txtTelefono3.Text = Funciones.SoloNumeros(CStr(text(12)))
                    txtMovil1.Text = Funciones.SoloNumeros(CStr(text(13)))
                    txtMovil2.Text = Funciones.SoloNumeros(CStr(text(14)))
                    txtMovil3.Text = Funciones.SoloNumeros(CStr(text(15)))
                    txtMail1.Text = CStr(text(16))
                    txtMail2.Text = CStr(text(17))
                    txtMail3.Text = CStr(text(18))
                    txtContacto1.Text = CStr(text(19))
                    txtContacto2.Text = CStr(text(20))
                    txtContacto3.Text = CStr(text(21))
                    btnGuardar.PerformClick()
                End While
            End Using
        End If
        'Mensaje("Datos importados correctamente")
        Cursor = System.Windows.Forms.Cursors.Default
    End Sub

End Class



