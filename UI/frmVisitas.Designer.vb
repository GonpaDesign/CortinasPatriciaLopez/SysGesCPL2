﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmVisitas
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.gcDatos = New DevExpress.XtraEditors.GroupControl()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.dtpHora = New System.Windows.Forms.DateTimePicker()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.dtpFecha = New System.Windows.Forms.DateTimePicker()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtObservaciones = New System.Windows.Forms.TextBox()
        Me.cmbNombre = New System.Windows.Forms.ComboBox()
        Me.txtClienteId = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.txtMail = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtCiudad = New System.Windows.Forms.TextBox()
        Me.txtMovil1 = New System.Windows.Forms.TextBox()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.txtTelefono1 = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtDireccion = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.btnNuevo = New DevExpress.XtraEditors.SimpleButton()
        Me.gcVisitas = New DevExpress.XtraEditors.GroupControl()
        Me.dgvVisitas = New System.Windows.Forms.DataGridView()
        Me.btnSalir = New DevExpress.XtraEditors.SimpleButton()
        Me.gcFiltros = New DevExpress.XtraEditors.GroupControl()
        Me.dtpFechaF = New System.Windows.Forms.DateTimePicker()
        Me.dtpFechaI = New System.Windows.Forms.DateTimePicker()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.ckbEntreFechas = New System.Windows.Forms.CheckBox()
        Me.btnActualizar = New DevExpress.XtraEditors.SimpleButton()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.btnGuardar = New DevExpress.XtraEditors.SimpleButton()
        Me.btnCancelar = New DevExpress.XtraEditors.SimpleButton()
        Me.gcBuscar = New DevExpress.XtraEditors.PanelControl()
        Me.lblTitulo = New System.Windows.Forms.Label()
        CType(Me.gcDatos, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gcDatos.SuspendLayout()
        CType(Me.gcVisitas, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gcVisitas.SuspendLayout()
        CType(Me.dgvVisitas, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gcFiltros, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gcFiltros.SuspendLayout()
        CType(Me.gcBuscar, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gcBuscar.SuspendLayout()
        Me.SuspendLayout()
        '
        'gcDatos
        '
        Me.gcDatos.Controls.Add(Me.Label5)
        Me.gcDatos.Controls.Add(Me.dtpHora)
        Me.gcDatos.Controls.Add(Me.Label11)
        Me.gcDatos.Controls.Add(Me.dtpFecha)
        Me.gcDatos.Controls.Add(Me.Label4)
        Me.gcDatos.Controls.Add(Me.txtObservaciones)
        Me.gcDatos.Controls.Add(Me.cmbNombre)
        Me.gcDatos.Controls.Add(Me.txtClienteId)
        Me.gcDatos.Controls.Add(Me.Label10)
        Me.gcDatos.Controls.Add(Me.txtMail)
        Me.gcDatos.Controls.Add(Me.Label1)
        Me.gcDatos.Controls.Add(Me.txtCiudad)
        Me.gcDatos.Controls.Add(Me.txtMovil1)
        Me.gcDatos.Controls.Add(Me.Label25)
        Me.gcDatos.Controls.Add(Me.txtTelefono1)
        Me.gcDatos.Controls.Add(Me.Label6)
        Me.gcDatos.Controls.Add(Me.Label3)
        Me.gcDatos.Controls.Add(Me.Label2)
        Me.gcDatos.Controls.Add(Me.txtDireccion)
        Me.gcDatos.Controls.Add(Me.Label8)
        Me.gcDatos.Location = New System.Drawing.Point(12, 88)
        Me.gcDatos.LookAndFeel.SkinName = "Visual Studio 2013 Dark"
        Me.gcDatos.LookAndFeel.UseDefaultLookAndFeel = False
        Me.gcDatos.Name = "gcDatos"
        Me.gcDatos.Size = New System.Drawing.Size(1090, 162)
        Me.gcDatos.TabIndex = 32
        Me.gcDatos.Text = "Datos del Cliente"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.BackColor = System.Drawing.Color.Transparent
        Me.Label5.Font = New System.Drawing.Font("Cambria", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Label5.Location = New System.Drawing.Point(914, 93)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(50, 19)
        Me.Label5.TabIndex = 76
        Me.Label5.Text = "Hora:"
        '
        'dtpHora
        '
        Me.dtpHora.Cursor = System.Windows.Forms.Cursors.Hand
        Me.dtpHora.CustomFormat = "HH:mm"
        Me.dtpHora.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpHora.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpHora.Location = New System.Drawing.Point(970, 91)
        Me.dtpHora.MinDate = New Date(1900, 1, 1, 0, 0, 0, 0)
        Me.dtpHora.Name = "dtpHora"
        Me.dtpHora.ShowUpDown = True
        Me.dtpHora.Size = New System.Drawing.Size(115, 27)
        Me.dtpHora.TabIndex = 60
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.BackColor = System.Drawing.Color.Transparent
        Me.Label11.Font = New System.Drawing.Font("Cambria", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Label11.Location = New System.Drawing.Point(5, 125)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(119, 19)
        Me.Label11.TabIndex = 73
        Me.Label11.Text = "Observaciones:"
        '
        'dtpFecha
        '
        Me.dtpFecha.CalendarFont = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpFecha.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpFecha.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFecha.Location = New System.Drawing.Point(970, 57)
        Me.dtpFecha.MaxDate = New Date(2030, 12, 31, 0, 0, 0, 0)
        Me.dtpFecha.MinDate = New Date(1900, 1, 1, 0, 0, 0, 0)
        Me.dtpFecha.Name = "dtpFecha"
        Me.dtpFecha.Size = New System.Drawing.Size(115, 27)
        Me.dtpFecha.TabIndex = 77
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.BackColor = System.Drawing.Color.Transparent
        Me.Label4.Font = New System.Drawing.Font("Cambria", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Label4.Location = New System.Drawing.Point(908, 60)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(56, 19)
        Me.Label4.TabIndex = 75
        Me.Label4.Text = "Fecha:"
        '
        'txtObservaciones
        '
        Me.txtObservaciones.Font = New System.Drawing.Font("Calibri", 12.0!)
        Me.txtObservaciones.Location = New System.Drawing.Point(129, 122)
        Me.txtObservaciones.Name = "txtObservaciones"
        Me.txtObservaciones.Size = New System.Drawing.Size(778, 27)
        Me.txtObservaciones.TabIndex = 72
        '
        'cmbNombre
        '
        Me.cmbNombre.AllowDrop = True
        Me.cmbNombre.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cmbNombre.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cmbNombre.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbNombre.Font = New System.Drawing.Font("Calibri", 12.0!)
        Me.cmbNombre.IntegralHeight = False
        Me.cmbNombre.Location = New System.Drawing.Point(129, 24)
        Me.cmbNombre.Name = "cmbNombre"
        Me.cmbNombre.Size = New System.Drawing.Size(659, 27)
        Me.cmbNombre.TabIndex = 71
        '
        'txtClienteId
        '
        Me.txtClienteId.Font = New System.Drawing.Font("Calibri", 12.0!)
        Me.txtClienteId.Location = New System.Drawing.Point(829, 24)
        Me.txtClienteId.Name = "txtClienteId"
        Me.txtClienteId.Size = New System.Drawing.Size(78, 27)
        Me.txtClienteId.TabIndex = 70
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.BackColor = System.Drawing.Color.Transparent
        Me.Label10.Font = New System.Drawing.Font("Cambria", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Label10.Location = New System.Drawing.Point(794, 27)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(29, 19)
        Me.Label10.TabIndex = 69
        Me.Label10.Text = "Id:"
        '
        'txtMail
        '
        Me.txtMail.Font = New System.Drawing.Font("Calibri", 12.0!)
        Me.txtMail.Location = New System.Drawing.Point(699, 90)
        Me.txtMail.Name = "txtMail"
        Me.txtMail.Size = New System.Drawing.Size(208, 27)
        Me.txtMail.TabIndex = 66
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Cambria", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Label1.Location = New System.Drawing.Point(647, 93)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(46, 19)
        Me.Label1.TabIndex = 65
        Me.Label1.Text = "Mail:"
        '
        'txtCiudad
        '
        Me.txtCiudad.Font = New System.Drawing.Font("Calibri", 12.0!)
        Me.txtCiudad.Location = New System.Drawing.Point(737, 57)
        Me.txtCiudad.Name = "txtCiudad"
        Me.txtCiudad.Size = New System.Drawing.Size(170, 27)
        Me.txtCiudad.TabIndex = 64
        '
        'txtMovil1
        '
        Me.txtMovil1.Font = New System.Drawing.Font("Calibri", 12.0!)
        Me.txtMovil1.Location = New System.Drawing.Point(433, 89)
        Me.txtMovil1.Name = "txtMovil1"
        Me.txtMovil1.Size = New System.Drawing.Size(208, 27)
        Me.txtMovil1.TabIndex = 63
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.BackColor = System.Drawing.Color.Transparent
        Me.Label25.Font = New System.Drawing.Font("Cambria", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Label25.Location = New System.Drawing.Point(343, 93)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(84, 19)
        Me.Label25.TabIndex = 62
        Me.Label25.Text = "Tel. Movil:"
        '
        'txtTelefono1
        '
        Me.txtTelefono1.Font = New System.Drawing.Font("Calibri", 12.0!)
        Me.txtTelefono1.Location = New System.Drawing.Point(129, 89)
        Me.txtTelefono1.Name = "txtTelefono1"
        Me.txtTelefono1.Size = New System.Drawing.Size(208, 27)
        Me.txtTelefono1.TabIndex = 61
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.BackColor = System.Drawing.Color.Transparent
        Me.Label6.Font = New System.Drawing.Font("Cambria", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Label6.Location = New System.Drawing.Point(16, 92)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(108, 19)
        Me.Label6.TabIndex = 60
        Me.Label6.Text = "Tel. Personal:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.Label3.Font = New System.Drawing.Font("Cambria", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Label3.Location = New System.Drawing.Point(665, 60)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(66, 19)
        Me.Label3.TabIndex = 58
        Me.Label3.Text = "Ciudad:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.Font = New System.Drawing.Font("Cambria", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Label2.Location = New System.Drawing.Point(39, 60)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(84, 19)
        Me.Label2.TabIndex = 57
        Me.Label2.Text = "Dirección:"
        '
        'txtDireccion
        '
        Me.txtDireccion.Font = New System.Drawing.Font("Calibri", 12.0!)
        Me.txtDireccion.Location = New System.Drawing.Point(129, 57)
        Me.txtDireccion.Name = "txtDireccion"
        Me.txtDireccion.Size = New System.Drawing.Size(530, 27)
        Me.txtDireccion.TabIndex = 59
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.BackColor = System.Drawing.Color.Transparent
        Me.Label8.Font = New System.Drawing.Font("Cambria", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Label8.Location = New System.Drawing.Point(53, 27)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(71, 19)
        Me.Label8.TabIndex = 56
        Me.Label8.Text = "Nombre:"
        '
        'btnNuevo
        '
        Me.btnNuevo.Appearance.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold)
        Me.btnNuevo.Appearance.Options.UseFont = True
        Me.btnNuevo.Location = New System.Drawing.Point(891, 5)
        Me.btnNuevo.LookAndFeel.SkinName = "Visual Studio 2013 Dark"
        Me.btnNuevo.LookAndFeel.UseDefaultLookAndFeel = False
        Me.btnNuevo.Name = "btnNuevo"
        Me.btnNuevo.Size = New System.Drawing.Size(94, 27)
        Me.btnNuevo.TabIndex = 67
        Me.btnNuevo.Text = "Nuevo"
        '
        'gcVisitas
        '
        Me.gcVisitas.Controls.Add(Me.dgvVisitas)
        Me.gcVisitas.Location = New System.Drawing.Point(12, 256)
        Me.gcVisitas.LookAndFeel.SkinName = "Visual Studio 2013 Dark"
        Me.gcVisitas.LookAndFeel.UseDefaultLookAndFeel = False
        Me.gcVisitas.Name = "gcVisitas"
        Me.gcVisitas.Size = New System.Drawing.Size(907, 433)
        Me.gcVisitas.TabIndex = 33
        Me.gcVisitas.Text = "Visitas Programadas"
        '
        'dgvVisitas
        '
        Me.dgvVisitas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(32, Byte), Integer), CType(CType(31, Byte), Integer), CType(CType(53, Byte), Integer))
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvVisitas.DefaultCellStyle = DataGridViewCellStyle1
        Me.dgvVisitas.Location = New System.Drawing.Point(129, 27)
        Me.dgvVisitas.Margin = New System.Windows.Forms.Padding(6)
        Me.dgvVisitas.Name = "dgvVisitas"
        Me.dgvVisitas.Size = New System.Drawing.Size(770, 398)
        Me.dgvVisitas.TabIndex = 0
        '
        'btnSalir
        '
        Me.btnSalir.Appearance.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold)
        Me.btnSalir.Appearance.Options.UseFont = True
        Me.btnSalir.Location = New System.Drawing.Point(1027, 662)
        Me.btnSalir.LookAndFeel.SkinName = "Visual Studio 2013 Dark"
        Me.btnSalir.LookAndFeel.UseDefaultLookAndFeel = False
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(75, 27)
        Me.btnSalir.TabIndex = 58
        Me.btnSalir.Text = "Cerrar"
        '
        'gcFiltros
        '
        Me.gcFiltros.Controls.Add(Me.dtpFechaF)
        Me.gcFiltros.Controls.Add(Me.dtpFechaI)
        Me.gcFiltros.Controls.Add(Me.Label9)
        Me.gcFiltros.Controls.Add(Me.ckbEntreFechas)
        Me.gcFiltros.Controls.Add(Me.btnActualizar)
        Me.gcFiltros.Controls.Add(Me.Label7)
        Me.gcFiltros.Location = New System.Drawing.Point(925, 256)
        Me.gcFiltros.LookAndFeel.SkinName = "Visual Studio 2013 Dark"
        Me.gcFiltros.LookAndFeel.UseDefaultLookAndFeel = False
        Me.gcFiltros.Name = "gcFiltros"
        Me.gcFiltros.Size = New System.Drawing.Size(177, 154)
        Me.gcFiltros.TabIndex = 62
        Me.gcFiltros.Text = "Configuracion de filtros"
        '
        'dtpFechaF
        '
        Me.dtpFechaF.CalendarFont = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpFechaF.Cursor = System.Windows.Forms.Cursors.Hand
        Me.dtpFechaF.Font = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpFechaF.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFechaF.Location = New System.Drawing.Point(62, 89)
        Me.dtpFechaF.Margin = New System.Windows.Forms.Padding(3, 6, 12, 3)
        Me.dtpFechaF.MaxDate = New Date(2030, 12, 31, 0, 0, 0, 0)
        Me.dtpFechaF.MinDate = New Date(1900, 1, 1, 0, 0, 0, 0)
        Me.dtpFechaF.Name = "dtpFechaF"
        Me.dtpFechaF.Size = New System.Drawing.Size(110, 25)
        Me.dtpFechaF.TabIndex = 83
        '
        'dtpFechaI
        '
        Me.dtpFechaI.CalendarFont = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpFechaI.Cursor = System.Windows.Forms.Cursors.Hand
        Me.dtpFechaI.Font = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpFechaI.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFechaI.Location = New System.Drawing.Point(62, 55)
        Me.dtpFechaI.Margin = New System.Windows.Forms.Padding(3, 6, 12, 3)
        Me.dtpFechaI.MaxDate = New Date(2030, 12, 31, 0, 0, 0, 0)
        Me.dtpFechaI.MinDate = New Date(1900, 1, 1, 0, 0, 0, 0)
        Me.dtpFechaI.Name = "dtpFechaI"
        Me.dtpFechaI.Size = New System.Drawing.Size(110, 25)
        Me.dtpFechaI.TabIndex = 82
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.BackColor = System.Drawing.Color.Transparent
        Me.Label9.Font = New System.Drawing.Font("Cambria", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Label9.Location = New System.Drawing.Point(0, 59)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(56, 19)
        Me.Label9.TabIndex = 81
        Me.Label9.Text = "Fecha:"
        '
        'ckbEntreFechas
        '
        Me.ckbEntreFechas.AutoSize = True
        Me.ckbEntreFechas.Font = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ckbEntreFechas.Location = New System.Drawing.Point(43, 24)
        Me.ckbEntreFechas.Name = "ckbEntreFechas"
        Me.ckbEntreFechas.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.ckbEntreFechas.Size = New System.Drawing.Size(129, 22)
        Me.ckbEntreFechas.TabIndex = 61
        Me.ckbEntreFechas.Text = "Ver Entre Fechas"
        Me.ckbEntreFechas.UseVisualStyleBackColor = True
        '
        'btnActualizar
        '
        Me.btnActualizar.Appearance.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold)
        Me.btnActualizar.Appearance.Options.UseFont = True
        Me.btnActualizar.Location = New System.Drawing.Point(57, 120)
        Me.btnActualizar.LookAndFeel.SkinName = "Visual Studio 2013 Dark"
        Me.btnActualizar.LookAndFeel.UseDefaultLookAndFeel = False
        Me.btnActualizar.Name = "btnActualizar"
        Me.btnActualizar.Size = New System.Drawing.Size(75, 27)
        Me.btnActualizar.TabIndex = 80
        Me.btnActualizar.Text = "Actualizar"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.BackColor = System.Drawing.Color.Transparent
        Me.Label7.Font = New System.Drawing.Font("Cambria", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Label7.Location = New System.Drawing.Point(0, 93)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(56, 19)
        Me.Label7.TabIndex = 78
        Me.Label7.Text = "Fecha:"
        '
        'btnGuardar
        '
        Me.btnGuardar.Appearance.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold)
        Me.btnGuardar.Appearance.Options.UseFont = True
        Me.btnGuardar.Location = New System.Drawing.Point(991, 5)
        Me.btnGuardar.LookAndFeel.SkinName = "Visual Studio 2013 Dark"
        Me.btnGuardar.LookAndFeel.UseDefaultLookAndFeel = False
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(94, 27)
        Me.btnGuardar.TabIndex = 60
        Me.btnGuardar.Text = "Guardar"
        '
        'btnCancelar
        '
        Me.btnCancelar.Appearance.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold)
        Me.btnCancelar.Appearance.Options.UseFont = True
        Me.btnCancelar.Location = New System.Drawing.Point(1027, 629)
        Me.btnCancelar.LookAndFeel.SkinName = "Visual Studio 2013 Dark"
        Me.btnCancelar.LookAndFeel.UseDefaultLookAndFeel = False
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 27)
        Me.btnCancelar.TabIndex = 63
        Me.btnCancelar.Text = "Cancelar"
        '
        'gcBuscar
        '
        Me.gcBuscar.Controls.Add(Me.btnGuardar)
        Me.gcBuscar.Controls.Add(Me.btnNuevo)
        Me.gcBuscar.Location = New System.Drawing.Point(12, 45)
        Me.gcBuscar.LookAndFeel.SkinName = "Visual Studio 2013 Dark"
        Me.gcBuscar.LookAndFeel.UseDefaultLookAndFeel = False
        Me.gcBuscar.Name = "gcBuscar"
        Me.gcBuscar.Size = New System.Drawing.Size(1090, 37)
        Me.gcBuscar.TabIndex = 64
        '
        'lblTitulo
        '
        Me.lblTitulo.AccessibleRole = System.Windows.Forms.AccessibleRole.TitleBar
        Me.lblTitulo.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.lblTitulo.AutoSize = True
        Me.lblTitulo.BackColor = System.Drawing.Color.Transparent
        Me.lblTitulo.Font = New System.Drawing.Font("Microsoft Sans Serif", 24.0!)
        Me.lblTitulo.ForeColor = System.Drawing.Color.White
        Me.lblTitulo.Location = New System.Drawing.Point(377, 4)
        Me.lblTitulo.Name = "lblTitulo"
        Me.lblTitulo.Size = New System.Drawing.Size(365, 37)
        Me.lblTitulo.TabIndex = 85
        Me.lblTitulo.Text = "Administrador de Visitas"
        '
        'frmVisitas
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Black
        Me.ClientSize = New System.Drawing.Size(1120, 701)
        Me.Controls.Add(Me.lblTitulo)
        Me.Controls.Add(Me.gcBuscar)
        Me.Controls.Add(Me.btnCancelar)
        Me.Controls.Add(Me.gcFiltros)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(Me.gcVisitas)
        Me.Controls.Add(Me.gcDatos)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.MaximumSize = New System.Drawing.Size(1366, 701)
        Me.MinimumSize = New System.Drawing.Size(1120, 701)
        Me.Name = "frmVisitas"
        Me.Text = "frmVisitas"
        CType(Me.gcDatos, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gcDatos.ResumeLayout(False)
        Me.gcDatos.PerformLayout()
        CType(Me.gcVisitas, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gcVisitas.ResumeLayout(False)
        CType(Me.dgvVisitas, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gcFiltros, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gcFiltros.ResumeLayout(False)
        Me.gcFiltros.PerformLayout()
        CType(Me.gcBuscar, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gcBuscar.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents gcDatos As DevExpress.XtraEditors.GroupControl
    Friend WithEvents gcVisitas As DevExpress.XtraEditors.GroupControl
    Friend WithEvents dgvVisitas As System.Windows.Forms.DataGridView
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtDireccion As System.Windows.Forms.TextBox
    Friend WithEvents txtTelefono1 As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtMail As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtCiudad As System.Windows.Forms.TextBox
    Friend WithEvents txtMovil1 As System.Windows.Forms.TextBox
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents btnSalir As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents gcFiltros As DevExpress.XtraEditors.GroupControl
    Friend WithEvents dtpFechaI As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents ckbEntreFechas As System.Windows.Forms.CheckBox
    Friend WithEvents btnActualizar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents btnGuardar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents dtpHora As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpFecha As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents btnNuevo As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents dtpFechaF As System.Windows.Forms.DateTimePicker
    Friend WithEvents txtClienteId As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents cmbNombre As System.Windows.Forms.ComboBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents txtObservaciones As System.Windows.Forms.TextBox
    Friend WithEvents btnCancelar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents gcBuscar As DevExpress.XtraEditors.PanelControl
    Friend WithEvents lblTitulo As System.Windows.Forms.Label
End Class
