﻿Public Class frmSillones
    Private UnGestorProducto As New Modelo.GestorProducto
    Private UnGestorArticulo As New Modelo.GestorArticulo
    Private UnGestorItem As New Modelo.GestorItem

    Public Operacion As Integer ' 0=Alta, 1=Modificacion
    Private Const Categoria As Integer = 4 'Cortinas y sistemas = 1, Telas = 2, Fundas y almohadones = 3, Sillones = 4


    '------ EVENTOS DE FORMULARIOS ------
    Private Sub frmSillones_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.ShowIcon = False
    End Sub

    '------ EVENTOS DE BOTONES ------
    Private Sub btnSalir_Click(sender As Object, e As EventArgs) Handles btnSalir.Click
        Operacion = 0
        Close()
        frmPresupuesto.EliminarItem()
        frmPresupuesto.ActualizarListaItems()
        frmPresupuesto.Dock = DockStyle.Fill
        frmPresupuesto.Show()
    End Sub
    Private Sub btnGuardar_Click(sender As Object, e As EventArgs) Handles btnGuardar.Click
        Dim UnItem As New Modelo.Item
        'Guardado del Item
        UnItem.IdPresupuesto = frmPresupuesto.PresupuestoId
        UnItem.Item = frmPresupuesto.Item
        'UnItem.Descripcion = txtDescripcion.Text
        UnItem.Observacion = txtObservaciones.Text
        UnItem.Precio = txtTotalP.Text
        UnItem.IdCategoria = Categoria
        UnItem.Ubicacion = txtUbicacion.Text
        UnItem.Opcion = cmbOpcion.Text
        Select Case Operacion
            Case 0 'Alta
                UnGestorItem.Nuevo(UnItem)
            Case 1 'Modificacion
                UnItem.IdItem = Double.Parse(frmPresupuesto.Item)
                UnGestorItem.Modificar(UnItem)
        End Select
        'Guardado de los Articulos
        Dim Items As Double = frmPresupuesto.Item

        'If txtGeneroQL1.Text <> "0" Then
        'GrabarArticulo(CDbl(Items + 0.01), CStr(cmbGenero1.Text), CDbl(txtGeneroQA1.Text), CDbl(txtGeneroQL1.Text), 0, CostoGenero1, CDbl(txtGenero1P.Text), CStr(txtGenero1O.Text)) 'GENERO1
        'End If
        frmPresupuesto.ActualizarListaItems()
        Operacion = 0
        Close()
        frmPresupuesto.Dock = DockStyle.Fill
        frmPresupuesto.Show()
    End Sub

    '------ EVENTOS GENERALES ------
    Private Sub GrabarArticulo(Item As Double, Nom As String, Dm1 As Double, Dm2 As Double, Dm3 As Double, Cst As Double, Precio As Double, Obs As String)

        Dim UnArticulo As New Modelo.Articulo       'Guardado del Articulo
        UnArticulo.IdPresupuesto = frmPresupuesto.PresupuestoId
        UnArticulo.Item = Item
        UnArticulo.Nombre = Nom
        UnArticulo.Dim1 = Dm1
        UnArticulo.Dim2 = Dm2
        UnArticulo.Dim3 = Dm3
        UnArticulo.Costo = Cst
        UnArticulo.Precio = Precio
        UnArticulo.Observacion = Obs


        Select Case Operacion
            Case 0 'Alta
                UnGestorArticulo.Nuevo(UnArticulo)
            Case 1 'Modificacion
                UnArticulo.Item = Double.Parse(frmPresupuesto.Item)
                UnArticulo.IdPresupuesto = frmPresupuesto.PresupuestoId
                UnGestorArticulo.Modificar(UnArticulo)
        End Select
    End Sub
End Class