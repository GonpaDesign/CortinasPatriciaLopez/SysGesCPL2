﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEntregas
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.gcEntregas = New DevExpress.XtraEditors.GroupControl()
        Me.dgvEntregas = New System.Windows.Forms.DataGridView()
        Me.btnSalir = New DevExpress.XtraEditors.SimpleButton()
        Me.gcFiltros = New DevExpress.XtraEditors.GroupControl()
        Me.DateTimePicker1 = New System.Windows.Forms.DateTimePicker()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.ckbSinProgramar = New System.Windows.Forms.CheckBox()
        Me.btnActualizar = New DevExpress.XtraEditors.SimpleButton()
        Me.dtpFechaSelect = New System.Windows.Forms.DateTimePicker()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.gcBuscar = New DevExpress.XtraEditors.PanelControl()
        Me.gcCliente = New DevExpress.XtraEditors.GroupControl()
        Me.txtClienteId = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.txtNombre = New System.Windows.Forms.TextBox()
        Me.txtMovil2 = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtCiudad = New System.Windows.Forms.TextBox()
        Me.txtMovil1 = New System.Windows.Forms.TextBox()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.txtTelefono1 = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtDireccion = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.lblTitulo = New System.Windows.Forms.Label()
        CType(Me.gcEntregas, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gcEntregas.SuspendLayout()
        CType(Me.dgvEntregas, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gcFiltros, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gcFiltros.SuspendLayout()
        CType(Me.gcBuscar, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gcCliente, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gcCliente.SuspendLayout()
        Me.SuspendLayout()
        '
        'gcEntregas
        '
        Me.gcEntregas.Controls.Add(Me.dgvEntregas)
        Me.gcEntregas.Location = New System.Drawing.Point(12, 216)
        Me.gcEntregas.LookAndFeel.SkinName = "Visual Studio 2013 Dark"
        Me.gcEntregas.LookAndFeel.UseDefaultLookAndFeel = False
        Me.gcEntregas.Name = "gcEntregas"
        Me.gcEntregas.Size = New System.Drawing.Size(907, 473)
        Me.gcEntregas.TabIndex = 33
        Me.gcEntregas.Text = "Entregas:"
        '
        'dgvEntregas
        '
        Me.dgvEntregas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(32, Byte), Integer), CType(CType(31, Byte), Integer), CType(CType(53, Byte), Integer))
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvEntregas.DefaultCellStyle = DataGridViewCellStyle1
        Me.dgvEntregas.Location = New System.Drawing.Point(121, 27)
        Me.dgvEntregas.Margin = New System.Windows.Forms.Padding(6)
        Me.dgvEntregas.Name = "dgvEntregas"
        Me.dgvEntregas.Size = New System.Drawing.Size(778, 438)
        Me.dgvEntregas.TabIndex = 0
        '
        'btnSalir
        '
        Me.btnSalir.Appearance.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold)
        Me.btnSalir.Appearance.Options.UseFont = True
        Me.btnSalir.Location = New System.Drawing.Point(1027, 662)
        Me.btnSalir.LookAndFeel.SkinName = "Visual Studio 2013 Dark"
        Me.btnSalir.LookAndFeel.UseDefaultLookAndFeel = False
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(75, 27)
        Me.btnSalir.TabIndex = 58
        Me.btnSalir.Text = "Cerrar"
        '
        'gcFiltros
        '
        Me.gcFiltros.Controls.Add(Me.DateTimePicker1)
        Me.gcFiltros.Controls.Add(Me.Label1)
        Me.gcFiltros.Controls.Add(Me.ckbSinProgramar)
        Me.gcFiltros.Controls.Add(Me.btnActualizar)
        Me.gcFiltros.Controls.Add(Me.dtpFechaSelect)
        Me.gcFiltros.Controls.Add(Me.Label7)
        Me.gcFiltros.Location = New System.Drawing.Point(925, 88)
        Me.gcFiltros.LookAndFeel.SkinName = "Visual Studio 2013 Dark"
        Me.gcFiltros.LookAndFeel.UseDefaultLookAndFeel = False
        Me.gcFiltros.Name = "gcFiltros"
        Me.gcFiltros.Size = New System.Drawing.Size(177, 147)
        Me.gcFiltros.TabIndex = 61
        Me.gcFiltros.Text = "Configuracion de filtros"
        '
        'DateTimePicker1
        '
        Me.DateTimePicker1.CalendarFont = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DateTimePicker1.Font = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DateTimePicker1.Location = New System.Drawing.Point(62, 52)
        Me.DateTimePicker1.MaxDate = New Date(2030, 12, 31, 0, 0, 0, 0)
        Me.DateTimePicker1.MinDate = New Date(1900, 1, 1, 0, 0, 0, 0)
        Me.DateTimePicker1.Name = "DateTimePicker1"
        Me.DateTimePicker1.Size = New System.Drawing.Size(110, 25)
        Me.DateTimePicker1.TabIndex = 82
        Me.DateTimePicker1.UseWaitCursor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Cambria", 11.0!, System.Drawing.FontStyle.Bold)
        Me.Label1.Location = New System.Drawing.Point(5, 57)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(52, 17)
        Me.Label1.TabIndex = 81
        Me.Label1.Text = "Inicio:"
        '
        'ckbSinProgramar
        '
        Me.ckbSinProgramar.AutoSize = True
        Me.ckbSinProgramar.Font = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ckbSinProgramar.Location = New System.Drawing.Point(43, 24)
        Me.ckbSinProgramar.Name = "ckbSinProgramar"
        Me.ckbSinProgramar.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.ckbSinProgramar.Size = New System.Drawing.Size(129, 22)
        Me.ckbSinProgramar.TabIndex = 61
        Me.ckbSinProgramar.Text = "Ver Entre Fechas"
        Me.ckbSinProgramar.UseVisualStyleBackColor = True
        '
        'btnActualizar
        '
        Me.btnActualizar.Appearance.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold)
        Me.btnActualizar.Appearance.Options.UseFont = True
        Me.btnActualizar.Location = New System.Drawing.Point(51, 114)
        Me.btnActualizar.LookAndFeel.SkinName = "Visual Studio 2013 Dark"
        Me.btnActualizar.LookAndFeel.UseDefaultLookAndFeel = False
        Me.btnActualizar.Name = "btnActualizar"
        Me.btnActualizar.Size = New System.Drawing.Size(75, 27)
        Me.btnActualizar.TabIndex = 80
        Me.btnActualizar.Text = "Actualizar"
        '
        'dtpFechaSelect
        '
        Me.dtpFechaSelect.CalendarFont = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpFechaSelect.Font = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpFechaSelect.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFechaSelect.Location = New System.Drawing.Point(62, 83)
        Me.dtpFechaSelect.MaxDate = New Date(2030, 12, 31, 0, 0, 0, 0)
        Me.dtpFechaSelect.MinDate = New Date(1900, 1, 1, 0, 0, 0, 0)
        Me.dtpFechaSelect.Name = "dtpFechaSelect"
        Me.dtpFechaSelect.Size = New System.Drawing.Size(110, 25)
        Me.dtpFechaSelect.TabIndex = 79
        Me.dtpFechaSelect.UseWaitCursor = True
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.BackColor = System.Drawing.Color.Transparent
        Me.Label7.Font = New System.Drawing.Font("Cambria", 11.0!, System.Drawing.FontStyle.Bold)
        Me.Label7.Location = New System.Drawing.Point(22, 88)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(34, 17)
        Me.Label7.TabIndex = 78
        Me.Label7.Text = "Fin:"
        '
        'gcBuscar
        '
        Me.gcBuscar.Location = New System.Drawing.Point(12, 44)
        Me.gcBuscar.LookAndFeel.SkinName = "Visual Studio 2013 Dark"
        Me.gcBuscar.LookAndFeel.UseDefaultLookAndFeel = False
        Me.gcBuscar.Name = "gcBuscar"
        Me.gcBuscar.Size = New System.Drawing.Size(1090, 37)
        Me.gcBuscar.TabIndex = 62
        '
        'gcCliente
        '
        Me.gcCliente.Controls.Add(Me.txtClienteId)
        Me.gcCliente.Controls.Add(Me.Label10)
        Me.gcCliente.Controls.Add(Me.txtNombre)
        Me.gcCliente.Controls.Add(Me.txtMovil2)
        Me.gcCliente.Controls.Add(Me.Label2)
        Me.gcCliente.Controls.Add(Me.txtCiudad)
        Me.gcCliente.Controls.Add(Me.txtMovil1)
        Me.gcCliente.Controls.Add(Me.Label25)
        Me.gcCliente.Controls.Add(Me.txtTelefono1)
        Me.gcCliente.Controls.Add(Me.Label6)
        Me.gcCliente.Controls.Add(Me.Label3)
        Me.gcCliente.Controls.Add(Me.Label4)
        Me.gcCliente.Controls.Add(Me.txtDireccion)
        Me.gcCliente.Controls.Add(Me.Label8)
        Me.gcCliente.Location = New System.Drawing.Point(12, 88)
        Me.gcCliente.LookAndFeel.SkinName = "Visual Studio 2013 Dark"
        Me.gcCliente.LookAndFeel.UseDefaultLookAndFeel = False
        Me.gcCliente.Name = "gcCliente"
        Me.gcCliente.Size = New System.Drawing.Size(907, 122)
        Me.gcCliente.TabIndex = 63
        Me.gcCliente.Text = "Datos del Cliente"
        '
        'txtClienteId
        '
        Me.txtClienteId.Font = New System.Drawing.Font("Calibri", 12.0!)
        Me.txtClienteId.Location = New System.Drawing.Point(821, 24)
        Me.txtClienteId.Name = "txtClienteId"
        Me.txtClienteId.Size = New System.Drawing.Size(81, 27)
        Me.txtClienteId.TabIndex = 81
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.BackColor = System.Drawing.Color.Transparent
        Me.Label10.Font = New System.Drawing.Font("Cambria", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Label10.Location = New System.Drawing.Point(786, 27)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(29, 19)
        Me.Label10.TabIndex = 80
        Me.Label10.Text = "Id:"
        '
        'txtNombre
        '
        Me.txtNombre.Font = New System.Drawing.Font("Calibri", 12.0!)
        Me.txtNombre.Location = New System.Drawing.Point(121, 24)
        Me.txtNombre.Name = "txtNombre"
        Me.txtNombre.Size = New System.Drawing.Size(659, 27)
        Me.txtNombre.TabIndex = 79
        '
        'txtMovil2
        '
        Me.txtMovil2.Font = New System.Drawing.Font("Calibri", 12.0!)
        Me.txtMovil2.Location = New System.Drawing.Point(706, 90)
        Me.txtMovil2.Name = "txtMovil2"
        Me.txtMovil2.Size = New System.Drawing.Size(196, 27)
        Me.txtMovil2.TabIndex = 78
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.Font = New System.Drawing.Font("Cambria", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Label2.Location = New System.Drawing.Point(616, 93)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(84, 19)
        Me.Label2.TabIndex = 77
        Me.Label2.Text = "Tel. Movil:"
        '
        'txtCiudad
        '
        Me.txtCiudad.Font = New System.Drawing.Font("Calibri", 12.0!)
        Me.txtCiudad.Location = New System.Drawing.Point(706, 57)
        Me.txtCiudad.Name = "txtCiudad"
        Me.txtCiudad.Size = New System.Drawing.Size(196, 27)
        Me.txtCiudad.TabIndex = 76
        '
        'txtMovil1
        '
        Me.txtMovil1.Font = New System.Drawing.Font("Calibri", 12.0!)
        Me.txtMovil1.Location = New System.Drawing.Point(414, 90)
        Me.txtMovil1.Name = "txtMovil1"
        Me.txtMovil1.Size = New System.Drawing.Size(196, 27)
        Me.txtMovil1.TabIndex = 75
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.BackColor = System.Drawing.Color.Transparent
        Me.Label25.Font = New System.Drawing.Font("Cambria", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Label25.Location = New System.Drawing.Point(324, 93)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(84, 19)
        Me.Label25.TabIndex = 74
        Me.Label25.Text = "Tel. Movil:"
        '
        'txtTelefono1
        '
        Me.txtTelefono1.Font = New System.Drawing.Font("Calibri", 12.0!)
        Me.txtTelefono1.Location = New System.Drawing.Point(122, 90)
        Me.txtTelefono1.Name = "txtTelefono1"
        Me.txtTelefono1.Size = New System.Drawing.Size(196, 27)
        Me.txtTelefono1.TabIndex = 73
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.BackColor = System.Drawing.Color.Transparent
        Me.Label6.Font = New System.Drawing.Font("Cambria", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Label6.Location = New System.Drawing.Point(8, 93)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(108, 19)
        Me.Label6.TabIndex = 72
        Me.Label6.Text = "Tel. Personal:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.Label3.Font = New System.Drawing.Font("Cambria", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Label3.Location = New System.Drawing.Point(634, 60)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(66, 19)
        Me.Label3.TabIndex = 70
        Me.Label3.Text = "Ciudad:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.BackColor = System.Drawing.Color.Transparent
        Me.Label4.Font = New System.Drawing.Font("Cambria", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Label4.Location = New System.Drawing.Point(32, 64)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(84, 19)
        Me.Label4.TabIndex = 69
        Me.Label4.Text = "Dirección:"
        '
        'txtDireccion
        '
        Me.txtDireccion.Font = New System.Drawing.Font("Calibri", 12.0!)
        Me.txtDireccion.Location = New System.Drawing.Point(122, 57)
        Me.txtDireccion.Name = "txtDireccion"
        Me.txtDireccion.Size = New System.Drawing.Size(506, 27)
        Me.txtDireccion.TabIndex = 71
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.BackColor = System.Drawing.Color.Transparent
        Me.Label8.Font = New System.Drawing.Font("Cambria", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Label8.Location = New System.Drawing.Point(45, 27)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(71, 19)
        Me.Label8.TabIndex = 68
        Me.Label8.Text = "Nombre:"
        '
        'lblTitulo
        '
        Me.lblTitulo.AccessibleRole = System.Windows.Forms.AccessibleRole.TitleBar
        Me.lblTitulo.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.lblTitulo.AutoSize = True
        Me.lblTitulo.BackColor = System.Drawing.Color.Transparent
        Me.lblTitulo.Font = New System.Drawing.Font("Microsoft Sans Serif", 24.0!)
        Me.lblTitulo.ForeColor = System.Drawing.Color.White
        Me.lblTitulo.Location = New System.Drawing.Point(361, 4)
        Me.lblTitulo.Name = "lblTitulo"
        Me.lblTitulo.Size = New System.Drawing.Size(398, 37)
        Me.lblTitulo.TabIndex = 84
        Me.lblTitulo.Text = "Administrador de Entregas"
        '
        'frmEntregas
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoSize = True
        Me.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.BackColor = System.Drawing.Color.Black
        Me.ClientSize = New System.Drawing.Size(1120, 701)
        Me.ControlBox = False
        Me.Controls.Add(Me.lblTitulo)
        Me.Controls.Add(Me.gcCliente)
        Me.Controls.Add(Me.gcBuscar)
        Me.Controls.Add(Me.gcFiltros)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(Me.gcEntregas)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.MaximumSize = New System.Drawing.Size(1366, 701)
        Me.MinimumSize = New System.Drawing.Size(1120, 701)
        Me.Name = "frmEntregas"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Entregas"
        CType(Me.gcEntregas, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gcEntregas.ResumeLayout(False)
        CType(Me.dgvEntregas, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gcFiltros, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gcFiltros.ResumeLayout(False)
        Me.gcFiltros.PerformLayout()
        CType(Me.gcBuscar, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gcCliente, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gcCliente.ResumeLayout(False)
        Me.gcCliente.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents gcEntregas As DevExpress.XtraEditors.GroupControl
    Friend WithEvents dgvEntregas As System.Windows.Forms.DataGridView
    Friend WithEvents btnSalir As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents gcFiltros As DevExpress.XtraEditors.GroupControl
    Friend WithEvents DateTimePicker1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents ckbSinProgramar As System.Windows.Forms.CheckBox
    Friend WithEvents btnActualizar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents dtpFechaSelect As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents gcBuscar As DevExpress.XtraEditors.PanelControl
    Friend WithEvents gcCliente As DevExpress.XtraEditors.GroupControl
    Friend WithEvents txtClienteId As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents txtNombre As System.Windows.Forms.TextBox
    Friend WithEvents txtMovil2 As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtCiudad As System.Windows.Forms.TextBox
    Friend WithEvents txtMovil1 As System.Windows.Forms.TextBox
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents txtTelefono1 As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtDireccion As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents lblTitulo As System.Windows.Forms.Label
End Class
