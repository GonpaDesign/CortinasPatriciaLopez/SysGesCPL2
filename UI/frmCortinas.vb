﻿Public Class frmCortinas
    Private Funciones As New Funciones
    Private Const Categoria As Integer = 1 'Cortinas y sistemas = 1, Telas = 2, Fundas y almohadones = 3, Sillones = 4
    Public Operacion As Integer = 0 'Alta = 0, Modificacion = 1
    Private Costo(12) As Double '0=Sistema, 1=Soportes, 2=Argollas, 3=Terminales, 4=Borlas, 5=Acc1, 6=Acc2, 7=Cabezal1, 8=Genero1, 9=Forro1, 10=Cabezal2, 11=Genero2, 12=Forro2

    Public SupMin As Double = 0.8
    Public Ganancia As Double = 1.6

    '---- EVENTOS DE FORMULARIO ----
    Private Sub frmCortinas_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        ConfigurarForm()
        ConfigurarTextBox()

        Ganancia = frmPrincipal.Ganancia
        cmbOpcion.SelectedIndex = 0

        gcSistema.Enabled = False
        gcSistema.Hide()
        gcCortina1.Enabled = False
        gcCortina1.Hide()
        gcCortina2.Enabled = False
        gcCortina2.Hide()

        chkSistema.Checked = False
        chkPaño1.Checked = False
        chkPaño2.Checked = False

        LimpiarCasilleros()
    End Sub
    Private Sub frmCortinas_KeyPress(sender As Object, e As KeyPressEventArgs) Handles MyBase.KeyPress, txtTotalP.KeyPress, txtTerminalesP.KeyPress, txtSubSistP.KeyPress, txtSoportesP.KeyPress, txtSistemaQ.KeyPress, txtSistemaP.KeyPress, txtLargoI1.KeyPress, txtGeneroQL1.KeyPress, txtGeneroQA1.KeyPress, txtGenero1P.KeyPress, txtForroQL1.KeyPress, txtForroQA1.KeyPress, txtForro1P.KeyPress, txtCabezal1Q.KeyPress, txtCabezal1P.KeyPress, txtArgollasP.KeyPress, txtAnchoI1.KeyPress
        If e.KeyChar = "." Then
            e.Handled = True
            SendKeys.Send(",")
        End If
        If e.KeyChar = ChrW(Keys.Enter) Then
            e.Handled = True
            SendKeys.Send("{TAB}")
        End If
    End Sub

    '------ CONFIGURACIONES ------
    Public Sub ConfigurarForm()
        Me.Dock = DockStyle.None
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.ShowIcon = False
        Me.MaximumSize = New Size(frmPrincipal.ClientSize.Width - 4, frmPrincipal.ClientSize.Height - 4)
        Me.MinimumSize = Me.MaximumSize
        Me.Dock = DockStyle.Fill
        Dim Separacion As Integer = (frmPrincipal.ClientSize.Width - 1090 + 150) / 2
        gcItem.Location = New Point(Separacion, 12)
        ReconfigurarGroupControl(0)
        'lblTitulo.Location = New Point((frmPrincipal.ClientSize.Width - 384 + 150) / 2, 4)
        Dim Separacionbtn As Integer = (frmPrincipal.ClientSize.Width - Separacion + 75)
        gcTotal.Location = New Point((frmPrincipal.ClientSize.Width - 448) / 2, frmPrincipal.ClientSize.Height - 66)
        btnGuardar.Location = New Point(Separacionbtn - 81, frmPrincipal.ClientSize.Height - 37)
        btnSalir.Location = New Point(Separacionbtn, frmPrincipal.ClientSize.Height - 37)
    End Sub
    Public Sub ReconfigurarGroupControl(Posicion As Integer)
        Dim Separacion As Integer = (frmPrincipal.ClientSize.Width - 1090 + 150) / 2
        gcSistema.Location = New Point(Separacion, 125 - Posicion)
        gcCortina1.Location = New Point(Separacion, 353 - Posicion)
        gcCortina2.Location = New Point(Separacion, 532 - Posicion)
    End Sub
    Public Sub ConfigurarTextBox()
        'Configuro la cantidad de cararcteres maximos
        'txtNombre.MaxLength = 200
        'txtDireccion.MaxLength = 150
        'txtCP.MaxLength = 8
        'txtEmpresa.MaxLength = 100
        'txtWeb.MaxLength = 75
        'txtMail1.MaxLength = 50
        'txtMail2.MaxLength = 50
        'txtMail3.MaxLength = 50
        'Configuro alineacion del texto
        txtTotalP.TextAlign = HorizontalAlignment.Center
        txtSistemaP.TextAlign = HorizontalAlignment.Center
        txtSoportesP.TextAlign = HorizontalAlignment.Center
        txtArgollasP.TextAlign = HorizontalAlignment.Center
        txtTerminalesP.TextAlign = HorizontalAlignment.Center
        txtCabezal1P.TextAlign = HorizontalAlignment.Center
        txtGenero1P.TextAlign = HorizontalAlignment.Center
        txtForro1P.TextAlign = HorizontalAlignment.Center
        txtAnchoI1.TextAlign = HorizontalAlignment.Center
        txtLargoI1.TextAlign = HorizontalAlignment.Center
        txtAnchoD1.TextAlign = HorizontalAlignment.Center
        txtLargoD1.TextAlign = HorizontalAlignment.Center
        txtCabezal2P.TextAlign = HorizontalAlignment.Center
        txtGenero2P.TextAlign = HorizontalAlignment.Center
        txtForro2P.TextAlign = HorizontalAlignment.Center
        txtAnchoI2.TextAlign = HorizontalAlignment.Center
        txtLargoI2.TextAlign = HorizontalAlignment.Center
        txtAnchoD2.TextAlign = HorizontalAlignment.Center
        txtLargoD2.TextAlign = HorizontalAlignment.Center
        txtGeneroQL1.TextAlign = HorizontalAlignment.Center
        txtForroQL1.TextAlign = HorizontalAlignment.Center
        txtGeneroQL2.TextAlign = HorizontalAlignment.Center
        txtForroQL2.TextAlign = HorizontalAlignment.Center
        txtIVA.TextAlign = HorizontalAlignment.Center
        txtFinal.TextAlign = HorizontalAlignment.Center
        txtForroQA1.TextAlign = HorizontalAlignment.Center
        txtForroQA2.TextAlign = HorizontalAlignment.Center
        txtGeneroQA1.TextAlign = HorizontalAlignment.Center
        txtGeneroQA2.TextAlign = HorizontalAlignment.Center
        txtCabezal1Q.TextAlign = HorizontalAlignment.Center
        txtCabezal2Q.TextAlign = HorizontalAlignment.Center
        txtSistemaQ.TextAlign = HorizontalAlignment.Center
        txtSubSistP.TextAlign = HorizontalAlignment.Center
        txtSubCort1P.TextAlign = HorizontalAlignment.Center
        txtSubCort2P.TextAlign = HorizontalAlignment.Center
        'Configuro los que seran siempre solo lectura
        txtCabezal1Q.ReadOnly = True
        txtCabezal2Q.ReadOnly = True
        txtGeneroQA1.ReadOnly = True
        txtForroQA1.ReadOnly = True
        txtGeneroQA2.ReadOnly = True
        txtForroQA2.ReadOnly = True
    End Sub
    Private Sub LimpiarCasilleros()
        cmbSistema.SelectedIndex = -1
        cmbSoportes.SelectedIndex = -1
        cmbArgollas.SelectedIndex = -1
        cmbTerminales.SelectedIndex = -1
        cmbCabezal1.SelectedIndex = -1
        cmbGenero1.SelectedIndex = -1
        cmbForro1.SelectedIndex = -1
        cmbCabezal2.SelectedIndex = -1
        cmbGenero2.SelectedIndex = -1
        cmbForro2.SelectedIndex = -1

        txtSistemaP.Text = Funciones.Precio("0")
        txtSoportesP.Text = Funciones.Precio("0")
        txtArgollasP.Text = Funciones.Precio("0")
        txtTerminalesP.Text = Funciones.Precio("0")
        txtSubSistP.Text = Funciones.Precio("0")
        txtCabezal1P.Text = Funciones.Precio("0")
        txtGenero1P.Text = Funciones.Precio("0")
        txtForro1P.Text = Funciones.Precio("0")
        txtSistemaQ.Text = "0"
        txtAnchoI1.Text = "0"
        txtLargoI1.Text = "0"
        txtAnchoD1.Text = "0"
        txtLargoD1.Text = "0"
        txtGeneroQL1.Text = "0"
        txtForroQL1.Text = "0"
        txtCabezal1Q.Text = "0"
        txtCabezal2Q.Text = "0"
        txtSubCort1P.Text = Funciones.Precio("0")
        txtCabezal2P.Text = Funciones.Precio("0")
        txtGenero2P.Text = Funciones.Precio("0")
        txtForro2P.Text = Funciones.Precio("0")
        txtAnchoI2.Text = "0"
        txtLargoI2.Text = "0"
        txtAnchoD2.Text = "0"
        txtLargoD2.Text = "0"
        txtGeneroQL2.Text = "0"
        txtForroQL2.Text = "0"
        txtSubCort2P.Text = Funciones.Precio("0")
        txtTotalP.Text = Funciones.Precio("0")
        txtIVA.Text = Funciones.Precio(txtIVA.Text)
        txtFinal.Text = Funciones.Precio("0")
    End Sub

    '---- EVENTOS DE BOTONES ----
    Private Sub btnGuardar_Click(sender As Object, e As EventArgs) Handles btnGuardar.Click
        Dim UnItem As New Modelo.Item
        'Guardado del Item
        UnItem.IdPresupuesto = frmPresupuesto.PresupuestoId
        UnItem.Item = frmPresupuesto.Item
        UnItem.Ubicacion = txtUbicacion.Text
        UnItem.Descripcion = txtItemDesc.Text
        UnItem.Observacion = txtObservaciones.Text
        UnItem.Cantidad = CInt(nupCantidad.Text)
        'UnItem.Costo =
        UnItem.Precio = txtTotalP.Text
        UnItem.IdCategoria = Categoria
        UnItem.Opcion = cmbOpcion.Text

        Dim UnGestorItem As New Modelo.GestorItem
        Select Case Operacion
            Case 0 'Alta
                UnGestorItem.Nuevo(UnItem)
            Case 1 'Modificacion
                UnGestorItem.Modificar(UnItem)
        End Select
        'Guardado de los Articulos
        Dim Items As Double = frmPresupuesto.Item

        If chkSistema.CheckState = CheckState.Checked Then
            If txtSistemaQ.Text <> "0" Then
                GrabarArticulo(CDbl(Items + 0.01), CStr(cmbSistema.Text), CDbl(txtSistemaQ.Text), 0, 0, Costo(0), CDbl(txtSistemaP.Text), "") 'SISTEMA
            End If
            If txtSoportesQ.Text <> "0" Then
                GrabarArticulo(CDbl(Items + 0.02), CStr(cmbSoportes.Text), CDbl(txtSoportesQ.Text), 0, 0, Costo(1), CDbl(txtSoportesP.Text), "") 'SOPORTES
            End If
            If txtTerminalesQ.Text <> "0" Then
                GrabarArticulo(CDbl(Items + 0.03), CStr(cmbTerminales.Text), CDbl(txtTerminalesQ.Text), 0, 0, Costo(3), CDbl(txtTerminalesP.Text), "") 'TERMINALES
            End If
            If txtArgollasQ.Text <> "0" Then
                GrabarArticulo(CDbl(Items + 0.04), CStr(cmbArgollas.Text), CDbl(txtArgollasQ.Text), 0, 0, Costo(2), CDbl(txtArgollasP.Text), "") 'ARGOLLAS
            End If
        Else
        End If

        If chkPaño1.CheckState = CheckState.Checked Then
            If txtCabezal1P.Text <> "0" Then
                GrabarArticulo(CDbl(Items + 0.11), CStr(cmbCabezal1.Text), CDbl(txtAnchoI1.Text), CDbl(txtLargoI1.Text), 0, Costo(4), CDbl(txtCabezal1P.Text), "") 'CABEZAL IZ1
            End If
            If txtCabezal1P.Text <> "0" Then
                GrabarArticulo(CDbl(Items + 0.12), CStr(cmbCabezal1.Text), CDbl(txtAnchoD1.Text), CDbl(txtLargoD1.Text), 0, Costo(4), CDbl(txtCabezal1P.Text), "") 'CABEZAL DE1
            End If
            If txtGeneroQL1.Text <> "0" Then
                GrabarArticulo(CDbl(Items + 0.13), CStr(cmbGenero1.Text), CDbl(txtGeneroQA1.Text), CDbl(txtGeneroQL1.Text), 0, Costo(5), CDbl(txtGenero1P.Text), "") 'GENERO1
            End If
            If txtForroQL1.Text <> "0" Then
                GrabarArticulo(CDbl(Items + 0.14), CStr(cmbForro1.Text), CDbl(txtForroQA1.Text), CDbl(txtForroQA2.Text), 0, Costo(6), (txtForro1P.Text), "") 'FORRO1
            End If
        Else
        End If
        If chkPaño1.CheckState = CheckState.Checked And chkPaño2.CheckState = CheckState.Checked Then
            If txtCabezal2P.Text <> "0" Then
                GrabarArticulo(CDbl(Items + 0.21), CStr(cmbCabezal2.Text), CDbl(txtAnchoI2.Text), CDbl(txtLargoI2.Text), 0, Costo(7), CDbl(txtCabezal2P.Text), "") 'CABEZAL IZ2
            End If
            If txtCabezal2P.Text <> "0" Then
                GrabarArticulo(CDbl(Items + 0.22), CStr(cmbCabezal2.Text), CDbl(txtAnchoD2.Text), CDbl(txtLargoD2.Text), 0, Costo(7), CDbl(txtCabezal2P.Text), "") 'CABEZAL DE2
            End If
            If txtGeneroQL2.Text <> "0" Then
                GrabarArticulo(CDbl(Items + 0.23), CStr(cmbGenero2.Text), CDbl(txtGeneroQA2.Text), CDbl(txtGeneroQL2.Text), 0, Costo(8), CDbl(txtGenero2P.Text), "") 'GENERO2
            End If
            If txtForroQL2.Text <> "0" Then
                GrabarArticulo(CDbl(Items + 0.24), CStr(cmbForro2.Text), CDbl(txtForroQA2.Text), CDbl(txtForroQA2.Text), 0, Costo(9), CDbl(txtForro2P.Text), "") 'FORRO2
            End If
        Else
        End If
        frmPresupuesto.ActualizarListaItems()
        Operacion = 0
        Me.Close()
        frmPresupuesto.Show()
        frmPresupuesto.SendToBack()
        frmDockBotones.BringToFront()
    End Sub
    Private Sub btnSalir_Click(sender As System.Object, e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
        frmPresupuesto.Show()
        frmPresupuesto.SendToBack()
        frmDockBotones.BringToFront()
    End Sub
    Private Sub GrabarArticulo(Item As Double, Nom As String, Dm1 As Double, Dm2 As Double, Dm3 As Double, Cst As Double, Precio As Double, Obs As String)
        Dim UnArticulo As New Modelo.Articulo       'Guardado del Articulo
        UnArticulo.IdPresupuesto = frmPresupuesto.PresupuestoId
        UnArticulo.Item = Item
        UnArticulo.Nombre = Nom
        UnArticulo.Dim1 = Dm1
        UnArticulo.Dim2 = Dm2
        UnArticulo.Dim3 = Dm3
        UnArticulo.Costo = Cst
        UnArticulo.Precio = Precio
        UnArticulo.Observacion = Obs

        Dim UnGestorArticulo As New Modelo.GestorArticulo

        Select Case Operacion
            Case 0 'Alta
                UnGestorArticulo.Nuevo(UnArticulo)
            Case 1 'Modificacion
                UnGestorArticulo.Modificar(UnArticulo)
        End Select
    End Sub

    '---- EVENTOS DE CHECKBOX ------
    Private Sub chkSistema_CheckStateChanged(sender As Object, e As EventArgs) Handles chkSistema.CheckStateChanged
        If chkSistema.Checked = True Then
            ReconfigurarGroupControl(0)
            gcSistema.Enabled = True
            gcSistema.Show()
            CargarcmbSistema()
            CargarcmbArgollas()
            CargarcmbSoportes()
            CargarcmbTerminales()
            CargarcmbAcc3()
            CargarcmbAcc1()
            CargarcmbAcc2()
        Else
            gcSistema.Enabled = False
            gcSistema.Hide()
            ReconfigurarGroupControl(150)
        End If
    End Sub
    Private Sub chkPaño1_CheckStateChanged(sender As Object, e As EventArgs) Handles chkPaño1.CheckStateChanged
        If chkPaño1.Checked = True Then
            If chkSistema.Checked = True Then
            Else
                ReconfigurarGroupControl(226)
            End If
            gcCortina1.Enabled = True
            gcCortina1.Show()
            CargarcmbCabezal1()
            CargarcmbGenero1()
            chkPaño2.Enabled = True
        Else
            chkPaño2.CheckState = CheckState.Unchecked
            gcCortina1.Enabled = False
            gcCortina1.Hide()
            chkPaño2.Enabled = False
        End If
    End Sub
    Private Sub chkPaño2_CheckStateChanged(sender As Object, e As EventArgs) Handles chkPaño2.CheckStateChanged
        If chkPaño2.Checked = True Then
            gcCortina2.Enabled = True
            gcCortina2.Show()
            CargarcmbCabezal2()
            CargarcmbGenero2()
        Else
            gcCortina2.Enabled = False
            gcCortina2.Hide()
        End If
    End Sub

    '---- FUNCIONES PARA CARGAR COMBOBOX ----
    Private Sub CargarcmbSistema()
        Dim UnGestorProducto As New Modelo.GestorProducto
        Dim UnProducto As New Modelo.Producto With {.IdCategoria = 2}
        cmbSistema.DataSource = UnGestorProducto.ListarProductosPorCategoria(UnProducto)
        cmbSistema.DisplayMember = "Nombre"
        cmbSistema.SelectedIndex = -1
    End Sub
    Private Sub CargarcmbSoportes()
        Dim UnGestorProducto As New Modelo.GestorProducto
        Dim UnProducto As New Modelo.Producto With {.IdCategoria = 5}
        cmbSoportes.DataSource = UnGestorProducto.ListarProductosPorCategoria(UnProducto)
        cmbSoportes.DisplayMember = "Nombre"
        cmbSoportes.SelectedIndex = -1
    End Sub
    Private Sub CargarcmbTerminales()
        Dim UnGestorProducto As New Modelo.GestorProducto
        Dim UnProducto As New Modelo.Producto With {.IdCategoria = 3}
        cmbTerminales.DataSource = UnGestorProducto.ListarProductosPorCategoria(UnProducto)
        cmbTerminales.DisplayMember = "Nombre"
        cmbTerminales.SelectedIndex = -1
    End Sub
    Private Sub CargarcmbArgollas()
        Dim UnGestorProducto As New Modelo.GestorProducto
        Dim UnProducto As New Modelo.Producto With {.IdCategoria = 4}
        cmbArgollas.DataSource = UnGestorProducto.ListarProductosPorCategoria(UnProducto)
        cmbArgollas.DisplayMember = "Nombre"
        cmbArgollas.SelectedIndex = -1
    End Sub
    Private Sub CargarcmbAcc3()
        Dim UnGestorProducto As New Modelo.GestorProducto
        Dim UnProducto As New Modelo.Producto With {.IdCategoria = 14}
        cmbAcc3.DataSource = UnGestorProducto.ListarProductosPorCategoria(UnProducto)
        cmbAcc3.DisplayMember = "Nombre"
        cmbAcc3.SelectedIndex = -1
    End Sub
    Private Sub CargarcmbAcc1()
        Dim UnGestorProducto As New Modelo.GestorProducto
        Dim UnProducto As New Modelo.Producto With {.IdCategoria = 14}
        cmbAcc1.DataSource = UnGestorProducto.ListarProductosPorCategoria(UnProducto)
        cmbAcc1.DisplayMember = "Nombre"
        cmbAcc1.SelectedIndex = -1
    End Sub
    Private Sub CargarcmbAcc2()
        Dim UnGestorProducto As New Modelo.GestorProducto
        Dim UnProducto As New Modelo.Producto With {.IdCategoria = 14}
        cmbAcc2.DataSource = UnGestorProducto.ListarProductosPorCategoria(UnProducto)
        cmbAcc2.DisplayMember = "Nombre"
        cmbAcc2.SelectedIndex = -1
    End Sub
    Private Sub CargarcmbCabezal1()
        Dim UnGestorProducto As New Modelo.GestorProducto
        Dim UnProducto As New Modelo.Producto With {.IdCategoria = 6}
        cmbCabezal1.DataSource = UnGestorProducto.ListarProductosPorCategoria(UnProducto)
        cmbCabezal1.DisplayMember = "Nombre"
        cmbCabezal1.SelectedIndex = -1
    End Sub
    Private Sub CargarcmbCabezal2()
        Dim UnGestorProducto As New Modelo.GestorProducto
        Dim UnProducto As New Modelo.Producto With {.IdCategoria = 6}
        cmbCabezal2.DataSource = UnGestorProducto.ListarProductosPorCategoria(UnProducto)
        cmbCabezal2.DisplayMember = "Nombre"
        cmbCabezal2.SelectedIndex = -1
    End Sub
    Private Sub CargarcmbGenero1()
        Dim UnGestorProducto As New Modelo.GestorProducto
        Dim UnProducto As New Modelo.Producto With {.IdCategoria = 1}
        cmbGenero1.DataSource = UnGestorProducto.ListarProductosPorCategoria(UnProducto)
        cmbForro1.DataSource = UnGestorProducto.ListarProductosPorCategoria(UnProducto)
        cmbGenero1.DisplayMember = "Nombre"
        cmbForro1.DisplayMember = "Nombre"
        cmbGenero1.SelectedIndex = -1
        cmbForro1.SelectedIndex = -1
    End Sub
    Private Sub CargarcmbGenero2()
        Dim UnGestorProducto As New Modelo.GestorProducto
        Dim UnProducto As New Modelo.Producto With {.IdCategoria = 1}
        cmbGenero2.DataSource = UnGestorProducto.ListarProductosPorCategoria(UnProducto)
        cmbForro2.DataSource = UnGestorProducto.ListarProductosPorCategoria(UnProducto)
        cmbGenero2.DisplayMember = "Nombre"
        cmbForro2.DisplayMember = "Nombre"
        cmbGenero2.SelectedIndex = -1
        cmbForro2.SelectedIndex = -1
    End Sub

    '---- EVENTOS DE COMBOBOX ----
    Private Sub cmbSistema_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbSistema.SelectedIndexChanged, txtSistemaQ.Leave
        Try
            Dim ProductoSeleccionado As New Modelo.Producto
            Dim UnGestorProducto As New Modelo.GestorProducto
            ProductoSeleccionado = UnGestorProducto.BuscarPorId(cmbSistema.SelectedItem)
            CalcularPrecioSistema(ProductoSeleccionado)
            If RevisarFecha(ProductoSeleccionado) = True Then
                txtSistemaP.BackColor = Color.Red
            End If
        Catch
        End Try
    End Sub
    Private Sub cmbCabezal1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbCabezal1.SelectedIndexChanged, txtCabezal1Q.Leave
        Try
            Dim ProductoSeleccionado As New Modelo.Producto
            Dim UnGestorProducto As New Modelo.GestorProducto
            ProductoSeleccionado = UnGestorProducto.BuscarPorId(cmbCabezal1.SelectedItem)
            CalcularPrecioCabezal1(ProductoSeleccionado)
            If RevisarFecha(ProductoSeleccionado) = True Then
                txtCabezal1P.BackColor = Color.Red
            End If
        Catch
        End Try
    End Sub
    Private Sub cmbCabezal2_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbCabezal2.SelectedIndexChanged, txtCabezal2Q.Leave
        Try
            Dim ProductoSeleccionado As New Modelo.Producto
            Dim UnGestorProducto As New Modelo.GestorProducto
            ProductoSeleccionado = UnGestorProducto.BuscarPorId(cmbCabezal1.SelectedItem)
            CalcularPrecioCabezal2(ProductoSeleccionado)
            If RevisarFecha(ProductoSeleccionado) = True Then
                txtCabezal2P.BackColor = Color.Red
            End If
        Catch
        End Try
    End Sub
    Private Sub cmbSoportes_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbSoportes.SelectedIndexChanged
        Try
            Dim ProductoSeleccionado As New Modelo.Producto
            Dim UnGestorProducto As New Modelo.GestorProducto
            ProductoSeleccionado = UnGestorProducto.BuscarPorId(cmbSoportes.SelectedItem)
            CalcularPrecioSoporte(ProductoSeleccionado)
            If RevisarFecha(ProductoSeleccionado) = True Then
                txtSoportesP.BackColor = Color.Red
            End If
        Catch
        End Try
    End Sub
    Private Sub cmbArgollas_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbArgollas.SelectedIndexChanged, txtArgollasQ.Leave
        Try
            Dim ProductoSeleccionado As New Modelo.Producto
            Dim UnGestorProducto As New Modelo.GestorProducto
            ProductoSeleccionado = UnGestorProducto.BuscarPorId(cmbArgollas.SelectedItem)
            CalcularPrecioArgollas(ProductoSeleccionado)
            If RevisarFecha(ProductoSeleccionado) = True Then
                txtArgollasP.BackColor = Color.Red
            End If
        Catch
        End Try
    End Sub
    Private Sub cmbTerminales_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbTerminales.SelectedIndexChanged, txtTerminalesQ.Leave
        Try
            Dim ProductoSeleccionado As New Modelo.Producto
            Dim UnGestorProducto As New Modelo.GestorProducto
            ProductoSeleccionado = UnGestorProducto.BuscarPorId(cmbTerminales.SelectedItem)
            CalcularPrecioTerminales(ProductoSeleccionado)
            If RevisarFecha(ProductoSeleccionado) = True Then
                txtTerminalesP.BackColor = Color.Red
            End If
        Catch
        End Try
    End Sub
    Private Sub cmbGenero1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbGenero1.SelectedIndexChanged, txtGeneroQL1.Leave
        Try
            If cmbGenero1.SelectedIndex <> -1 Then
                Dim ProductoSeleccionado As New Modelo.Producto
                Dim UnGestorProducto As New Modelo.GestorProducto
                ProductoSeleccionado = UnGestorProducto.BuscarPorId(cmbGenero1.SelectedItem)
                txtGeneroQA1.Text = ProductoSeleccionado.Unidad
                CalcularPrecioGenero1(ProductoSeleccionado)
                If RevisarFecha(ProductoSeleccionado) = True Then
                    txtGenero1P.BackColor = Color.Red
                End If
            Else
            End If
        Catch
        End Try
    End Sub
    Private Sub cmbForro1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbForro1.SelectedIndexChanged, txtForroQL1.Leave
        Try
            If cmbForro1.SelectedIndex <> -1 Then
                Dim ProductoSeleccionado As New Modelo.Producto
                Dim UnGestorProducto As New Modelo.GestorProducto
                ProductoSeleccionado = UnGestorProducto.BuscarPorId(cmbForro1.SelectedItem)
                txtForroQA1.Text = ProductoSeleccionado.Unidad
                CalcularPrecioForro1(ProductoSeleccionado)
                If RevisarFecha(ProductoSeleccionado) = True Then
                    txtForro1P.BackColor = Color.Red
                End If
            Else
            End If
        Catch
        End Try
    End Sub
    Private Sub cmbGenero2_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbGenero2.SelectedIndexChanged, txtGeneroQL2.Leave
        Try
            Dim ProductoSeleccionado As New Modelo.Producto
            Dim UnGestorProducto As New Modelo.GestorProducto
            ProductoSeleccionado = UnGestorProducto.BuscarPorId(cmbGenero2.SelectedItem)
            txtGeneroQA2.Text = ProductoSeleccionado.Unidad
            If RevisarFecha(ProductoSeleccionado) = True Then
                txtGenero2P.BackColor = Color.Red
            End If
        Catch
        End Try
    End Sub
    Private Sub cmbForro2_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbForro2.SelectedIndexChanged, txtForroQL2.Leave
        Try
            Dim ProductoSeleccionado As New Modelo.Producto
            Dim UnGestorProducto As New Modelo.GestorProducto
            ProductoSeleccionado = UnGestorProducto.BuscarPorId(cmbForro2.SelectedItem)
            txtForroQA2.Text = ProductoSeleccionado.Unidad
            If RevisarFecha(ProductoSeleccionado) = True Then
                txtForro2P.BackColor = Color.Red
            End If
        Catch
        End Try
    End Sub
    Private Sub cmbAcc3_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbAcc3.SelectedIndexChanged, txtAccQ3.Leave
        Try
            Dim ProductoSeleccionado As New Modelo.Producto
            Dim UnGestorProducto As New Modelo.GestorProducto
            ProductoSeleccionado = UnGestorProducto.BuscarPorId(cmbAcc3.SelectedItem)
            txtAccQ3.Text = ProductoSeleccionado.Unidad
            If RevisarFecha(ProductoSeleccionado) = True Then
                txtAccP3.BackColor = Color.Red
            End If
        Catch
        End Try
    End Sub
    Private Sub cmbAcc1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbAcc1.SelectedIndexChanged, txtAccQ1.Leave
        Try
            Dim ProductoSeleccionado As New Modelo.Producto
            Dim UnGestorProducto As New Modelo.GestorProducto
            ProductoSeleccionado = UnGestorProducto.BuscarPorId(cmbAcc1.SelectedItem)
            txtAccQ1.Text = ProductoSeleccionado.Unidad
            If RevisarFecha(ProductoSeleccionado) = True Then
                txtAccP1.BackColor = Color.Red
            End If
        Catch
        End Try
    End Sub
    Private Sub cmbAcc2_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbAcc2.SelectedIndexChanged, txtAccQ2.Leave
        Try
            Dim ProductoSeleccionado As New Modelo.Producto
            Dim UnGestorProducto As New Modelo.GestorProducto
            ProductoSeleccionado = UnGestorProducto.BuscarPorId(cmbAcc2.SelectedItem)
            txtAccQ2.Text = ProductoSeleccionado.Unidad
            If RevisarFecha(ProductoSeleccionado) = True Then
                txtAccP2.BackColor = Color.Red
            End If
        Catch
        End Try
    End Sub

    '---- EVENTOS EN TEXTBOX ----
    Private Sub txtAnchoI1_TextChanged(sender As Object, e As EventArgs) Handles txtAnchoI1.TextChanged
        Try
            txtCabezal1Q.Text = CDbl(txtAnchoI1.Text) + CDbl(txtAnchoD1.Text)
        Catch
        End Try
    End Sub
    Private Sub txtAnchoI2_TextChanged(sender As Object, e As EventArgs)
        Try
            txtCabezal2Q.Text = CDbl(txtAnchoI2.Text) + CDbl(txtAnchoD2.Text)
        Catch
        End Try
    End Sub
    Private Sub ItemDescripcion(sender As Object, e As EventArgs) Handles chkSistema.CheckStateChanged, chkPaño1.CheckStateChanged, chkPaño2.CheckStateChanged
        Dim Descripcion As String = ""
        Dim Opcion As Integer = 0 '0= Vacio, 1= Sistema, 2= Sistema + 1 paño, 3= Sistema + 2 Paños, 4= 1 Paño, 5= 2 Paños
        If chkPaño2.Checked = True Then
            If chkSistema.Checked = True Then
                Opcion = 3
            Else
                Opcion = 5
            End If
        Else
            If chkPaño1.Checked = True Then
                If chkSistema.Checked = True Then
                    Opcion = 2
                Else
                    Opcion = 4
                End If
            Else
                If chkSistema.Checked = True Then
                    Opcion = 1
                Else
                    Opcion = 0
                End If
            End If
        End If

        Select Case Opcion
            Case 0
                Descripcion = ""
            Case 1
                Descripcion = String.Format("Sistema {0}", cmbSistema.Text)
            Case 2
                Descripcion = String.Format("Cortina {0} en genero {1} con sistema {2}", cmbCabezal1.Text, cmbGenero1.Text, cmbSistema.Text)
            Case 3
                Descripcion = String.Format("Doble Cortinado, Cortina Ppal {0} en genero {1} Cortina Sec. {2} en genero {3} con sistema {4}", cmbCabezal1.Text, cmbGenero1.Text, cmbCabezal2.Text, cmbGenero2.Text, cmbSistema.Text)
            Case 4
                Descripcion = String.Format("Cortina {0} en genero {1} sin sistema ", cmbCabezal1.Text, cmbGenero1.Text)
            Case 5
                Descripcion = String.Format("Doble Cortinado, Cortina Ppal {0} en genero {1} Cortina Sec. {2} en genero {3} sin sistema", cmbCabezal1.Text, cmbGenero1.Text, cmbCabezal2.Text, cmbGenero2.Text)
        End Select
        txtItemDesc.Text = Descripcion
    End Sub

    '---- FUNCIONES PARA CALCULAR PRECIOS ----

    Private Sub CalcularPrecioSistema(UnProducto As Modelo.Producto)
        Dim Funcion As New Funciones
        Costo(0) = Funcion.Costo(UnProducto.Costo, txtSistemaQ.Text)
        txtSistemaP.Text = Funciones.Precio(Math.Round((Costo(0) * frmPrincipal.Ganancia), 2))
    End Sub
    Private Sub CalcularPrecioSoporte(UnProducto As Modelo.Producto)
        Dim Funcion As New Funciones
        Costo(1) = Funcion.Costo(UnProducto.Costo, txtSoportesQ.Text)
        txtSoportesP.Text = Funciones.Precio(Math.Round(Costo(1) * frmPrincipal.Ganancia, 2))
    End Sub
    Private Sub CalcularPrecioArgollas(UnProducto As Modelo.Producto)
        Dim Funcion As New Funciones
        Costo(2) = Funcion.Costo(UnProducto.Costo, txtArgollasQ.Text)
        txtArgollasP.Text = Funciones.Precio(Math.Round(Costo(2) * (1 + frmPrincipal.Ganancia), 2))
    End Sub
    Private Sub CalcularPrecioTerminales(UnProducto As Modelo.Producto)
        Dim Funcion As New Funciones
        Costo(3) = Funcion.Costo(UnProducto.Costo, txtTerminalesP.Text)
        txtTerminalesP.Text = Funciones.Precio(Math.Round(Costo(3) * (1 + frmPrincipal.Ganancia), 2))
    End Sub
    Private Sub CalcularPrecioBorlas(UnProducto As Modelo.Producto)
        Dim Funcion As New Funciones
        Costo(4) = Funcion.Costo(UnProducto.Costo, txtTerminalesP.Text)
        txtTerminalesP.Text = Funciones.Precio(Math.Round(Costo(4) * (1 + frmPrincipal.Ganancia), 2))
    End Sub
    Private Sub CalcularPrecioAcc1(UnProducto As Modelo.Producto)
        Dim Funcion As New Funciones
        Costo(5) = Funcion.Costo(UnProducto.Costo, txtTerminalesP.Text)
        txtTerminalesP.Text = Funciones.Precio(Math.Round(Costo(5) * (1 + frmPrincipal.Ganancia), 2))
    End Sub
    Private Sub CalcularPrecioAcc2(UnProducto As Modelo.Producto)
        Dim Funcion As New Funciones
        Costo(6) = Funcion.Costo(UnProducto.Costo, txtTerminalesP.Text)
        txtTerminalesP.Text = Funciones.Precio(Math.Round(Costo(6) * (1 + frmPrincipal.Ganancia), 2))
    End Sub

    Private Sub CalcularPrecioCabezal1(UnProducto As Modelo.Producto)
        Dim Funcion As New Funciones
        txtCabezal1P.Text = "0"
        If cmbCabezal1.Text = "Romana" Or cmbCabezal1.Text = "Roller" Or cmbCabezal1.Text = "Oriental" Then
            Costo(7) = Funcion.CostoSup(txtAnchoI1.Text, txtLargoI1.Text, txtAnchoD1.Text, txtLargoD1.Text, SupMin, UnProducto.Costo)
        Else
            Costo(7) = Funcion.CostoLin(txtCabezal1Q.Text, UnProducto.Costo)
        End If
        txtCabezal1P.Text = Funciones.Precio(Math.Round(Costo(7) * Ganancia, 2))
    End Sub
    Private Sub CalcularPrecioGenero1(UnProducto As Modelo.Producto)
        Dim Funcion As New Funciones
        Costo(8) = Funcion.Costo(UnProducto.Costo, txtGeneroQL1.Text)
        txtGenero1P.Text = Funciones.Precio(Math.Round(Costo(8) * Ganancia, 2))
    End Sub
    Private Sub CalcularPrecioForro1(UnProducto As Modelo.Producto)
        Dim Funcion As New Funciones
        Costo(9) = Funcion.Costo(UnProducto.Costo, txtForroQL1.Text)
        txtForro1P.Text = Funciones.Precio(Math.Round(Costo(9) * frmPrincipal.ValorAgregado, 2))
    End Sub

    Private Sub CalcularPrecioCabezal2(UnProducto As Modelo.Producto)
        Dim Funcion As New Funciones
        txtCabezal2P.Text = "0"
        If cmbCabezal2.Text = "Romana" Or cmbCabezal2.Text = "Roller" Or cmbCabezal2.Text = "Oriental" Then
            Costo(10) = Funcion.CostoSup(txtAnchoI2.Text, txtLargoI2.Text, txtAnchoD2.Text, txtLargoD2.Text, SupMin, UnProducto.Costo)
        Else
            Costo(10) = Funcion.CostoLin(txtCabezal2Q.Text, UnProducto.Costo)
        End If
        txtCabezal2P.Text = Funciones.Precio(Math.Round(Costo(10) * Ganancia, 2))
    End Sub
    Private Sub CalcularPrecioGenero2(UnProducto As Modelo.Producto)
        Dim Funcion As New Funciones
        Costo(11) = Funcion.Costo(UnProducto.Costo, txtGeneroQL2.Text)
        txtGenero2P.Text = Funciones.Precio(Math.Round(Costo(11) * frmPrincipal.ValorAgregado, 2))
    End Sub
    Private Sub CalcularPrecioForro2(UnProducto As Modelo.Producto)
        Dim Funcion As New Funciones
        Costo(12) = Funcion.Costo(UnProducto.Costo, txtForroQL2.Text)
        txtForro2P.Text = Funciones.Precio(Math.Round(Costo(12) * frmPrincipal.ValorAgregado, 2))
    End Sub

    '---- FUNCIONES PARA RECALCULAR PRECIOS ----
    Private Sub RecalcularSistema() Handles txtSistemaP.Leave
        If txtSistemaP.Text = "" Then
            If cmbSistema.SelectedIndex = -1 Then
            Else
                Dim ProductoSeleccionado As New Modelo.Producto
                Dim UnGestorProducto As New Modelo.GestorProducto
                ProductoSeleccionado = UnGestorProducto.BuscarPorId(cmbSistema.SelectedItem)
                CalcularPrecioSistema(ProductoSeleccionado)
                If RevisarFecha(ProductoSeleccionado) = True Then
                    txtSistemaP.BackColor = Color.Red
                End If
            End If
        Else
            txtSistemaP.Text = Funciones.Precio(Funciones.QuitarPrecio(txtSistemaP.Text))
        End If
    End Sub
    Private Sub RecalcularSoporte() Handles txtSoportesP.Leave
        If txtSoportesP.Text = "" Then
            If cmbSoportes.SelectedIndex = -1 Then
            Else
                Dim ProductoSeleccionado As New Modelo.Producto
                Dim UnGestorProducto As New Modelo.GestorProducto
                ProductoSeleccionado = UnGestorProducto.BuscarPorId(cmbSoportes.SelectedItem)
                CalcularPrecioSoporte(ProductoSeleccionado)
                If RevisarFecha(ProductoSeleccionado) = True Then
                    txtSoportesP.BackColor = Color.Red
                End If
            End If
        Else
            txtSoportesP.Text = Funciones.Precio(Funciones.QuitarPrecio(txtSoportesP.Text))
        End If
    End Sub
    Private Sub RecalcularTerminales() Handles txtTerminalesP.Leave
        If txtTerminalesP.Text = "" Then
            If cmbTerminales.SelectedIndex = -1 Then
            Else
                Dim ProductoSeleccionado As New Modelo.Producto
                Dim UnGestorProducto As New Modelo.GestorProducto
                ProductoSeleccionado = UnGestorProducto.BuscarPorId(cmbTerminales.SelectedItem)
                CalcularPrecioTerminales(ProductoSeleccionado)
                If RevisarFecha(ProductoSeleccionado) = True Then
                    txtTerminalesP.BackColor = Color.Red
                End If
            End If
        Else
            txtTerminalesP.Text = Funciones.Precio(Funciones.QuitarPrecio(txtTerminalesP.Text))
        End If
    End Sub
    Private Sub RecalcularArgollas() Handles txtArgollasP.Leave
        If txtArgollasP.Text = "" Then
            If cmbArgollas.SelectedIndex = -1 Then
            Else
                Dim ProductoSeleccionado As New Modelo.Producto
                Dim UnGestorProducto As New Modelo.GestorProducto
                ProductoSeleccionado = UnGestorProducto.BuscarPorId(cmbArgollas.SelectedItem)
                CalcularPrecioArgollas(ProductoSeleccionado)
                If RevisarFecha(ProductoSeleccionado) = True Then
                    txtArgollasP.BackColor = Color.Red
                End If
            End If
        Else
            txtArgollasP.Text = Funciones.Precio(Funciones.QuitarPrecio(txtArgollasP.Text))
        End If
    End Sub
    Private Sub RecalcularCabezal1() Handles txtCabezal1P.Leave
        If txtCabezal1P.Text = "" Then
            If cmbCabezal1.SelectedIndex = -1 Then
            Else
                Dim ProductoSeleccionado As New Modelo.Producto
                Dim UnGestorProducto As New Modelo.GestorProducto
                ProductoSeleccionado = UnGestorProducto.BuscarPorId(cmbCabezal1.SelectedItem)
                CalcularPrecioCabezal1(ProductoSeleccionado)
                If RevisarFecha(ProductoSeleccionado) = True Then
                    txtCabezal1P.BackColor = Color.Red
                End If
            End If
        Else
            txtCabezal1P.Text = Funciones.Precio(Funciones.QuitarPrecio(txtCabezal1P.Text))
        End If
    End Sub
    Private Sub RecalcularGenero1() Handles txtGenero1P.Leave
        If txtGenero1P.Text = "" Then
            If cmbGenero1.SelectedIndex = -1 Then
            Else
                Dim ProductoSeleccionado As New Modelo.Producto
                Dim UnGestorProducto As New Modelo.GestorProducto
                ProductoSeleccionado = UnGestorProducto.BuscarPorId(cmbGenero1.SelectedItem)
                txtGeneroQA1.Text = ProductoSeleccionado.Unidad
                CalcularPrecioGenero1(ProductoSeleccionado)
                If RevisarFecha(ProductoSeleccionado) = True Then
                    txtGenero1P.BackColor = Color.Red
                End If
            End If
        Else
            txtGenero1P.Text = Funciones.Precio(Funciones.QuitarPrecio(txtGenero1P.Text))
        End If
    End Sub
    Private Sub RecalcularForro1() Handles txtForro1P.Leave
        If txtForro1P.Text = "" Then
            If cmbForro1.SelectedIndex = -1 Then
            Else
                Dim ProductoSeleccionado As New Modelo.Producto
                Dim UnGestorProducto As New Modelo.GestorProducto
                ProductoSeleccionado = UnGestorProducto.BuscarPorId(cmbForro1.SelectedItem)
                txtForroQA1.Text = ProductoSeleccionado.Unidad
                CalcularPrecioForro1(ProductoSeleccionado)
                If RevisarFecha(ProductoSeleccionado) = True Then
                    txtForro1P.BackColor = Color.Red
                End If
            End If
        Else
            txtForro1P.Text = Funciones.Precio(Funciones.QuitarPrecio(txtForro1P.Text))
        End If
    End Sub
    Private Sub RecalcularCabezal2() Handles txtCabezal2P.Leave
        If txtCabezal2P.Text = "" Then
            If cmbCabezal2.SelectedIndex = -1 Then
            Else
                Dim ProductoSeleccionado As New Modelo.Producto
                Dim UnGestorProducto As New Modelo.GestorProducto
                ProductoSeleccionado = UnGestorProducto.BuscarPorId(cmbCabezal2.SelectedItem)
                CalcularPrecioCabezal2(ProductoSeleccionado)
                If RevisarFecha(ProductoSeleccionado) = True Then
                    txtCabezal2P.BackColor = Color.Red
                End If
            End If
        Else
            txtCabezal2P.Text = Funciones.Precio(Funciones.QuitarPrecio(txtCabezal2P.Text))
        End If
    End Sub
    Private Sub RecalcularGenero2() Handles txtGenero2P.Leave
        If txtGenero2P.Text = "" Then
            If cmbGenero2.SelectedIndex = -1 Then
            Else
                Dim ProductoSeleccionado As New Modelo.Producto
                Dim UnGestorProducto As New Modelo.GestorProducto
                ProductoSeleccionado = UnGestorProducto.BuscarPorId(cmbGenero2.SelectedItem)
                txtGeneroQA2.Text = ProductoSeleccionado.Unidad
                CalcularPrecioGenero1(ProductoSeleccionado)
                If RevisarFecha(ProductoSeleccionado) = True Then
                    txtGenero2P.BackColor = Color.Red
                End If
            End If
        Else
            txtGenero2P.Text = Funciones.Precio(Funciones.QuitarPrecio(txtGenero2P.Text))
        End If
    End Sub
    Private Sub RecalcularForro2() Handles txtForro2P.Leave
        If txtForro2P.Text = "" Then
            If cmbForro2.SelectedIndex = -1 Then
            Else
                Dim ProductoSeleccionado As New Modelo.Producto
                Dim UnGestorProducto As New Modelo.GestorProducto
                ProductoSeleccionado = UnGestorProducto.BuscarPorId(cmbForro2.SelectedItem)
                txtForroQA2.Text = ProductoSeleccionado.Unidad
                CalcularPrecioForro1(ProductoSeleccionado)
                If RevisarFecha(ProductoSeleccionado) = True Then
                    txtForro2P.BackColor = Color.Red
                End If
            End If
        Else
            txtForro2P.Text = Funciones.Precio(Funciones.QuitarPrecio(txtForro2P.Text))
        End If
    End Sub
    Private Sub RecalcularAcc3() Handles txtAccP3.Leave
        If txtAccP3.Text = "" Then
            If cmbAcc3.SelectedIndex = -1 Then
            Else
                Dim ProductoSeleccionado As New Modelo.Producto
                Dim UnGestorProducto As New Modelo.GestorProducto
                ProductoSeleccionado = UnGestorProducto.BuscarPorId(cmbAcc3.SelectedItem)
                txtAccQ3.Text = ProductoSeleccionado.Unidad
                CalcularPrecioBorlas(ProductoSeleccionado)
                If RevisarFecha(ProductoSeleccionado) = True Then
                    txtAccP3.BackColor = Color.Red
                End If
        End If
        Else
            txtAccP3.Text = Funciones.Precio(Funciones.QuitarPrecio(txtAccP3.Text))
        End If
    End Sub
    Private Sub RecalcularAcc1() Handles txtAccP1.Leave
        If txtAccP1.Text = "" Then
            If cmbAcc1.SelectedIndex = -1 Then
            Else
                Dim ProductoSeleccionado As New Modelo.Producto
                Dim UnGestorProducto As New Modelo.GestorProducto
                ProductoSeleccionado = UnGestorProducto.BuscarPorId(cmbAcc1.SelectedItem)
                txtAccQ1.Text = ProductoSeleccionado.Unidad
                CalcularPrecioAcc1(ProductoSeleccionado)
                If RevisarFecha(ProductoSeleccionado) = True Then
                    txtAccP1.BackColor = Color.Red
                End If
            End If
        Else
            txtAccP1.Text = Funciones.Precio(Funciones.QuitarPrecio(txtAccP1.Text))
        End If
    End Sub
    Private Sub RecalcularAcc2() Handles txtAccP1.Leave
        If txtAccP2.Text = "" Then
            If cmbAcc2.SelectedIndex = -1 Then
            Else
                Dim ProductoSeleccionado As New Modelo.Producto
                Dim UnGestorProducto As New Modelo.GestorProducto
                ProductoSeleccionado = UnGestorProducto.BuscarPorId(cmbAcc2.SelectedItem)
                txtAccQ2.Text = ProductoSeleccionado.Unidad
                CalcularPrecioAcc2(ProductoSeleccionado)
                If RevisarFecha(ProductoSeleccionado) = True Then
                    txtAccP2.BackColor = Color.Red
                End If
            End If
        Else
            txtAccP2.Text = Funciones.Precio(Funciones.QuitarPrecio(txtAccP2.Text))
        End If
    End Sub

    '---- FUNCIONES PARA CARGAR SUBTOTALES Y TOTALES ----
    Private Sub SubtotalSistema() Handles txtSistemaP.TextChanged, txtArgollasP.TextChanged, txtTerminalesP.TextChanged, txtSoportesP.TextChanged
        txtSubSistP.Text = Funciones.Precio(Funciones.QuitarPrecio(txtSistemaP.Text) + Funciones.QuitarPrecio(txtSoportesP.Text) + Funciones.QuitarPrecio(txtArgollasP.Text) + Funciones.QuitarPrecio(txtTerminalesP.Text) + Funciones.QuitarPrecio(txtAccP3.Text) + Funciones.QuitarPrecio(txtAccP1.Text) + Funciones.QuitarPrecio(txtAccP2.Text))
    End Sub
    Private Sub SubtotalCortina1() Handles txtCabezal1P.TextChanged, txtGenero1P.TextChanged, txtForro1P.TextChanged
        txtSubCort1P.Text = Funciones.Precio(Funciones.QuitarPrecio(txtCabezal1P.Text) + Funciones.QuitarPrecio(txtGenero1P.Text) + Funciones.QuitarPrecio(txtForro1P.Text))
    End Sub
    Private Sub SubtotalCortina2() Handles txtCabezal2P.TextChanged, txtGenero2P.TextChanged, txtForro2P.TextChanged
        txtSubCort2P.Text = Funciones.Precio(Funciones.QuitarPrecio(txtCabezal2P.Text) + Funciones.QuitarPrecio(txtGenero2P.Text) + Funciones.QuitarPrecio(txtForro2P.Text))
    End Sub
    Private Sub TotalCortina() Handles txtSubCort1P.TextChanged, txtSubCort2P.TextChanged, txtSubSistP.TextChanged
        txtFinal.Text = Funciones.Precio(Funciones.QuitarPrecio(txtSubSistP.Text) + Funciones.QuitarPrecio(txtSubCort1P.Text) + Funciones.QuitarPrecio(txtSubCort2P.Text))
        txtTotalP.Text = Funciones.Precio(Math.Round((Funciones.QuitarPrecio(txtFinal.Text) / (1 + frmPrincipal.IVA)), 2))
        txtIVA.Text = Funciones.Precio(Math.Round((Funciones.QuitarPrecio(txtFinal.Text) - Funciones.QuitarPrecio(txtTotalP.Text)), 2))
    End Sub

    '---- EVENTOS DE CALCULO DE PRECIOS ----
    Private Sub AnchoCabezal1(sender As Object, e As EventArgs) Handles txtAnchoI1.TextChanged, txtAnchoD1.TextChanged, txtCabezal1Q.TextChanged, txtLargoI1.TextChanged
        Try
            Dim ProductoSeleccionado As New Modelo.Producto
            Dim UnGestorProducto As New Modelo.GestorProducto
            ProductoSeleccionado = UnGestorProducto.BuscarPorId(cmbCabezal1.SelectedItem)
            CalcularPrecioCabezal1(ProductoSeleccionado)
        Catch
        End Try
    End Sub
    Private Sub AnchoCabezal2(sender As Object, e As EventArgs) Handles txtAnchoI2.TextChanged, txtAnchoD2.TextChanged
        Try
            Dim ProductoSeleccionado As New Modelo.Producto
            Dim UnGestorProducto As New Modelo.GestorProducto
            ProductoSeleccionado = UnGestorProducto.BuscarPorId(cmbCabezal1.SelectedItem)
            CalcularPrecioCabezal2(ProductoSeleccionado)
        Catch
        End Try
    End Sub

    '------ EVENTO PARA LA CARGA DE ITEM ------
    Public Sub CargarArticulo(Item As Modelo.Item)
        Dim Funcion As New Funciones
        If Funcion.BuscarTexto(Item.Descripcion, "Con Sistema") = -1 Then
            chkSistema.Checked = True
            CargarSistema(Item)
        Else
            chkSistema.Checked = False
        End If
        If Funcion.BuscarTexto(Item.Descripcion, "Cortina") = -1 Then
            chkPaño1.Checked = True
            CargarCortina1(Item)
        Else
            chkPaño1.Checked = False
        End If
        txtUbicacion.Text = Item.Ubicacion
        txtObservaciones.Text = Item.Observacion
        cmbOpcion.Text = Item.Opcion
        txtTotalP.Text = Item.Precio

        If Funcion.BuscarTexto(Item.Descripcion, "Doble Cortinado") = -1 Then
            chkPaño2.Checked = True
            chkPaño1.Checked = True
            CargarCortina1(Item)
            CargarCortina2(Item)
        Else
            chkPaño2.Checked = False
        End If
    End Sub
    Public Sub CargarSistema(Item As Modelo.Item)
        Dim Funcion As New Funciones
        Try
            Funcion.CargarItem1DimO(cmbSistema, txtSistemaP, txtSistemaQ, txtSistemaO, Item, 0.01)
            RecalcularSistema()
        Catch
        End Try
        Try
            Funcion.CargarItem1Dim(cmbSoportes, txtSoportesP, txtSoportesQ, Item, 0.02)
            RecalcularSoporte()
        Catch
        End Try
        Try
            Funcion.CargarItem1Dim(cmbArgollas, txtArgollasP, txtArgollasQ, Item, 0.03)
            RecalcularArgollas()
        Catch
        End Try
        Try
            Funcion.CargarItem1Dim(cmbTerminales, txtTerminalesP, txtTerminalesQ, Item, 0.04)
            RecalcularTerminales()
        Catch
        End Try
    End Sub
    Public Sub CargarCortina1(Item As Modelo.Item)
        Dim Funcion As New Funciones
        Try
            Funcion.CargarItem2DimCom(cmbCabezal1, txtCabezal1P, txtAnchoI1, txtLargoI1, chkCI1, Item, 0.11)
            RecalcularCabezal1()
        Catch
        End Try
        Try
            Funcion.CargarItem2DimCom(cmbCabezal1, txtCabezal1P, txtAnchoD1, txtLargoD1, chkCD1, Item, 0.12)
            RecalcularCabezal1()
        Catch
        End Try
        Try
            Funcion.CargarItem2Dim(cmbGenero1, txtGenero1P, txtGeneroQA1, txtGeneroQL1, Item, 0.13)
            RecalcularGenero1()
        Catch
        End Try
        Try
            Funcion.CargarItem2Dim(cmbForro1, txtForro1P, txtForroQA1, txtForroQL1, Item, 0.14)
            RecalcularForro1()
        Catch
        End Try
    End Sub
    Public Sub CargarCortina2(Item As Modelo.Item)
        Dim Funcion As New Funciones
        Try
            Funcion.CargarItem2DimCom(cmbCabezal2, txtCabezal2P, txtAnchoI2, txtLargoI2, chkCI2, Item, 0.21)
            RecalcularCabezal2()
        Catch
        End Try
        Try
            Funcion.CargarItem2DimCom(cmbCabezal2, txtCabezal2P, txtAnchoD2, txtLargoD2, chkCD2, Item, 0.22)
            RecalcularCabezal2()
        Catch
        End Try
        Try
            Funcion.CargarItem2Dim(cmbGenero2, txtGenero2P, txtGeneroQA2, txtGeneroQL2, Item, 0.23)
            RecalcularGenero2()
        Catch
        End Try
        Try
            Funcion.CargarItem2Dim(cmbForro2, txtForro2P, txtForroQA2, txtForroQL2, Item, 0.24)
            RecalcularForro2()
        Catch
        End Try
    End Sub

    '------ FUNCIONES GENERALES ------
    Private Function RevisarFecha(Producto As Modelo.Producto) As Boolean
        Dim UnaListaPrecio As New Modelo.ListaPrecio
        Dim UnGestorListaPrecio As New Modelo.GestorListaPrecio
        UnaListaPrecio.IdProveedor = Producto.IdProveedor
        RevisarFecha = False
        If Producto.Fecha < UnGestorListaPrecio.Fecha(UnaListaPrecio) Then
            Return RevisarFecha = False
        Else
            Return RevisarFecha = True
        End If
    End Function
End Class