﻿Imports System.Globalization
Imports iTextSharp
Imports iTextSharp.text
Imports iTextSharp.text.pdf
Imports System.IO
Imports System.Xml

Public Class frmPresupuesto
    Private Operacion As Integer ' 0=Alta, 1=Modificacion, 2=Operacion nula
    Private ABM As Boolean = False 'True=Alta, False=Modificacion
    Public Item As Integer ' Variable que indicara el numero de Item a generar
    Public PresupuestoId As Integer ' Variable que indicara el numero de Presupuesto a generar
    Private Aprobado As Boolean = False


    '----- EVENTOS DE FORMULARIO ------
    Private Sub frmPresupuesto_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        ConfigurarForm()
        ConfigurarDataGridView()
        ConfigurarTextBox()

        ActualizarCmbcliente()
        ActualizarCmbIdPresupuesto()

        LimpiarCasilleros()

        btnAprobarItem.Enabled = False

        btnSillones.Enabled = False
        btnTela.Enabled = False

        ExportarOff()
        ModoNormal()
        ModoReadOnly()
        ModoItemsReadOnly()
        cmbIdPresupuesto.Focus()
        cmbIdPresupuesto.SelectedIndex = -1
    End Sub
    Private Sub frmClientes_Click(sender As Object, e As EventArgs) Handles MyBase.Click, MyBase.GotFocus
        Me.SendToBack()
    End Sub

    Private Sub Solonumeros(sender As Object, e As KeyPressEventArgs) Handles txtVigencia.KeyPress
        If Not (Char.IsNumber(e.KeyChar) Or Char.IsControl(e.KeyChar)) Then e.Handled = True
    End Sub

    '------ EVENTO DE BOTONES ------
    Private Sub btnNuevo_Click(sender As System.Object, e As System.EventArgs) Handles btnNuevo.Click
        Operacion = 0

        LimpiarCasilleros()
        ExportarOff()
        ModoABM()
        ModoEditar()
        txtNotas.Focus()
    End Sub
    Private Sub btnEliminar_Click(sender As Object, e As EventArgs) Handles btnEliminar.Click
        If cmbIdPresupuesto.Text = "" Then
        Else
            Dim UnPresupuesto As New Modelo.Presupuesto
            UnPresupuesto.IdPresupuesto = Integer.Parse(cmbIdPresupuesto.Text)
            If MsgBox(String.Format("Desea eliminar el presupuesto N° {0} perteneciente al cliente {1} ?", cmbIdPresupuesto.Text, cmbCliente.Text), vbOKCancel, "Eliminar Cliente") = vbOK Then
                Dim UnGestorPresupuesto As New Modelo.GestorPresupuesto
                UnGestorPresupuesto.Eliminar(UnPresupuesto)
                ModoNormal()
                ModoReadOnly()
            End If
        End If
    End Sub
    Private Sub btnGuardar_Click(sender As System.Object, e As System.EventArgs) Handles btnGuardar.Click
        If txtIdCliente.Text = "" Then
        Else
            Dim UnGestorPresupuesto As New Modelo.GestorPresupuesto
            Dim UnPresupuesto As New Modelo.Presupuesto

            UnPresupuesto.IdCliente = CInt(txtIdCliente.Text)
            UnPresupuesto.FechaGenerado = CStr(Date.Today)
            UnPresupuesto.Colocacion = chkColocacion.Checked
            UnPresupuesto.FechaColocacion = CDate("1 / 1 / 2000")
            UnPresupuesto.Vigencia = CInt(txtVigencia.Text)
            UnPresupuesto.FechaEntrega = dtpEntrega.Text
            UnPresupuesto.Notas = txtNotas.Text

            Select Case Operacion
                Case 0 ' ALTA
                    UnGestorPresupuesto.Nuevo(UnPresupuesto)
                Case 1
                    ' MODIFICACION
                    UnPresupuesto.IdPresupuesto = Integer.Parse(cmbIdPresupuesto.Text)
                    UnGestorPresupuesto.Modificar(UnPresupuesto)
            End Select
        End If
        ModoNormal()
        ModoReadOnly()
        Dim Presupuesto As New Modelo.Presupuesto
        Dim GestorPresupuesto As New Modelo.GestorPresupuesto
        Select Case Operacion
            Case 0
                Presupuesto.IdPresupuesto = GestorPresupuesto.MaxId
            Case 1
                Presupuesto.IdPresupuesto = Integer.Parse(cmbIdPresupuesto.Text)
        End Select
        ActualizarCmbIdPresupuesto()
        cmbIdPresupuesto.SelectedText = Presupuesto.IdPresupuesto
        Operacion = 2
    End Sub
    Private Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        ModoNormal()
        ModoReadOnly()
        Operacion = 2
    End Sub
    Private Sub btnSalir_Click(sender As System.Object, e As System.EventArgs) Handles btnSalir.Click
        ModoReadOnly()
        Operacion = 2
        Me.Close()
    End Sub

    Private Sub btnCortina_Click(sender As Object, e As EventArgs) Handles btnCortina.Click
        Cursor = System.Windows.Forms.Cursors.WaitCursor
        AgregarItem()
        frmCortinas.MdiParent = frmPrincipal
        frmCortinas.Show()
        Me.Hide()
        frmCortinas.SendToBack()
        frmDockBotones.BringToFront()
        Cursor = System.Windows.Forms.Cursors.Default
    End Sub
    Private Sub btnTela_Click(sender As Object, e As EventArgs) Handles btnTela.Click
        Cursor = System.Windows.Forms.Cursors.WaitCursor
        AgregarItem()
        frmGenero.MdiParent = frmPrincipal
        frmGenero.Show()
        Me.Hide()
        frmGenero.SendToBack()
        frmDockBotones.BringToFront()
        Cursor = System.Windows.Forms.Cursors.Default
    End Sub
    Private Sub btnFundas_Click(sender As Object, e As EventArgs) Handles btnFundas.Click
        Cursor = System.Windows.Forms.Cursors.WaitCursor
        AgregarItem()
        frmFundas.MdiParent = frmPrincipal
        frmFundas.Show()
        Me.Hide()
        frmFundas.SendToBack()
        frmDockBotones.BringToFront()
        Cursor = System.Windows.Forms.Cursors.Default
    End Sub
    Private Sub btnSillones_Click(sender As Object, e As EventArgs) Handles btnSillones.Click
        Cursor = System.Windows.Forms.Cursors.WaitCursor
        AgregarItem()
        frmSillones.MdiParent = frmPrincipal
        frmSillones.Show()
        Me.Hide()
        frmSillones.SendToBack()
        frmDockBotones.BringToFront()
        Cursor = System.Windows.Forms.Cursors.Default
    End Sub

    Private Sub btnAprobar_Click(sender As Object, e As EventArgs) Handles btnAprobar.Click
        Dim UnGestorPresupuesto As New Modelo.GestorPresupuesto
        Dim UnPresupuesto As New Modelo.Presupuesto
        UnPresupuesto.IdPresupuesto = Integer.Parse(cmbIdPresupuesto.Text)
        If MsgBox(String.Format("Desea dar por aprobado el presupuesto N° {0} perteneciente al cliente {1} ?", cmbIdPresupuesto.Text, cmbCliente.Text), vbOKCancel, "Aprobar Cliente") = vbOK Then
            UnGestorPresupuesto.Aprobar(UnPresupuesto)
            ModoItemsReadOnly()
            Aprobado = True
        End If
    End Sub
    Private Sub btnAprobarItem_Click(sender As Object, e As EventArgs) Handles btnAprobarItem.Click
        Dim UnItem As New Modelo.Item
        Dim UnGestorItem As New Modelo.GestorItem
        If CBool(dgvItems.CurrentRow.Cells(7).Value) = False Then
            UnItem.IdItem = CInt(dgvItems.CurrentRow.Cells(0).Value)
            UnGestorItem.Aprobar(UnItem)
        Else
            UnItem.IdItem = CInt(dgvItems.CurrentRow.Cells(0).Value)
            UnGestorItem.Desaprobar(UnItem)
        End If
        Dim UnPresupuesto As New Modelo.Presupuesto
        UnPresupuesto.IdPresupuesto = CInt(dgvItems.CurrentRow.Cells(1).Value)
        Actualizardvg(UnPresupuesto)
    End Sub
    Private Sub btnPresupuestopdf(sender As Object, e As EventArgs) Handles btnPresPdf.Click
        Try
            Dim documentoPDF As New Document(iTextSharp.text.PageSize.A4.Rotate, 50, 50, 50, 35)
            Dim saveFileDialog1 As New SaveFileDialog()

            saveFileDialog1.FileName = String.Format("Presupuesto N° {0}", cmbIdPresupuesto.Text)
            saveFileDialog1.Filter = "PDF Files (*.pdf)|*.pdf"
            saveFileDialog1.FilterIndex = 2
            saveFileDialog1.RestoreDirectory = True
            Dim writer As PdfWriter
            If saveFileDialog1.ShowDialog() = DialogResult.OK Then
                writer = PdfWriter.GetInstance(documentoPDF, New FileStream(saveFileDialog1.FileName, FileMode.Create))
               End If

            documentoPDF.Open()

            'Añadimos los metadatos para el fichero PDF
            documentoPDF.AddAuthor("Cortinas Patricia Lopez")
            'documentoPDF.AddCreator(Usuario)
            Dim Key As String = String.Format("Presupuesto N° {0}", cmbIdPresupuesto.Text)
            'documentoPDF.AddKeywords(Key)
            'documentoPDF.AddSubject(Key)
            documentoPDF.AddTitle(Key)
            documentoPDF.AddCreationDate()

            'Se agrega el PDFTable al documento.
            documentoPDF.Add(Titulo())
            documentoPDF.Add(Encabezado())
            documentoPDF.Add(Datoscliente())
            documentoPDF.Add(ObtenerTablaItems(dgvItems))
            documentoPDF.Add(TablaSubtotales(cmbIdPresupuesto.SelectedItem))
            'documentoPDF.Add(Totales())
            documentoPDF.Add(Notasdepago())

            documentoPDF.Close() 'Cerramos el objeto documento, guardamos y creamos el PDF

            'Comprobamos si se ha creado el fichero PDF
            If System.IO.File.Exists(saveFileDialog1.FileName) Then
                If MsgBox("Datos Exportados correctamente " + "¿Desea abrir el archivo?", MsgBoxStyle.Question + MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
                    'Abrimos el fichero PDF con la aplicación asociada
                    System.Diagnostics.Process.Start(saveFileDialog1.FileName)
                End If
            Else
                MsgBox("El fichero PDF no se ha generado, " + "compruebe que tiene permisos en la carpeta de destino.", MsgBoxStyle.Exclamation + MsgBoxStyle.OkOnly)
            End If
        Catch ex As Exception
            MsgBox("Se ha producido un error al intentar convertir el presupuesto a PDF: " + ex.Message, MsgBoxStyle.Critical + MsgBoxStyle.OkOnly)
        End Try
    End Sub
    Private Sub btnTallerPDF_Click(sender As Object, e As EventArgs) Handles btnTallerPDF.Click
        Try
            Dim documentoPDF As New Document(iTextSharp.text.PageSize.A4.Rotate, 50, 50, 50, 35)
            Dim saveFileDialog1 As New SaveFileDialog()

            saveFileDialog1.FileName = String.Format("Orden Taller - Presupuesto N° {0}", cmbIdPresupuesto.Text)
            saveFileDialog1.Filter = "PDF Files (*.pdf)|*.pdf"
            saveFileDialog1.FilterIndex = 2
            saveFileDialog1.RestoreDirectory = True
            Dim writer As PdfWriter
            If saveFileDialog1.ShowDialog() = DialogResult.OK Then
                writer = PdfWriter.GetInstance(documentoPDF, New FileStream(saveFileDialog1.FileName, FileMode.Create))
            End If

            documentoPDF.Open()

            'Añadimos los metadatos para el fichero PDF
            documentoPDF.AddAuthor("Cortinas Patricia Lopez")
            'documentoPDF.AddCreator(Usuario)
            Dim Key As String = String.Format("Orden Taller - Presupuesto N° {0}", cmbIdPresupuesto.Text)
            'documentoPDF.AddKeywords(Key)
            'documentoPDF.AddSubject(Key)
            documentoPDF.AddTitle(Key)
            documentoPDF.AddCreationDate()

            'Se agrega el PDFTable al documento.
            documentoPDF.Add(Titulo())
            documentoPDF.Add(Datoscliente())
            documentoPDF.Add(ObtenerTablaItems(dgvItems))

            documentoPDF.Close() 'Cerramos el objeto documento, guardamos y creamos el PDF

            'Comprobamos si se ha creado el fichero PDF
            If System.IO.File.Exists(saveFileDialog1.FileName) Then
                If MsgBox("Datos Exportados correctamente " + "¿Desea abrir el archivo?", MsgBoxStyle.Question + MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
                    'Abrimos el fichero PDF con la aplicación asociada
                    System.Diagnostics.Process.Start(saveFileDialog1.FileName)
                End If
            Else
                MsgBox("El fichero PDF no se ha generado, " + "compruebe que tiene permisos en la carpeta de destino.", MsgBoxStyle.Exclamation + MsgBoxStyle.OkOnly)
            End If
        Catch ex As Exception
            MsgBox("Se ha producido un error al intentar convertir el presupuesto a PDF: " + ex.Message, MsgBoxStyle.Critical + MsgBoxStyle.OkOnly)
        End Try
    End Sub

    '------ FUNCIONES PARA EXPORTAR A PDF -----
    Private Function ObtenerTablaItems(dg As DataGridView)
        'Genero tabla con items del presupuesto
        Dim DataTable As New PdfPTable(6) 'Se crea un objeto PDFTable con el numero de columnas del DataGridView.

        Dim fntHeader As iTextSharp.text.Font = FontFactory.GetFont(Font.Name = "Cambria", 10, iTextSharp.text.Font.BOLD, BaseColor.WHITE)
        Dim fntBody As iTextSharp.text.Font = FontFactory.GetFont(Font.Name = "Calibri", 9, iTextSharp.text.Font.NORMAL, BaseColor.BLACK)

        'Se asignan algunas propiedades para el diseño del pdf
        DataTable.DefaultCell.Padding = 3

        DataTable.WidthPercentage = 100
        Dim TblWidths(5) As Single
        TblWidths(0) = 45
        TblWidths(1) = 115
        TblWidths(2) = 500
        TblWidths(3) = 180
        TblWidths(4) = 75
        TblWidths(5) = 75
        DataTable.SetWidths(TblWidths)

        DataTable.DefaultCell.BorderWidth = 2
        DataTable.DefaultCell.HorizontalAlignment = Element.ALIGN_CENTER


        DataTable.WidthPercentage = 100

        'Se capturan los nombres de las columnas del DataGridView.

        Dim Cell1 As New PdfPCell(New Phrase(dg.Columns(2).HeaderText, fntHeader))
        Cell1.HorizontalAlignment = HorizontalAlignment.Right
        DataTable.AddCell(Cell1).GrayFill = True
        Dim Cell2 As New PdfPCell(New Phrase(dg.Columns(9).HeaderText, fntHeader))
        Cell2.HorizontalAlignment = HorizontalAlignment.Right
        DataTable.AddCell(Cell2).GrayFill = True
        Dim Cell3 As New PdfPCell(New Phrase(dg.Columns(3).HeaderText, fntHeader))
        Cell3.HorizontalAlignment = HorizontalAlignment.Right
        DataTable.AddCell(Cell3).GrayFill = True
        Dim Cell4 As New PdfPCell(New Phrase(dg.Columns(4).HeaderText, fntHeader))
        Cell4.HorizontalAlignment = HorizontalAlignment.Right
        DataTable.AddCell(Cell4).GrayFill = True
        Dim Cell5 As New PdfPCell(New Phrase(dg.Columns(10).HeaderText, fntHeader))
        Cell5.HorizontalAlignment = HorizontalAlignment.Right
        DataTable.AddCell(Cell5).GrayFill = True
        Dim Cell6 As New PdfPCell(New Phrase(dg.Columns(5).HeaderText, fntHeader))
        Cell6.HorizontalAlignment = HorizontalAlignment.Right
        DataTable.AddCell(Cell6).GrayFill = True
        DataTable.HeaderRows = 1
        DataTable.DefaultCell.BorderWidth = 1
        'Se generan las columnas del DataGridView. 
        For i As Integer = 0 To dg.RowCount - 1
            Dim Cell01 As New PdfPCell(New Phrase(CStr(i + 1), fntBody))
            DataTable.AddCell(Cell01).HorizontalAlignment = Element.ALIGN_CENTER
            Dim Cell02 As New PdfPCell(New Phrase(dg(9, i).Value.ToString(), fntBody))
            DataTable.AddCell(Cell02).HorizontalAlignment = Element.ALIGN_CENTER
            Dim Cell03 As New PdfPCell(New Phrase(dg(3, i).Value.ToString(), fntBody))
            DataTable.AddCell(Cell03)
            Dim Cell04 As New PdfPCell(New Phrase(dg(4, i).Value.ToString(), fntBody))
            DataTable.AddCell(Cell04).HorizontalAlignment = Element.ALIGN_CENTER
            Dim Cell05 As New PdfPCell(New Phrase(dg(10, i).Value.ToString(), fntBody))
            DataTable.AddCell(Cell05).HorizontalAlignment = Element.ALIGN_CENTER
            Dim Cell06 As New PdfPCell(New Phrase(String.Format("$ {0}", dg(5, i).Value.ToString()), fntBody))
            DataTable.AddCell(Cell06).HorizontalAlignment = Element.ALIGN_CENTER
            DataTable.CompleteRow()
        Next
        Return DataTable
    End Function
    Private Function TablaSubtotales(Unpresupuesto As Modelo.Presupuesto)
        'Genero tabla con items del presupuesto
        Dim DataTable As New PdfPTable(2) 'Se crea un objeto PDFTable con el numero de columnas del DataGridView.

        Dim fntHeader As iTextSharp.text.Font = FontFactory.GetFont(Font.Name = "Cambria", 10, iTextSharp.text.Font.BOLD, BaseColor.WHITE)
        Dim fntBody As iTextSharp.text.Font = FontFactory.GetFont(Font.Name = "Calibri", 9, iTextSharp.text.Font.NORMAL, BaseColor.BLACK)

        'Se asignan algunas propiedades para el diseño del pdf
        DataTable.DefaultCell.Padding = 3

        DataTable.HorizontalAlignment = Element.ALIGN_RIGHT
        DataTable.WidthPercentage = 15
        Dim TblWidths(1) As Single
        TblWidths(0) = 99
        TblWidths(1) = 99
        DataTable.SetWidths(TblWidths)

        DataTable.DefaultCell.BorderWidth = 2
        DataTable.DefaultCell.HorizontalAlignment = Element.ALIGN_CENTER


        DataTable.WidthPercentage = 15

        Dim UnItem As New Modelo.Item With {.IdPresupuesto = cmbIdPresupuesto.SelectedItem}
        Dim UnGestorItem As New Modelo.GestorItem
        Dim Op As Integer
        Op = UnGestorItem.ObtenerItemMax(UnItem)

        'Se capturan los nombres de las columnas del DataGridView.
        'Se generan las columnas del DataGridView. 
        For i As Integer = 0 To Op - 1
            If i = 0 Then
                Dim Cell01 As New PdfPCell(New Phrase("General:", fntHeader))
                Cell01.HorizontalAlignment = HorizontalAlignment.Right
                DataTable.AddCell(Cell01).GrayFill = True
            Else
                Dim Cell01 As New PdfPCell(New Phrase(String.Format("Opcion {0}:", i), fntHeader))
                Cell01.HorizontalAlignment = HorizontalAlignment.Right
                DataTable.AddCell(Cell01).GrayFill = True
            End If
            Dim Precio As Double = UnGestorItem.CalcularPrecioOpcion(Unpresupuesto.IdPresupuesto, i)
            Dim Cell02 As New PdfPCell(New Phrase(String.Format("$ {0}", Precio), fntBody))
            DataTable.AddCell(Cell02).HorizontalAlignment = Element.ALIGN_CENTER
            DataTable.CompleteRow()
        Next
        Return DataTable
    End Function
    Private Function Titulo() As Paragraph
        Dim fntHeader As iTextSharp.text.Font = FontFactory.GetFont(Font.Name = "Cambria", 20, iTextSharp.text.Font.BOLD, BaseColor.BLACK)
        Dim fntBody As iTextSharp.text.Font = FontFactory.GetFont(Font.Name = "Calibri", 9, iTextSharp.text.Font.NORMAL, BaseColor.BLACK)

        Dim P As New Paragraph
        Dim C0 As New Chunk(vbNewLine)
        Dim C1 As New Chunk(String.Format("Cortinas Patricia López", cmbIdPresupuesto.Text), fntHeader)
        Dim Linea As New Chunk(New draw.LineSeparator())

        P.Alignment = HorizontalAlignment.Right
        P.Add(C1)
        P.Add(C0)
        P.Add(Linea)

        Titulo = P
        Return Titulo
    End Function
    Private Function Encabezado() As Paragraph
        Dim fntHeader As iTextSharp.text.Font = FontFactory.GetFont(Font.Name = "Cambria", 10, iTextSharp.text.Font.BOLD, BaseColor.BLACK)
        Dim fntBody As iTextSharp.text.Font = FontFactory.GetFont(Font.Name = "Calibri", 9, iTextSharp.text.Font.NORMAL, BaseColor.BLACK)

        Dim P As New Paragraph
        Dim C0 As New Chunk(vbNewLine)
        Dim C1 As New Chunk("Cuit: 27-14818451-3", fntBody)
        Dim C2 As New Chunk("IIBB: C.M. 902-649712-0", fntBody)
        Dim C3 As New Chunk("Visitenos en: Uruguay 1000, Beccar (1643)", fntBody)
        Dim C4 As New Chunk("Telefono: 4742-3205", fntBody)
        Dim glue As New Chunk(New draw.VerticalPositionMark())
        Dim Linea As New Chunk(New draw.LineSeparator())

        P.Alignment = HorizontalAlignment.Center
        P.Add(C3)
        P.Add(glue)
        P.Add(C1)
        P.Add(C0)
        P.Add(C4)
        P.Add(glue)
        P.Add(C2)
        P.Add(C0)
        P.Add(Linea)

        Encabezado = P
        Return Encabezado
    End Function
    Private Function Datoscliente() As Paragraph
        Dim fntHeader As iTextSharp.text.Font = FontFactory.GetFont(Font.Name = "Cambria", 10, iTextSharp.text.Font.BOLD, BaseColor.BLACK)
        Dim fntBody As iTextSharp.text.Font = FontFactory.GetFont(Font.Name = "Calibri", 9, iTextSharp.text.Font.NORMAL, BaseColor.BLACK)

        Dim P As New Paragraph
        Dim C0 As New Chunk(vbNewLine)
        Dim C1 As New Chunk("Cliente: ", fntHeader)
        Dim C2 As New Chunk(cmbCliente.Text, fntBody)
        Dim C3 As New Chunk("Direccion: ", fntHeader)
        'Dim C4 As New Chunk(txt, fntBody)
        Dim C5 As New Chunk("Mail: ", fntHeader)
        'Dim C6 As New Chunk(txt, fntBody)
        Dim C7 As New Chunk("Telefono: ", fntHeader)
        'Dim C8 As New Chunk(txt, fntBody)
        Dim C9 As New Chunk(String.Format("Presupuesto N°: "), fntHeader)
        Dim C10 As New Chunk(String.Format("Fecha: "), fntHeader)
        Dim C11 As New Chunk(String.Format("{0}", cmbIdPresupuesto.Text), fntBody)
        Dim C12 As New Chunk(String.Format("{0}", Date.Today.ToShortDateString), fntBody)
        Dim glue As New Chunk(New draw.VerticalPositionMark())

        P.Add(C0)
        P.Add(C1)
        P.Add(C2)
        P.Add(glue)
        P.Add(C9)
        P.Add(C11)
        P.Add(C0)
        P.Add(C3)
        'P.Add(C4)
        P.Add(glue)
        P.Add(C10)
        P.Add(C12)
        P.Add(C0)
        P.Add(C5)
        'P.Add(C6)
        P.Add(C0)
        P.Add(C7)
        'P.Add(C8)
        P.Add(C0)
        P.Add(C0)

        Datoscliente = P
        Return Datoscliente
    End Function
    Private Function Notasdepago() As Paragraph
        Dim fntHeader As iTextSharp.text.Font = FontFactory.GetFont(Font.Name = "Cambria", 10, iTextSharp.text.Font.BOLD, BaseColor.BLACK)
        Dim fntBody As iTextSharp.text.Font = FontFactory.GetFont(Font.Name = "Calibri", 9, iTextSharp.text.Font.NORMAL, BaseColor.BLACK)

        Dim P As New Paragraph
        Dim C0 As New Chunk(vbNewLine)
        Dim C1 As New Chunk("Observaciones: ", fntHeader)
        Dim C2 As New Chunk("Los precios son en efectivo.", fntBody)
        Dim C3 As New Chunk("Forma de Pago: ", fntHeader)
        Dim C4 As New Chunk("Se abona 50% por adelantado, saldo contra entrega.", fntBody)
        Dim C5 As New Chunk("Tarjetas: ", fntHeader)
        Dim C6 As New Chunk("Consultar", fntBody)
        Dim C7 As New Chunk(String.Format("El presupuesto tiene valides por {0} días", txtVigencia.Text), fntHeader)

        P.Add(C0)
        P.Add(C1)
        P.Add(C2)
        P.Add(C0)
        P.Add(C3)
        P.Add(C4)
        P.Add(C0)
        P.Add(C5)
        P.Add(C6)
        P.Add(C0)
        P.Add(C7)

        Notasdepago = P
        Return Notasdepago
    End Function

    '------ EVENTOS DE COMBOBOX ------
    Private Sub ActualizarCmbcliente()
        Dim UnGestorClientes As New Modelo.GestorClientes
        cmbCliente.DataSource = UnGestorClientes.ListarTodosNombre()
        cmbCliente.DisplayMember = "Nombre"
    End Sub
    Private Sub ActualizarCmbIdPresupuesto()
        Dim UnGestorPresupuesto As New Modelo.GestorPresupuesto
        cmbIdPresupuesto.DataSource = UnGestorPresupuesto.ListarTodosNombre
        cmbIdPresupuesto.DisplayMember = "IdPresupuesto"
    End Sub
    Private Sub cmbCliente_SelectionChangeCommitted(sender As System.Object, e As System.EventArgs) Handles cmbCliente.SelectionChangeCommitted
        Dim ClienteSeleccionado As New Modelo.Cliente
        Dim UnGestorClientes As New Modelo.GestorClientes
        ClienteSeleccionado = UnGestorClientes.BuscarPorId(cmbCliente.SelectedItem)
        txtIdCliente.Text = ClienteSeleccionado.IdCliente
    End Sub
    Private Sub cmbIdPresupuesto_SelectionChangeCommitted(sender As System.Object, e As System.EventArgs) Handles cmbIdPresupuesto.SelectionChangeCommitted
        Dim UnGestorPresupuesto As New Modelo.GestorPresupuesto
        Dim PresupuestoSeleccionado As New Modelo.Presupuesto

        PresupuestoSeleccionado = UnGestorPresupuesto.BuscarPorId(cmbIdPresupuesto.SelectedItem)
        MostrarDetalleDePresupuesto(PresupuestoSeleccionado)

        If PresupuestoSeleccionado.Aprobado = True Then
            btnAprobarItem.Enabled = False
        Else
            btnAprobarItem.Enabled = True
        End If

        ExportarON()
        ModoNormal()
        ModoReadOnly()
    End Sub

    '------ FUNCIONES PARA CARGAR OBJETOS ------
    Private Sub MostrarDetalleDePresupuesto(UnPresupuesto As Modelo.Presupuesto)
        Operacion = 1
        Try
            Dim UnGestorClientes As New Modelo.GestorClientes
            Dim UnCliente As New Modelo.Cliente
            UnCliente.IdCliente = UnPresupuesto.IdCliente
            cmbCliente.Text = UnGestorClientes.BuscarPorId(UnCliente).Nombre
            txtIdCliente.Text = UnPresupuesto.IdCliente

            chkColocacion.Checked = UnPresupuesto.Colocacion
            txtVigencia.Text = UnPresupuesto.Vigencia
            dtpEntrega.Text = UnPresupuesto.FechaEntrega
            txtNotas.Text = UnPresupuesto.Notas
            PresupuestoId = CInt(cmbIdPresupuesto.Text)

            If UnPresupuesto.Aprobado = True Then
                ModoItemsReadOnly()
            Else
                ModoItemsEditar()
            End If

            Actualizardvg(UnPresupuesto)
        Catch
            MsgBox("Se ha producido un error al intentar cargar el Presupuesto", MsgBoxStyle.OkOnly)
        End Try
    End Sub
    Public Sub CargarPresupuesto(UnPresupuesto As Modelo.Presupuesto)
        Cursor = System.Windows.Forms.Cursors.WaitCursor
        Try
            Dim PresupuestoSeleccionado As New Modelo.Presupuesto
            Dim UnGestorPresupuesto As New Modelo.GestorPresupuesto
            PresupuestoSeleccionado = UnGestorPresupuesto.BuscarPorId(UnPresupuesto)
            MostrarDetalleDePresupuesto(PresupuestoSeleccionado)
            cmbIdPresupuesto.Enabled = False
        Catch
        End Try
        Cursor = System.Windows.Forms.Cursors.Default
    End Sub

    '------ FUNCIONES GENERALES ------
    Private Sub ModoNormal()
        btnNuevo.Enabled = True
        btnCancelar.Enabled = False
        btnEliminar.Enabled = True
        btnGuardar.Enabled = False
    End Sub
    Private Sub ModoReadOnly()
        cmbIdPresupuesto.Enabled = True
        cmbCliente.Enabled = False

        dtpEntrega.Enabled = False

        txtNotas.ReadOnly = True
        txtVigencia.ReadOnly = True
        txtTotal.ReadOnly = True

        chkColocacion.Enabled = False

        ABM = False
    End Sub
    Private Sub ModoABM()
        btnNuevo.Enabled = False
        btnCancelar.Enabled = True
        btnEliminar.Enabled = False
        btnGuardar.Enabled = True
    End Sub
    Private Sub ModoEditar()
        cmbIdPresupuesto.Enabled = False
        cmbCliente.Enabled = True

        dtpEntrega.Enabled = True

        txtNotas.ReadOnly = False
        txtVigencia.ReadOnly = False
        txtTotal.ReadOnly = False

        chkColocacion.Enabled = True

        ABM = True
    End Sub

    Private Sub ModoItemsReadOnly()
        btnCortina.Enabled = False
        btnSillones.Enabled = False
        btnTela.Enabled = False
        btnFundas.Enabled = False
    End Sub
    Private Sub ModoItemsEditar()
        btnCortina.Enabled = True
        btnSillones.Enabled = True
        btnTela.Enabled = True
        btnFundas.Enabled = True
    End Sub

    Private Sub LimpiarCasilleros()
        dtpEntrega.Text = Date.Today.AddDays(15)
        txtNotas.Text = ""
        txtVigencia.Text = "7"
        cmbCliente.SelectedIndex = -1
        chkColocacion.Checked = False
    End Sub

    Private Sub AgregarItem()
        Dim UnGestorItem As New Modelo.GestorItem
        Dim ItemSeleccionado As New Modelo.Item
        ItemSeleccionado.IdPresupuesto = cmbIdPresupuesto.Text
        Item = UnGestorItem.ObtenerItemMax(ItemSeleccionado)
    End Sub
    Public Sub EliminarItem()
        Item = Item - 1
    End Sub

    Private Sub ExportarON()
        btnPresPdf.Enabled = True
        btnTallerPDF.Enabled = True
        btnAprobar.Enabled = True
    End Sub
    Private Sub ExportarOff()
        btnPresPdf.Enabled = False
        btnTallerPDF.Enabled = False
        btnAprobar.Enabled = False
    End Sub



    '------ CONFIGURACIONES ------
    Public Sub ConfigurarForm()
        Me.Dock = DockStyle.None
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.ShowIcon = False
        Me.MaximumSize = New Size(frmPrincipal.ClientSize.Width - 4, frmPrincipal.ClientSize.Height - 4)
        Me.MinimumSize = Me.MaximumSize
        Me.Dock = DockStyle.Fill
        Dim Separacion As Integer = (frmPrincipal.ClientSize.Width - 1090 + 150) / 2
        gcBuscar.Location = New Point(Separacion, 45)
        gcPresupuesto.Location = New Point(Separacion, 88)
        gcInfoAdicional.Location = New Point(Separacion, 150)
        gcItems.Location = New Point(Separacion, 275)
        gcItems.Size = New Size(1090, frmPrincipal.ClientSize.Height - (275 + 75))
        gcNuevoItem.Location = New Point(Separacion, frmPrincipal.ClientSize.Height - 70)
        lblTitulo.Location = New Point((frmPrincipal.ClientSize.Width - 465 + 150) / 2, 4)
        Dim Separacionbtn As Integer = (frmPrincipal.ClientSize.Width - Separacion + 75)
        btnCancelar.Location = New Point(Separacionbtn, frmPrincipal.ClientSize.Height - 70)
        btnSalir.Location = New Point(Separacionbtn, frmPrincipal.ClientSize.Height - 37)
        btnAprobarItem.Location = New Point(124, gcItems.ClientSize.Height - 30)
        txtTotal.Location = New Point(1006, gcItems.ClientSize.Height - 30)
        lblTotal.Location = New Point(961, gcItems.ClientSize.Height - 27)
    End Sub
    Public Sub ConfigurarTextBox()
        'Configuro la cantidad de cararcteres maximos
        txtNotas.MaxLength = 200
        'Configuro alineacion del texto
        txtIdCliente.TextAlign = HorizontalAlignment.Center
        txtVigencia.TextAlign = HorizontalAlignment.Center
        txtTotal.TextAlign = HorizontalAlignment.Center
        'Configuro los que seran siempre solo lectura
        txtIdCliente.ReadOnly = True
        txtTotal.ReadOnly = True
    End Sub
    Private Sub ConfigurarDataGridView()
        dgvItems.Location = New Point(124, 27)
        dgvItems.Size = New Size(958, gcItems.ClientSize.Height - 60)
    End Sub

    '------ EVENTOS DE DATAGRIDVIEW ------
    Public Sub Actualizardvg(Unpresupuesto As Modelo.Presupuesto)
        Dim UnItem As New Modelo.Item With {.IdPresupuesto = Unpresupuesto.IdPresupuesto}
        Dim unAccesoItem As New Modelo.GestorItem
        dgvItems.DataSource = unAccesoItem.SelectItemPorPresupuesto(UnItem)
        Dim UnGestorItem As New Modelo.GestorItem
        Item = UnGestorItem.ObtenerItemMax(UnItem)
        txtTotal.Text = UnGestorItem.CalcularTotal(UnItem)
        Configdvg()
    End Sub
    Private Sub dgvItems_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvItems.CellDoubleClick
        Cursor = System.Windows.Forms.Cursors.WaitCursor
        Dim UnItem As New Modelo.Item
        UnItem.IdPresupuesto = CInt(cmbIdPresupuesto.Text)
        Item = CInt(dgvItems.CurrentRow.Cells("Item").Value)
        UnItem.Item = Item
        UnItem.Descripcion = dgvItems.CurrentRow.Cells("Descripcion").Value
        UnItem.Ubicacion = dgvItems.CurrentRow.Cells("Ubicacion").Value
        UnItem.Observacion = dgvItems.CurrentRow.Cells("Observacion").Value
        UnItem.Opcion = CInt(dgvItems.CurrentRow.Cells("Opcion").Value)
        UnItem.Precio = CDbl(dgvItems.CurrentRow.Cells("Precio").Value)
        Dim M As Integer = CInt(dgvItems.CurrentRow.Cells("IdCategoria").Value)
        Select Case M
            Case 1
                frmCortinas.MdiParent = frmPrincipal
                frmCortinas.Show()
                Me.Hide()
                frmCortinas.SendToBack()
                frmDockBotones.BringToFront()
                frmCortinas.Operacion = 1
                frmCortinas.CargarArticulo(UnItem)
            Case 2
                frmGenero.MdiParent = frmPrincipal
                frmGenero.Show()
                Me.Hide()
                frmGenero.SendToBack()
                frmDockBotones.BringToFront()
                frmGenero.Operacion = 1
                'frmGenero.CargarArticulo(UnItem)
            Case 3
                frmFundas.MdiParent = frmPrincipal
                frmGenero.Show()
                Me.Hide()
                frmFundas.SendToBack()
                frmDockBotones.BringToFront()
                frmFundas.Operacion = 1
                'frmFundas.CargarArticulo(UnItem)
            Case 4
                frmSillones.MdiParent = frmPrincipal
                frmSillones.Show()
                Me.Hide()
                frmSillones.SendToBack()
                frmDockBotones.BringToFront()
                frmSillones.Operacion = 1
                'frmSillones.CargarArticulo(UnItem)
        End Select
        Cursor = System.Windows.Forms.Cursors.Default
    End Sub
    Private Sub dgvItems_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvItems.CellClick
        If CBool(dgvItems.CurrentRow.Cells(7).Value) = False Then
            btnAprobarItem.Text = "Aprobar Item"
        Else
            btnAprobarItem.Text = "Desaprobar Item"
        End If
    End Sub
    Private Sub Configdvg()
        dgvItems.Columns("IdItem").Visible = False
        dgvItems.Columns("IdPresupuesto").Visible = False
        dgvItems.Columns("IdCategoria").Visible = False
        dgvItems.Columns("Costo").Visible = False
        dgvItems.Columns("Item").DisplayIndex = 1
        dgvItems.Columns("Item").Width = 35
        dgvItems.Columns("Item").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgvItems.Columns("Ubicacion").DisplayIndex = 2
        dgvItems.Columns("Ubicacion").Width = 115
        dgvItems.Columns("Ubicacion").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgvItems.Columns("Descripcion").DisplayIndex = 3
        dgvItems.Columns("Descripcion").Width = 328
        dgvItems.Columns("Observacion").DisplayIndex = 4
        dgvItems.Columns("Observacion").Width = 200
        dgvItems.Columns("Observacion").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgvItems.Columns("Precio").DisplayIndex = 5
        dgvItems.Columns("Precio").Width = 60
        dgvItems.Columns("Precio").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgvItems.Columns("Opcion").DisplayIndex = 6
        dgvItems.Columns("Opcion").Width = 60
        dgvItems.Columns("Opcion").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgvItems.Columns("Cantidad").DisplayIndex = 8
        dgvItems.Columns("Cantidad").Width = 60
        dgvItems.Columns("Cantidad").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgvItems.Columns("Aprobado").DisplayIndex = 7
        dgvItems.Columns("Aprobado").Width = 60
        dgvItems.SelectionMode = DataGridViewSelectionMode.FullRowSelect
    End Sub
    Public Sub ActualizarListaItems()
        Dim UnGestorPresupuesto As New Modelo.GestorPresupuesto
        Dim PresupuestoSeleccionado As New Modelo.Presupuesto
        PresupuestoSeleccionado = UnGestorPresupuesto.BuscarPorId(cmbIdPresupuesto.SelectedItem)
        Actualizardvg(PresupuestoSeleccionado)
    End Sub

    '------ EVENTOS DE TEXTBOX ------
    Private Sub txtEdicion(sender As Object, e As EventArgs) Handles dtpEntrega.Click, cmbCliente.Click, txtNotas.Click, txtVigencia.Click, chkColocacion.Click
        If ABM = False Then
            Operacion = 1
            ModoABM()
            ModoEditar()
        Else
        End If
    End Sub
End Class