﻿Public Class frmCobros

    '------ EVENTOS DE FORMULARIOS ------
    Private Sub frmCobros_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        ConfigurarForm()
        ConfigurarTextBox()
        ConfigurarDataGridView()
    End Sub

    '------ EVENTOS DE BOTONES ------
    Private Sub btnSalir_Click(sender As Object, e As EventArgs) Handles btnSalir.Click
        'ModoReadOnly()
        'Operacion = 0
        Me.Close()
    End Sub

    '------ CONFIGURACIONES ------
    Public Sub ConfigurarForm()
        Me.Dock = DockStyle.None
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.ShowIcon = False
        Me.MaximumSize = New Size(frmPrincipal.ClientSize.Width - 4, frmPrincipal.ClientSize.Height - 4)
        Me.MinimumSize = Me.MaximumSize
        Me.Dock = DockStyle.Fill
        Dim Separacion As Integer = (frmPrincipal.ClientSize.Width - 1090 + 150) / 2
        'gcBuscar.Location = New Point(Separacion, 45)
        'gcInfoGral.Location = New Point(Separacion, 88)
        'gcDomicilio.Location = New Point(Separacion, 216)
        'gcContacto.Location = New Point(Separacion, 278)
        'gcHistorial.Size = New Size(1009, frmPrincipal.ClientSize.Height - (464 + 10))
        'gcHistorial.Location = New Point(Separacion, 464)
        'lblTitulo.Location = New Point((frmPrincipal.ClientSize.Width - 384 + 150) / 2, 4)
        Dim Separacionbtn As Integer = (frmPrincipal.ClientSize.Width - Separacion + 75)
        btnCancelar.Location = New Point(Separacionbtn, frmPrincipal.ClientSize.Height - 103)
        btnEliminar.Location = New Point(Separacionbtn, frmPrincipal.ClientSize.Height - 70)
        btnSalir.Location = New Point(Separacionbtn, frmPrincipal.ClientSize.Height - 37)
    End Sub
    Public Sub ConfigurarTextBox()
        'Configuro la cantidad de cararcteres maximos
        'txtNombre.MaxLength = 200
        'txtDireccion.MaxLength = 150
        'txtCP.MaxLength = 8
        'txtEmpresa.MaxLength = 100
        'txtWeb.MaxLength = 75
        'txtMail1.MaxLength = 50
        'txtMail2.MaxLength = 50
        'txtMail3.MaxLength = 50
        'txtContacto1.MaxLength = 50
        'txtContacto2.MaxLength = 50
        'txtContacto3.MaxLength = 50
        'Configuro alineacion del texto
        'txtId.TextAlign = HorizontalAlignment.Center
        'txtCP.TextAlign = HorizontalAlignment.Center
        'txtTelefono1.TextAlign = HorizontalAlignment.Center
        'txtTelefono2.TextAlign = HorizontalAlignment.Center
        'txtTelefono3.TextAlign = HorizontalAlignment.Center
        'txtMovil1.TextAlign = HorizontalAlignment.Center
        'txtMovil2.TextAlign = HorizontalAlignment.Center
        'txtMovil3.TextAlign = HorizontalAlignment.Center
        'txtMail1.TextAlign = HorizontalAlignment.Center
        'txtMail2.TextAlign = HorizontalAlignment.Center
        'txtMail3.TextAlign = HorizontalAlignment.Center
        'txtContacto1.TextAlign = HorizontalAlignment.Center
        'txtContacto2.TextAlign = HorizontalAlignment.Center
        'txtContacto3.TextAlign = HorizontalAlignment.Center
        'txtCUIT.TextAlign = HorizontalAlignment.Center
        'txtDNI.TextAlign = HorizontalAlignment.Center
        'Configuro los que seran siempre solo lectura
        'txtId.ReadOnly = True
    End Sub
    Private Sub ConfigurarDataGridView()
        'dgvHistorial.Location = New Point(82, 27)
        'dgvHistorial.Size = New Size(916, gcHistorial.ClientSize.Height - 33)
    End Sub
End Class