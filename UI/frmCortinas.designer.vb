﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCortinas
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.gcSistema = New DevExpress.XtraEditors.GroupControl()
        Me.txtAccP2 = New System.Windows.Forms.TextBox()
        Me.txtAccP1 = New System.Windows.Forms.TextBox()
        Me.txtAccP3 = New System.Windows.Forms.TextBox()
        Me.Label57 = New System.Windows.Forms.Label()
        Me.Label58 = New System.Windows.Forms.Label()
        Me.Label59 = New System.Windows.Forms.Label()
        Me.Label60 = New System.Windows.Forms.Label()
        Me.Label61 = New System.Windows.Forms.Label()
        Me.Label62 = New System.Windows.Forms.Label()
        Me.cmbAcc2 = New System.Windows.Forms.ComboBox()
        Me.cmbAcc1 = New System.Windows.Forms.ComboBox()
        Me.cmbAcc3 = New System.Windows.Forms.ComboBox()
        Me.txtAccQ1 = New DevExpress.XtraEditors.SpinEdit()
        Me.txtAccQ2 = New DevExpress.XtraEditors.SpinEdit()
        Me.txtAccQ3 = New DevExpress.XtraEditors.SpinEdit()
        Me.Label63 = New System.Windows.Forms.Label()
        Me.Label64 = New System.Windows.Forms.Label()
        Me.Label65 = New System.Windows.Forms.Label()
        Me.txtSubSistP = New System.Windows.Forms.TextBox()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.txtSistemaO = New System.Windows.Forms.TextBox()
        Me.Label53 = New System.Windows.Forms.Label()
        Me.txtTerminalesP = New System.Windows.Forms.TextBox()
        Me.txtArgollasP = New System.Windows.Forms.TextBox()
        Me.txtSoportesP = New System.Windows.Forms.TextBox()
        Me.txtSistemaP = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txtSistemaQ = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.cmbTerminales = New System.Windows.Forms.ComboBox()
        Me.cmbArgollas = New System.Windows.Forms.ComboBox()
        Me.cmbSoportes = New System.Windows.Forms.ComboBox()
        Me.cmbSistema = New System.Windows.Forms.ComboBox()
        Me.txtTerminalesQ = New DevExpress.XtraEditors.SpinEdit()
        Me.txtArgollasQ = New DevExpress.XtraEditors.SpinEdit()
        Me.txtSoportesQ = New DevExpress.XtraEditors.SpinEdit()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.gcCortina1 = New DevExpress.XtraEditors.GroupControl()
        Me.gcPañoD1 = New DevExpress.XtraEditors.GroupControl()
        Me.chkCD1 = New System.Windows.Forms.CheckBox()
        Me.txtLargoD1 = New System.Windows.Forms.TextBox()
        Me.txtAnchoD1 = New System.Windows.Forms.TextBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.txtSubCort1P = New System.Windows.Forms.TextBox()
        Me.Label29 = New System.Windows.Forms.Label()
        Me.Label51 = New System.Windows.Forms.Label()
        Me.txtForro1P = New System.Windows.Forms.TextBox()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.txtGenero1P = New System.Windows.Forms.TextBox()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.txtGeneroQL1 = New System.Windows.Forms.TextBox()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.txtForroQL1 = New System.Windows.Forms.TextBox()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.txtGeneroQA1 = New System.Windows.Forms.TextBox()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.txtForroQA1 = New System.Windows.Forms.TextBox()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.gcPañoI1 = New DevExpress.XtraEditors.GroupControl()
        Me.chkCI1 = New System.Windows.Forms.CheckBox()
        Me.txtLargoI1 = New System.Windows.Forms.TextBox()
        Me.txtAnchoI1 = New System.Windows.Forms.TextBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.txtCabezal1P = New System.Windows.Forms.TextBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.txtCabezal1Q = New System.Windows.Forms.TextBox()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.cmbForro1 = New System.Windows.Forms.ComboBox()
        Me.cmbGenero1 = New System.Windows.Forms.ComboBox()
        Me.cmbCabezal1 = New System.Windows.Forms.ComboBox()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.gcTotal = New DevExpress.XtraEditors.GroupControl()
        Me.txtFinal = New System.Windows.Forms.TextBox()
        Me.Label50 = New System.Windows.Forms.Label()
        Me.txtIVA = New System.Windows.Forms.TextBox()
        Me.Label49 = New System.Windows.Forms.Label()
        Me.txtTotalP = New System.Windows.Forms.TextBox()
        Me.Label46 = New System.Windows.Forms.Label()
        Me.gcItem = New DevExpress.XtraEditors.GroupControl()
        Me.chkPaño2 = New System.Windows.Forms.CheckBox()
        Me.chkPaño1 = New System.Windows.Forms.CheckBox()
        Me.chkSistema = New System.Windows.Forms.CheckBox()
        Me.Label56 = New System.Windows.Forms.Label()
        Me.nupCantidad = New DevExpress.XtraEditors.SpinEdit()
        Me.txtItemDesc = New System.Windows.Forms.TextBox()
        Me.Label55 = New System.Windows.Forms.Label()
        Me.cmbOpcion = New System.Windows.Forms.ComboBox()
        Me.Label54 = New System.Windows.Forms.Label()
        Me.txtObservaciones = New System.Windows.Forms.TextBox()
        Me.Label48 = New System.Windows.Forms.Label()
        Me.txtUbicacion = New System.Windows.Forms.TextBox()
        Me.Label47 = New System.Windows.Forms.Label()
        Me.btnSalir = New DevExpress.XtraEditors.SimpleButton()
        Me.btnGuardar = New DevExpress.XtraEditors.SimpleButton()
        Me.gcCortina2 = New DevExpress.XtraEditors.GroupControl()
        Me.gcPañoD2 = New DevExpress.XtraEditors.GroupControl()
        Me.chkCD2 = New System.Windows.Forms.CheckBox()
        Me.txtLargoD2 = New System.Windows.Forms.TextBox()
        Me.txtAnchoD2 = New System.Windows.Forms.TextBox()
        Me.Label30 = New System.Windows.Forms.Label()
        Me.Label31 = New System.Windows.Forms.Label()
        Me.TextBox7 = New System.Windows.Forms.TextBox()
        Me.txtSubCort2P = New System.Windows.Forms.TextBox()
        Me.Label32 = New System.Windows.Forms.Label()
        Me.Label33 = New System.Windows.Forms.Label()
        Me.txtForro2P = New System.Windows.Forms.TextBox()
        Me.Label34 = New System.Windows.Forms.Label()
        Me.txtGenero2P = New System.Windows.Forms.TextBox()
        Me.Label35 = New System.Windows.Forms.Label()
        Me.txtGeneroQL2 = New System.Windows.Forms.TextBox()
        Me.Label36 = New System.Windows.Forms.Label()
        Me.txtForroQL2 = New System.Windows.Forms.TextBox()
        Me.Label37 = New System.Windows.Forms.Label()
        Me.txtGeneroQA2 = New System.Windows.Forms.TextBox()
        Me.Label38 = New System.Windows.Forms.Label()
        Me.txtForroQA2 = New System.Windows.Forms.TextBox()
        Me.Label39 = New System.Windows.Forms.Label()
        Me.gcPañoI2 = New DevExpress.XtraEditors.GroupControl()
        Me.chkCI2 = New System.Windows.Forms.CheckBox()
        Me.txtLargoI2 = New System.Windows.Forms.TextBox()
        Me.txtAnchoI2 = New System.Windows.Forms.TextBox()
        Me.Label40 = New System.Windows.Forms.Label()
        Me.Label41 = New System.Windows.Forms.Label()
        Me.txtCabezal2P = New System.Windows.Forms.TextBox()
        Me.Label42 = New System.Windows.Forms.Label()
        Me.txtCabezal2Q = New System.Windows.Forms.TextBox()
        Me.Label43 = New System.Windows.Forms.Label()
        Me.cmbForro2 = New System.Windows.Forms.ComboBox()
        Me.cmbGenero2 = New System.Windows.Forms.ComboBox()
        Me.cmbCabezal2 = New System.Windows.Forms.ComboBox()
        Me.Label44 = New System.Windows.Forms.Label()
        Me.Label45 = New System.Windows.Forms.Label()
        Me.Label52 = New System.Windows.Forms.Label()
        CType(Me.gcSistema, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gcSistema.SuspendLayout()
        CType(Me.txtAccQ1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtAccQ2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtAccQ3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTerminalesQ.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtArgollasQ.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtSoportesQ.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gcCortina1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gcCortina1.SuspendLayout()
        CType(Me.gcPañoD1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gcPañoD1.SuspendLayout()
        CType(Me.gcPañoI1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gcPañoI1.SuspendLayout()
        CType(Me.gcTotal, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gcTotal.SuspendLayout()
        CType(Me.gcItem, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gcItem.SuspendLayout()
        CType(Me.nupCantidad.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gcCortina2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gcCortina2.SuspendLayout()
        CType(Me.gcPañoD2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gcPañoD2.SuspendLayout()
        CType(Me.gcPañoI2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gcPañoI2.SuspendLayout()
        Me.SuspendLayout()
        '
        'gcSistema
        '
        Me.gcSistema.Controls.Add(Me.txtAccP2)
        Me.gcSistema.Controls.Add(Me.txtAccP1)
        Me.gcSistema.Controls.Add(Me.txtAccP3)
        Me.gcSistema.Controls.Add(Me.Label57)
        Me.gcSistema.Controls.Add(Me.Label58)
        Me.gcSistema.Controls.Add(Me.Label59)
        Me.gcSistema.Controls.Add(Me.Label60)
        Me.gcSistema.Controls.Add(Me.Label61)
        Me.gcSistema.Controls.Add(Me.Label62)
        Me.gcSistema.Controls.Add(Me.cmbAcc2)
        Me.gcSistema.Controls.Add(Me.cmbAcc1)
        Me.gcSistema.Controls.Add(Me.cmbAcc3)
        Me.gcSistema.Controls.Add(Me.txtAccQ1)
        Me.gcSistema.Controls.Add(Me.txtAccQ2)
        Me.gcSistema.Controls.Add(Me.txtAccQ3)
        Me.gcSistema.Controls.Add(Me.Label63)
        Me.gcSistema.Controls.Add(Me.Label64)
        Me.gcSistema.Controls.Add(Me.Label65)
        Me.gcSistema.Controls.Add(Me.txtSubSistP)
        Me.gcSistema.Controls.Add(Me.Label28)
        Me.gcSistema.Controls.Add(Me.txtSistemaO)
        Me.gcSistema.Controls.Add(Me.Label53)
        Me.gcSistema.Controls.Add(Me.txtTerminalesP)
        Me.gcSistema.Controls.Add(Me.txtArgollasP)
        Me.gcSistema.Controls.Add(Me.txtSoportesP)
        Me.gcSistema.Controls.Add(Me.txtSistemaP)
        Me.gcSistema.Controls.Add(Me.Label12)
        Me.gcSistema.Controls.Add(Me.Label11)
        Me.gcSistema.Controls.Add(Me.Label10)
        Me.gcSistema.Controls.Add(Me.Label9)
        Me.gcSistema.Controls.Add(Me.txtSistemaQ)
        Me.gcSistema.Controls.Add(Me.Label8)
        Me.gcSistema.Controls.Add(Me.Label6)
        Me.gcSistema.Controls.Add(Me.Label5)
        Me.gcSistema.Controls.Add(Me.Label7)
        Me.gcSistema.Controls.Add(Me.cmbTerminales)
        Me.gcSistema.Controls.Add(Me.cmbArgollas)
        Me.gcSistema.Controls.Add(Me.cmbSoportes)
        Me.gcSistema.Controls.Add(Me.cmbSistema)
        Me.gcSistema.Controls.Add(Me.txtTerminalesQ)
        Me.gcSistema.Controls.Add(Me.txtArgollasQ)
        Me.gcSistema.Controls.Add(Me.txtSoportesQ)
        Me.gcSistema.Controls.Add(Me.Label4)
        Me.gcSistema.Controls.Add(Me.Label3)
        Me.gcSistema.Controls.Add(Me.Label2)
        Me.gcSistema.Controls.Add(Me.Label1)
        Me.gcSistema.Location = New System.Drawing.Point(12, 126)
        Me.gcSistema.LookAndFeel.SkinName = "Visual Studio 2013 Dark"
        Me.gcSistema.LookAndFeel.UseDefaultLookAndFeel = False
        Me.gcSistema.Name = "gcSistema"
        Me.gcSistema.Size = New System.Drawing.Size(1090, 221)
        Me.gcSistema.TabIndex = 0
        Me.gcSistema.Text = "Sistema"
        '
        'txtAccP2
        '
        Me.txtAccP2.Font = New System.Drawing.Font("SansSerif", 9.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.txtAccP2.Location = New System.Drawing.Point(733, 166)
        Me.txtAccP2.Name = "txtAccP2"
        Me.txtAccP2.Size = New System.Drawing.Size(70, 22)
        Me.txtAccP2.TabIndex = 58
        '
        'txtAccP1
        '
        Me.txtAccP1.Font = New System.Drawing.Font("SansSerif", 9.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.txtAccP1.Location = New System.Drawing.Point(733, 138)
        Me.txtAccP1.Name = "txtAccP1"
        Me.txtAccP1.Size = New System.Drawing.Size(70, 22)
        Me.txtAccP1.TabIndex = 57
        '
        'txtAccP3
        '
        Me.txtAccP3.Font = New System.Drawing.Font("SansSerif", 9.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.txtAccP3.Location = New System.Drawing.Point(733, 194)
        Me.txtAccP3.Name = "txtAccP3"
        Me.txtAccP3.Size = New System.Drawing.Size(70, 22)
        Me.txtAccP3.TabIndex = 56
        '
        'Label57
        '
        Me.Label57.AutoSize = True
        Me.Label57.Font = New System.Drawing.Font("SansSerif", 8.999999!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.Label57.Location = New System.Drawing.Point(679, 195)
        Me.Label57.Name = "Label57"
        Me.Label57.Size = New System.Drawing.Size(48, 14)
        Me.Label57.TabIndex = 55
        Me.Label57.Text = "Precio:"
        '
        'Label58
        '
        Me.Label58.AutoSize = True
        Me.Label58.Font = New System.Drawing.Font("SansSerif", 8.999999!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.Label58.Location = New System.Drawing.Point(679, 168)
        Me.Label58.Name = "Label58"
        Me.Label58.Size = New System.Drawing.Size(48, 14)
        Me.Label58.TabIndex = 54
        Me.Label58.Text = "Precio:"
        '
        'Label59
        '
        Me.Label59.AutoSize = True
        Me.Label59.Font = New System.Drawing.Font("SansSerif", 8.999999!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.Label59.Location = New System.Drawing.Point(679, 141)
        Me.Label59.Name = "Label59"
        Me.Label59.Size = New System.Drawing.Size(48, 14)
        Me.Label59.TabIndex = 53
        Me.Label59.Text = "Precio:"
        '
        'Label60
        '
        Me.Label60.AutoSize = True
        Me.Label60.Font = New System.Drawing.Font("SansSerif", 8.999999!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.Label60.Location = New System.Drawing.Point(565, 141)
        Me.Label60.Name = "Label60"
        Me.Label60.Size = New System.Drawing.Size(62, 14)
        Me.Label60.TabIndex = 52
        Me.Label60.Text = "Cantidad:"
        '
        'Label61
        '
        Me.Label61.AutoSize = True
        Me.Label61.Font = New System.Drawing.Font("SansSerif", 8.999999!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.Label61.Location = New System.Drawing.Point(565, 169)
        Me.Label61.Name = "Label61"
        Me.Label61.Size = New System.Drawing.Size(62, 14)
        Me.Label61.TabIndex = 51
        Me.Label61.Text = "Cantidad:"
        '
        'Label62
        '
        Me.Label62.AutoSize = True
        Me.Label62.Font = New System.Drawing.Font("SansSerif", 8.999999!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.Label62.Location = New System.Drawing.Point(565, 197)
        Me.Label62.Name = "Label62"
        Me.Label62.Size = New System.Drawing.Size(62, 14)
        Me.Label62.TabIndex = 50
        Me.Label62.Text = "Cantidad:"
        '
        'cmbAcc2
        '
        Me.cmbAcc2.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.cmbAcc2.FormattingEnabled = True
        Me.cmbAcc2.Location = New System.Drawing.Point(99, 167)
        Me.cmbAcc2.Name = "cmbAcc2"
        Me.cmbAcc2.Size = New System.Drawing.Size(454, 22)
        Me.cmbAcc2.TabIndex = 49
        '
        'cmbAcc1
        '
        Me.cmbAcc1.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.cmbAcc1.FormattingEnabled = True
        Me.cmbAcc1.Location = New System.Drawing.Point(99, 139)
        Me.cmbAcc1.Name = "cmbAcc1"
        Me.cmbAcc1.Size = New System.Drawing.Size(454, 22)
        Me.cmbAcc1.TabIndex = 48
        '
        'cmbAcc3
        '
        Me.cmbAcc3.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.cmbAcc3.FormattingEnabled = True
        Me.cmbAcc3.Location = New System.Drawing.Point(99, 195)
        Me.cmbAcc3.Name = "cmbAcc3"
        Me.cmbAcc3.Size = New System.Drawing.Size(454, 22)
        Me.cmbAcc3.TabIndex = 41
        '
        'txtAccQ1
        '
        Me.txtAccQ1.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtAccQ1.Location = New System.Drawing.Point(633, 138)
        Me.txtAccQ1.Name = "txtAccQ1"
        Me.txtAccQ1.Properties.Appearance.Font = New System.Drawing.Font("Calibri", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAccQ1.Properties.Appearance.Options.UseFont = True
        Me.txtAccQ1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.txtAccQ1.Properties.MaxValue = New Decimal(New Integer() {100, 0, 0, 0})
        Me.txtAccQ1.Size = New System.Drawing.Size(40, 22)
        Me.txtAccQ1.TabIndex = 47
        '
        'txtAccQ2
        '
        Me.txtAccQ2.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtAccQ2.Location = New System.Drawing.Point(633, 166)
        Me.txtAccQ2.Name = "txtAccQ2"
        Me.txtAccQ2.Properties.Appearance.Font = New System.Drawing.Font("Calibri", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAccQ2.Properties.Appearance.Options.UseFont = True
        Me.txtAccQ2.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.txtAccQ2.Properties.MaxValue = New Decimal(New Integer() {4, 0, 0, 0})
        Me.txtAccQ2.Size = New System.Drawing.Size(40, 22)
        Me.txtAccQ2.TabIndex = 46
        '
        'txtAccQ3
        '
        Me.txtAccQ3.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtAccQ3.Location = New System.Drawing.Point(633, 194)
        Me.txtAccQ3.Name = "txtAccQ3"
        Me.txtAccQ3.Properties.Appearance.Font = New System.Drawing.Font("Calibri", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAccQ3.Properties.Appearance.Options.UseFont = True
        Me.txtAccQ3.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.txtAccQ3.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtAccQ3.Properties.MaxValue = New Decimal(New Integer() {20, 0, 0, 0})
        Me.txtAccQ3.Size = New System.Drawing.Size(40, 22)
        Me.txtAccQ3.TabIndex = 45
        '
        'Label63
        '
        Me.Label63.AutoSize = True
        Me.Label63.Font = New System.Drawing.Font("SansSerif", 8.999999!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.Label63.Location = New System.Drawing.Point(6, 197)
        Me.Label63.Name = "Label63"
        Me.Label63.Size = New System.Drawing.Size(87, 14)
        Me.Label63.TabIndex = 44
        Me.Label63.Text = "Accesorios 3:"
        '
        'Label64
        '
        Me.Label64.AutoSize = True
        Me.Label64.Font = New System.Drawing.Font("SansSerif", 8.999999!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.Label64.Location = New System.Drawing.Point(6, 170)
        Me.Label64.Name = "Label64"
        Me.Label64.Size = New System.Drawing.Size(87, 14)
        Me.Label64.TabIndex = 43
        Me.Label64.Text = "Accesorios 2:"
        '
        'Label65
        '
        Me.Label65.AutoSize = True
        Me.Label65.Font = New System.Drawing.Font("SansSerif", 8.999999!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.Label65.Location = New System.Drawing.Point(6, 142)
        Me.Label65.Name = "Label65"
        Me.Label65.Size = New System.Drawing.Size(87, 14)
        Me.Label65.TabIndex = 42
        Me.Label65.Text = "Accesorios 1:"
        '
        'txtSubSistP
        '
        Me.txtSubSistP.Font = New System.Drawing.Font("SansSerif", 8.999999!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.txtSubSistP.Location = New System.Drawing.Point(1005, 195)
        Me.txtSubSistP.Name = "txtSubSistP"
        Me.txtSubSistP.Size = New System.Drawing.Size(80, 21)
        Me.txtSubSistP.TabIndex = 28
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.Font = New System.Drawing.Font("SansSerif", 8.999999!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.Label28.Location = New System.Drawing.Point(941, 198)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(58, 14)
        Me.Label28.TabIndex = 27
        Me.Label28.Text = "Subtotal:"
        '
        'txtSistemaO
        '
        Me.txtSistemaO.Font = New System.Drawing.Font("SansSerif", 8.999999!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.txtSistemaO.Location = New System.Drawing.Point(812, 38)
        Me.txtSistemaO.Multiline = True
        Me.txtSistemaO.Name = "txtSistemaO"
        Me.txtSistemaO.Size = New System.Drawing.Size(273, 151)
        Me.txtSistemaO.TabIndex = 39
        '
        'Label53
        '
        Me.Label53.AutoSize = True
        Me.Label53.Font = New System.Drawing.Font("SansSerif", 8.999999!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.Label53.Location = New System.Drawing.Point(809, 21)
        Me.Label53.Name = "Label53"
        Me.Label53.Size = New System.Drawing.Size(98, 14)
        Me.Label53.TabIndex = 40
        Me.Label53.Text = "Observaciones:"
        '
        'txtTerminalesP
        '
        Me.txtTerminalesP.Font = New System.Drawing.Font("SansSerif", 9.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.txtTerminalesP.Location = New System.Drawing.Point(733, 110)
        Me.txtTerminalesP.Name = "txtTerminalesP"
        Me.txtTerminalesP.Size = New System.Drawing.Size(70, 22)
        Me.txtTerminalesP.TabIndex = 26
        '
        'txtArgollasP
        '
        Me.txtArgollasP.Font = New System.Drawing.Font("SansSerif", 9.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.txtArgollasP.Location = New System.Drawing.Point(733, 82)
        Me.txtArgollasP.Name = "txtArgollasP"
        Me.txtArgollasP.Size = New System.Drawing.Size(70, 22)
        Me.txtArgollasP.TabIndex = 25
        '
        'txtSoportesP
        '
        Me.txtSoportesP.Font = New System.Drawing.Font("SansSerif", 9.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.txtSoportesP.Location = New System.Drawing.Point(733, 54)
        Me.txtSoportesP.Name = "txtSoportesP"
        Me.txtSoportesP.Size = New System.Drawing.Size(70, 22)
        Me.txtSoportesP.TabIndex = 24
        '
        'txtSistemaP
        '
        Me.txtSistemaP.Font = New System.Drawing.Font("SansSerif", 9.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.txtSistemaP.Location = New System.Drawing.Point(733, 26)
        Me.txtSistemaP.Name = "txtSistemaP"
        Me.txtSistemaP.Size = New System.Drawing.Size(70, 22)
        Me.txtSistemaP.TabIndex = 1
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("SansSerif", 8.999999!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.Label12.Location = New System.Drawing.Point(679, 111)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(48, 14)
        Me.Label12.TabIndex = 23
        Me.Label12.Text = "Precio:"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("SansSerif", 8.999999!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.Label11.Location = New System.Drawing.Point(679, 84)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(48, 14)
        Me.Label11.TabIndex = 22
        Me.Label11.Text = "Precio:"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("SansSerif", 8.999999!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.Label10.Location = New System.Drawing.Point(679, 57)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(48, 14)
        Me.Label10.TabIndex = 21
        Me.Label10.Text = "Precio:"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("SansSerif", 8.999999!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.Label9.Location = New System.Drawing.Point(679, 30)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(48, 14)
        Me.Label9.TabIndex = 1
        Me.Label9.Text = "Precio:"
        '
        'txtSistemaQ
        '
        Me.txtSistemaQ.Font = New System.Drawing.Font("SansSerif", 8.999999!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.txtSistemaQ.Location = New System.Drawing.Point(633, 27)
        Me.txtSistemaQ.MaxLength = 5
        Me.txtSistemaQ.Name = "txtSistemaQ"
        Me.txtSistemaQ.Size = New System.Drawing.Size(40, 21)
        Me.txtSistemaQ.TabIndex = 1
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("SansSerif", 8.999999!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.Label8.Location = New System.Drawing.Point(561, 30)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(66, 14)
        Me.Label8.TabIndex = 20
        Me.Label8.Text = "Largo (m):"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("SansSerif", 8.999999!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.Label6.Location = New System.Drawing.Point(565, 57)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(62, 14)
        Me.Label6.TabIndex = 19
        Me.Label6.Text = "Cantidad:"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("SansSerif", 8.999999!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.Label5.Location = New System.Drawing.Point(565, 85)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(62, 14)
        Me.Label5.TabIndex = 18
        Me.Label5.Text = "Cantidad:"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("SansSerif", 8.999999!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.Label7.Location = New System.Drawing.Point(565, 113)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(62, 14)
        Me.Label7.TabIndex = 17
        Me.Label7.Text = "Cantidad:"
        '
        'cmbTerminales
        '
        Me.cmbTerminales.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.cmbTerminales.FormattingEnabled = True
        Me.cmbTerminales.Location = New System.Drawing.Point(99, 111)
        Me.cmbTerminales.Name = "cmbTerminales"
        Me.cmbTerminales.Size = New System.Drawing.Size(454, 22)
        Me.cmbTerminales.TabIndex = 14
        '
        'cmbArgollas
        '
        Me.cmbArgollas.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.cmbArgollas.FormattingEnabled = True
        Me.cmbArgollas.Location = New System.Drawing.Point(99, 83)
        Me.cmbArgollas.Name = "cmbArgollas"
        Me.cmbArgollas.Size = New System.Drawing.Size(454, 22)
        Me.cmbArgollas.TabIndex = 13
        '
        'cmbSoportes
        '
        Me.cmbSoportes.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.cmbSoportes.FormattingEnabled = True
        Me.cmbSoportes.Location = New System.Drawing.Point(99, 55)
        Me.cmbSoportes.Name = "cmbSoportes"
        Me.cmbSoportes.Size = New System.Drawing.Size(454, 22)
        Me.cmbSoportes.TabIndex = 1
        '
        'cmbSistema
        '
        Me.cmbSistema.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.cmbSistema.FormattingEnabled = True
        Me.cmbSistema.Location = New System.Drawing.Point(99, 26)
        Me.cmbSistema.Margin = New System.Windows.Forms.Padding(3, 5, 3, 3)
        Me.cmbSistema.Name = "cmbSistema"
        Me.cmbSistema.Size = New System.Drawing.Size(454, 22)
        Me.cmbSistema.TabIndex = 12
        '
        'txtTerminalesQ
        '
        Me.txtTerminalesQ.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtTerminalesQ.Location = New System.Drawing.Point(633, 82)
        Me.txtTerminalesQ.Name = "txtTerminalesQ"
        Me.txtTerminalesQ.Properties.Appearance.Font = New System.Drawing.Font("Calibri", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTerminalesQ.Properties.Appearance.Options.UseFont = True
        Me.txtTerminalesQ.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.txtTerminalesQ.Properties.MaxValue = New Decimal(New Integer() {100, 0, 0, 0})
        Me.txtTerminalesQ.Size = New System.Drawing.Size(40, 22)
        Me.txtTerminalesQ.TabIndex = 11
        '
        'txtArgollasQ
        '
        Me.txtArgollasQ.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtArgollasQ.Location = New System.Drawing.Point(633, 110)
        Me.txtArgollasQ.Name = "txtArgollasQ"
        Me.txtArgollasQ.Properties.Appearance.Font = New System.Drawing.Font("Calibri", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtArgollasQ.Properties.Appearance.Options.UseFont = True
        Me.txtArgollasQ.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.txtArgollasQ.Properties.MaxValue = New Decimal(New Integer() {4, 0, 0, 0})
        Me.txtArgollasQ.Size = New System.Drawing.Size(40, 22)
        Me.txtArgollasQ.TabIndex = 10
        '
        'txtSoportesQ
        '
        Me.txtSoportesQ.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtSoportesQ.Location = New System.Drawing.Point(633, 54)
        Me.txtSoportesQ.Name = "txtSoportesQ"
        Me.txtSoportesQ.Properties.Appearance.Font = New System.Drawing.Font("Calibri", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSoportesQ.Properties.Appearance.Options.UseFont = True
        Me.txtSoportesQ.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.txtSoportesQ.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtSoportesQ.Properties.MaxValue = New Decimal(New Integer() {20, 0, 0, 0})
        Me.txtSoportesQ.Size = New System.Drawing.Size(40, 22)
        Me.txtSoportesQ.TabIndex = 8
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("SansSerif", 8.999999!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.Label4.Location = New System.Drawing.Point(18, 114)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(75, 14)
        Me.Label4.TabIndex = 4
        Me.Label4.Text = "Terminales:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("SansSerif", 8.999999!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.Label3.Location = New System.Drawing.Point(34, 86)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(59, 14)
        Me.Label3.TabIndex = 3
        Me.Label3.Text = "Argollas:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("SansSerif", 8.999999!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.Label2.Location = New System.Drawing.Point(30, 58)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(63, 14)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Soportes:"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("SansSerif", 8.999999!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.Label1.Location = New System.Drawing.Point(35, 29)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(58, 14)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Sistema:"
        '
        'gcCortina1
        '
        Me.gcCortina1.Controls.Add(Me.gcPañoD1)
        Me.gcCortina1.Controls.Add(Me.TextBox1)
        Me.gcCortina1.Controls.Add(Me.txtSubCort1P)
        Me.gcCortina1.Controls.Add(Me.Label29)
        Me.gcCortina1.Controls.Add(Me.Label51)
        Me.gcCortina1.Controls.Add(Me.txtForro1P)
        Me.gcCortina1.Controls.Add(Me.Label27)
        Me.gcCortina1.Controls.Add(Me.txtGenero1P)
        Me.gcCortina1.Controls.Add(Me.Label26)
        Me.gcCortina1.Controls.Add(Me.txtGeneroQL1)
        Me.gcCortina1.Controls.Add(Me.Label21)
        Me.gcCortina1.Controls.Add(Me.txtForroQL1)
        Me.gcCortina1.Controls.Add(Me.Label25)
        Me.gcCortina1.Controls.Add(Me.txtGeneroQA1)
        Me.gcCortina1.Controls.Add(Me.Label20)
        Me.gcCortina1.Controls.Add(Me.txtForroQA1)
        Me.gcCortina1.Controls.Add(Me.Label19)
        Me.gcCortina1.Controls.Add(Me.gcPañoI1)
        Me.gcCortina1.Controls.Add(Me.txtCabezal1P)
        Me.gcCortina1.Controls.Add(Me.Label16)
        Me.gcCortina1.Controls.Add(Me.txtCabezal1Q)
        Me.gcCortina1.Controls.Add(Me.Label17)
        Me.gcCortina1.Controls.Add(Me.cmbForro1)
        Me.gcCortina1.Controls.Add(Me.cmbGenero1)
        Me.gcCortina1.Controls.Add(Me.cmbCabezal1)
        Me.gcCortina1.Controls.Add(Me.Label22)
        Me.gcCortina1.Controls.Add(Me.Label23)
        Me.gcCortina1.Controls.Add(Me.Label24)
        Me.gcCortina1.Location = New System.Drawing.Point(12, 353)
        Me.gcCortina1.LookAndFeel.SkinName = "Visual Studio 2013 Dark"
        Me.gcCortina1.LookAndFeel.UseDefaultLookAndFeel = False
        Me.gcCortina1.Name = "gcCortina1"
        Me.gcCortina1.Size = New System.Drawing.Size(1090, 173)
        Me.gcCortina1.TabIndex = 27
        Me.gcCortina1.Text = "Cortina N°1"
        '
        'gcPañoD1
        '
        Me.gcPañoD1.Controls.Add(Me.chkCD1)
        Me.gcPañoD1.Controls.Add(Me.txtLargoD1)
        Me.gcPañoD1.Controls.Add(Me.txtAnchoD1)
        Me.gcPañoD1.Controls.Add(Me.Label15)
        Me.gcPañoD1.Controls.Add(Me.Label18)
        Me.gcPañoD1.Location = New System.Drawing.Point(407, 108)
        Me.gcPañoD1.LookAndFeel.SkinName = "Visual Studio 2013 Dark"
        Me.gcPañoD1.LookAndFeel.UseDefaultLookAndFeel = False
        Me.gcPañoD1.Name = "gcPañoD1"
        Me.gcPañoD1.Size = New System.Drawing.Size(396, 56)
        Me.gcPañoD1.TabIndex = 22
        Me.gcPañoD1.Text = "Paño Derecho:"
        '
        'chkCD1
        '
        Me.chkCD1.AutoSize = True
        Me.chkCD1.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkCD1.Location = New System.Drawing.Point(258, 26)
        Me.chkCD1.Name = "chkCD1"
        Me.chkCD1.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.chkCD1.Size = New System.Drawing.Size(131, 19)
        Me.chkCD1.TabIndex = 4
        Me.chkCD1.Text = "Comando Derecho"
        Me.chkCD1.UseVisualStyleBackColor = True
        '
        'txtLargoD1
        '
        Me.txtLargoD1.Font = New System.Drawing.Font("SansSerif", 9.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.txtLargoD1.Location = New System.Drawing.Point(212, 24)
        Me.txtLargoD1.MaxLength = 5
        Me.txtLargoD1.Name = "txtLargoD1"
        Me.txtLargoD1.Size = New System.Drawing.Size(40, 22)
        Me.txtLargoD1.TabIndex = 3
        '
        'txtAnchoD1
        '
        Me.txtAnchoD1.Font = New System.Drawing.Font("SansSerif", 9.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.txtAnchoD1.Location = New System.Drawing.Point(94, 24)
        Me.txtAnchoD1.MaxLength = 5
        Me.txtAnchoD1.Name = "txtAnchoD1"
        Me.txtAnchoD1.Size = New System.Drawing.Size(40, 22)
        Me.txtAnchoD1.TabIndex = 2
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.Label15.Location = New System.Drawing.Point(140, 27)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(66, 14)
        Me.Label15.TabIndex = 1
        Me.Label15.Text = "Largo (m):"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.Label18.Location = New System.Drawing.Point(18, 27)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(70, 14)
        Me.Label18.TabIndex = 0
        Me.Label18.Text = "Ancho (m):"
        '
        'TextBox1
        '
        Me.TextBox1.Font = New System.Drawing.Font("SansSerif", 8.999999!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.TextBox1.Location = New System.Drawing.Point(812, 38)
        Me.TextBox1.Multiline = True
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(273, 99)
        Me.TextBox1.TabIndex = 41
        '
        'txtSubCort1P
        '
        Me.txtSubCort1P.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.txtSubCort1P.Location = New System.Drawing.Point(1005, 143)
        Me.txtSubCort1P.Name = "txtSubCort1P"
        Me.txtSubCort1P.Size = New System.Drawing.Size(80, 21)
        Me.txtSubCort1P.TabIndex = 40
        '
        'Label29
        '
        Me.Label29.AutoSize = True
        Me.Label29.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.Label29.Location = New System.Drawing.Point(935, 148)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(58, 14)
        Me.Label29.TabIndex = 39
        Me.Label29.Text = "Subtotal:"
        '
        'Label51
        '
        Me.Label51.AutoSize = True
        Me.Label51.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.Label51.Location = New System.Drawing.Point(809, 21)
        Me.Label51.Name = "Label51"
        Me.Label51.Size = New System.Drawing.Size(98, 14)
        Me.Label51.TabIndex = 38
        Me.Label51.Text = "Observaciones:"
        '
        'txtForro1P
        '
        Me.txtForro1P.Font = New System.Drawing.Font("SansSerif", 9.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.txtForro1P.Location = New System.Drawing.Point(733, 80)
        Me.txtForro1P.Name = "txtForro1P"
        Me.txtForro1P.Size = New System.Drawing.Size(70, 22)
        Me.txtForro1P.TabIndex = 34
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.Label27.Location = New System.Drawing.Point(679, 83)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(48, 14)
        Me.Label27.TabIndex = 33
        Me.Label27.Text = "Precio:"
        '
        'txtGenero1P
        '
        Me.txtGenero1P.Font = New System.Drawing.Font("SansSerif", 9.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.txtGenero1P.Location = New System.Drawing.Point(733, 52)
        Me.txtGenero1P.Name = "txtGenero1P"
        Me.txtGenero1P.Size = New System.Drawing.Size(70, 22)
        Me.txtGenero1P.TabIndex = 32
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.Label26.Location = New System.Drawing.Point(679, 55)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(48, 14)
        Me.Label26.TabIndex = 31
        Me.Label26.Text = "Precio:"
        '
        'txtGeneroQL1
        '
        Me.txtGeneroQL1.Font = New System.Drawing.Font("SansSerif", 9.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.txtGeneroQL1.Location = New System.Drawing.Point(633, 52)
        Me.txtGeneroQL1.MaxLength = 5
        Me.txtGeneroQL1.Name = "txtGeneroQL1"
        Me.txtGeneroQL1.Size = New System.Drawing.Size(40, 22)
        Me.txtGeneroQL1.TabIndex = 29
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.Label21.Location = New System.Drawing.Point(561, 55)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(66, 14)
        Me.Label21.TabIndex = 30
        Me.Label21.Text = "Largo (m):"
        '
        'txtForroQL1
        '
        Me.txtForroQL1.Font = New System.Drawing.Font("SansSerif", 9.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.txtForroQL1.Location = New System.Drawing.Point(633, 80)
        Me.txtForroQL1.MaxLength = 5
        Me.txtForroQL1.Name = "txtForroQL1"
        Me.txtForroQL1.Size = New System.Drawing.Size(40, 22)
        Me.txtForroQL1.TabIndex = 27
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.Label25.Location = New System.Drawing.Point(561, 83)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(66, 14)
        Me.Label25.TabIndex = 28
        Me.Label25.Text = "Largo (m):"
        '
        'txtGeneroQA1
        '
        Me.txtGeneroQA1.Font = New System.Drawing.Font("SansSerif", 9.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.txtGeneroQA1.Location = New System.Drawing.Point(512, 52)
        Me.txtGeneroQA1.MaxLength = 5
        Me.txtGeneroQA1.Name = "txtGeneroQA1"
        Me.txtGeneroQA1.Size = New System.Drawing.Size(40, 22)
        Me.txtGeneroQA1.TabIndex = 25
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.Label20.Location = New System.Drawing.Point(436, 55)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(70, 14)
        Me.Label20.TabIndex = 26
        Me.Label20.Text = "Ancho (m):"
        '
        'txtForroQA1
        '
        Me.txtForroQA1.Font = New System.Drawing.Font("SansSerif", 9.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.txtForroQA1.Location = New System.Drawing.Point(512, 80)
        Me.txtForroQA1.MaxLength = 5
        Me.txtForroQA1.Name = "txtForroQA1"
        Me.txtForroQA1.Size = New System.Drawing.Size(40, 22)
        Me.txtForroQA1.TabIndex = 23
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.Label19.Location = New System.Drawing.Point(436, 83)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(70, 14)
        Me.Label19.TabIndex = 24
        Me.Label19.Text = "Ancho (m):"
        '
        'gcPañoI1
        '
        Me.gcPañoI1.Controls.Add(Me.chkCI1)
        Me.gcPañoI1.Controls.Add(Me.txtLargoI1)
        Me.gcPañoI1.Controls.Add(Me.txtAnchoI1)
        Me.gcPañoI1.Controls.Add(Me.Label14)
        Me.gcPañoI1.Controls.Add(Me.Label13)
        Me.gcPañoI1.Location = New System.Drawing.Point(5, 108)
        Me.gcPañoI1.LookAndFeel.SkinName = "Visual Studio 2013 Dark"
        Me.gcPañoI1.LookAndFeel.UseDefaultLookAndFeel = False
        Me.gcPañoI1.Name = "gcPañoI1"
        Me.gcPañoI1.Size = New System.Drawing.Size(396, 56)
        Me.gcPañoI1.TabIndex = 21
        Me.gcPañoI1.Text = "Paño Izquierdo:"
        '
        'chkCI1
        '
        Me.chkCI1.AutoSize = True
        Me.chkCI1.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkCI1.Location = New System.Drawing.Point(258, 26)
        Me.chkCI1.Name = "chkCI1"
        Me.chkCI1.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.chkCI1.Size = New System.Drawing.Size(131, 19)
        Me.chkCI1.TabIndex = 4
        Me.chkCI1.Text = "Comando Derecho"
        Me.chkCI1.UseVisualStyleBackColor = True
        '
        'txtLargoI1
        '
        Me.txtLargoI1.Font = New System.Drawing.Font("SansSerif", 9.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.txtLargoI1.Location = New System.Drawing.Point(212, 24)
        Me.txtLargoI1.MaxLength = 5
        Me.txtLargoI1.Name = "txtLargoI1"
        Me.txtLargoI1.Size = New System.Drawing.Size(40, 22)
        Me.txtLargoI1.TabIndex = 3
        '
        'txtAnchoI1
        '
        Me.txtAnchoI1.Font = New System.Drawing.Font("SansSerif", 9.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.txtAnchoI1.Location = New System.Drawing.Point(94, 24)
        Me.txtAnchoI1.MaxLength = 5
        Me.txtAnchoI1.Name = "txtAnchoI1"
        Me.txtAnchoI1.Size = New System.Drawing.Size(40, 22)
        Me.txtAnchoI1.TabIndex = 2
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.Label14.Location = New System.Drawing.Point(140, 27)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(66, 14)
        Me.Label14.TabIndex = 1
        Me.Label14.Text = "Largo (m):"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.Label13.Location = New System.Drawing.Point(18, 27)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(70, 14)
        Me.Label13.TabIndex = 0
        Me.Label13.Text = "Ancho (m):"
        '
        'txtCabezal1P
        '
        Me.txtCabezal1P.Font = New System.Drawing.Font("SansSerif", 9.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.txtCabezal1P.Location = New System.Drawing.Point(733, 24)
        Me.txtCabezal1P.Name = "txtCabezal1P"
        Me.txtCabezal1P.Size = New System.Drawing.Size(70, 22)
        Me.txtCabezal1P.TabIndex = 1
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.Label16.Location = New System.Drawing.Point(679, 27)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(48, 14)
        Me.Label16.TabIndex = 1
        Me.Label16.Text = "Precio:"
        '
        'txtCabezal1Q
        '
        Me.txtCabezal1Q.Font = New System.Drawing.Font("SansSerif", 9.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.txtCabezal1Q.Location = New System.Drawing.Point(512, 24)
        Me.txtCabezal1Q.MaxLength = 5
        Me.txtCabezal1Q.Name = "txtCabezal1Q"
        Me.txtCabezal1Q.Size = New System.Drawing.Size(40, 22)
        Me.txtCabezal1Q.TabIndex = 1
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.Label17.Location = New System.Drawing.Point(436, 27)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(70, 14)
        Me.Label17.TabIndex = 20
        Me.Label17.Text = "Ancho (m):"
        '
        'cmbForro1
        '
        Me.cmbForro1.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.cmbForro1.FormattingEnabled = True
        Me.cmbForro1.Location = New System.Drawing.Point(99, 80)
        Me.cmbForro1.Name = "cmbForro1"
        Me.cmbForro1.Size = New System.Drawing.Size(331, 22)
        Me.cmbForro1.TabIndex = 13
        '
        'cmbGenero1
        '
        Me.cmbGenero1.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.cmbGenero1.FormattingEnabled = True
        Me.cmbGenero1.Location = New System.Drawing.Point(99, 52)
        Me.cmbGenero1.Name = "cmbGenero1"
        Me.cmbGenero1.Size = New System.Drawing.Size(331, 22)
        Me.cmbGenero1.TabIndex = 1
        '
        'cmbCabezal1
        '
        Me.cmbCabezal1.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.cmbCabezal1.FormattingEnabled = True
        Me.cmbCabezal1.Location = New System.Drawing.Point(99, 24)
        Me.cmbCabezal1.Name = "cmbCabezal1"
        Me.cmbCabezal1.Size = New System.Drawing.Size(331, 22)
        Me.cmbCabezal1.TabIndex = 12
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.Label22.Location = New System.Drawing.Point(51, 83)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(42, 14)
        Me.Label22.TabIndex = 3
        Me.Label22.Text = "Forro:"
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.Label23.Location = New System.Drawing.Point(40, 55)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(53, 14)
        Me.Label23.TabIndex = 2
        Me.Label23.Text = "Genero:"
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.Label24.Location = New System.Drawing.Point(36, 27)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(57, 14)
        Me.Label24.TabIndex = 1
        Me.Label24.Text = "Cabezal:"
        '
        'gcTotal
        '
        Me.gcTotal.Controls.Add(Me.txtFinal)
        Me.gcTotal.Controls.Add(Me.Label50)
        Me.gcTotal.Controls.Add(Me.txtIVA)
        Me.gcTotal.Controls.Add(Me.Label49)
        Me.gcTotal.Controls.Add(Me.txtTotalP)
        Me.gcTotal.Controls.Add(Me.Label46)
        Me.gcTotal.Location = New System.Drawing.Point(343, 718)
        Me.gcTotal.LookAndFeel.SkinName = "Visual Studio 2013 Dark"
        Me.gcTotal.LookAndFeel.UseDefaultLookAndFeel = False
        Me.gcTotal.Name = "gcTotal"
        Me.gcTotal.Size = New System.Drawing.Size(448, 50)
        Me.gcTotal.TabIndex = 31
        Me.gcTotal.Text = "Total:"
        '
        'txtFinal
        '
        Me.txtFinal.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.txtFinal.Location = New System.Drawing.Point(340, 24)
        Me.txtFinal.Name = "txtFinal"
        Me.txtFinal.Size = New System.Drawing.Size(90, 21)
        Me.txtFinal.TabIndex = 32
        '
        'Label50
        '
        Me.Label50.AutoSize = True
        Me.Label50.Font = New System.Drawing.Font("SansSerif", 8.999999!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.Label50.Location = New System.Drawing.Point(296, 27)
        Me.Label50.Name = "Label50"
        Me.Label50.Size = New System.Drawing.Size(38, 14)
        Me.Label50.TabIndex = 31
        Me.Label50.Text = "Final:"
        '
        'txtIVA
        '
        Me.txtIVA.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.txtIVA.Location = New System.Drawing.Point(196, 24)
        Me.txtIVA.Name = "txtIVA"
        Me.txtIVA.Size = New System.Drawing.Size(90, 21)
        Me.txtIVA.TabIndex = 30
        '
        'Label49
        '
        Me.Label49.AutoSize = True
        Me.Label49.Font = New System.Drawing.Font("SansSerif", 8.999999!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.Label49.Location = New System.Drawing.Point(159, 27)
        Me.Label49.Name = "Label49"
        Me.Label49.Size = New System.Drawing.Size(31, 14)
        Me.Label49.TabIndex = 29
        Me.Label49.Text = "IVA:"
        '
        'txtTotalP
        '
        Me.txtTotalP.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.txtTotalP.Location = New System.Drawing.Point(58, 24)
        Me.txtTotalP.Name = "txtTotalP"
        Me.txtTotalP.Size = New System.Drawing.Size(90, 21)
        Me.txtTotalP.TabIndex = 28
        '
        'Label46
        '
        Me.Label46.AutoSize = True
        Me.Label46.Font = New System.Drawing.Font("SansSerif", 8.999999!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.Label46.Location = New System.Drawing.Point(5, 27)
        Me.Label46.Name = "Label46"
        Me.Label46.Size = New System.Drawing.Size(48, 14)
        Me.Label46.TabIndex = 27
        Me.Label46.Text = "Precio:"
        '
        'gcItem
        '
        Me.gcItem.Controls.Add(Me.chkPaño2)
        Me.gcItem.Controls.Add(Me.chkPaño1)
        Me.gcItem.Controls.Add(Me.chkSistema)
        Me.gcItem.Controls.Add(Me.Label56)
        Me.gcItem.Controls.Add(Me.nupCantidad)
        Me.gcItem.Controls.Add(Me.txtItemDesc)
        Me.gcItem.Controls.Add(Me.Label55)
        Me.gcItem.Controls.Add(Me.cmbOpcion)
        Me.gcItem.Controls.Add(Me.Label54)
        Me.gcItem.Controls.Add(Me.txtObservaciones)
        Me.gcItem.Controls.Add(Me.Label48)
        Me.gcItem.Controls.Add(Me.txtUbicacion)
        Me.gcItem.Controls.Add(Me.Label47)
        Me.gcItem.Location = New System.Drawing.Point(12, 12)
        Me.gcItem.LookAndFeel.SkinName = "Visual Studio 2013 Dark"
        Me.gcItem.LookAndFeel.UseDefaultLookAndFeel = False
        Me.gcItem.Name = "gcItem"
        Me.gcItem.Size = New System.Drawing.Size(1090, 108)
        Me.gcItem.TabIndex = 32
        Me.gcItem.Text = "Datos Item:"
        '
        'chkPaño2
        '
        Me.chkPaño2.AutoSize = True
        Me.chkPaño2.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkPaño2.Location = New System.Drawing.Point(1020, 74)
        Me.chkPaño2.Name = "chkPaño2"
        Me.chkPaño2.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.chkPaño2.Size = New System.Drawing.Size(65, 19)
        Me.chkPaño2.TabIndex = 67
        Me.chkPaño2.Text = "Paño 2"
        Me.chkPaño2.UseVisualStyleBackColor = True
        '
        'chkPaño1
        '
        Me.chkPaño1.AutoSize = True
        Me.chkPaño1.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkPaño1.Location = New System.Drawing.Point(1020, 49)
        Me.chkPaño1.Name = "chkPaño1"
        Me.chkPaño1.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.chkPaño1.Size = New System.Drawing.Size(65, 19)
        Me.chkPaño1.TabIndex = 66
        Me.chkPaño1.Text = "Paño 1"
        Me.chkPaño1.UseVisualStyleBackColor = True
        '
        'chkSistema
        '
        Me.chkSistema.AutoSize = True
        Me.chkSistema.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkSistema.Location = New System.Drawing.Point(1012, 24)
        Me.chkSistema.Name = "chkSistema"
        Me.chkSistema.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.chkSistema.Size = New System.Drawing.Size(73, 19)
        Me.chkSistema.TabIndex = 65
        Me.chkSistema.Text = "Sistema"
        Me.chkSistema.UseVisualStyleBackColor = True
        '
        'Label56
        '
        Me.Label56.AutoSize = True
        Me.Label56.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label56.Location = New System.Drawing.Point(876, 85)
        Me.Label56.Name = "Label56"
        Me.Label56.Size = New System.Drawing.Size(60, 15)
        Me.Label56.TabIndex = 64
        Me.Label56.Text = "Cantidad:"
        '
        'nupCantidad
        '
        Me.nupCantidad.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.nupCantidad.Location = New System.Drawing.Point(942, 81)
        Me.nupCantidad.Name = "nupCantidad"
        Me.nupCantidad.Properties.Appearance.Font = New System.Drawing.Font("SansSerif", 9.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nupCantidad.Properties.Appearance.Options.UseFont = True
        Me.nupCantidad.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.nupCantidad.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.nupCantidad.Properties.MaxValue = New Decimal(New Integer() {20, 0, 0, 0})
        Me.nupCantidad.Size = New System.Drawing.Size(64, 22)
        Me.nupCantidad.TabIndex = 63
        '
        'txtItemDesc
        '
        Me.txtItemDesc.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtItemDesc.Location = New System.Drawing.Point(99, 24)
        Me.txtItemDesc.Multiline = True
        Me.txtItemDesc.Name = "txtItemDesc"
        Me.txtItemDesc.Size = New System.Drawing.Size(608, 23)
        Me.txtItemDesc.TabIndex = 62
        '
        'Label55
        '
        Me.Label55.AutoSize = True
        Me.Label55.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label55.Location = New System.Drawing.Point(15, 28)
        Me.Label55.Name = "Label55"
        Me.Label55.Size = New System.Drawing.Size(78, 15)
        Me.Label55.TabIndex = 61
        Me.Label55.Text = "Descripción:"
        '
        'cmbOpcion
        '
        Me.cmbOpcion.DisplayMember = "0"
        Me.cmbOpcion.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbOpcion.FormattingEnabled = True
        Me.cmbOpcion.Items.AddRange(New Object() {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9"})
        Me.cmbOpcion.Location = New System.Drawing.Point(945, 53)
        Me.cmbOpcion.Name = "cmbOpcion"
        Me.cmbOpcion.Size = New System.Drawing.Size(61, 22)
        Me.cmbOpcion.TabIndex = 60
        '
        'Label54
        '
        Me.Label54.AutoSize = True
        Me.Label54.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label54.Location = New System.Drawing.Point(913, 56)
        Me.Label54.Name = "Label54"
        Me.Label54.Size = New System.Drawing.Size(26, 15)
        Me.Label54.TabIndex = 31
        Me.Label54.Text = "Op:"
        '
        'txtObservaciones
        '
        Me.txtObservaciones.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtObservaciones.Location = New System.Drawing.Point(99, 53)
        Me.txtObservaciones.Multiline = True
        Me.txtObservaciones.Name = "txtObservaciones"
        Me.txtObservaciones.Size = New System.Drawing.Size(771, 46)
        Me.txtObservaciones.TabIndex = 30
        '
        'Label48
        '
        Me.Label48.AutoSize = True
        Me.Label48.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label48.Location = New System.Drawing.Point(11, 55)
        Me.Label48.Name = "Label48"
        Me.Label48.Size = New System.Drawing.Size(82, 15)
        Me.Label48.TabIndex = 29
        Me.Label48.Text = "Observación:"
        '
        'txtUbicacion
        '
        Me.txtUbicacion.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtUbicacion.Location = New System.Drawing.Point(785, 24)
        Me.txtUbicacion.Multiline = True
        Me.txtUbicacion.Name = "txtUbicacion"
        Me.txtUbicacion.Size = New System.Drawing.Size(221, 23)
        Me.txtUbicacion.TabIndex = 28
        '
        'Label47
        '
        Me.Label47.AutoSize = True
        Me.Label47.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label47.Location = New System.Drawing.Point(713, 28)
        Me.Label47.Name = "Label47"
        Me.Label47.Size = New System.Drawing.Size(66, 15)
        Me.Label47.TabIndex = 27
        Me.Label47.Text = "Ubicación:"
        '
        'btnSalir
        '
        Me.btnSalir.Appearance.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold)
        Me.btnSalir.Appearance.Options.UseFont = True
        Me.btnSalir.Location = New System.Drawing.Point(1027, 741)
        Me.btnSalir.LookAndFeel.SkinName = "Visual Studio 2013 Dark"
        Me.btnSalir.LookAndFeel.UseDefaultLookAndFeel = False
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(75, 27)
        Me.btnSalir.TabIndex = 58
        Me.btnSalir.Text = "Cerrar"
        '
        'btnGuardar
        '
        Me.btnGuardar.Appearance.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold)
        Me.btnGuardar.Appearance.Options.UseFont = True
        Me.btnGuardar.Cursor = System.Windows.Forms.Cursors.Default
        Me.btnGuardar.Location = New System.Drawing.Point(946, 741)
        Me.btnGuardar.LookAndFeel.SkinName = "Visual Studio 2013 Dark"
        Me.btnGuardar.LookAndFeel.UseDefaultLookAndFeel = False
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(75, 27)
        Me.btnGuardar.TabIndex = 59
        Me.btnGuardar.Text = "Guardar"
        '
        'gcCortina2
        '
        Me.gcCortina2.Controls.Add(Me.gcPañoD2)
        Me.gcCortina2.Controls.Add(Me.TextBox7)
        Me.gcCortina2.Controls.Add(Me.txtSubCort2P)
        Me.gcCortina2.Controls.Add(Me.Label32)
        Me.gcCortina2.Controls.Add(Me.Label33)
        Me.gcCortina2.Controls.Add(Me.txtForro2P)
        Me.gcCortina2.Controls.Add(Me.Label34)
        Me.gcCortina2.Controls.Add(Me.txtGenero2P)
        Me.gcCortina2.Controls.Add(Me.Label35)
        Me.gcCortina2.Controls.Add(Me.txtGeneroQL2)
        Me.gcCortina2.Controls.Add(Me.Label36)
        Me.gcCortina2.Controls.Add(Me.txtForroQL2)
        Me.gcCortina2.Controls.Add(Me.Label37)
        Me.gcCortina2.Controls.Add(Me.txtGeneroQA2)
        Me.gcCortina2.Controls.Add(Me.Label38)
        Me.gcCortina2.Controls.Add(Me.txtForroQA2)
        Me.gcCortina2.Controls.Add(Me.Label39)
        Me.gcCortina2.Controls.Add(Me.gcPañoI2)
        Me.gcCortina2.Controls.Add(Me.txtCabezal2P)
        Me.gcCortina2.Controls.Add(Me.Label42)
        Me.gcCortina2.Controls.Add(Me.txtCabezal2Q)
        Me.gcCortina2.Controls.Add(Me.Label43)
        Me.gcCortina2.Controls.Add(Me.cmbForro2)
        Me.gcCortina2.Controls.Add(Me.cmbGenero2)
        Me.gcCortina2.Controls.Add(Me.cmbCabezal2)
        Me.gcCortina2.Controls.Add(Me.Label44)
        Me.gcCortina2.Controls.Add(Me.Label45)
        Me.gcCortina2.Controls.Add(Me.Label52)
        Me.gcCortina2.Location = New System.Drawing.Point(12, 532)
        Me.gcCortina2.LookAndFeel.SkinName = "Visual Studio 2013 Dark"
        Me.gcCortina2.LookAndFeel.UseDefaultLookAndFeel = False
        Me.gcCortina2.Name = "gcCortina2"
        Me.gcCortina2.Size = New System.Drawing.Size(1090, 173)
        Me.gcCortina2.TabIndex = 60
        Me.gcCortina2.Text = "Cortina N°2"
        '
        'gcPañoD2
        '
        Me.gcPañoD2.Controls.Add(Me.chkCD2)
        Me.gcPañoD2.Controls.Add(Me.txtLargoD2)
        Me.gcPañoD2.Controls.Add(Me.txtAnchoD2)
        Me.gcPañoD2.Controls.Add(Me.Label30)
        Me.gcPañoD2.Controls.Add(Me.Label31)
        Me.gcPañoD2.Location = New System.Drawing.Point(407, 108)
        Me.gcPañoD2.LookAndFeel.SkinName = "Visual Studio 2013 Dark"
        Me.gcPañoD2.LookAndFeel.UseDefaultLookAndFeel = False
        Me.gcPañoD2.Name = "gcPañoD2"
        Me.gcPañoD2.Size = New System.Drawing.Size(396, 56)
        Me.gcPañoD2.TabIndex = 22
        Me.gcPañoD2.Text = "Paño Derecho:"
        '
        'chkCD2
        '
        Me.chkCD2.AutoSize = True
        Me.chkCD2.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkCD2.Location = New System.Drawing.Point(258, 26)
        Me.chkCD2.Name = "chkCD2"
        Me.chkCD2.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.chkCD2.Size = New System.Drawing.Size(131, 19)
        Me.chkCD2.TabIndex = 4
        Me.chkCD2.Text = "Comando Derecho"
        Me.chkCD2.UseVisualStyleBackColor = True
        '
        'txtLargoD2
        '
        Me.txtLargoD2.Font = New System.Drawing.Font("SansSerif", 9.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.txtLargoD2.Location = New System.Drawing.Point(212, 24)
        Me.txtLargoD2.MaxLength = 5
        Me.txtLargoD2.Name = "txtLargoD2"
        Me.txtLargoD2.Size = New System.Drawing.Size(40, 22)
        Me.txtLargoD2.TabIndex = 3
        '
        'txtAnchoD2
        '
        Me.txtAnchoD2.Font = New System.Drawing.Font("SansSerif", 9.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.txtAnchoD2.Location = New System.Drawing.Point(94, 24)
        Me.txtAnchoD2.MaxLength = 5
        Me.txtAnchoD2.Name = "txtAnchoD2"
        Me.txtAnchoD2.Size = New System.Drawing.Size(40, 22)
        Me.txtAnchoD2.TabIndex = 2
        '
        'Label30
        '
        Me.Label30.AutoSize = True
        Me.Label30.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.Label30.Location = New System.Drawing.Point(140, 27)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(66, 14)
        Me.Label30.TabIndex = 1
        Me.Label30.Text = "Largo (m):"
        '
        'Label31
        '
        Me.Label31.AutoSize = True
        Me.Label31.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.Label31.Location = New System.Drawing.Point(18, 27)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(70, 14)
        Me.Label31.TabIndex = 0
        Me.Label31.Text = "Ancho (m):"
        '
        'TextBox7
        '
        Me.TextBox7.Font = New System.Drawing.Font("SansSerif", 8.999999!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.TextBox7.Location = New System.Drawing.Point(812, 38)
        Me.TextBox7.Multiline = True
        Me.TextBox7.Name = "TextBox7"
        Me.TextBox7.Size = New System.Drawing.Size(273, 99)
        Me.TextBox7.TabIndex = 41
        '
        'txtSubCort2P
        '
        Me.txtSubCort2P.Font = New System.Drawing.Font("SansSerif", 8.999999!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.txtSubCort2P.Location = New System.Drawing.Point(1005, 143)
        Me.txtSubCort2P.Name = "txtSubCort2P"
        Me.txtSubCort2P.Size = New System.Drawing.Size(80, 21)
        Me.txtSubCort2P.TabIndex = 40
        '
        'Label32
        '
        Me.Label32.AutoSize = True
        Me.Label32.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.Label32.Location = New System.Drawing.Point(935, 148)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(58, 14)
        Me.Label32.TabIndex = 39
        Me.Label32.Text = "Subtotal:"
        '
        'Label33
        '
        Me.Label33.AutoSize = True
        Me.Label33.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.Label33.Location = New System.Drawing.Point(809, 21)
        Me.Label33.Name = "Label33"
        Me.Label33.Size = New System.Drawing.Size(98, 14)
        Me.Label33.TabIndex = 38
        Me.Label33.Text = "Observaciones:"
        '
        'txtForro2P
        '
        Me.txtForro2P.Font = New System.Drawing.Font("SansSerif", 9.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.txtForro2P.Location = New System.Drawing.Point(733, 80)
        Me.txtForro2P.Name = "txtForro2P"
        Me.txtForro2P.Size = New System.Drawing.Size(70, 22)
        Me.txtForro2P.TabIndex = 34
        '
        'Label34
        '
        Me.Label34.AutoSize = True
        Me.Label34.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.Label34.Location = New System.Drawing.Point(679, 83)
        Me.Label34.Name = "Label34"
        Me.Label34.Size = New System.Drawing.Size(48, 14)
        Me.Label34.TabIndex = 33
        Me.Label34.Text = "Precio:"
        '
        'txtGenero2P
        '
        Me.txtGenero2P.Font = New System.Drawing.Font("SansSerif", 9.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.txtGenero2P.Location = New System.Drawing.Point(733, 52)
        Me.txtGenero2P.Name = "txtGenero2P"
        Me.txtGenero2P.Size = New System.Drawing.Size(70, 22)
        Me.txtGenero2P.TabIndex = 32
        '
        'Label35
        '
        Me.Label35.AutoSize = True
        Me.Label35.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.Label35.Location = New System.Drawing.Point(679, 55)
        Me.Label35.Name = "Label35"
        Me.Label35.Size = New System.Drawing.Size(48, 14)
        Me.Label35.TabIndex = 31
        Me.Label35.Text = "Precio:"
        '
        'txtGeneroQL2
        '
        Me.txtGeneroQL2.Font = New System.Drawing.Font("SansSerif", 9.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.txtGeneroQL2.Location = New System.Drawing.Point(633, 52)
        Me.txtGeneroQL2.MaxLength = 5
        Me.txtGeneroQL2.Name = "txtGeneroQL2"
        Me.txtGeneroQL2.Size = New System.Drawing.Size(40, 22)
        Me.txtGeneroQL2.TabIndex = 29
        '
        'Label36
        '
        Me.Label36.AutoSize = True
        Me.Label36.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.Label36.Location = New System.Drawing.Point(561, 55)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(66, 14)
        Me.Label36.TabIndex = 30
        Me.Label36.Text = "Largo (m):"
        '
        'txtForroQL2
        '
        Me.txtForroQL2.Font = New System.Drawing.Font("SansSerif", 9.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.txtForroQL2.Location = New System.Drawing.Point(633, 80)
        Me.txtForroQL2.MaxLength = 5
        Me.txtForroQL2.Name = "txtForroQL2"
        Me.txtForroQL2.Size = New System.Drawing.Size(40, 22)
        Me.txtForroQL2.TabIndex = 27
        '
        'Label37
        '
        Me.Label37.AutoSize = True
        Me.Label37.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.Label37.Location = New System.Drawing.Point(561, 83)
        Me.Label37.Name = "Label37"
        Me.Label37.Size = New System.Drawing.Size(66, 14)
        Me.Label37.TabIndex = 28
        Me.Label37.Text = "Largo (m):"
        '
        'txtGeneroQA2
        '
        Me.txtGeneroQA2.Font = New System.Drawing.Font("SansSerif", 9.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.txtGeneroQA2.Location = New System.Drawing.Point(512, 52)
        Me.txtGeneroQA2.MaxLength = 5
        Me.txtGeneroQA2.Name = "txtGeneroQA2"
        Me.txtGeneroQA2.Size = New System.Drawing.Size(40, 22)
        Me.txtGeneroQA2.TabIndex = 25
        '
        'Label38
        '
        Me.Label38.AutoSize = True
        Me.Label38.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.Label38.Location = New System.Drawing.Point(436, 55)
        Me.Label38.Name = "Label38"
        Me.Label38.Size = New System.Drawing.Size(70, 14)
        Me.Label38.TabIndex = 26
        Me.Label38.Text = "Ancho (m):"
        '
        'txtForroQA2
        '
        Me.txtForroQA2.Font = New System.Drawing.Font("SansSerif", 9.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.txtForroQA2.Location = New System.Drawing.Point(512, 80)
        Me.txtForroQA2.MaxLength = 5
        Me.txtForroQA2.Name = "txtForroQA2"
        Me.txtForroQA2.Size = New System.Drawing.Size(40, 22)
        Me.txtForroQA2.TabIndex = 23
        '
        'Label39
        '
        Me.Label39.AutoSize = True
        Me.Label39.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.Label39.Location = New System.Drawing.Point(436, 83)
        Me.Label39.Name = "Label39"
        Me.Label39.Size = New System.Drawing.Size(70, 14)
        Me.Label39.TabIndex = 24
        Me.Label39.Text = "Ancho (m):"
        '
        'gcPañoI2
        '
        Me.gcPañoI2.Controls.Add(Me.chkCI2)
        Me.gcPañoI2.Controls.Add(Me.txtLargoI2)
        Me.gcPañoI2.Controls.Add(Me.txtAnchoI2)
        Me.gcPañoI2.Controls.Add(Me.Label40)
        Me.gcPañoI2.Controls.Add(Me.Label41)
        Me.gcPañoI2.Location = New System.Drawing.Point(5, 108)
        Me.gcPañoI2.LookAndFeel.SkinName = "Visual Studio 2013 Dark"
        Me.gcPañoI2.LookAndFeel.UseDefaultLookAndFeel = False
        Me.gcPañoI2.Name = "gcPañoI2"
        Me.gcPañoI2.Size = New System.Drawing.Size(396, 56)
        Me.gcPañoI2.TabIndex = 21
        Me.gcPañoI2.Text = "Paño Izquierdo:"
        '
        'chkCI2
        '
        Me.chkCI2.AutoSize = True
        Me.chkCI2.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkCI2.Location = New System.Drawing.Point(258, 26)
        Me.chkCI2.Name = "chkCI2"
        Me.chkCI2.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.chkCI2.Size = New System.Drawing.Size(131, 19)
        Me.chkCI2.TabIndex = 4
        Me.chkCI2.Text = "Comando Derecho"
        Me.chkCI2.UseVisualStyleBackColor = True
        '
        'txtLargoI2
        '
        Me.txtLargoI2.Font = New System.Drawing.Font("SansSerif", 9.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.txtLargoI2.Location = New System.Drawing.Point(212, 24)
        Me.txtLargoI2.MaxLength = 5
        Me.txtLargoI2.Name = "txtLargoI2"
        Me.txtLargoI2.Size = New System.Drawing.Size(40, 22)
        Me.txtLargoI2.TabIndex = 3
        '
        'txtAnchoI2
        '
        Me.txtAnchoI2.Font = New System.Drawing.Font("SansSerif", 9.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.txtAnchoI2.Location = New System.Drawing.Point(94, 24)
        Me.txtAnchoI2.MaxLength = 5
        Me.txtAnchoI2.Name = "txtAnchoI2"
        Me.txtAnchoI2.Size = New System.Drawing.Size(40, 22)
        Me.txtAnchoI2.TabIndex = 2
        '
        'Label40
        '
        Me.Label40.AutoSize = True
        Me.Label40.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.Label40.Location = New System.Drawing.Point(140, 27)
        Me.Label40.Name = "Label40"
        Me.Label40.Size = New System.Drawing.Size(66, 14)
        Me.Label40.TabIndex = 1
        Me.Label40.Text = "Largo (m):"
        '
        'Label41
        '
        Me.Label41.AutoSize = True
        Me.Label41.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.Label41.Location = New System.Drawing.Point(18, 27)
        Me.Label41.Name = "Label41"
        Me.Label41.Size = New System.Drawing.Size(70, 14)
        Me.Label41.TabIndex = 0
        Me.Label41.Text = "Ancho (m):"
        '
        'txtCabezal2P
        '
        Me.txtCabezal2P.Font = New System.Drawing.Font("SansSerif", 9.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.txtCabezal2P.Location = New System.Drawing.Point(733, 24)
        Me.txtCabezal2P.Name = "txtCabezal2P"
        Me.txtCabezal2P.Size = New System.Drawing.Size(70, 22)
        Me.txtCabezal2P.TabIndex = 1
        '
        'Label42
        '
        Me.Label42.AutoSize = True
        Me.Label42.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.Label42.Location = New System.Drawing.Point(679, 27)
        Me.Label42.Name = "Label42"
        Me.Label42.Size = New System.Drawing.Size(48, 14)
        Me.Label42.TabIndex = 1
        Me.Label42.Text = "Precio:"
        '
        'txtCabezal2Q
        '
        Me.txtCabezal2Q.Font = New System.Drawing.Font("SansSerif", 9.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.txtCabezal2Q.Location = New System.Drawing.Point(512, 24)
        Me.txtCabezal2Q.MaxLength = 5
        Me.txtCabezal2Q.Name = "txtCabezal2Q"
        Me.txtCabezal2Q.Size = New System.Drawing.Size(40, 22)
        Me.txtCabezal2Q.TabIndex = 1
        '
        'Label43
        '
        Me.Label43.AutoSize = True
        Me.Label43.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.Label43.Location = New System.Drawing.Point(436, 27)
        Me.Label43.Name = "Label43"
        Me.Label43.Size = New System.Drawing.Size(70, 14)
        Me.Label43.TabIndex = 20
        Me.Label43.Text = "Ancho (m):"
        '
        'cmbForro2
        '
        Me.cmbForro2.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.cmbForro2.FormattingEnabled = True
        Me.cmbForro2.Location = New System.Drawing.Point(99, 80)
        Me.cmbForro2.Name = "cmbForro2"
        Me.cmbForro2.Size = New System.Drawing.Size(331, 22)
        Me.cmbForro2.TabIndex = 13
        '
        'cmbGenero2
        '
        Me.cmbGenero2.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.cmbGenero2.FormattingEnabled = True
        Me.cmbGenero2.Location = New System.Drawing.Point(99, 52)
        Me.cmbGenero2.Name = "cmbGenero2"
        Me.cmbGenero2.Size = New System.Drawing.Size(331, 22)
        Me.cmbGenero2.TabIndex = 1
        '
        'cmbCabezal2
        '
        Me.cmbCabezal2.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.cmbCabezal2.FormattingEnabled = True
        Me.cmbCabezal2.Location = New System.Drawing.Point(99, 24)
        Me.cmbCabezal2.Name = "cmbCabezal2"
        Me.cmbCabezal2.Size = New System.Drawing.Size(331, 22)
        Me.cmbCabezal2.TabIndex = 12
        '
        'Label44
        '
        Me.Label44.AutoSize = True
        Me.Label44.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.Label44.Location = New System.Drawing.Point(51, 83)
        Me.Label44.Name = "Label44"
        Me.Label44.Size = New System.Drawing.Size(42, 14)
        Me.Label44.TabIndex = 3
        Me.Label44.Text = "Forro:"
        '
        'Label45
        '
        Me.Label45.AutoSize = True
        Me.Label45.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.Label45.Location = New System.Drawing.Point(40, 55)
        Me.Label45.Name = "Label45"
        Me.Label45.Size = New System.Drawing.Size(53, 14)
        Me.Label45.TabIndex = 2
        Me.Label45.Text = "Genero:"
        '
        'Label52
        '
        Me.Label52.AutoSize = True
        Me.Label52.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.Label52.Location = New System.Drawing.Point(36, 27)
        Me.Label52.Name = "Label52"
        Me.Label52.Size = New System.Drawing.Size(57, 14)
        Me.Label52.TabIndex = 1
        Me.Label52.Text = "Cabezal:"
        '
        'frmCortinas
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoSize = True
        Me.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.BackColor = System.Drawing.Color.Black
        Me.ClientSize = New System.Drawing.Size(1120, 780)
        Me.ControlBox = False
        Me.Controls.Add(Me.gcCortina2)
        Me.Controls.Add(Me.btnGuardar)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(Me.gcItem)
        Me.Controls.Add(Me.gcTotal)
        Me.Controls.Add(Me.gcCortina1)
        Me.Controls.Add(Me.gcSistema)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.MaximumSize = New System.Drawing.Size(1366, 1080)
        Me.MinimumSize = New System.Drawing.Size(1120, 701)
        Me.Name = "frmCortinas"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Tag = "Cortinas y Sistemas"
        Me.Text = "Cortinas y Sistemas"
        CType(Me.gcSistema, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gcSistema.ResumeLayout(False)
        Me.gcSistema.PerformLayout()
        CType(Me.txtAccQ1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtAccQ2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtAccQ3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTerminalesQ.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtArgollasQ.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSoportesQ.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gcCortina1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gcCortina1.ResumeLayout(False)
        Me.gcCortina1.PerformLayout()
        CType(Me.gcPañoD1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gcPañoD1.ResumeLayout(False)
        Me.gcPañoD1.PerformLayout()
        CType(Me.gcPañoI1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gcPañoI1.ResumeLayout(False)
        Me.gcPañoI1.PerformLayout()
        CType(Me.gcTotal, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gcTotal.ResumeLayout(False)
        Me.gcTotal.PerformLayout()
        CType(Me.gcItem, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gcItem.ResumeLayout(False)
        Me.gcItem.PerformLayout()
        CType(Me.nupCantidad.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gcCortina2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gcCortina2.ResumeLayout(False)
        Me.gcCortina2.PerformLayout()
        CType(Me.gcPañoD2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gcPañoD2.ResumeLayout(False)
        Me.gcPañoD2.PerformLayout()
        CType(Me.gcPañoI2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gcPañoI2.ResumeLayout(False)
        Me.gcPañoI2.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gcSistema As DevExpress.XtraEditors.GroupControl
    Friend WithEvents txtSubSistP As System.Windows.Forms.TextBox
    Friend WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents txtTerminalesP As System.Windows.Forms.TextBox
    Friend WithEvents txtArgollasP As System.Windows.Forms.TextBox
    Friend WithEvents txtSoportesP As System.Windows.Forms.TextBox
    Friend WithEvents txtSistemaP As System.Windows.Forms.TextBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents txtSistemaQ As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents cmbTerminales As System.Windows.Forms.ComboBox
    Friend WithEvents cmbArgollas As System.Windows.Forms.ComboBox
    Friend WithEvents cmbSoportes As System.Windows.Forms.ComboBox
    Friend WithEvents cmbSistema As System.Windows.Forms.ComboBox
    Friend WithEvents txtTerminalesQ As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents txtArgollasQ As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents txtSoportesQ As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents gcCortina1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents txtForro1P As System.Windows.Forms.TextBox
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents txtGenero1P As System.Windows.Forms.TextBox
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents txtGeneroQL1 As System.Windows.Forms.TextBox
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents txtForroQL1 As System.Windows.Forms.TextBox
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents txtGeneroQA1 As System.Windows.Forms.TextBox
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents txtForroQA1 As System.Windows.Forms.TextBox
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents gcPañoI1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents txtLargoI1 As System.Windows.Forms.TextBox
    Friend WithEvents txtAnchoI1 As System.Windows.Forms.TextBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents txtCabezal1P As System.Windows.Forms.TextBox
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents txtCabezal1Q As System.Windows.Forms.TextBox
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents cmbForro1 As System.Windows.Forms.ComboBox
    Friend WithEvents cmbGenero1 As System.Windows.Forms.ComboBox
    Friend WithEvents cmbCabezal1 As System.Windows.Forms.ComboBox
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents gcTotal As DevExpress.XtraEditors.GroupControl
    Friend WithEvents txtTotalP As System.Windows.Forms.TextBox
    Friend WithEvents Label46 As System.Windows.Forms.Label
    Friend WithEvents gcItem As DevExpress.XtraEditors.GroupControl
    Friend WithEvents txtUbicacion As System.Windows.Forms.TextBox
    Friend WithEvents Label47 As System.Windows.Forms.Label
    Friend WithEvents txtObservaciones As System.Windows.Forms.TextBox
    Friend WithEvents Label48 As System.Windows.Forms.Label
    Friend WithEvents txtFinal As System.Windows.Forms.TextBox
    Friend WithEvents Label50 As System.Windows.Forms.Label
    Friend WithEvents txtIVA As System.Windows.Forms.TextBox
    Friend WithEvents Label49 As System.Windows.Forms.Label
    Friend WithEvents Label51 As System.Windows.Forms.Label
    Friend WithEvents txtSistemaO As System.Windows.Forms.TextBox
    Friend WithEvents Label53 As System.Windows.Forms.Label
    Friend WithEvents btnSalir As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnGuardar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmbOpcion As System.Windows.Forms.ComboBox
    Friend WithEvents Label54 As System.Windows.Forms.Label
    Friend WithEvents txtSubCort1P As System.Windows.Forms.TextBox
    Friend WithEvents Label29 As System.Windows.Forms.Label
    Friend WithEvents txtItemDesc As System.Windows.Forms.TextBox
    Friend WithEvents Label55 As System.Windows.Forms.Label
    Friend WithEvents Label56 As System.Windows.Forms.Label
    Friend WithEvents nupCantidad As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents gcPañoD1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents chkCD1 As System.Windows.Forms.CheckBox
    Friend WithEvents txtLargoD1 As System.Windows.Forms.TextBox
    Friend WithEvents txtAnchoD1 As System.Windows.Forms.TextBox
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents chkCI1 As System.Windows.Forms.CheckBox
    Friend WithEvents chkPaño2 As System.Windows.Forms.CheckBox
    Friend WithEvents chkPaño1 As System.Windows.Forms.CheckBox
    Friend WithEvents chkSistema As System.Windows.Forms.CheckBox
    Friend WithEvents gcCortina2 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents gcPañoD2 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents chkCD2 As System.Windows.Forms.CheckBox
    Friend WithEvents txtLargoD2 As System.Windows.Forms.TextBox
    Friend WithEvents txtAnchoD2 As System.Windows.Forms.TextBox
    Friend WithEvents Label30 As System.Windows.Forms.Label
    Friend WithEvents Label31 As System.Windows.Forms.Label
    Friend WithEvents TextBox7 As System.Windows.Forms.TextBox
    Friend WithEvents txtSubCort2P As System.Windows.Forms.TextBox
    Friend WithEvents Label32 As System.Windows.Forms.Label
    Friend WithEvents Label33 As System.Windows.Forms.Label
    Friend WithEvents txtForro2P As System.Windows.Forms.TextBox
    Friend WithEvents Label34 As System.Windows.Forms.Label
    Friend WithEvents txtGenero2P As System.Windows.Forms.TextBox
    Friend WithEvents Label35 As System.Windows.Forms.Label
    Friend WithEvents txtGeneroQL2 As System.Windows.Forms.TextBox
    Friend WithEvents Label36 As System.Windows.Forms.Label
    Friend WithEvents txtForroQL2 As System.Windows.Forms.TextBox
    Friend WithEvents Label37 As System.Windows.Forms.Label
    Friend WithEvents txtGeneroQA2 As System.Windows.Forms.TextBox
    Friend WithEvents Label38 As System.Windows.Forms.Label
    Friend WithEvents txtForroQA2 As System.Windows.Forms.TextBox
    Friend WithEvents Label39 As System.Windows.Forms.Label
    Friend WithEvents gcPañoI2 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents chkCI2 As System.Windows.Forms.CheckBox
    Friend WithEvents txtLargoI2 As System.Windows.Forms.TextBox
    Friend WithEvents txtAnchoI2 As System.Windows.Forms.TextBox
    Friend WithEvents Label40 As System.Windows.Forms.Label
    Friend WithEvents Label41 As System.Windows.Forms.Label
    Friend WithEvents txtCabezal2P As System.Windows.Forms.TextBox
    Friend WithEvents Label42 As System.Windows.Forms.Label
    Friend WithEvents txtCabezal2Q As System.Windows.Forms.TextBox
    Friend WithEvents Label43 As System.Windows.Forms.Label
    Friend WithEvents cmbForro2 As System.Windows.Forms.ComboBox
    Friend WithEvents cmbGenero2 As System.Windows.Forms.ComboBox
    Friend WithEvents cmbCabezal2 As System.Windows.Forms.ComboBox
    Friend WithEvents Label44 As System.Windows.Forms.Label
    Friend WithEvents Label45 As System.Windows.Forms.Label
    Friend WithEvents Label52 As System.Windows.Forms.Label
    Friend WithEvents txtAccP2 As System.Windows.Forms.TextBox
    Friend WithEvents txtAccP1 As System.Windows.Forms.TextBox
    Friend WithEvents txtAccP3 As System.Windows.Forms.TextBox
    Friend WithEvents Label57 As System.Windows.Forms.Label
    Friend WithEvents Label58 As System.Windows.Forms.Label
    Friend WithEvents Label59 As System.Windows.Forms.Label
    Friend WithEvents Label60 As System.Windows.Forms.Label
    Friend WithEvents Label61 As System.Windows.Forms.Label
    Friend WithEvents Label62 As System.Windows.Forms.Label
    Friend WithEvents cmbAcc2 As System.Windows.Forms.ComboBox
    Friend WithEvents cmbAcc1 As System.Windows.Forms.ComboBox
    Friend WithEvents cmbAcc3 As System.Windows.Forms.ComboBox
    Friend WithEvents txtAccQ1 As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents txtAccQ2 As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents txtAccQ3 As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents Label63 As System.Windows.Forms.Label
    Friend WithEvents Label64 As System.Windows.Forms.Label
    Friend WithEvents Label65 As System.Windows.Forms.Label
End Class
