﻿Public Class frmProveedores
    Private Funciones As New Funciones
    Private Operacion As Integer = 0 ' 0=Alta, 1=Modificacion
    Private ABM As Boolean = False 'True=Alta, False=Modificacion

    '------ EVENTOS DE FORMULARIO ------
    Private Sub frmProveedores_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        ConfigurarForm()
        ConfigurarTextBox()

        ActualizarCmbBuscar()
        ActualizarCmbciudad()

        LimpiarCasilleros()

        ModoNormal()
        ModoReadOnly()

        cmbBuscar.SelectedIndex = -1
        cmbBuscar.Focus()
    End Sub
    Private Sub frmProveedores_Click(sender As Object, e As EventArgs) Handles MyBase.Click, MyBase.GotFocus
        Me.SendToBack()
    End Sub

    '------ EVENTOS DE BOTONES ------
    Private Sub btnSalir_Click(sender As Object, e As EventArgs) Handles btnSalir.Click
        ModoReadOnly()
        Operacion = 0
        Me.Close()
    End Sub
    Private Sub btnNuevo_Click(sender As System.Object, e As System.EventArgs) Handles btnNuevo.Click
        Operacion = 0
        LimpiarCasilleros()
        ModoAbm()
        ModoEditar()
        txtNombre.Focus()
    End Sub
    Private Sub btnGuardar_Click(sender As Object, e As EventArgs) Handles btnGuardar.Click
        Dim UnGestorProveedor As New Modelo.GestorProveedor
        Dim UnProveedor As New Modelo.Proveedor

        UnProveedor.Nombre = txtNombre.Text
        UnProveedor.RazonSocial = txtEmpresa.Text
        UnProveedor.Sucursal = txtSucursal.Text
        UnProveedor.Web = txtWeb.Text
        UnProveedor.CUIT = Funciones.SoloNumeros(txtCUIT.Text)
        UnProveedor.Direccion = txtDireccion.Text
        UnProveedor.IdCiudad = cmbCiudad.SelectedIndex
        UnProveedor.CP = txtCP.Text
        UnProveedor.Mail1 = txtMail1.Text
        UnProveedor.Mail2 = txtMail2.Text
        UnProveedor.Mail3 = txtMail3.Text
        UnProveedor.Contacto1 = txtContacto1.Text
        UnProveedor.Contacto2 = txtContacto2.Text
        UnProveedor.Contacto3 = txtContacto3.Text
        UnProveedor.TelContacto1 = Funciones.SoloNumeros(txtTelContacto1.Text)
        UnProveedor.TelContacto2 = Funciones.SoloNumeros(txtTelContacto2.Text)
        UnProveedor.TelContacto3 = Funciones.SoloNumeros(txtTelContacto3.Text)

        Select Case Operacion
            Case 0 ' ALTA
                UnGestorProveedor.Nuevo(UnProveedor)
            Case 1
                ' MODIFICACION
                UnProveedor.IdProveedor = Integer.Parse(txtId.Text)
                UnGestorProveedor.Modificar(UnProveedor)
        End Select
        ModoNormal()
        ModoReadOnly()
        Operacion = 1
    End Sub
    Private Sub btnEliminar_Click(sender As Object, e As EventArgs) Handles btnEliminar.Click
        If txtId.Text = "" Then
        Else
            Dim UnProveedor As New Modelo.Proveedor
            UnProveedor.IdProveedor = Integer.Parse(txtId.Text)
            If MsgBox(String.Format("¿Desea eliminar el cliente {0}?", txtNombre.Text), vbOKCancel, "Eliminar Proveedor") = vbOK Then
                Dim UnGestorProveedor As New Modelo.GestorProveedor
                UnGestorProveedor.Eliminar(UnProveedor)
                LimpiarCasilleros()
                ModoNormal()
                ModoReadOnly()
            End If
        End If
        Operacion = 1
    End Sub
    Private Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        Try
            Dim UnGestorProveedor As New Modelo.GestorProveedor
            Dim ProveedorSeleccionado As New Modelo.Proveedor
            ProveedorSeleccionado.IdProveedor = txtId.Text
            ProveedorSeleccionado = UnGestorProveedor.BuscarPorId(ProveedorSeleccionado)
            MostrarDetalleDeProveedor(ProveedorSeleccionado)
        Catch
        End Try

        ModoNormal()
        ModoReadOnly()
        Operacion = 1
    End Sub

    '------ EVENTOS DE COMBOBOX ------
    Private Sub cmbBuscar_SelectionChangeCommitted(sender As Object, e As EventArgs) Handles cmbBuscar.SelectionChangeCommitted
        Dim UnGestorProveedor As New Modelo.GestorProveedor
        Dim ProveedorSeleccionado As New Modelo.Proveedor
        Dim Index As Integer = cmbBuscar.SelectedIndex

        ProveedorSeleccionado = UnGestorProveedor.BuscarPorId(cmbBuscar.SelectedItem)
        MostrarDetalleDeProveedor(ProveedorSeleccionado)

        ModoNormal()
        ModoReadOnly()

        cmbBuscar.SelectedIndex = Index
        Operacion = 1
    End Sub

    '------ FUNCIONES PARA COMBOBOX ------
    Private Sub ActualizarCmbBuscar()
        Dim UnGestorProveedor As New Modelo.GestorProveedor
        cmbBuscar.DataSource = UnGestorProveedor.ListarTodosNombre()
        cmbBuscar.DisplayMember = "Nombre"
    End Sub
    Private Sub ActualizarCmbciudad()
        Dim UnGestorCiudad As New Modelo.GestorCiudad
        cmbCiudad.DataSource = UnGestorCiudad.ListarTodasCiudades()
        cmbCiudad.DisplayMember = "Localidad"
    End Sub

    '------ FUNCIONES GENERALES ------
    Private Sub MostrarDetalleDeProveedor(UnProveedor As Modelo.Proveedor)
        Try
            txtId.Text = UnProveedor.IdProveedor
            txtNombre.Text = UnProveedor.Nombre
            txtEmpresa.Text = UnProveedor.RazonSocial
            txtCUIT.Text = Funciones.CUIT(CStr(UnProveedor.CUIT))
            txtWeb.Text = UnProveedor.Web
            txtDireccion.Text = UnProveedor.Direccion
            If UnProveedor.IdCiudad = -1 Then
                cmbCiudad.SelectedIndex = -1
            Else
                cmbCiudad.SelectedIndex = UnProveedor.IdCiudad
            End If
            txtCP.Text = UnProveedor.CP
            txtMail1.Text = UnProveedor.Mail1
            txtMail2.Text = UnProveedor.Mail2
            txtMail3.Text = UnProveedor.Mail3
            txtContacto1.Text = UnProveedor.Contacto1
            txtContacto2.Text = UnProveedor.Contacto2
            txtContacto3.Text = UnProveedor.Contacto3
            txtTelContacto1.Text = Funciones.Telefono(CStr(UnProveedor.TelContacto1))
            txtTelContacto2.Text = Funciones.Telefono(CStr(UnProveedor.TelContacto2))
            txtTelContacto3.Text = Funciones.Telefono(CStr(UnProveedor.TelContacto3))
        Catch
            MsgBox("Se ha producido un error al intentar cargar el proveedor", MsgBoxStyle.OkOnly)
        End Try
    End Sub

    '------ CONFIGURACIONES ------
    Public Sub ConfigurarForm()
        Me.Dock = DockStyle.None
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.ShowIcon = False
        Me.MaximumSize = New Size(frmPrincipal.ClientSize.Width - 4, frmPrincipal.ClientSize.Height - 4)
        Me.MinimumSize = Me.MaximumSize
        Me.Dock = DockStyle.Fill
        Dim Separacion As Integer = (frmPrincipal.ClientSize.Width - 1090 + 150) / 2
        gcBuscar.Location = New Point(Separacion, 45)
        gcInfogral.Location = New Point(Separacion, 88)
        gcDomicilio.Location = New Point(Separacion, 216)
        gcContacto.Location = New Point(Separacion, 278)
        lblTitulo.Location = New Point((frmPrincipal.ClientSize.Width - 386 + 150) / 2, 4)
        btnCancelar.Location = New Point((Separacion + 1090) - 75, frmPrincipal.ClientSize.Height - 103)
        btnEliminar.Location = New Point((Separacion + 1090) - 75, frmPrincipal.ClientSize.Height - 70)
        btnSalir.Location = New Point((Separacion + 1090) - 75, frmPrincipal.ClientSize.Height - 37)
    End Sub
    Private Sub ConfigurarTextBox()
        'Configuro la cantidad de cararcteres maximos
        txtNombre.MaxLength = 200
        txtDireccion.MaxLength = 150
        txtCP.MaxLength = 8
        txtSucursal.MaxLength = 50
        txtEmpresa.MaxLength = 100
        txtWeb.MaxLength = 75
        txtMail1.MaxLength = 50
        txtMail2.MaxLength = 50
        txtMail3.MaxLength = 50
        txtContacto1.MaxLength = 50
        txtContacto2.MaxLength = 50
        txtContacto3.MaxLength = 50
        'Configuro alineacion del texto
        txtCUIT.TextAlign = HorizontalAlignment.Center
        txtId.TextAlign = HorizontalAlignment.Center
        txtCP.TextAlign = HorizontalAlignment.Center
        txtSucursal.TextAlign = HorizontalAlignment.Center
        txtContacto1.TextAlign = HorizontalAlignment.Center
        txtContacto2.TextAlign = HorizontalAlignment.Center
        txtContacto3.TextAlign = HorizontalAlignment.Center
        txtTelContacto1.TextAlign = HorizontalAlignment.Center
        txtTelContacto2.TextAlign = HorizontalAlignment.Center
        txtTelContacto3.TextAlign = HorizontalAlignment.Center
        'Configuro los que seran siempre solo lectura
        txtId.ReadOnly = True
    End Sub

    '------ FUNCIONES DE FUNCIONAMIENTO ------
    Public Sub ModoNormal()
        Dim UnGestorProveedor As New Modelo.GestorProveedor
        ActualizarCmbBuscar()

        If txtId.Text <> "" Then
            Dim UnProveedor As New Modelo.Proveedor
            UnProveedor.IdProveedor = CInt(txtId.Text)
            Dim ProveedorSeleccionado As New Modelo.Proveedor
            ProveedorSeleccionado = UnGestorProveedor.BuscarPorId(UnProveedor)
            MostrarDetalleDeProveedor(ProveedorSeleccionado)
        End If

        btnNuevo.Enabled = True
        btnGuardar.Enabled = False
        btnEliminar.Enabled = True
        btnCancelar.Enabled = False
    End Sub
    Public Sub ModoReadOnly()
        cmbCiudad.Enabled = False
        cmbBuscar.Enabled = True

        txtId.ReadOnly = True
        txtNombre.ReadOnly = True
        txtCUIT.ReadOnly = True
        txtEmpresa.ReadOnly = True
        txtSucursal.ReadOnly = True
        txtWeb.ReadOnly = True
        txtDireccion.ReadOnly = True
        txtCP.ReadOnly = True
        txtMail1.ReadOnly = True
        txtMail2.ReadOnly = True
        txtMail3.ReadOnly = True
        txtContacto1.ReadOnly = True
        txtContacto2.ReadOnly = True
        txtContacto3.ReadOnly = True
        txtTelContacto1.ReadOnly = True
        txtTelContacto2.ReadOnly = True
        txtTelContacto3.ReadOnly = True

        ABM = False
    End Sub
    Public Sub ModoAbm()
        btnNuevo.Enabled = False
        btnGuardar.Enabled = True
        btnEliminar.Enabled = False
        btnCancelar.Enabled = True
    End Sub
    Public Sub ModoEditar()
        cmbCiudad.Enabled = True
        cmbBuscar.Enabled = False

        txtNombre.ReadOnly = False
        txtCUIT.ReadOnly = False
        txtEmpresa.ReadOnly = False
        txtSucursal.ReadOnly = False
        txtWeb.ReadOnly = False
        txtDireccion.ReadOnly = False
        txtCP.ReadOnly = False
        txtMail1.ReadOnly = False
        txtMail2.ReadOnly = False
        txtMail3.ReadOnly = False
        txtContacto1.ReadOnly = False
        txtContacto2.ReadOnly = False
        txtContacto3.ReadOnly = False
        txtTelContacto1.ReadOnly = False
        txtTelContacto2.ReadOnly = False
        txtTelContacto3.ReadOnly = False

        ABM = True
    End Sub

    '------ EVENTOS QUE LLAMAN FUNCIONES ------
    Private Sub txtTextChanged(sender As Object, e As EventArgs) Handles txtNombre.Click, txtEmpresa.Click, txtWeb.Click, txtCUIT.Click, txtSucursal.Click, txtDireccion.Click, txtCP.Click, txtMail1.Click, txtMail2.Click, txtMail3.Click, txtContacto1.Click, txtContacto1.Click, txtContacto3.Click, txtTelContacto1.Click, txtTelContacto2.Click, txtTelContacto3.Click
        If ABM = True Then
        Else
            ModoAbm()
            ModoEditar()
        End If
        If Operacion = 0 Then
        Else
            Operacion = 1
        End If
    End Sub

    '------ EVENTOS DE TEXTBOX ------
    Private Sub LimpiarCasilleros()
        txtId.Text = ""
        txtNombre.Text = ""
        txtSucursal.Text = ""
        txtDireccion.Text = ""
        txtCP.Text = ""
        txtMail1.Text = ""
        txtMail2.Text = ""
        txtMail3.Text = ""
        txtEmpresa.Text = ""
        txtCUIT.Text = ""
        txtWeb.Text = ""
        txtContacto1.Text = ""
        txtContacto2.Text = ""
        txtContacto3.Text = ""
        txtTelContacto1.Text = ""
        txtTelContacto2.Text = ""
        txtTelContacto3.Text = ""

        cmbCiudad.SelectedIndex = -1
    End Sub
    Private Sub txtTelContacto1_Leave(sender As Object, e As EventArgs) Handles txtTelContacto1.Leave
        txtTelContacto1.Text = Funciones.Telefono(txtTelContacto1.Text)
    End Sub
    Private Sub txtTelContacto2_Leave(sender As Object, e As EventArgs) Handles txtTelContacto2.Leave
        txtTelContacto2.Text = Funciones.Telefono(txtTelContacto2.Text)
    End Sub
    Private Sub txtTelContacto3_Leave(sender As Object, e As EventArgs) Handles txtTelContacto3.Leave
        txtTelContacto3.Text = Funciones.Telefono(txtTelContacto3.Text)
    End Sub
    Private Sub txtCUIT_Leave(sender As Object, e As EventArgs) Handles txtCUIT.Leave
        txtCUIT.Text = Funciones.CUIT(txtCUIT.Text)
    End Sub
End Class