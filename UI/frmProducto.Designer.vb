﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmProducto
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.lblTitulo = New System.Windows.Forms.Label()
        Me.cbxBuscar = New System.Windows.Forms.ComboBox()
        Me.txtDescripcion = New System.Windows.Forms.TextBox()
        Me.txtUnidad = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.txtNombre = New System.Windows.Forms.TextBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.txtCodigo = New System.Windows.Forms.TextBox()
        Me.gcInfoGral = New DevExpress.XtraEditors.GroupControl()
        Me.txtPrecio = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.cmbCategoria = New System.Windows.Forms.ComboBox()
        Me.txtCosto = New System.Windows.Forms.TextBox()
        Me.txtId = New System.Windows.Forms.TextBox()
        Me.dtpFecha = New System.Windows.Forms.DateTimePicker()
        Me.gcProveedor = New DevExpress.XtraEditors.GroupControl()
        Me.cmbProveedor = New System.Windows.Forms.ComboBox()
        Me.gcBuscar = New DevExpress.XtraEditors.PanelControl()
        Me.btnGuardar = New DevExpress.XtraEditors.SimpleButton()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.btnNuevo = New DevExpress.XtraEditors.SimpleButton()
        Me.cmbBuscar = New System.Windows.Forms.ComboBox()
        Me.btnCancelar = New DevExpress.XtraEditors.SimpleButton()
        Me.btnSalir = New DevExpress.XtraEditors.SimpleButton()
        Me.btnEliminar = New DevExpress.XtraEditors.SimpleButton()
        Me.gcBusqueda = New DevExpress.XtraEditors.GroupControl()
        Me.dgvProductos = New System.Windows.Forms.DataGridView()
        Me.btnCargardgv = New DevExpress.XtraEditors.SimpleButton()
        Me.cmbBuscarCat = New System.Windows.Forms.ComboBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.cmbBuscarProv = New System.Windows.Forms.ComboBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.ToolTip = New System.Windows.Forms.ToolTip(Me.components)
        CType(Me.gcInfoGral, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gcInfoGral.SuspendLayout()
        CType(Me.gcProveedor, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gcProveedor.SuspendLayout()
        CType(Me.gcBuscar, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gcBuscar.SuspendLayout()
        CType(Me.gcBusqueda, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gcBusqueda.SuspendLayout()
        CType(Me.dgvProductos, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.Font = New System.Drawing.Font("Cambria", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Label2.Location = New System.Drawing.Point(62, 61)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(101, 19)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Descripción:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.Label3.Font = New System.Drawing.Font("Cambria", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Label3.Location = New System.Drawing.Point(684, 28)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(83, 19)
        Me.Label3.TabIndex = 3
        Me.Label3.Text = "Categoria:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.BackColor = System.Drawing.Color.Transparent
        Me.Label4.Font = New System.Drawing.Font("Cambria", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Label4.Location = New System.Drawing.Point(699, 60)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(68, 19)
        Me.Label4.TabIndex = 4
        Me.Label4.Text = "Unidad:"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.BackColor = System.Drawing.Color.Transparent
        Me.Label6.Font = New System.Drawing.Font("Cambria", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Label6.Location = New System.Drawing.Point(76, 30)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(89, 19)
        Me.Label6.TabIndex = 6
        Me.Label6.Text = "Proveedor:"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Cambria", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Label1.Location = New System.Drawing.Point(92, 27)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(71, 19)
        Me.Label1.TabIndex = 7
        Me.Label1.Text = "Nombre:"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.BackColor = System.Drawing.Color.Transparent
        Me.Label7.Font = New System.Drawing.Font("Cambria", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Label7.Location = New System.Drawing.Point(715, 91)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(53, 19)
        Me.Label7.TabIndex = 8
        Me.Label7.Text = "Costo:"
        '
        'lblTitulo
        '
        Me.lblTitulo.AccessibleRole = System.Windows.Forms.AccessibleRole.TitleBar
        Me.lblTitulo.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.lblTitulo.AutoSize = True
        Me.lblTitulo.BackColor = System.Drawing.Color.Transparent
        Me.lblTitulo.Font = New System.Drawing.Font("Microsoft Sans Serif", 24.0!)
        Me.lblTitulo.ForeColor = System.Drawing.Color.White
        Me.lblTitulo.Location = New System.Drawing.Point(385, 4)
        Me.lblTitulo.Name = "lblTitulo"
        Me.lblTitulo.Size = New System.Drawing.Size(415, 37)
        Me.lblTitulo.TabIndex = 11
        Me.lblTitulo.Text = "Administrador de Productos"
        Me.lblTitulo.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'cbxBuscar
        '
        Me.cbxBuscar.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.cbxBuscar.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cbxBuscar.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.cbxBuscar.Font = New System.Drawing.Font("Calibri", 12.0!)
        Me.cbxBuscar.FormattingEnabled = True
        Me.cbxBuscar.IntegralHeight = False
        Me.cbxBuscar.Location = New System.Drawing.Point(260, 5)
        Me.cbxBuscar.Name = "cbxBuscar"
        Me.cbxBuscar.Size = New System.Drawing.Size(915, 27)
        Me.cbxBuscar.TabIndex = 13
        '
        'txtDescripcion
        '
        Me.txtDescripcion.Font = New System.Drawing.Font("Calibri", 12.0!)
        Me.txtDescripcion.Location = New System.Drawing.Point(169, 57)
        Me.txtDescripcion.Name = "txtDescripcion"
        Me.txtDescripcion.Size = New System.Drawing.Size(513, 27)
        Me.txtDescripcion.TabIndex = 6
        '
        'txtUnidad
        '
        Me.txtUnidad.Font = New System.Drawing.Font("Calibri", 12.0!)
        Me.txtUnidad.Location = New System.Drawing.Point(773, 57)
        Me.txtUnidad.Name = "txtUnidad"
        Me.txtUnidad.Size = New System.Drawing.Size(120, 27)
        Me.txtUnidad.TabIndex = 9
        Me.txtUnidad.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.ToolTip.SetToolTip(Me.txtUnidad, "Las unidades son en mts. o piezas si correspondiera")
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.BackColor = System.Drawing.Color.Transparent
        Me.Label12.Font = New System.Drawing.Font("Cambria", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Label12.Location = New System.Drawing.Point(962, 27)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(29, 19)
        Me.Label12.TabIndex = 25
        Me.Label12.Text = "Id:"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.BackColor = System.Drawing.Color.Transparent
        Me.Label14.Font = New System.Drawing.Font("Cambria", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Label14.ForeColor = System.Drawing.Color.Black
        Me.Label14.Location = New System.Drawing.Point(188, 8)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(64, 19)
        Me.Label14.TabIndex = 28
        Me.Label14.Text = "Buscar:"
        '
        'txtNombre
        '
        Me.txtNombre.Font = New System.Drawing.Font("Calibri", 12.0!)
        Me.txtNombre.Location = New System.Drawing.Point(169, 24)
        Me.txtNombre.Name = "txtNombre"
        Me.txtNombre.Size = New System.Drawing.Size(513, 27)
        Me.txtNombre.TabIndex = 4
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.BackColor = System.Drawing.Color.Transparent
        Me.Label15.Font = New System.Drawing.Font("Cambria", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Label15.Location = New System.Drawing.Point(800, 30)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(162, 19)
        Me.Label15.TabIndex = 40
        Me.Label15.Text = "Fecha Actualizacion:"
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.BackColor = System.Drawing.Color.Transparent
        Me.Label20.Font = New System.Drawing.Font("Cambria", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Label20.Location = New System.Drawing.Point(100, 91)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(63, 19)
        Me.Label20.TabIndex = 44
        Me.Label20.Text = "Codigo:"
        '
        'txtCodigo
        '
        Me.txtCodigo.Font = New System.Drawing.Font("Calibri", 12.0!)
        Me.txtCodigo.Location = New System.Drawing.Point(169, 88)
        Me.txtCodigo.Name = "txtCodigo"
        Me.txtCodigo.Size = New System.Drawing.Size(513, 27)
        Me.txtCodigo.TabIndex = 8
        '
        'gcInfoGral
        '
        Me.gcInfoGral.Controls.Add(Me.txtPrecio)
        Me.gcInfoGral.Controls.Add(Me.Label10)
        Me.gcInfoGral.Controls.Add(Me.cmbCategoria)
        Me.gcInfoGral.Controls.Add(Me.txtCosto)
        Me.gcInfoGral.Controls.Add(Me.Label4)
        Me.gcInfoGral.Controls.Add(Me.Label7)
        Me.gcInfoGral.Controls.Add(Me.txtUnidad)
        Me.gcInfoGral.Controls.Add(Me.Label3)
        Me.gcInfoGral.Controls.Add(Me.Label2)
        Me.gcInfoGral.Controls.Add(Me.txtDescripcion)
        Me.gcInfoGral.Controls.Add(Me.Label20)
        Me.gcInfoGral.Controls.Add(Me.txtCodigo)
        Me.gcInfoGral.Controls.Add(Me.Label1)
        Me.gcInfoGral.Controls.Add(Me.txtNombre)
        Me.gcInfoGral.Controls.Add(Me.Label12)
        Me.gcInfoGral.Controls.Add(Me.txtId)
        Me.gcInfoGral.Location = New System.Drawing.Point(12, 88)
        Me.gcInfoGral.LookAndFeel.SkinName = "Visual Studio 2013 Dark"
        Me.gcInfoGral.LookAndFeel.UseDefaultLookAndFeel = False
        Me.gcInfoGral.Name = "gcInfoGral"
        Me.gcInfoGral.Size = New System.Drawing.Size(1090, 120)
        Me.gcInfoGral.TabIndex = 48
        Me.gcInfoGral.Text = "Información General"
        '
        'txtPrecio
        '
        Me.txtPrecio.Font = New System.Drawing.Font("Calibri", 12.0!)
        Me.txtPrecio.Location = New System.Drawing.Point(964, 88)
        Me.txtPrecio.Name = "txtPrecio"
        Me.txtPrecio.Size = New System.Drawing.Size(119, 27)
        Me.txtPrecio.TabIndex = 47
        Me.txtPrecio.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.ToolTip.SetToolTip(Me.txtPrecio, "El precio es: Costo * Ganancia (60 %) * IVA (21 %)")
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.BackColor = System.Drawing.Color.Transparent
        Me.Label10.Font = New System.Drawing.Font("Cambria", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Label10.Location = New System.Drawing.Point(905, 91)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(60, 19)
        Me.Label10.TabIndex = 46
        Me.Label10.Text = "Precio:"
        '
        'cmbCategoria
        '
        Me.cmbCategoria.AllowDrop = True
        Me.cmbCategoria.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cmbCategoria.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cmbCategoria.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbCategoria.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbCategoria.FormattingEnabled = True
        Me.cmbCategoria.Location = New System.Drawing.Point(773, 24)
        Me.cmbCategoria.Name = "cmbCategoria"
        Me.cmbCategoria.Size = New System.Drawing.Size(183, 27)
        Me.cmbCategoria.TabIndex = 45
        '
        'txtCosto
        '
        Me.txtCosto.Font = New System.Drawing.Font("Calibri", 12.0!)
        Me.txtCosto.Location = New System.Drawing.Point(774, 88)
        Me.txtCosto.Name = "txtCosto"
        Me.txtCosto.Size = New System.Drawing.Size(119, 27)
        Me.txtCosto.TabIndex = 10
        Me.txtCosto.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.ToolTip.SetToolTip(Me.txtCosto, "Costo en pesos sin impuestos ni ganancia}")
        '
        'txtId
        '
        Me.txtId.Font = New System.Drawing.Font("Calibri", 12.0!)
        Me.txtId.Location = New System.Drawing.Point(997, 24)
        Me.txtId.Name = "txtId"
        Me.txtId.Size = New System.Drawing.Size(87, 27)
        Me.txtId.TabIndex = 5
        Me.txtId.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'dtpFecha
        '
        Me.dtpFecha.CustomFormat = "dd/MM/yyyy"
        Me.dtpFecha.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpFecha.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpFecha.Location = New System.Drawing.Point(968, 25)
        Me.dtpFecha.MaxDate = New Date(2050, 12, 31, 0, 0, 0, 0)
        Me.dtpFecha.MinDate = New Date(1900, 1, 1, 0, 0, 0, 0)
        Me.dtpFecha.Name = "dtpFecha"
        Me.dtpFecha.Size = New System.Drawing.Size(117, 26)
        Me.dtpFecha.TabIndex = 12
        '
        'gcProveedor
        '
        Me.gcProveedor.Controls.Add(Me.cmbProveedor)
        Me.gcProveedor.Controls.Add(Me.Label6)
        Me.gcProveedor.Controls.Add(Me.dtpFecha)
        Me.gcProveedor.Controls.Add(Me.Label15)
        Me.gcProveedor.Location = New System.Drawing.Point(12, 214)
        Me.gcProveedor.LookAndFeel.SkinName = "Visual Studio 2013 Dark"
        Me.gcProveedor.LookAndFeel.UseDefaultLookAndFeel = False
        Me.gcProveedor.Name = "gcProveedor"
        Me.gcProveedor.Size = New System.Drawing.Size(1090, 56)
        Me.gcProveedor.TabIndex = 48
        Me.gcProveedor.Text = "Proveedor"
        '
        'cmbProveedor
        '
        Me.cmbProveedor.AllowDrop = True
        Me.cmbProveedor.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cmbProveedor.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cmbProveedor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbProveedor.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbProveedor.FormattingEnabled = True
        Me.cmbProveedor.Location = New System.Drawing.Point(171, 24)
        Me.cmbProveedor.Name = "cmbProveedor"
        Me.cmbProveedor.Size = New System.Drawing.Size(623, 27)
        Me.cmbProveedor.TabIndex = 46
        '
        'gcBuscar
        '
        Me.gcBuscar.Controls.Add(Me.btnGuardar)
        Me.gcBuscar.Controls.Add(Me.Label8)
        Me.gcBuscar.Controls.Add(Me.btnNuevo)
        Me.gcBuscar.Controls.Add(Me.cmbBuscar)
        Me.gcBuscar.Location = New System.Drawing.Point(12, 45)
        Me.gcBuscar.LookAndFeel.SkinName = "Visual Studio 2013 Dark"
        Me.gcBuscar.LookAndFeel.UseDefaultLookAndFeel = False
        Me.gcBuscar.Name = "gcBuscar"
        Me.gcBuscar.Size = New System.Drawing.Size(1090, 37)
        Me.gcBuscar.TabIndex = 52
        '
        'btnGuardar
        '
        Me.btnGuardar.Appearance.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold)
        Me.btnGuardar.Appearance.Options.UseFont = True
        Me.btnGuardar.Cursor = System.Windows.Forms.Cursors.Default
        Me.btnGuardar.Location = New System.Drawing.Point(1010, 5)
        Me.btnGuardar.LookAndFeel.SkinName = "Visual Studio 2013 Dark"
        Me.btnGuardar.LookAndFeel.UseDefaultLookAndFeel = False
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(75, 27)
        Me.btnGuardar.TabIndex = 3
        Me.btnGuardar.Text = "Guardar"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.BackColor = System.Drawing.Color.Transparent
        Me.Label8.Font = New System.Drawing.Font("Cambria", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Label8.Location = New System.Drawing.Point(92, 7)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(71, 19)
        Me.Label8.TabIndex = 54
        Me.Label8.Text = "Nombre:"
        '
        'btnNuevo
        '
        Me.btnNuevo.Appearance.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold)
        Me.btnNuevo.Appearance.Options.UseFont = True
        Me.btnNuevo.Location = New System.Drawing.Point(929, 5)
        Me.btnNuevo.LookAndFeel.SkinName = "Visual Studio 2013 Dark"
        Me.btnNuevo.LookAndFeel.UseDefaultLookAndFeel = False
        Me.btnNuevo.Name = "btnNuevo"
        Me.btnNuevo.Size = New System.Drawing.Size(75, 27)
        Me.btnNuevo.TabIndex = 2
        Me.btnNuevo.Text = "Nuevo"
        '
        'cmbBuscar
        '
        Me.cmbBuscar.AllowDrop = True
        Me.cmbBuscar.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cmbBuscar.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cmbBuscar.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbBuscar.Font = New System.Drawing.Font("Calibri", 12.0!)
        Me.cmbBuscar.IntegralHeight = False
        Me.cmbBuscar.Location = New System.Drawing.Point(169, 4)
        Me.cmbBuscar.Name = "cmbBuscar"
        Me.cmbBuscar.Size = New System.Drawing.Size(754, 27)
        Me.cmbBuscar.TabIndex = 1
        '
        'btnCancelar
        '
        Me.btnCancelar.Appearance.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold)
        Me.btnCancelar.Appearance.Options.UseFont = True
        Me.btnCancelar.Cursor = System.Windows.Forms.Cursors.Default
        Me.btnCancelar.Location = New System.Drawing.Point(1027, 608)
        Me.btnCancelar.LookAndFeel.SkinName = "Visual Studio 2013 Dark"
        Me.btnCancelar.LookAndFeel.UseDefaultLookAndFeel = False
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 27)
        Me.btnCancelar.TabIndex = 14
        Me.btnCancelar.Text = "Cancelar"
        '
        'btnSalir
        '
        Me.btnSalir.Appearance.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold)
        Me.btnSalir.Appearance.Options.UseFont = True
        Me.btnSalir.Location = New System.Drawing.Point(1027, 641)
        Me.btnSalir.LookAndFeel.SkinName = "Visual Studio 2013 Dark"
        Me.btnSalir.LookAndFeel.UseDefaultLookAndFeel = False
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(75, 27)
        Me.btnSalir.TabIndex = 15
        Me.btnSalir.Text = "Cerrar"
        '
        'btnEliminar
        '
        Me.btnEliminar.Appearance.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold)
        Me.btnEliminar.Appearance.Options.UseFont = True
        Me.btnEliminar.Location = New System.Drawing.Point(1027, 575)
        Me.btnEliminar.LookAndFeel.SkinName = "Visual Studio 2013 Dark"
        Me.btnEliminar.LookAndFeel.UseDefaultLookAndFeel = False
        Me.btnEliminar.Name = "btnEliminar"
        Me.btnEliminar.Size = New System.Drawing.Size(75, 27)
        Me.btnEliminar.TabIndex = 13
        Me.btnEliminar.Text = "Eliminar"
        '
        'gcBusqueda
        '
        Me.gcBusqueda.Controls.Add(Me.dgvProductos)
        Me.gcBusqueda.Controls.Add(Me.btnCargardgv)
        Me.gcBusqueda.Controls.Add(Me.cmbBuscarCat)
        Me.gcBusqueda.Controls.Add(Me.Label9)
        Me.gcBusqueda.Controls.Add(Me.cmbBuscarProv)
        Me.gcBusqueda.Controls.Add(Me.Label5)
        Me.gcBusqueda.Location = New System.Drawing.Point(12, 276)
        Me.gcBusqueda.LookAndFeel.SkinName = "Visual Studio 2013 Dark"
        Me.gcBusqueda.LookAndFeel.UseDefaultLookAndFeel = False
        Me.gcBusqueda.Name = "gcBusqueda"
        Me.gcBusqueda.Size = New System.Drawing.Size(1009, 392)
        Me.gcBusqueda.TabIndex = 53
        Me.gcBusqueda.Text = "Busqueda"
        '
        'dgvProductos
        '
        Me.dgvProductos.AllowUserToDeleteRows = False
        Me.dgvProductos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(32, Byte), Integer), CType(CType(31, Byte), Integer), CType(CType(53, Byte), Integer))
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvProductos.DefaultCellStyle = DataGridViewCellStyle1
        Me.dgvProductos.Location = New System.Drawing.Point(169, 57)
        Me.dgvProductos.Name = "dgvProductos"
        Me.dgvProductos.ReadOnly = True
        Me.dgvProductos.Size = New System.Drawing.Size(835, 330)
        Me.dgvProductos.TabIndex = 50
        '
        'btnCargardgv
        '
        Me.btnCargardgv.Appearance.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold)
        Me.btnCargardgv.Appearance.Options.UseFont = True
        Me.btnCargardgv.Location = New System.Drawing.Point(929, 24)
        Me.btnCargardgv.LookAndFeel.SkinName = "Visual Studio 2013 Dark"
        Me.btnCargardgv.LookAndFeel.UseDefaultLookAndFeel = False
        Me.btnCargardgv.Name = "btnCargardgv"
        Me.btnCargardgv.Size = New System.Drawing.Size(75, 27)
        Me.btnCargardgv.TabIndex = 49
        Me.btnCargardgv.Text = "Buscar"
        '
        'cmbBuscarCat
        '
        Me.cmbBuscarCat.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbBuscarCat.FormattingEnabled = True
        Me.cmbBuscarCat.Location = New System.Drawing.Point(594, 24)
        Me.cmbBuscarCat.Name = "cmbBuscarCat"
        Me.cmbBuscarCat.Size = New System.Drawing.Size(329, 27)
        Me.cmbBuscarCat.TabIndex = 48
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.BackColor = System.Drawing.Color.Transparent
        Me.Label9.Font = New System.Drawing.Font("Cambria", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Label9.Location = New System.Drawing.Point(505, 27)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(83, 19)
        Me.Label9.TabIndex = 47
        Me.Label9.Text = "Categoria:"
        '
        'cmbBuscarProv
        '
        Me.cmbBuscarProv.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbBuscarProv.FormattingEnabled = True
        Me.cmbBuscarProv.Location = New System.Drawing.Point(169, 24)
        Me.cmbBuscarProv.Name = "cmbBuscarProv"
        Me.cmbBuscarProv.Size = New System.Drawing.Size(330, 27)
        Me.cmbBuscarProv.TabIndex = 46
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.BackColor = System.Drawing.Color.Transparent
        Me.Label5.Font = New System.Drawing.Font("Cambria", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Label5.Location = New System.Drawing.Point(74, 27)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(89, 19)
        Me.Label5.TabIndex = 6
        Me.Label5.Text = "Proveedor:"
        '
        'frmProducto
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoSize = True
        Me.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.BackColor = System.Drawing.Color.Black
        Me.ClientSize = New System.Drawing.Size(1120, 701)
        Me.ControlBox = False
        Me.Controls.Add(Me.gcBusqueda)
        Me.Controls.Add(Me.btnEliminar)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(Me.btnCancelar)
        Me.Controls.Add(Me.gcBuscar)
        Me.Controls.Add(Me.gcProveedor)
        Me.Controls.Add(Me.gcInfoGral)
        Me.Controls.Add(Me.lblTitulo)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.MaximumSize = New System.Drawing.Size(1366, 701)
        Me.MinimumSize = New System.Drawing.Size(1120, 701)
        Me.Name = "frmProducto"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Tag = "Clientes"
        Me.Text = "Productos"
        CType(Me.gcInfoGral, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gcInfoGral.ResumeLayout(False)
        Me.gcInfoGral.PerformLayout()
        CType(Me.gcProveedor, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gcProveedor.ResumeLayout(False)
        Me.gcProveedor.PerformLayout()
        CType(Me.gcBuscar, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gcBuscar.ResumeLayout(False)
        Me.gcBuscar.PerformLayout()
        CType(Me.gcBusqueda, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gcBusqueda.ResumeLayout(False)
        Me.gcBusqueda.PerformLayout()
        CType(Me.dgvProductos, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents lblTitulo As System.Windows.Forms.Label
    Friend WithEvents cbxBuscar As System.Windows.Forms.ComboBox
    Friend WithEvents txtDescripcion As System.Windows.Forms.TextBox
    Friend WithEvents txtUnidad As System.Windows.Forms.TextBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents txtNombre As System.Windows.Forms.TextBox
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents txtCodigo As System.Windows.Forms.TextBox
    Friend WithEvents gcInfoGral As DevExpress.XtraEditors.GroupControl
    Friend WithEvents gcProveedor As DevExpress.XtraEditors.GroupControl
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents txtId As System.Windows.Forms.TextBox
    Friend WithEvents gcBuscar As DevExpress.XtraEditors.PanelControl
    Friend WithEvents btnGuardar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents btnNuevo As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmbBuscar As System.Windows.Forms.ComboBox
    Friend WithEvents btnCancelar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnSalir As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnEliminar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents dtpFecha As System.Windows.Forms.DateTimePicker
    Friend WithEvents txtCosto As System.Windows.Forms.TextBox
    Friend WithEvents cmbCategoria As System.Windows.Forms.ComboBox
    Friend WithEvents cmbProveedor As System.Windows.Forms.ComboBox
    Friend WithEvents gcBusqueda As DevExpress.XtraEditors.GroupControl
    Friend WithEvents dgvProductos As System.Windows.Forms.DataGridView
    Friend WithEvents btnCargardgv As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmbBuscarCat As System.Windows.Forms.ComboBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents cmbBuscarProv As System.Windows.Forms.ComboBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtPrecio As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents ToolTip As System.Windows.Forms.ToolTip

End Class
