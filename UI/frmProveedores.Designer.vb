﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmProveedores
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnSalir = New DevExpress.XtraEditors.SimpleButton()
        Me.gcBuscar = New DevExpress.XtraEditors.PanelControl()
        Me.btnGuardar = New DevExpress.XtraEditors.SimpleButton()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.btnNuevo = New DevExpress.XtraEditors.SimpleButton()
        Me.cmbBuscar = New System.Windows.Forms.ComboBox()
        Me.gcDomicilio = New DevExpress.XtraEditors.GroupControl()
        Me.cmbCiudad = New System.Windows.Forms.ComboBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtDireccion = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtCP = New System.Windows.Forms.TextBox()
        Me.txtMail3 = New System.Windows.Forms.TextBox()
        Me.txtMail1 = New System.Windows.Forms.TextBox()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.txtMail2 = New System.Windows.Forms.TextBox()
        Me.txtTelContacto1 = New System.Windows.Forms.TextBox()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.gcInfogral = New DevExpress.XtraEditors.GroupControl()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.txtSucursal = New System.Windows.Forms.TextBox()
        Me.txtNombre = New System.Windows.Forms.TextBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.txtWeb = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.txtCUIT = New System.Windows.Forms.TextBox()
        Me.txtEmpresa = New System.Windows.Forms.TextBox()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.txtId = New System.Windows.Forms.TextBox()
        Me.lblTitulo = New System.Windows.Forms.Label()
        Me.btnEliminar = New DevExpress.XtraEditors.SimpleButton()
        Me.btnCancelar = New DevExpress.XtraEditors.SimpleButton()
        Me.gcContacto = New DevExpress.XtraEditors.GroupControl()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.txtContacto3 = New System.Windows.Forms.TextBox()
        Me.txtTelContacto3 = New System.Windows.Forms.TextBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txtContacto2 = New System.Windows.Forms.TextBox()
        Me.txtTelContacto2 = New System.Windows.Forms.TextBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.txtContacto1 = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        CType(Me.gcBuscar, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gcBuscar.SuspendLayout()
        CType(Me.gcDomicilio, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gcDomicilio.SuspendLayout()
        CType(Me.gcInfogral, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gcInfogral.SuspendLayout()
        CType(Me.gcContacto, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gcContacto.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnSalir
        '
        Me.btnSalir.Appearance.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold)
        Me.btnSalir.Appearance.Options.UseFont = True
        Me.btnSalir.Location = New System.Drawing.Point(1027, 625)
        Me.btnSalir.LookAndFeel.SkinName = "Visual Studio 2013 Dark"
        Me.btnSalir.LookAndFeel.UseDefaultLookAndFeel = False
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(75, 27)
        Me.btnSalir.TabIndex = 58
        Me.btnSalir.Text = "Cerrar"
        '
        'gcBuscar
        '
        Me.gcBuscar.Controls.Add(Me.btnGuardar)
        Me.gcBuscar.Controls.Add(Me.Label8)
        Me.gcBuscar.Controls.Add(Me.btnNuevo)
        Me.gcBuscar.Controls.Add(Me.cmbBuscar)
        Me.gcBuscar.Location = New System.Drawing.Point(12, 45)
        Me.gcBuscar.LookAndFeel.SkinName = "Visual Studio 2013 Dark"
        Me.gcBuscar.LookAndFeel.UseDefaultLookAndFeel = False
        Me.gcBuscar.Name = "gcBuscar"
        Me.gcBuscar.Size = New System.Drawing.Size(1090, 37)
        Me.gcBuscar.TabIndex = 65
        '
        'btnGuardar
        '
        Me.btnGuardar.Appearance.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold)
        Me.btnGuardar.Appearance.Options.UseFont = True
        Me.btnGuardar.Cursor = System.Windows.Forms.Cursors.Default
        Me.btnGuardar.Location = New System.Drawing.Point(1010, 5)
        Me.btnGuardar.LookAndFeel.SkinName = "Visual Studio 2013 Dark"
        Me.btnGuardar.LookAndFeel.UseDefaultLookAndFeel = False
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(75, 27)
        Me.btnGuardar.TabIndex = 58
        Me.btnGuardar.Text = "Guardar"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.BackColor = System.Drawing.Color.Transparent
        Me.Label8.Font = New System.Drawing.Font("Cambria", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Label8.Location = New System.Drawing.Point(92, 9)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(71, 19)
        Me.Label8.TabIndex = 54
        Me.Label8.Text = "Nombre:"
        '
        'btnNuevo
        '
        Me.btnNuevo.Appearance.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold)
        Me.btnNuevo.Appearance.Options.UseFont = True
        Me.btnNuevo.Location = New System.Drawing.Point(929, 5)
        Me.btnNuevo.LookAndFeel.SkinName = "Visual Studio 2013 Dark"
        Me.btnNuevo.LookAndFeel.UseDefaultLookAndFeel = False
        Me.btnNuevo.Name = "btnNuevo"
        Me.btnNuevo.Size = New System.Drawing.Size(75, 27)
        Me.btnNuevo.TabIndex = 57
        Me.btnNuevo.Text = "Nuevo"
        '
        'cmbBuscar
        '
        Me.cmbBuscar.AllowDrop = True
        Me.cmbBuscar.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cmbBuscar.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cmbBuscar.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbBuscar.Font = New System.Drawing.Font("Calibri", 12.0!)
        Me.cmbBuscar.IntegralHeight = False
        Me.cmbBuscar.Location = New System.Drawing.Point(169, 6)
        Me.cmbBuscar.Name = "cmbBuscar"
        Me.cmbBuscar.Size = New System.Drawing.Size(754, 27)
        Me.cmbBuscar.TabIndex = 53
        '
        'gcDomicilio
        '
        Me.gcDomicilio.Controls.Add(Me.cmbCiudad)
        Me.gcDomicilio.Controls.Add(Me.Label3)
        Me.gcDomicilio.Controls.Add(Me.Label2)
        Me.gcDomicilio.Controls.Add(Me.txtDireccion)
        Me.gcDomicilio.Controls.Add(Me.Label4)
        Me.gcDomicilio.Controls.Add(Me.txtCP)
        Me.gcDomicilio.Location = New System.Drawing.Point(12, 216)
        Me.gcDomicilio.LookAndFeel.SkinName = "Visual Studio 2013 Dark"
        Me.gcDomicilio.LookAndFeel.UseDefaultLookAndFeel = False
        Me.gcDomicilio.Name = "gcDomicilio"
        Me.gcDomicilio.Size = New System.Drawing.Size(1090, 56)
        Me.gcDomicilio.TabIndex = 64
        Me.gcDomicilio.Text = "Domicilio"
        '
        'cmbCiudad
        '
        Me.cmbCiudad.AllowDrop = True
        Me.cmbCiudad.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cmbCiudad.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cmbCiudad.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbCiudad.Font = New System.Drawing.Font("Calibri", 12.0!)
        Me.cmbCiudad.IntegralHeight = False
        Me.cmbCiudad.Location = New System.Drawing.Point(694, 24)
        Me.cmbCiudad.Name = "cmbCiudad"
        Me.cmbCiudad.Size = New System.Drawing.Size(266, 27)
        Me.cmbCiudad.TabIndex = 59
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.Label3.Font = New System.Drawing.Font("Cambria", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Label3.Location = New System.Drawing.Point(622, 27)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(66, 19)
        Me.Label3.TabIndex = 3
        Me.Label3.Text = "Ciudad:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.Font = New System.Drawing.Font("Cambria", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Label2.Location = New System.Drawing.Point(79, 27)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(84, 19)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Dirección:"
        '
        'txtDireccion
        '
        Me.txtDireccion.Font = New System.Drawing.Font("Calibri", 12.0!)
        Me.txtDireccion.Location = New System.Drawing.Point(169, 24)
        Me.txtDireccion.Name = "txtDireccion"
        Me.txtDireccion.Size = New System.Drawing.Size(449, 27)
        Me.txtDireccion.TabIndex = 14
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.BackColor = System.Drawing.Color.Transparent
        Me.Label4.Font = New System.Drawing.Font("Cambria", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Label4.Location = New System.Drawing.Point(966, 27)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(32, 19)
        Me.Label4.TabIndex = 4
        Me.Label4.Text = "CP:"
        '
        'txtCP
        '
        Me.txtCP.Font = New System.Drawing.Font("Calibri", 12.0!)
        Me.txtCP.Location = New System.Drawing.Point(1004, 24)
        Me.txtCP.Name = "txtCP"
        Me.txtCP.Size = New System.Drawing.Size(80, 27)
        Me.txtCP.TabIndex = 16
        Me.txtCP.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtMail3
        '
        Me.txtMail3.Font = New System.Drawing.Font("Calibri", 12.0!)
        Me.txtMail3.Location = New System.Drawing.Point(90, 85)
        Me.txtMail3.Name = "txtMail3"
        Me.txtMail3.Size = New System.Drawing.Size(260, 27)
        Me.txtMail3.TabIndex = 36
        '
        'txtMail1
        '
        Me.txtMail1.Font = New System.Drawing.Font("Calibri", 12.0!)
        Me.txtMail1.Location = New System.Drawing.Point(90, 85)
        Me.txtMail1.Name = "txtMail1"
        Me.txtMail1.Size = New System.Drawing.Size(260, 27)
        Me.txtMail1.TabIndex = 17
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.BackColor = System.Drawing.Color.Transparent
        Me.Label28.Font = New System.Drawing.Font("Cambria", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Label28.Location = New System.Drawing.Point(38, 88)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(46, 19)
        Me.Label28.TabIndex = 6
        Me.Label28.Text = "Mail:"
        '
        'txtMail2
        '
        Me.txtMail2.Font = New System.Drawing.Font("Calibri", 12.0!)
        Me.txtMail2.Location = New System.Drawing.Point(90, 85)
        Me.txtMail2.Name = "txtMail2"
        Me.txtMail2.Size = New System.Drawing.Size(260, 27)
        Me.txtMail2.TabIndex = 32
        '
        'txtTelContacto1
        '
        Me.txtTelContacto1.Font = New System.Drawing.Font("Calibri", 12.0!)
        Me.txtTelContacto1.Location = New System.Drawing.Point(90, 52)
        Me.txtTelContacto1.Name = "txtTelContacto1"
        Me.txtTelContacto1.Size = New System.Drawing.Size(260, 27)
        Me.txtTelContacto1.TabIndex = 17
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.BackColor = System.Drawing.Color.Transparent
        Me.Label25.Font = New System.Drawing.Font("Cambria", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Label25.Location = New System.Drawing.Point(8, 55)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(76, 19)
        Me.Label25.TabIndex = 6
        Me.Label25.Text = "Telefono:"
        '
        'gcInfogral
        '
        Me.gcInfogral.Controls.Add(Me.Label15)
        Me.gcInfogral.Controls.Add(Me.txtSucursal)
        Me.gcInfogral.Controls.Add(Me.txtNombre)
        Me.gcInfogral.Controls.Add(Me.Label14)
        Me.gcInfogral.Controls.Add(Me.txtWeb)
        Me.gcInfogral.Controls.Add(Me.Label5)
        Me.gcInfogral.Controls.Add(Me.Label20)
        Me.gcInfogral.Controls.Add(Me.txtCUIT)
        Me.gcInfogral.Controls.Add(Me.txtEmpresa)
        Me.gcInfogral.Controls.Add(Me.Label19)
        Me.gcInfogral.Controls.Add(Me.Label12)
        Me.gcInfogral.Controls.Add(Me.txtId)
        Me.gcInfogral.Location = New System.Drawing.Point(12, 88)
        Me.gcInfogral.LookAndFeel.SkinName = "Visual Studio 2013 Dark"
        Me.gcInfogral.LookAndFeel.UseDefaultLookAndFeel = False
        Me.gcInfogral.Name = "gcInfogral"
        Me.gcInfogral.Size = New System.Drawing.Size(1090, 122)
        Me.gcInfogral.TabIndex = 61
        Me.gcInfogral.Text = "Información General"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.BackColor = System.Drawing.Color.Transparent
        Me.Label15.Font = New System.Drawing.Font("Cambria", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Label15.Location = New System.Drawing.Point(634, 28)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(77, 19)
        Me.Label15.TabIndex = 52
        Me.Label15.Text = "Sucursal:"
        '
        'txtSucursal
        '
        Me.txtSucursal.Font = New System.Drawing.Font("Calibri", 12.0!)
        Me.txtSucursal.Location = New System.Drawing.Point(717, 24)
        Me.txtSucursal.Name = "txtSucursal"
        Me.txtSucursal.Size = New System.Drawing.Size(172, 27)
        Me.txtSucursal.TabIndex = 53
        Me.txtSucursal.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtNombre
        '
        Me.txtNombre.Font = New System.Drawing.Font("Calibri", 12.0!)
        Me.txtNombre.Location = New System.Drawing.Point(169, 24)
        Me.txtNombre.Name = "txtNombre"
        Me.txtNombre.Size = New System.Drawing.Size(459, 27)
        Me.txtNombre.TabIndex = 51
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.BackColor = System.Drawing.Color.Transparent
        Me.Label14.Font = New System.Drawing.Font("Cambria", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Label14.Location = New System.Drawing.Point(92, 27)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(71, 19)
        Me.Label14.TabIndex = 50
        Me.Label14.Text = "Nombre:"
        '
        'txtWeb
        '
        Me.txtWeb.Font = New System.Drawing.Font("Calibri", 12.0!)
        Me.txtWeb.Location = New System.Drawing.Point(169, 90)
        Me.txtWeb.Name = "txtWeb"
        Me.txtWeb.Size = New System.Drawing.Size(720, 27)
        Me.txtWeb.TabIndex = 49
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.BackColor = System.Drawing.Color.Transparent
        Me.Label5.Font = New System.Drawing.Font("Cambria", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Label5.Location = New System.Drawing.Point(64, 93)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(99, 19)
        Me.Label5.TabIndex = 48
        Me.Label5.Text = "Página Web:"
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.BackColor = System.Drawing.Color.Transparent
        Me.Label20.Font = New System.Drawing.Font("Cambria", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Label20.Location = New System.Drawing.Point(634, 61)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(49, 19)
        Me.Label20.TabIndex = 44
        Me.Label20.Text = "CUIT:"
        '
        'txtCUIT
        '
        Me.txtCUIT.Font = New System.Drawing.Font("Calibri", 12.0!)
        Me.txtCUIT.Location = New System.Drawing.Point(687, 58)
        Me.txtCUIT.Name = "txtCUIT"
        Me.txtCUIT.Size = New System.Drawing.Size(202, 27)
        Me.txtCUIT.TabIndex = 46
        Me.txtCUIT.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtEmpresa
        '
        Me.txtEmpresa.Font = New System.Drawing.Font("Calibri", 12.0!)
        Me.txtEmpresa.Location = New System.Drawing.Point(169, 57)
        Me.txtEmpresa.Name = "txtEmpresa"
        Me.txtEmpresa.Size = New System.Drawing.Size(459, 27)
        Me.txtEmpresa.TabIndex = 43
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.BackColor = System.Drawing.Color.Transparent
        Me.Label19.Font = New System.Drawing.Font("Cambria", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Label19.Location = New System.Drawing.Point(55, 60)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(108, 19)
        Me.Label19.TabIndex = 42
        Me.Label19.Text = "Razón Social:"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.BackColor = System.Drawing.Color.Transparent
        Me.Label12.Font = New System.Drawing.Font("Cambria", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Label12.Location = New System.Drawing.Point(962, 27)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(29, 19)
        Me.Label12.TabIndex = 25
        Me.Label12.Text = "Id:"
        '
        'txtId
        '
        Me.txtId.Font = New System.Drawing.Font("Calibri", 12.0!)
        Me.txtId.Location = New System.Drawing.Point(997, 24)
        Me.txtId.Name = "txtId"
        Me.txtId.Size = New System.Drawing.Size(87, 27)
        Me.txtId.TabIndex = 26
        Me.txtId.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'lblTitulo
        '
        Me.lblTitulo.AccessibleRole = System.Windows.Forms.AccessibleRole.TitleBar
        Me.lblTitulo.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.lblTitulo.AutoSize = True
        Me.lblTitulo.BackColor = System.Drawing.Color.Transparent
        Me.lblTitulo.Font = New System.Drawing.Font("Microsoft Sans Serif", 24.0!)
        Me.lblTitulo.ForeColor = System.Drawing.Color.White
        Me.lblTitulo.Location = New System.Drawing.Point(386, 4)
        Me.lblTitulo.Name = "lblTitulo"
        Me.lblTitulo.Size = New System.Drawing.Size(449, 37)
        Me.lblTitulo.TabIndex = 59
        Me.lblTitulo.Text = "Administrador de Proveedores"
        '
        'btnEliminar
        '
        Me.btnEliminar.Appearance.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold)
        Me.btnEliminar.Appearance.Options.UseFont = True
        Me.btnEliminar.Location = New System.Drawing.Point(1027, 559)
        Me.btnEliminar.LookAndFeel.SkinName = "Visual Studio 2013 Dark"
        Me.btnEliminar.LookAndFeel.UseDefaultLookAndFeel = False
        Me.btnEliminar.Name = "btnEliminar"
        Me.btnEliminar.Size = New System.Drawing.Size(75, 27)
        Me.btnEliminar.TabIndex = 67
        Me.btnEliminar.Text = "Eliminar"
        '
        'btnCancelar
        '
        Me.btnCancelar.Appearance.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold)
        Me.btnCancelar.Appearance.Options.UseFont = True
        Me.btnCancelar.Cursor = System.Windows.Forms.Cursors.Default
        Me.btnCancelar.Location = New System.Drawing.Point(1027, 592)
        Me.btnCancelar.LookAndFeel.SkinName = "Visual Studio 2013 Dark"
        Me.btnCancelar.LookAndFeel.UseDefaultLookAndFeel = False
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 27)
        Me.btnCancelar.TabIndex = 66
        Me.btnCancelar.Text = "Cancelar"
        '
        'gcContacto
        '
        Me.gcContacto.Controls.Add(Me.GroupBox3)
        Me.gcContacto.Controls.Add(Me.GroupBox2)
        Me.gcContacto.Controls.Add(Me.GroupBox1)
        Me.gcContacto.Location = New System.Drawing.Point(12, 278)
        Me.gcContacto.LookAndFeel.SkinName = "Visual Studio 2013 Dark"
        Me.gcContacto.LookAndFeel.UseDefaultLookAndFeel = False
        Me.gcContacto.Name = "gcContacto"
        Me.gcContacto.Size = New System.Drawing.Size(1090, 150)
        Me.gcContacto.TabIndex = 68
        Me.gcContacto.Text = "Contacto"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.Label7)
        Me.GroupBox3.Controls.Add(Me.Label10)
        Me.GroupBox3.Controls.Add(Me.Label13)
        Me.GroupBox3.Controls.Add(Me.txtMail3)
        Me.GroupBox3.Controls.Add(Me.txtContacto3)
        Me.GroupBox3.Controls.Add(Me.txtTelContacto3)
        Me.GroupBox3.ForeColor = System.Drawing.Color.White
        Me.GroupBox3.Location = New System.Drawing.Point(730, 24)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(355, 121)
        Me.GroupBox3.TabIndex = 38
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Administracion"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.BackColor = System.Drawing.Color.Transparent
        Me.Label7.Font = New System.Drawing.Font("Cambria", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Label7.Location = New System.Drawing.Point(38, 88)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(46, 19)
        Me.Label7.TabIndex = 37
        Me.Label7.Text = "Mail:"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.BackColor = System.Drawing.Color.Transparent
        Me.Label10.Font = New System.Drawing.Font("Cambria", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Label10.Location = New System.Drawing.Point(13, 23)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(71, 19)
        Me.Label10.TabIndex = 18
        Me.Label10.Text = "Nombre:"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.BackColor = System.Drawing.Color.Transparent
        Me.Label13.Font = New System.Drawing.Font("Cambria", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Label13.Location = New System.Drawing.Point(8, 55)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(76, 19)
        Me.Label13.TabIndex = 19
        Me.Label13.Text = "Telefono:"
        '
        'txtContacto3
        '
        Me.txtContacto3.Font = New System.Drawing.Font("Calibri", 12.0!)
        Me.txtContacto3.Location = New System.Drawing.Point(90, 19)
        Me.txtContacto3.Name = "txtContacto3"
        Me.txtContacto3.Size = New System.Drawing.Size(260, 27)
        Me.txtContacto3.TabIndex = 17
        '
        'txtTelContacto3
        '
        Me.txtTelContacto3.Font = New System.Drawing.Font("Calibri", 12.0!)
        Me.txtTelContacto3.Location = New System.Drawing.Point(90, 52)
        Me.txtTelContacto3.Name = "txtTelContacto3"
        Me.txtTelContacto3.Size = New System.Drawing.Size(260, 27)
        Me.txtTelContacto3.TabIndex = 17
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.Label6)
        Me.GroupBox2.Controls.Add(Me.Label1)
        Me.GroupBox2.Controls.Add(Me.Label9)
        Me.GroupBox2.Controls.Add(Me.txtContacto2)
        Me.GroupBox2.Controls.Add(Me.txtMail2)
        Me.GroupBox2.Controls.Add(Me.txtTelContacto2)
        Me.GroupBox2.ForeColor = System.Drawing.Color.White
        Me.GroupBox2.Location = New System.Drawing.Point(366, 24)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(355, 121)
        Me.GroupBox2.TabIndex = 38
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Ventas 2"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.BackColor = System.Drawing.Color.Transparent
        Me.Label6.Font = New System.Drawing.Font("Cambria", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Label6.Location = New System.Drawing.Point(38, 88)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(46, 19)
        Me.Label6.TabIndex = 33
        Me.Label6.Text = "Mail:"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Cambria", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Label1.Location = New System.Drawing.Point(13, 23)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(71, 19)
        Me.Label1.TabIndex = 18
        Me.Label1.Text = "Nombre:"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.BackColor = System.Drawing.Color.Transparent
        Me.Label9.Font = New System.Drawing.Font("Cambria", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Label9.Location = New System.Drawing.Point(8, 55)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(76, 19)
        Me.Label9.TabIndex = 19
        Me.Label9.Text = "Telefono:"
        '
        'txtContacto2
        '
        Me.txtContacto2.Font = New System.Drawing.Font("Calibri", 12.0!)
        Me.txtContacto2.Location = New System.Drawing.Point(90, 19)
        Me.txtContacto2.Name = "txtContacto2"
        Me.txtContacto2.Size = New System.Drawing.Size(260, 27)
        Me.txtContacto2.TabIndex = 17
        '
        'txtTelContacto2
        '
        Me.txtTelContacto2.Font = New System.Drawing.Font("Calibri", 12.0!)
        Me.txtTelContacto2.Location = New System.Drawing.Point(90, 52)
        Me.txtTelContacto2.Name = "txtTelContacto2"
        Me.txtTelContacto2.Size = New System.Drawing.Size(260, 27)
        Me.txtTelContacto2.TabIndex = 17
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.txtContacto1)
        Me.GroupBox1.Controls.Add(Me.Label11)
        Me.GroupBox1.Controls.Add(Me.txtTelContacto1)
        Me.GroupBox1.Controls.Add(Me.txtMail1)
        Me.GroupBox1.Controls.Add(Me.Label25)
        Me.GroupBox1.Controls.Add(Me.Label28)
        Me.GroupBox1.ForeColor = System.Drawing.Color.White
        Me.GroupBox1.Location = New System.Drawing.Point(5, 24)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(355, 121)
        Me.GroupBox1.TabIndex = 37
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Ventas 1"
        '
        'txtContacto1
        '
        Me.txtContacto1.Font = New System.Drawing.Font("Calibri", 12.0!)
        Me.txtContacto1.Location = New System.Drawing.Point(90, 19)
        Me.txtContacto1.Name = "txtContacto1"
        Me.txtContacto1.Size = New System.Drawing.Size(260, 27)
        Me.txtContacto1.TabIndex = 17
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.BackColor = System.Drawing.Color.Transparent
        Me.Label11.Font = New System.Drawing.Font("Cambria", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Label11.Location = New System.Drawing.Point(13, 23)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(71, 19)
        Me.Label11.TabIndex = 6
        Me.Label11.Text = "Nombre:"
        '
        'frmProveedores
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoSize = True
        Me.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.BackColor = System.Drawing.Color.Black
        Me.ClientSize = New System.Drawing.Size(1120, 701)
        Me.ControlBox = False
        Me.Controls.Add(Me.gcContacto)
        Me.Controls.Add(Me.btnEliminar)
        Me.Controls.Add(Me.btnCancelar)
        Me.Controls.Add(Me.gcBuscar)
        Me.Controls.Add(Me.gcDomicilio)
        Me.Controls.Add(Me.gcInfogral)
        Me.Controls.Add(Me.lblTitulo)
        Me.Controls.Add(Me.btnSalir)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.MaximumSize = New System.Drawing.Size(1364, 701)
        Me.MinimumSize = New System.Drawing.Size(1120, 701)
        Me.Name = "frmProveedores"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Tag = "Proveedores"
        Me.Text = "Proveedores"
        CType(Me.gcBuscar, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gcBuscar.ResumeLayout(False)
        Me.gcBuscar.PerformLayout()
        CType(Me.gcDomicilio, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gcDomicilio.ResumeLayout(False)
        Me.gcDomicilio.PerformLayout()
        CType(Me.gcInfogral, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gcInfogral.ResumeLayout(False)
        Me.gcInfogral.PerformLayout()
        CType(Me.gcContacto, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gcContacto.ResumeLayout(False)
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnSalir As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents gcBuscar As DevExpress.XtraEditors.PanelControl
    Friend WithEvents btnGuardar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents btnNuevo As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmbBuscar As System.Windows.Forms.ComboBox
    Friend WithEvents gcDomicilio As DevExpress.XtraEditors.GroupControl
    Friend WithEvents cmbCiudad As System.Windows.Forms.ComboBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtDireccion As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtCP As System.Windows.Forms.TextBox
    Friend WithEvents txtMail3 As System.Windows.Forms.TextBox
    Friend WithEvents txtMail1 As System.Windows.Forms.TextBox
    Friend WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents txtMail2 As System.Windows.Forms.TextBox
    Friend WithEvents txtTelContacto1 As System.Windows.Forms.TextBox
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents gcInfogral As DevExpress.XtraEditors.GroupControl
    Friend WithEvents txtWeb As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents txtCUIT As System.Windows.Forms.TextBox
    Friend WithEvents txtEmpresa As System.Windows.Forms.TextBox
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents txtId As System.Windows.Forms.TextBox
    Friend WithEvents lblTitulo As System.Windows.Forms.Label
    Friend WithEvents btnEliminar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnCancelar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents gcContacto As DevExpress.XtraEditors.GroupControl
    Friend WithEvents txtContacto1 As System.Windows.Forms.TextBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents txtSucursal As System.Windows.Forms.TextBox
    Friend WithEvents txtNombre As System.Windows.Forms.TextBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents txtContacto3 As System.Windows.Forms.TextBox
    Friend WithEvents txtTelContacto3 As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents txtContacto2 As System.Windows.Forms.TextBox
    Friend WithEvents txtTelContacto2 As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
End Class
