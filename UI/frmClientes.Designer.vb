﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmClientes
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.lblTitulo = New System.Windows.Forms.Label()
        Me.cbxBuscar = New System.Windows.Forms.ComboBox()
        Me.txtDireccion = New System.Windows.Forms.TextBox()
        Me.txtCP = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.txtNombre = New System.Windows.Forms.TextBox()
        Me.dgvHistorial = New System.Windows.Forms.DataGridView()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.txtEmpresa = New System.Windows.Forms.TextBox()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.txtCUIT = New System.Windows.Forms.TextBox()
        Me.txtDNI = New System.Windows.Forms.TextBox()
        Me.gcInfoGral = New DevExpress.XtraEditors.GroupControl()
        Me.dtpFechaNac = New System.Windows.Forms.DateTimePicker()
        Me.txtWeb = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtId = New System.Windows.Forms.TextBox()
        Me.cmbCiudad = New System.Windows.Forms.ComboBox()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.txtMovil3 = New System.Windows.Forms.TextBox()
        Me.txtMovil1 = New System.Windows.Forms.TextBox()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.txtMovil2 = New System.Windows.Forms.TextBox()
        Me.gcDomicilio = New DevExpress.XtraEditors.GroupControl()
        Me.gcBuscar = New DevExpress.XtraEditors.PanelControl()
        Me.btnGuardar = New DevExpress.XtraEditors.SimpleButton()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.btnNuevo = New DevExpress.XtraEditors.SimpleButton()
        Me.cmbBuscar = New System.Windows.Forms.ComboBox()
        Me.btnCancelar = New DevExpress.XtraEditors.SimpleButton()
        Me.btnSalir = New DevExpress.XtraEditors.SimpleButton()
        Me.btnEliminar = New DevExpress.XtraEditors.SimpleButton()
        Me.gcHistorial = New DevExpress.XtraEditors.GroupControl()
        Me.btnImportar = New DevExpress.XtraEditors.SimpleButton()
        Me.gcContacto = New DevExpress.XtraEditors.GroupControl()
        Me.gcContacto3 = New System.Windows.Forms.GroupBox()
        Me.Label32 = New System.Windows.Forms.Label()
        Me.Label33 = New System.Windows.Forms.Label()
        Me.Label34 = New System.Windows.Forms.Label()
        Me.txtMail3 = New System.Windows.Forms.TextBox()
        Me.txtContacto3 = New System.Windows.Forms.TextBox()
        Me.txtTelefono3 = New System.Windows.Forms.TextBox()
        Me.gcContacto2 = New System.Windows.Forms.GroupBox()
        Me.Label35 = New System.Windows.Forms.Label()
        Me.Label36 = New System.Windows.Forms.Label()
        Me.Label37 = New System.Windows.Forms.Label()
        Me.txtContacto2 = New System.Windows.Forms.TextBox()
        Me.txtMail2 = New System.Windows.Forms.TextBox()
        Me.txtTelefono2 = New System.Windows.Forms.TextBox()
        Me.gcContacto1 = New System.Windows.Forms.GroupBox()
        Me.txtContacto1 = New System.Windows.Forms.TextBox()
        Me.Label38 = New System.Windows.Forms.Label()
        Me.txtTelefono1 = New System.Windows.Forms.TextBox()
        Me.txtMail1 = New System.Windows.Forms.TextBox()
        Me.Label39 = New System.Windows.Forms.Label()
        Me.Label40 = New System.Windows.Forms.Label()
        CType(Me.dgvHistorial, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gcInfoGral, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gcInfoGral.SuspendLayout()
        CType(Me.gcDomicilio, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gcDomicilio.SuspendLayout()
        CType(Me.gcBuscar, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gcBuscar.SuspendLayout()
        CType(Me.gcHistorial, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gcHistorial.SuspendLayout()
        CType(Me.gcContacto, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gcContacto.SuspendLayout()
        Me.gcContacto3.SuspendLayout()
        Me.gcContacto2.SuspendLayout()
        Me.gcContacto1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.Font = New System.Drawing.Font("Cambria", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Label2.Location = New System.Drawing.Point(79, 27)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(84, 19)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Dirección:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.Label3.Font = New System.Drawing.Font("Cambria", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Label3.Location = New System.Drawing.Point(622, 27)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(66, 19)
        Me.Label3.TabIndex = 3
        Me.Label3.Text = "Ciudad:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.BackColor = System.Drawing.Color.Transparent
        Me.Label4.Font = New System.Drawing.Font("Cambria", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Label4.Location = New System.Drawing.Point(966, 27)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(32, 19)
        Me.Label4.TabIndex = 4
        Me.Label4.Text = "CP:"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Cambria", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Label1.Location = New System.Drawing.Point(92, 27)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(71, 19)
        Me.Label1.TabIndex = 7
        Me.Label1.Text = "Nombre:"
        '
        'lblTitulo
        '
        Me.lblTitulo.AccessibleRole = System.Windows.Forms.AccessibleRole.TitleBar
        Me.lblTitulo.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.lblTitulo.AutoSize = True
        Me.lblTitulo.BackColor = System.Drawing.Color.Transparent
        Me.lblTitulo.Font = New System.Drawing.Font("Microsoft Sans Serif", 24.0!)
        Me.lblTitulo.ForeColor = System.Drawing.Color.White
        Me.lblTitulo.Location = New System.Drawing.Point(385, 4)
        Me.lblTitulo.Name = "lblTitulo"
        Me.lblTitulo.Size = New System.Drawing.Size(384, 37)
        Me.lblTitulo.TabIndex = 11
        Me.lblTitulo.Text = "Administrador de Clientes"
        '
        'cbxBuscar
        '
        Me.cbxBuscar.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.cbxBuscar.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cbxBuscar.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.cbxBuscar.Font = New System.Drawing.Font("Calibri", 12.0!)
        Me.cbxBuscar.FormattingEnabled = True
        Me.cbxBuscar.IntegralHeight = False
        Me.cbxBuscar.Location = New System.Drawing.Point(260, 5)
        Me.cbxBuscar.Name = "cbxBuscar"
        Me.cbxBuscar.Size = New System.Drawing.Size(915, 27)
        Me.cbxBuscar.TabIndex = 13
        '
        'txtDireccion
        '
        Me.txtDireccion.Font = New System.Drawing.Font("Calibri", 12.0!)
        Me.txtDireccion.Location = New System.Drawing.Point(169, 24)
        Me.txtDireccion.Name = "txtDireccion"
        Me.txtDireccion.Size = New System.Drawing.Size(449, 27)
        Me.txtDireccion.TabIndex = 14
        '
        'txtCP
        '
        Me.txtCP.Font = New System.Drawing.Font("Calibri", 12.0!)
        Me.txtCP.Location = New System.Drawing.Point(1004, 24)
        Me.txtCP.Name = "txtCP"
        Me.txtCP.Size = New System.Drawing.Size(80, 27)
        Me.txtCP.TabIndex = 16
        Me.txtCP.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.BackColor = System.Drawing.Color.Transparent
        Me.Label12.Font = New System.Drawing.Font("Cambria", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Label12.Location = New System.Drawing.Point(962, 27)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(29, 19)
        Me.Label12.TabIndex = 25
        Me.Label12.Text = "Id:"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.BackColor = System.Drawing.Color.Transparent
        Me.Label14.Font = New System.Drawing.Font("Cambria", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Label14.ForeColor = System.Drawing.Color.Black
        Me.Label14.Location = New System.Drawing.Point(188, 8)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(64, 19)
        Me.Label14.TabIndex = 28
        Me.Label14.Text = "Buscar:"
        '
        'txtNombre
        '
        Me.txtNombre.Font = New System.Drawing.Font("Calibri", 12.0!)
        Me.txtNombre.Location = New System.Drawing.Point(169, 24)
        Me.txtNombre.Name = "txtNombre"
        Me.txtNombre.Size = New System.Drawing.Size(513, 27)
        Me.txtNombre.TabIndex = 29
        '
        'dgvHistorial
        '
        Me.dgvHistorial.AllowUserToAddRows = False
        Me.dgvHistorial.AllowUserToDeleteRows = False
        Me.dgvHistorial.AllowUserToResizeColumns = False
        Me.dgvHistorial.AllowUserToResizeRows = False
        Me.dgvHistorial.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvHistorial.Location = New System.Drawing.Point(95, 27)
        Me.dgvHistorial.Margin = New System.Windows.Forms.Padding(3, 6, 3, 6)
        Me.dgvHistorial.MultiSelect = False
        Me.dgvHistorial.Name = "dgvHistorial"
        Me.dgvHistorial.ReadOnly = True
        Me.dgvHistorial.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvHistorial.RowTemplate.ReadOnly = True
        Me.dgvHistorial.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvHistorial.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvHistorial.Size = New System.Drawing.Size(903, 190)
        Me.dgvHistorial.TabIndex = 38
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.BackColor = System.Drawing.Color.Transparent
        Me.Label15.Font = New System.Drawing.Font("Cambria", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Label15.Location = New System.Drawing.Point(688, 27)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(145, 19)
        Me.Label15.TabIndex = 40
        Me.Label15.Text = "Fecha Nacimiento:"
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.BackColor = System.Drawing.Color.Transparent
        Me.Label19.Font = New System.Drawing.Font("Cambria", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Label19.Location = New System.Drawing.Point(55, 60)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(108, 19)
        Me.Label19.TabIndex = 42
        Me.Label19.Text = "Razón Social:"
        '
        'txtEmpresa
        '
        Me.txtEmpresa.Font = New System.Drawing.Font("Calibri", 12.0!)
        Me.txtEmpresa.Location = New System.Drawing.Point(169, 57)
        Me.txtEmpresa.Name = "txtEmpresa"
        Me.txtEmpresa.Size = New System.Drawing.Size(684, 27)
        Me.txtEmpresa.TabIndex = 43
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.BackColor = System.Drawing.Color.Transparent
        Me.Label20.Font = New System.Drawing.Font("Cambria", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Label20.Location = New System.Drawing.Point(859, 60)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(49, 19)
        Me.Label20.TabIndex = 44
        Me.Label20.Text = "CUIT:"
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.BackColor = System.Drawing.Color.Transparent
        Me.Label21.Font = New System.Drawing.Font("Cambria", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Label21.Location = New System.Drawing.Point(865, 93)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(41, 19)
        Me.Label21.TabIndex = 45
        Me.Label21.Text = "DNI:"
        '
        'txtCUIT
        '
        Me.txtCUIT.Font = New System.Drawing.Font("Calibri", 12.0!)
        Me.txtCUIT.Location = New System.Drawing.Point(912, 57)
        Me.txtCUIT.Name = "txtCUIT"
        Me.txtCUIT.Size = New System.Drawing.Size(172, 27)
        Me.txtCUIT.TabIndex = 46
        Me.txtCUIT.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtDNI
        '
        Me.txtDNI.Font = New System.Drawing.Font("Calibri", 12.0!)
        Me.txtDNI.Location = New System.Drawing.Point(912, 90)
        Me.txtDNI.Name = "txtDNI"
        Me.txtDNI.Size = New System.Drawing.Size(172, 27)
        Me.txtDNI.TabIndex = 47
        Me.txtDNI.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'gcInfoGral
        '
        Me.gcInfoGral.Controls.Add(Me.dtpFechaNac)
        Me.gcInfoGral.Controls.Add(Me.txtWeb)
        Me.gcInfoGral.Controls.Add(Me.Label5)
        Me.gcInfoGral.Controls.Add(Me.Label20)
        Me.gcInfoGral.Controls.Add(Me.txtDNI)
        Me.gcInfoGral.Controls.Add(Me.txtCUIT)
        Me.gcInfoGral.Controls.Add(Me.Label21)
        Me.gcInfoGral.Controls.Add(Me.Label1)
        Me.gcInfoGral.Controls.Add(Me.txtNombre)
        Me.gcInfoGral.Controls.Add(Me.txtEmpresa)
        Me.gcInfoGral.Controls.Add(Me.Label19)
        Me.gcInfoGral.Controls.Add(Me.Label12)
        Me.gcInfoGral.Controls.Add(Me.txtId)
        Me.gcInfoGral.Controls.Add(Me.Label15)
        Me.gcInfoGral.Location = New System.Drawing.Point(12, 88)
        Me.gcInfoGral.LookAndFeel.SkinName = "Visual Studio 2013 Dark"
        Me.gcInfoGral.LookAndFeel.UseDefaultLookAndFeel = False
        Me.gcInfoGral.Name = "gcInfoGral"
        Me.gcInfoGral.Size = New System.Drawing.Size(1090, 122)
        Me.gcInfoGral.TabIndex = 48
        Me.gcInfoGral.Text = "Información General"
        '
        'dtpFechaNac
        '
        Me.dtpFechaNac.CustomFormat = "dd/MM/yyyy"
        Me.dtpFechaNac.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpFechaNac.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpFechaNac.Location = New System.Drawing.Point(839, 24)
        Me.dtpFechaNac.MaxDate = New Date(2050, 12, 31, 0, 0, 0, 0)
        Me.dtpFechaNac.MinDate = New Date(1900, 1, 1, 0, 0, 0, 0)
        Me.dtpFechaNac.Name = "dtpFechaNac"
        Me.dtpFechaNac.Size = New System.Drawing.Size(117, 26)
        Me.dtpFechaNac.TabIndex = 59
        '
        'txtWeb
        '
        Me.txtWeb.Font = New System.Drawing.Font("Calibri", 12.0!)
        Me.txtWeb.Location = New System.Drawing.Point(169, 90)
        Me.txtWeb.Name = "txtWeb"
        Me.txtWeb.Size = New System.Drawing.Size(684, 27)
        Me.txtWeb.TabIndex = 49
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.BackColor = System.Drawing.Color.Transparent
        Me.Label5.Font = New System.Drawing.Font("Cambria", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Label5.Location = New System.Drawing.Point(64, 93)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(99, 19)
        Me.Label5.TabIndex = 48
        Me.Label5.Text = "Página Web:"
        '
        'txtId
        '
        Me.txtId.Font = New System.Drawing.Font("Calibri", 12.0!)
        Me.txtId.Location = New System.Drawing.Point(997, 24)
        Me.txtId.Name = "txtId"
        Me.txtId.Size = New System.Drawing.Size(87, 27)
        Me.txtId.TabIndex = 26
        Me.txtId.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'cmbCiudad
        '
        Me.cmbCiudad.AllowDrop = True
        Me.cmbCiudad.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cmbCiudad.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cmbCiudad.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbCiudad.Font = New System.Drawing.Font("Calibri", 12.0!)
        Me.cmbCiudad.IntegralHeight = False
        Me.cmbCiudad.Location = New System.Drawing.Point(694, 24)
        Me.cmbCiudad.Name = "cmbCiudad"
        Me.cmbCiudad.Size = New System.Drawing.Size(266, 27)
        Me.cmbCiudad.TabIndex = 59
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.BackColor = System.Drawing.Color.Transparent
        Me.Label23.Font = New System.Drawing.Font("Cambria", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Label23.Location = New System.Drawing.Point(30, 88)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(54, 19)
        Me.Label23.TabIndex = 8
        Me.Label23.Text = "Movil:"
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.BackColor = System.Drawing.Color.Transparent
        Me.Label24.Font = New System.Drawing.Font("Cambria", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Label24.Location = New System.Drawing.Point(30, 88)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(54, 19)
        Me.Label24.TabIndex = 35
        Me.Label24.Text = "Movil:"
        '
        'txtMovil3
        '
        Me.txtMovil3.Font = New System.Drawing.Font("Calibri", 12.0!)
        Me.txtMovil3.Location = New System.Drawing.Point(90, 85)
        Me.txtMovil3.Name = "txtMovil3"
        Me.txtMovil3.Size = New System.Drawing.Size(260, 27)
        Me.txtMovil3.TabIndex = 36
        '
        'txtMovil1
        '
        Me.txtMovil1.Font = New System.Drawing.Font("Calibri", 12.0!)
        Me.txtMovil1.Location = New System.Drawing.Point(90, 85)
        Me.txtMovil1.Name = "txtMovil1"
        Me.txtMovil1.Size = New System.Drawing.Size(260, 27)
        Me.txtMovil1.TabIndex = 17
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.BackColor = System.Drawing.Color.Transparent
        Me.Label25.Font = New System.Drawing.Font("Cambria", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Label25.Location = New System.Drawing.Point(30, 88)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(54, 19)
        Me.Label25.TabIndex = 6
        Me.Label25.Text = "Movil:"
        '
        'txtMovil2
        '
        Me.txtMovil2.Font = New System.Drawing.Font("Calibri", 12.0!)
        Me.txtMovil2.Location = New System.Drawing.Point(90, 85)
        Me.txtMovil2.Name = "txtMovil2"
        Me.txtMovil2.Size = New System.Drawing.Size(260, 27)
        Me.txtMovil2.TabIndex = 32
        '
        'gcDomicilio
        '
        Me.gcDomicilio.Controls.Add(Me.cmbCiudad)
        Me.gcDomicilio.Controls.Add(Me.Label3)
        Me.gcDomicilio.Controls.Add(Me.Label2)
        Me.gcDomicilio.Controls.Add(Me.txtDireccion)
        Me.gcDomicilio.Controls.Add(Me.Label4)
        Me.gcDomicilio.Controls.Add(Me.txtCP)
        Me.gcDomicilio.Location = New System.Drawing.Point(12, 216)
        Me.gcDomicilio.LookAndFeel.SkinName = "Visual Studio 2013 Dark"
        Me.gcDomicilio.LookAndFeel.UseDefaultLookAndFeel = False
        Me.gcDomicilio.Name = "gcDomicilio"
        Me.gcDomicilio.Size = New System.Drawing.Size(1090, 56)
        Me.gcDomicilio.TabIndex = 51
        Me.gcDomicilio.Text = "Domicilio"
        '
        'gcBuscar
        '
        Me.gcBuscar.Controls.Add(Me.btnGuardar)
        Me.gcBuscar.Controls.Add(Me.Label8)
        Me.gcBuscar.Controls.Add(Me.btnNuevo)
        Me.gcBuscar.Controls.Add(Me.cmbBuscar)
        Me.gcBuscar.Location = New System.Drawing.Point(12, 45)
        Me.gcBuscar.LookAndFeel.SkinName = "Visual Studio 2013 Dark"
        Me.gcBuscar.LookAndFeel.UseDefaultLookAndFeel = False
        Me.gcBuscar.Name = "gcBuscar"
        Me.gcBuscar.Size = New System.Drawing.Size(1090, 37)
        Me.gcBuscar.TabIndex = 52
        '
        'btnGuardar
        '
        Me.btnGuardar.Appearance.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold)
        Me.btnGuardar.Appearance.Options.UseFont = True
        Me.btnGuardar.Cursor = System.Windows.Forms.Cursors.Default
        Me.btnGuardar.Location = New System.Drawing.Point(1009, 5)
        Me.btnGuardar.LookAndFeel.SkinName = "Visual Studio 2013 Dark"
        Me.btnGuardar.LookAndFeel.UseDefaultLookAndFeel = False
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(75, 27)
        Me.btnGuardar.TabIndex = 58
        Me.btnGuardar.Text = "Guardar"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.BackColor = System.Drawing.Color.Transparent
        Me.Label8.Font = New System.Drawing.Font("Cambria", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Label8.Location = New System.Drawing.Point(92, 8)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(71, 19)
        Me.Label8.TabIndex = 54
        Me.Label8.Text = "Nombre:"
        '
        'btnNuevo
        '
        Me.btnNuevo.Appearance.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold)
        Me.btnNuevo.Appearance.Options.UseFont = True
        Me.btnNuevo.Location = New System.Drawing.Point(929, 5)
        Me.btnNuevo.LookAndFeel.SkinName = "Visual Studio 2013 Dark"
        Me.btnNuevo.LookAndFeel.UseDefaultLookAndFeel = False
        Me.btnNuevo.Name = "btnNuevo"
        Me.btnNuevo.Size = New System.Drawing.Size(75, 27)
        Me.btnNuevo.TabIndex = 57
        Me.btnNuevo.Text = "Nuevo"
        '
        'cmbBuscar
        '
        Me.cmbBuscar.AllowDrop = True
        Me.cmbBuscar.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cmbBuscar.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cmbBuscar.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbBuscar.Font = New System.Drawing.Font("Calibri", 12.0!)
        Me.cmbBuscar.IntegralHeight = False
        Me.cmbBuscar.Location = New System.Drawing.Point(169, 5)
        Me.cmbBuscar.Name = "cmbBuscar"
        Me.cmbBuscar.Size = New System.Drawing.Size(755, 27)
        Me.cmbBuscar.TabIndex = 53
        '
        'btnCancelar
        '
        Me.btnCancelar.Appearance.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold)
        Me.btnCancelar.Appearance.Options.UseFont = True
        Me.btnCancelar.Cursor = System.Windows.Forms.Cursors.Default
        Me.btnCancelar.Location = New System.Drawing.Point(1027, 608)
        Me.btnCancelar.LookAndFeel.SkinName = "Visual Studio 2013 Dark"
        Me.btnCancelar.LookAndFeel.UseDefaultLookAndFeel = False
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 27)
        Me.btnCancelar.TabIndex = 56
        Me.btnCancelar.Text = "Cancelar"
        '
        'btnSalir
        '
        Me.btnSalir.Appearance.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold)
        Me.btnSalir.Appearance.Options.UseFont = True
        Me.btnSalir.Location = New System.Drawing.Point(1027, 641)
        Me.btnSalir.LookAndFeel.SkinName = "Visual Studio 2013 Dark"
        Me.btnSalir.LookAndFeel.UseDefaultLookAndFeel = False
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(75, 27)
        Me.btnSalir.TabIndex = 57
        Me.btnSalir.Text = "Cerrar"
        '
        'btnEliminar
        '
        Me.btnEliminar.Appearance.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold)
        Me.btnEliminar.Appearance.Options.UseFont = True
        Me.btnEliminar.Location = New System.Drawing.Point(1027, 575)
        Me.btnEliminar.LookAndFeel.SkinName = "Visual Studio 2013 Dark"
        Me.btnEliminar.LookAndFeel.UseDefaultLookAndFeel = False
        Me.btnEliminar.Name = "btnEliminar"
        Me.btnEliminar.Size = New System.Drawing.Size(75, 27)
        Me.btnEliminar.TabIndex = 58
        Me.btnEliminar.Text = "Eliminar"
        '
        'gcHistorial
        '
        Me.gcHistorial.Controls.Add(Me.dgvHistorial)
        Me.gcHistorial.Location = New System.Drawing.Point(12, 464)
        Me.gcHistorial.LookAndFeel.SkinName = "Visual Studio 2013 Dark"
        Me.gcHistorial.LookAndFeel.UseDefaultLookAndFeel = False
        Me.gcHistorial.Name = "gcHistorial"
        Me.gcHistorial.Size = New System.Drawing.Size(1009, 225)
        Me.gcHistorial.TabIndex = 59
        Me.gcHistorial.Text = "Historial"
        '
        'btnImportar
        '
        Me.btnImportar.Appearance.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold)
        Me.btnImportar.Appearance.Options.UseFont = True
        Me.btnImportar.Cursor = System.Windows.Forms.Cursors.Default
        Me.btnImportar.Location = New System.Drawing.Point(1021, 12)
        Me.btnImportar.LookAndFeel.SkinName = "Visual Studio 2013 Dark"
        Me.btnImportar.LookAndFeel.UseDefaultLookAndFeel = False
        Me.btnImportar.Name = "btnImportar"
        Me.btnImportar.Size = New System.Drawing.Size(75, 27)
        Me.btnImportar.TabIndex = 60
        Me.btnImportar.Text = "Importar"
        Me.btnImportar.Visible = False
        '
        'gcContacto
        '
        Me.gcContacto.Controls.Add(Me.gcContacto3)
        Me.gcContacto.Controls.Add(Me.gcContacto2)
        Me.gcContacto.Controls.Add(Me.gcContacto1)
        Me.gcContacto.Location = New System.Drawing.Point(12, 278)
        Me.gcContacto.LookAndFeel.SkinName = "Visual Studio 2013 Dark"
        Me.gcContacto.LookAndFeel.UseDefaultLookAndFeel = False
        Me.gcContacto.Name = "gcContacto"
        Me.gcContacto.Size = New System.Drawing.Size(1090, 180)
        Me.gcContacto.TabIndex = 69
        Me.gcContacto.Text = "Contacto"
        '
        'gcContacto3
        '
        Me.gcContacto3.Controls.Add(Me.Label24)
        Me.gcContacto3.Controls.Add(Me.Label32)
        Me.gcContacto3.Controls.Add(Me.txtMovil3)
        Me.gcContacto3.Controls.Add(Me.Label33)
        Me.gcContacto3.Controls.Add(Me.Label34)
        Me.gcContacto3.Controls.Add(Me.txtMail3)
        Me.gcContacto3.Controls.Add(Me.txtContacto3)
        Me.gcContacto3.Controls.Add(Me.txtTelefono3)
        Me.gcContacto3.ForeColor = System.Drawing.Color.White
        Me.gcContacto3.Location = New System.Drawing.Point(730, 24)
        Me.gcContacto3.Name = "gcContacto3"
        Me.gcContacto3.Size = New System.Drawing.Size(355, 151)
        Me.gcContacto3.TabIndex = 38
        Me.gcContacto3.TabStop = False
        Me.gcContacto3.Text = "Contacto 3"
        '
        'Label32
        '
        Me.Label32.AutoSize = True
        Me.Label32.BackColor = System.Drawing.Color.Transparent
        Me.Label32.Font = New System.Drawing.Font("Cambria", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Label32.Location = New System.Drawing.Point(38, 121)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(46, 19)
        Me.Label32.TabIndex = 37
        Me.Label32.Text = "Mail:"
        '
        'Label33
        '
        Me.Label33.AutoSize = True
        Me.Label33.BackColor = System.Drawing.Color.Transparent
        Me.Label33.Font = New System.Drawing.Font("Cambria", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Label33.Location = New System.Drawing.Point(13, 23)
        Me.Label33.Name = "Label33"
        Me.Label33.Size = New System.Drawing.Size(71, 19)
        Me.Label33.TabIndex = 18
        Me.Label33.Text = "Nombre:"
        '
        'Label34
        '
        Me.Label34.AutoSize = True
        Me.Label34.BackColor = System.Drawing.Color.Transparent
        Me.Label34.Font = New System.Drawing.Font("Cambria", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Label34.Location = New System.Drawing.Point(8, 55)
        Me.Label34.Name = "Label34"
        Me.Label34.Size = New System.Drawing.Size(76, 19)
        Me.Label34.TabIndex = 19
        Me.Label34.Text = "Telefono:"
        '
        'txtMail3
        '
        Me.txtMail3.Font = New System.Drawing.Font("Calibri", 12.0!)
        Me.txtMail3.Location = New System.Drawing.Point(90, 118)
        Me.txtMail3.Name = "txtMail3"
        Me.txtMail3.Size = New System.Drawing.Size(260, 27)
        Me.txtMail3.TabIndex = 36
        '
        'txtContacto3
        '
        Me.txtContacto3.Font = New System.Drawing.Font("Calibri", 12.0!)
        Me.txtContacto3.Location = New System.Drawing.Point(90, 19)
        Me.txtContacto3.Name = "txtContacto3"
        Me.txtContacto3.Size = New System.Drawing.Size(260, 27)
        Me.txtContacto3.TabIndex = 17
        '
        'txtTelefono3
        '
        Me.txtTelefono3.Font = New System.Drawing.Font("Calibri", 12.0!)
        Me.txtTelefono3.Location = New System.Drawing.Point(90, 52)
        Me.txtTelefono3.Name = "txtTelefono3"
        Me.txtTelefono3.Size = New System.Drawing.Size(260, 27)
        Me.txtTelefono3.TabIndex = 17
        '
        'gcContacto2
        '
        Me.gcContacto2.Controls.Add(Me.Label23)
        Me.gcContacto2.Controls.Add(Me.Label35)
        Me.gcContacto2.Controls.Add(Me.Label36)
        Me.gcContacto2.Controls.Add(Me.Label37)
        Me.gcContacto2.Controls.Add(Me.txtMovil2)
        Me.gcContacto2.Controls.Add(Me.txtContacto2)
        Me.gcContacto2.Controls.Add(Me.txtMail2)
        Me.gcContacto2.Controls.Add(Me.txtTelefono2)
        Me.gcContacto2.ForeColor = System.Drawing.Color.White
        Me.gcContacto2.Location = New System.Drawing.Point(366, 24)
        Me.gcContacto2.Name = "gcContacto2"
        Me.gcContacto2.Size = New System.Drawing.Size(355, 151)
        Me.gcContacto2.TabIndex = 38
        Me.gcContacto2.TabStop = False
        Me.gcContacto2.Text = "Contacto 2"
        '
        'Label35
        '
        Me.Label35.AutoSize = True
        Me.Label35.BackColor = System.Drawing.Color.Transparent
        Me.Label35.Font = New System.Drawing.Font("Cambria", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Label35.Location = New System.Drawing.Point(38, 121)
        Me.Label35.Name = "Label35"
        Me.Label35.Size = New System.Drawing.Size(46, 19)
        Me.Label35.TabIndex = 33
        Me.Label35.Text = "Mail:"
        '
        'Label36
        '
        Me.Label36.AutoSize = True
        Me.Label36.BackColor = System.Drawing.Color.Transparent
        Me.Label36.Font = New System.Drawing.Font("Cambria", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Label36.Location = New System.Drawing.Point(13, 23)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(71, 19)
        Me.Label36.TabIndex = 18
        Me.Label36.Text = "Nombre:"
        '
        'Label37
        '
        Me.Label37.AutoSize = True
        Me.Label37.BackColor = System.Drawing.Color.Transparent
        Me.Label37.Font = New System.Drawing.Font("Cambria", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Label37.Location = New System.Drawing.Point(8, 55)
        Me.Label37.Name = "Label37"
        Me.Label37.Size = New System.Drawing.Size(76, 19)
        Me.Label37.TabIndex = 19
        Me.Label37.Text = "Telefono:"
        '
        'txtContacto2
        '
        Me.txtContacto2.Font = New System.Drawing.Font("Calibri", 12.0!)
        Me.txtContacto2.Location = New System.Drawing.Point(90, 19)
        Me.txtContacto2.Name = "txtContacto2"
        Me.txtContacto2.Size = New System.Drawing.Size(260, 27)
        Me.txtContacto2.TabIndex = 17
        '
        'txtMail2
        '
        Me.txtMail2.Font = New System.Drawing.Font("Calibri", 12.0!)
        Me.txtMail2.Location = New System.Drawing.Point(90, 118)
        Me.txtMail2.Name = "txtMail2"
        Me.txtMail2.Size = New System.Drawing.Size(260, 27)
        Me.txtMail2.TabIndex = 32
        '
        'txtTelefono2
        '
        Me.txtTelefono2.Font = New System.Drawing.Font("Calibri", 12.0!)
        Me.txtTelefono2.Location = New System.Drawing.Point(90, 52)
        Me.txtTelefono2.Name = "txtTelefono2"
        Me.txtTelefono2.Size = New System.Drawing.Size(260, 27)
        Me.txtTelefono2.TabIndex = 17
        '
        'gcContacto1
        '
        Me.gcContacto1.Controls.Add(Me.txtContacto1)
        Me.gcContacto1.Controls.Add(Me.Label38)
        Me.gcContacto1.Controls.Add(Me.txtTelefono1)
        Me.gcContacto1.Controls.Add(Me.Label25)
        Me.gcContacto1.Controls.Add(Me.txtMovil1)
        Me.gcContacto1.Controls.Add(Me.txtMail1)
        Me.gcContacto1.Controls.Add(Me.Label39)
        Me.gcContacto1.Controls.Add(Me.Label40)
        Me.gcContacto1.ForeColor = System.Drawing.Color.White
        Me.gcContacto1.Location = New System.Drawing.Point(5, 24)
        Me.gcContacto1.Name = "gcContacto1"
        Me.gcContacto1.Size = New System.Drawing.Size(355, 151)
        Me.gcContacto1.TabIndex = 37
        Me.gcContacto1.TabStop = False
        Me.gcContacto1.Text = "Contacto 1"
        '
        'txtContacto1
        '
        Me.txtContacto1.Font = New System.Drawing.Font("Calibri", 12.0!)
        Me.txtContacto1.Location = New System.Drawing.Point(90, 19)
        Me.txtContacto1.Name = "txtContacto1"
        Me.txtContacto1.Size = New System.Drawing.Size(260, 27)
        Me.txtContacto1.TabIndex = 17
        '
        'Label38
        '
        Me.Label38.AutoSize = True
        Me.Label38.BackColor = System.Drawing.Color.Transparent
        Me.Label38.Font = New System.Drawing.Font("Cambria", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Label38.Location = New System.Drawing.Point(13, 23)
        Me.Label38.Name = "Label38"
        Me.Label38.Size = New System.Drawing.Size(71, 19)
        Me.Label38.TabIndex = 6
        Me.Label38.Text = "Nombre:"
        '
        'txtTelefono1
        '
        Me.txtTelefono1.Font = New System.Drawing.Font("Calibri", 12.0!)
        Me.txtTelefono1.Location = New System.Drawing.Point(90, 52)
        Me.txtTelefono1.Name = "txtTelefono1"
        Me.txtTelefono1.Size = New System.Drawing.Size(260, 27)
        Me.txtTelefono1.TabIndex = 17
        '
        'txtMail1
        '
        Me.txtMail1.Font = New System.Drawing.Font("Calibri", 12.0!)
        Me.txtMail1.Location = New System.Drawing.Point(90, 118)
        Me.txtMail1.Name = "txtMail1"
        Me.txtMail1.Size = New System.Drawing.Size(260, 27)
        Me.txtMail1.TabIndex = 17
        '
        'Label39
        '
        Me.Label39.AutoSize = True
        Me.Label39.BackColor = System.Drawing.Color.Transparent
        Me.Label39.Font = New System.Drawing.Font("Cambria", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Label39.Location = New System.Drawing.Point(8, 55)
        Me.Label39.Name = "Label39"
        Me.Label39.Size = New System.Drawing.Size(76, 19)
        Me.Label39.TabIndex = 6
        Me.Label39.Text = "Telefono:"
        '
        'Label40
        '
        Me.Label40.AutoSize = True
        Me.Label40.BackColor = System.Drawing.Color.Transparent
        Me.Label40.Font = New System.Drawing.Font("Cambria", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Label40.Location = New System.Drawing.Point(38, 121)
        Me.Label40.Name = "Label40"
        Me.Label40.Size = New System.Drawing.Size(46, 19)
        Me.Label40.TabIndex = 6
        Me.Label40.Text = "Mail:"
        '
        'frmClientes
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoSize = True
        Me.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.BackColor = System.Drawing.Color.Black
        Me.ClientSize = New System.Drawing.Size(1120, 701)
        Me.ControlBox = False
        Me.Controls.Add(Me.gcContacto)
        Me.Controls.Add(Me.btnImportar)
        Me.Controls.Add(Me.gcHistorial)
        Me.Controls.Add(Me.btnEliminar)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(Me.btnCancelar)
        Me.Controls.Add(Me.gcBuscar)
        Me.Controls.Add(Me.gcDomicilio)
        Me.Controls.Add(Me.gcInfoGral)
        Me.Controls.Add(Me.lblTitulo)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.MaximumSize = New System.Drawing.Size(1366, 701)
        Me.MinimumSize = New System.Drawing.Size(1120, 701)
        Me.Name = "frmClientes"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Tag = "Clientes"
        Me.Text = "Clientes"
        CType(Me.dgvHistorial, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gcInfoGral, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gcInfoGral.ResumeLayout(False)
        Me.gcInfoGral.PerformLayout()
        CType(Me.gcDomicilio, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gcDomicilio.ResumeLayout(False)
        Me.gcDomicilio.PerformLayout()
        CType(Me.gcBuscar, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gcBuscar.ResumeLayout(False)
        Me.gcBuscar.PerformLayout()
        CType(Me.gcHistorial, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gcHistorial.ResumeLayout(False)
        CType(Me.gcContacto, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gcContacto.ResumeLayout(False)
        Me.gcContacto3.ResumeLayout(False)
        Me.gcContacto3.PerformLayout()
        Me.gcContacto2.ResumeLayout(False)
        Me.gcContacto2.PerformLayout()
        Me.gcContacto1.ResumeLayout(False)
        Me.gcContacto1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents lblTitulo As System.Windows.Forms.Label
    Friend WithEvents cbxBuscar As System.Windows.Forms.ComboBox
    Friend WithEvents txtDireccion As System.Windows.Forms.TextBox
    Friend WithEvents txtCP As System.Windows.Forms.TextBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents txtNombre As System.Windows.Forms.TextBox
    Friend WithEvents dgvHistorial As System.Windows.Forms.DataGridView
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents txtEmpresa As System.Windows.Forms.TextBox
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents txtCUIT As System.Windows.Forms.TextBox
    Friend WithEvents txtDNI As System.Windows.Forms.TextBox
    Friend WithEvents gcInfoGral As DevExpress.XtraEditors.GroupControl
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents txtMovil3 As System.Windows.Forms.TextBox
    Friend WithEvents txtMovil1 As System.Windows.Forms.TextBox
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents txtMovil2 As System.Windows.Forms.TextBox
    Friend WithEvents gcDomicilio As DevExpress.XtraEditors.GroupControl
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents txtWeb As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtId As System.Windows.Forms.TextBox
    Friend WithEvents gcBuscar As DevExpress.XtraEditors.PanelControl
    Friend WithEvents btnGuardar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents btnNuevo As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmbBuscar As System.Windows.Forms.ComboBox
    Friend WithEvents btnCancelar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnSalir As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnEliminar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents dtpFechaNac As System.Windows.Forms.DateTimePicker
    Friend WithEvents cmbCiudad As System.Windows.Forms.ComboBox
    Friend WithEvents gcHistorial As DevExpress.XtraEditors.GroupControl
    Friend WithEvents btnImportar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents gcContacto As DevExpress.XtraEditors.GroupControl
    Friend WithEvents gcContacto3 As System.Windows.Forms.GroupBox
    Friend WithEvents Label32 As System.Windows.Forms.Label
    Friend WithEvents Label33 As System.Windows.Forms.Label
    Friend WithEvents Label34 As System.Windows.Forms.Label
    Friend WithEvents txtMail3 As System.Windows.Forms.TextBox
    Friend WithEvents txtContacto3 As System.Windows.Forms.TextBox
    Friend WithEvents txtTelefono3 As System.Windows.Forms.TextBox
    Friend WithEvents gcContacto2 As System.Windows.Forms.GroupBox
    Friend WithEvents Label35 As System.Windows.Forms.Label
    Friend WithEvents Label36 As System.Windows.Forms.Label
    Friend WithEvents Label37 As System.Windows.Forms.Label
    Friend WithEvents txtContacto2 As System.Windows.Forms.TextBox
    Friend WithEvents txtMail2 As System.Windows.Forms.TextBox
    Friend WithEvents txtTelefono2 As System.Windows.Forms.TextBox
    Friend WithEvents gcContacto1 As System.Windows.Forms.GroupBox
    Friend WithEvents txtContacto1 As System.Windows.Forms.TextBox
    Friend WithEvents Label38 As System.Windows.Forms.Label
    Friend WithEvents txtTelefono1 As System.Windows.Forms.TextBox
    Friend WithEvents txtMail1 As System.Windows.Forms.TextBox
    Friend WithEvents Label39 As System.Windows.Forms.Label
    Friend WithEvents Label40 As System.Windows.Forms.Label

End Class
