﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmDockBotones
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnClientes = New DevExpress.XtraEditors.SimpleButton()
        Me.btnProveedores = New DevExpress.XtraEditors.SimpleButton()
        Me.btnVisitas = New DevExpress.XtraEditors.SimpleButton()
        Me.btnEntregas = New DevExpress.XtraEditors.SimpleButton()
        Me.btnColocaciones = New DevExpress.XtraEditors.SimpleButton()
        Me.btnPresupuestos = New DevExpress.XtraEditors.SimpleButton()
        Me.btnProductos = New DevExpress.XtraEditors.SimpleButton()
        Me.btnSalir = New DevExpress.XtraEditors.SimpleButton()
        Me.btnConfiguracion = New DevExpress.XtraEditors.SimpleButton()
        Me.btnCobros = New DevExpress.XtraEditors.SimpleButton()
        Me.SuspendLayout()
        '
        'btnClientes
        '
        Me.btnClientes.Appearance.Font = New System.Drawing.Font("Cambria", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClientes.Appearance.Options.UseFont = True
        Me.btnClientes.Location = New System.Drawing.Point(12, 50)
        Me.btnClientes.LookAndFeel.SkinName = "Visual Studio 2013 Dark"
        Me.btnClientes.LookAndFeel.UseDefaultLookAndFeel = False
        Me.btnClientes.Name = "btnClientes"
        Me.btnClientes.Size = New System.Drawing.Size(126, 30)
        Me.btnClientes.TabIndex = 8
        Me.btnClientes.Text = "Clientes"
        '
        'btnProveedores
        '
        Me.btnProveedores.Appearance.Font = New System.Drawing.Font("Cambria", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnProveedores.Appearance.Options.UseFont = True
        Me.btnProveedores.Location = New System.Drawing.Point(12, 122)
        Me.btnProveedores.LookAndFeel.SkinName = "Visual Studio 2013 Dark"
        Me.btnProveedores.LookAndFeel.UseDefaultLookAndFeel = False
        Me.btnProveedores.Name = "btnProveedores"
        Me.btnProveedores.Size = New System.Drawing.Size(126, 30)
        Me.btnProveedores.TabIndex = 22
        Me.btnProveedores.Text = "Proveedores"
        '
        'btnVisitas
        '
        Me.btnVisitas.Appearance.Font = New System.Drawing.Font("Cambria", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisitas.Appearance.Options.UseFont = True
        Me.btnVisitas.Location = New System.Drawing.Point(12, 302)
        Me.btnVisitas.LookAndFeel.SkinName = "Visual Studio 2013 Dark"
        Me.btnVisitas.LookAndFeel.UseDefaultLookAndFeel = False
        Me.btnVisitas.Name = "btnVisitas"
        Me.btnVisitas.Size = New System.Drawing.Size(126, 30)
        Me.btnVisitas.TabIndex = 21
        Me.btnVisitas.Text = "Visitas"
        '
        'btnEntregas
        '
        Me.btnEntregas.Appearance.Font = New System.Drawing.Font("Cambria", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEntregas.Appearance.Options.UseFont = True
        Me.btnEntregas.Location = New System.Drawing.Point(12, 266)
        Me.btnEntregas.LookAndFeel.SkinName = "Visual Studio 2013 Dark"
        Me.btnEntregas.LookAndFeel.UseDefaultLookAndFeel = False
        Me.btnEntregas.Name = "btnEntregas"
        Me.btnEntregas.Size = New System.Drawing.Size(126, 30)
        Me.btnEntregas.TabIndex = 20
        Me.btnEntregas.Text = "Entregas"
        '
        'btnColocaciones
        '
        Me.btnColocaciones.Appearance.Font = New System.Drawing.Font("Cambria", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnColocaciones.Appearance.Options.UseFont = True
        Me.btnColocaciones.Location = New System.Drawing.Point(12, 230)
        Me.btnColocaciones.LookAndFeel.SkinName = "Visual Studio 2013 Dark"
        Me.btnColocaciones.LookAndFeel.UseDefaultLookAndFeel = False
        Me.btnColocaciones.Name = "btnColocaciones"
        Me.btnColocaciones.Size = New System.Drawing.Size(126, 30)
        Me.btnColocaciones.TabIndex = 19
        Me.btnColocaciones.Text = "Colocaciones"
        '
        'btnPresupuestos
        '
        Me.btnPresupuestos.Appearance.Font = New System.Drawing.Font("Cambria", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPresupuestos.Appearance.Options.UseFont = True
        Me.btnPresupuestos.Location = New System.Drawing.Point(12, 158)
        Me.btnPresupuestos.LookAndFeel.SkinName = "Visual Studio 2013 Dark"
        Me.btnPresupuestos.LookAndFeel.UseDefaultLookAndFeel = False
        Me.btnPresupuestos.Name = "btnPresupuestos"
        Me.btnPresupuestos.Size = New System.Drawing.Size(126, 30)
        Me.btnPresupuestos.TabIndex = 18
        Me.btnPresupuestos.Text = "Presupuestos"
        '
        'btnProductos
        '
        Me.btnProductos.Appearance.Font = New System.Drawing.Font("Cambria", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnProductos.Appearance.Options.UseFont = True
        Me.btnProductos.Location = New System.Drawing.Point(12, 86)
        Me.btnProductos.LookAndFeel.SkinName = "Visual Studio 2013 Dark"
        Me.btnProductos.LookAndFeel.UseDefaultLookAndFeel = False
        Me.btnProductos.Name = "btnProductos"
        Me.btnProductos.Size = New System.Drawing.Size(126, 30)
        Me.btnProductos.TabIndex = 17
        Me.btnProductos.Text = "Productos"
        '
        'btnSalir
        '
        Me.btnSalir.Appearance.Font = New System.Drawing.Font("Cambria", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSalir.Appearance.Options.UseFont = True
        Me.btnSalir.Location = New System.Drawing.Point(12, 659)
        Me.btnSalir.LookAndFeel.SkinName = "Visual Studio 2013 Dark"
        Me.btnSalir.LookAndFeel.UseDefaultLookAndFeel = False
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(126, 30)
        Me.btnSalir.TabIndex = 23
        Me.btnSalir.Text = "Salir"
        '
        'btnConfiguracion
        '
        Me.btnConfiguracion.Appearance.BackColor = System.Drawing.Color.Black
        Me.btnConfiguracion.Appearance.Font = New System.Drawing.Font("Cambria", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnConfiguracion.Appearance.Image = Global.UI.My.Resources.Resources.tool
        Me.btnConfiguracion.Appearance.Options.UseBackColor = True
        Me.btnConfiguracion.Appearance.Options.UseFont = True
        Me.btnConfiguracion.Appearance.Options.UseImage = True
        Me.btnConfiguracion.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat
        Me.btnConfiguracion.Image = Global.UI.My.Resources.Resources.tool
        Me.btnConfiguracion.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter
        Me.btnConfiguracion.Location = New System.Drawing.Point(106, 12)
        Me.btnConfiguracion.Name = "btnConfiguracion"
        Me.btnConfiguracion.Size = New System.Drawing.Size(32, 32)
        Me.btnConfiguracion.TabIndex = 24
        '
        'btnCobros
        '
        Me.btnCobros.Appearance.Font = New System.Drawing.Font("Cambria", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCobros.Appearance.Options.UseFont = True
        Me.btnCobros.Location = New System.Drawing.Point(12, 194)
        Me.btnCobros.LookAndFeel.SkinName = "Visual Studio 2013 Dark"
        Me.btnCobros.LookAndFeel.UseDefaultLookAndFeel = False
        Me.btnCobros.Name = "btnCobros"
        Me.btnCobros.Size = New System.Drawing.Size(126, 30)
        Me.btnCobros.TabIndex = 25
        Me.btnCobros.Text = "Cobranza"
        '
        'frmDockBotones
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Black
        Me.ClientSize = New System.Drawing.Size(150, 701)
        Me.Controls.Add(Me.btnCobros)
        Me.Controls.Add(Me.btnConfiguracion)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(Me.btnProveedores)
        Me.Controls.Add(Me.btnVisitas)
        Me.Controls.Add(Me.btnEntregas)
        Me.Controls.Add(Me.btnColocaciones)
        Me.Controls.Add(Me.btnPresupuestos)
        Me.Controls.Add(Me.btnProductos)
        Me.Controls.Add(Me.btnClientes)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.MinimumSize = New System.Drawing.Size(150, 701)
        Me.Name = "frmDockBotones"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "DockBotones"
        Me.TopMost = True
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnClientes As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnProveedores As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnVisitas As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnEntregas As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnColocaciones As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnPresupuestos As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnProductos As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnSalir As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnConfiguracion As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnCobros As DevExpress.XtraEditors.SimpleButton
End Class
