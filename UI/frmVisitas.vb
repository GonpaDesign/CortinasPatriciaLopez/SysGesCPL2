﻿Public Class frmVisitas
    Private UnGestorVisita As New Modelo.GestorVisita
    Private UnGestorClientes As New Modelo.GestorClientes
    Dim Operacion As Integer = 0

    '------ EVENTOS DE FORMULARIO ------
    Private Sub frmVisitas_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        ConfigurarForm()
        ConfigurarDataGridView()

    End Sub

    '------ CONFIGURACIONES ------
    Public Sub ConfigurarForm()
        Me.Dock = DockStyle.None
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.ShowIcon = False
        Me.MaximumSize = New Size(frmPrincipal.ClientSize.Width - 4, frmPrincipal.ClientSize.Height - 4)
        Me.MinimumSize = Me.MaximumSize
        Me.Dock = DockStyle.Fill
        Dim Separacion As Integer = (frmPrincipal.ClientSize.Width - 1090 + 150) / 2
        gcBuscar.Location = New Point(Separacion, 45)
        gcDatos.Location = New Point(Separacion, 88)
        gcFiltros.Location = New Point(Separacion + 913, 256)
        gcVisitas.Size = New Size(907, frmPrincipal.ClientSize.Height - (256 + 10))
        gcVisitas.Location = New Point(Separacion, 256)
        lblTitulo.Location = New Point((frmPrincipal.ClientSize.Width - 365 + 150) / 2, 4)
        Dim Separacionbtn As Integer = (frmPrincipal.ClientSize.Width - Separacion + 75)
        btnCancelar.Location = New Point(Separacionbtn, frmPrincipal.ClientSize.Height - 70)
        btnSalir.Location = New Point(Separacionbtn, frmPrincipal.ClientSize.Height - 37)
    End Sub
    Public Sub ConfigurarTextBox()
        'Configuro la cantidad de cararcteres maximos
        'txtNombre.MaxLength = 200
        'txtDireccion.MaxLength = 150
        'Configuro alineacion del texto
        'txtClienteId.TextAlign = HorizontalAlignment.Center
        'txtCiudad.TextAlign = HorizontalAlignment.Center
        'txtTelefono1.TextAlign = HorizontalAlignment.Center
        'txtMovil1.TextAlign = HorizontalAlignment.Center
        'txtMovil2.TextAlign = HorizontalAlignment.Center
        'Configuro los que seran siempre solo lectura
        'txtCiudad.ReadOnly = True
        'txtClienteId.ReadOnly = True
        'txtNombre.ReadOnly = True
        'txtTelefono1.ReadOnly = True
        'txtMovil1.ReadOnly = True
        'txtMovil2.ReadOnly = True
    End Sub
    Private Sub ConfigurarDataGridView()
        dgvVisitas.Location = New Point(129, 27)
        dgvVisitas.Size = New Size(770, gcVisitas.ClientSize.Height - 33)
    End Sub

    '------ EVENTOS DE BOTONES ------
    Private Sub btnSalir_Click(sender As System.Object, e As System.EventArgs) Handles btnSalir.Click
        Close()
        frmPrincipal.Main = 0
        frmPrincipal.Show()
    End Sub
    Private Sub btnNuevo_Click(sender As Object, e As EventArgs) Handles btnNuevo.Click
        Operacion = 0
        ModoABM()
        Borrartxt()
    End Sub
    Private Sub btnGuardar_Click(sender As Object, e As EventArgs) Handles btnGuardar.Click
        Dim UnaVisita As New Modelo.Visita
        UnaVisita.IdCliente = CInt(txtClienteId.Text)
        UnaVisita.Direccion = txtDireccion.Text()
        UnaVisita.IdCiudad = txtCiudad.Text
        UnaVisita.Observacion = txtObservaciones.Text
        UnaVisita.Fecha = CDate(dtpHora.Value)
        Select Case Operacion
            Case 0 ' ALTA
                UnGestorVisita.Nuevo(UnaVisita)
            Case 1
                ' MODIFICACION
                UnaVisita.IdVisita = CInt(dgvVisitas.CurrentRow.Cells(0).Value)
                UnGestorVisita.Modificar(UnaVisita)
        End Select
        ModoReadOnly()
    End Sub
    Private Sub btnActualizar_Click(sender As Object, e As EventArgs) Handles btnActualizar.Click
        Cargardgv()
    End Sub
    Private Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        Operacion = 0
        ModoReadOnly()
    End Sub

    '------ EVENTOS DE COMBOBOX ------
    Private Sub cmbBuscar_SelectionChangeCommitted(sender As Object, e As EventArgs) Handles cmbNombre.SelectionChangeCommitted
        Dim ClienteSeleccionado As New Modelo.Cliente
        ClienteSeleccionado = UnGestorClientes.BuscarPorId(cmbNombre.SelectedItem)
        CargarCliente(ClienteSeleccionado)
    End Sub

    '------ FUNCIONES PARA COMBOBOX ------
    Private Sub ActualizarCmbNombre()
        cmbNombre.DataSource = UnGestorClientes.ListarTodosNombre(frmPrincipal.Server)
        cmbNombre.DisplayMember = "Nombre"
    End Sub

    '------ FUNCIONES PARA TEXTBOX ------
    Private Sub CargarCliente(UnCliente As Modelo.Cliente)
        txtClienteId.Text = UnCliente.IdCliente
        cmbNombre.Text = UnCliente.Nombre
        txtDireccion.Text = UnCliente.Direccion
        txtCiudad.Text = UnCliente.IdCiudad
        txtTelefono1.Text = UnCliente.Telefono1
        txtMovil1.Text = UnCliente.Movil1
        txtMail.Text = UnCliente.Mail1
        btnGuardar.Enabled = True
        dtpFecha.Enabled = True
        dtpHora.Enabled = True
    End Sub
    Private Sub txtTextChanged(sender As Object, e As EventArgs) Handles txtDireccion.Click, txtCiudad.Click, txtTelefono1.Click, txtMovil1.Click, txtMail.Click, txtObservaciones.Click
        If Operacion = 0 Then
            Operacion = 1
            ModoABM()
        Else
            ModoABM()
        End If
    End Sub

    '------ FUNCIONES GENERALES -------
    Private Sub ModoABM()
        cmbNombre.Enabled = True

        txtDireccion.ReadOnly = False
        txtCiudad.ReadOnly = False
        txtObservaciones.ReadOnly = False
        txtTelefono1.ReadOnly = False
        txtMovil1.ReadOnly = False
        txtMail.ReadOnly = False

        dtpFecha.Enabled = True
        dtpHora.Enabled = True

        btnGuardar.Enabled = True
        btnCancelar.Show()
    End Sub
    Private Sub ModoReadOnly()
        cmbNombre.Enabled = False

        txtDireccion.ReadOnly = True
        txtCiudad.ReadOnly = True
        txtObservaciones.ReadOnly = True
        txtTelefono1.ReadOnly = True
        txtMovil1.ReadOnly = True
        txtMail.ReadOnly = True

        dtpFecha.Enabled = False
        dtpHora.Enabled = False

        btnGuardar.Enabled = False
        btnCancelar.Hide()
    End Sub
    Private Sub Borrartxt()
        txtDireccion.Text = ""
        txtCiudad.Text = ""
        txtObservaciones.Text = ""
        txtTelefono1.Text = ""
        txtMovil1.Text = ""
        txtMail.Text = ""
    End Sub

    '------ EVENTOS DE DATAGRIDVIEW ------
    Private Sub Cargardgv()
        Dim UnaVisita As New Modelo.Visita
        Dim OtraVisita As New Modelo.Visita
        If ckbEntreFechas.Checked = True Then
            UnaVisita.Fecha = dtpFechaI.Value.Date
            Dim Dias As Integer = 0
            Dias = DateDiff(DateInterval.Day, dtpFechaI.Value, dtpFechaF.Value)
            Dim unAccesoVisita As New Modelo.DbVisita
            dgvVisitas.DataSource = unAccesoVisita.SelectVisitaEntreFecha(UnaVisita, Dias + 1)
        Else
            UnaVisita.Fecha = dtpFechaI.Value.Date
            Dim unAccesoVisita As New Modelo.DbVisita
            dgvVisitas.DataSource = unAccesoVisita.SelectVisitaEntreFecha(UnaVisita, 1)
        End If
        Configdgv()
    End Sub
    Private Sub Configdgv()
        'dgvVisitas.Columns(2).Visible = False
        'dgvVisitas.Columns(2).Width = 35
        dgvVisitas.SelectionMode = DataGridViewSelectionMode.FullRowSelect
    End Sub
    Private Sub dgvVisitas_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvVisitas.CellDoubleClick
        Cursor = Cursors.WaitCursor
        Operacion = 1
        Dim UnCliente As New Modelo.Cliente
        UnCliente.IdCliente = CInt(dgvVisitas.CurrentRow.Cells(1).Value)
        Dim UnGestorCliente As New Modelo.GestorClientes
        Dim ClienteSeleccionado As New Modelo.Cliente
        ClienteSeleccionado = UnGestorCliente.BuscarPorId(UnCliente)
        CargarCliente(ClienteSeleccionado)
        dtpFecha.Value = dgvVisitas.CurrentRow.Cells(3).Value
        Cursor = Cursors.Default
    End Sub

    '------ EVENTOS DE DATETIMEPICKER ------
    Private Sub dtpFecha_ValueChanged(sender As Object, e As EventArgs) Handles dtpFecha.ValueChanged
        dtpHora.Value = dtpFecha.Value
    End Sub

    '------ EVENTOS PARA CHECKBOX ------
    Private Sub ckbEntreFechas_CheckedChanged(sender As Object, e As EventArgs) Handles ckbEntreFechas.CheckedChanged
        If ckbEntreFechas.Checked = True Then
            dtpFechaF.Enabled = True
        Else
            dtpFechaF.Enabled = False
        End If
    End Sub
End Class