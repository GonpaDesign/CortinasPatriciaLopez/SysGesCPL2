﻿Public Class frmDockBotones
    '------ EVENTOS DE FORMULARIO ------
    Private Sub frmDockBotones_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        ConfigurarForm()
        btnConfiguracion.Enabled = True
    End Sub

    '------ EVENTOS DE BOTONES ------
    Private Sub btnClientes_Click(sender As Object, e As EventArgs) Handles btnClientes.Click
        Cursor = System.Windows.Forms.Cursors.WaitCursor
        OcultarForms()
        frmClientes.MdiParent = frmPrincipal
        frmClientes.Show()
        frmClientes.SendToBack()
        Me.BringToFront()
        Cursor = System.Windows.Forms.Cursors.Default
    End Sub
    Private Sub btnProductos_Click(sender As Object, e As EventArgs) Handles btnProductos.Click
        Cursor = System.Windows.Forms.Cursors.WaitCursor
        OcultarForms()
        frmProducto.MdiParent = frmPrincipal
        frmProducto.Show()
        frmProducto.SendToBack()
        Me.BringToFront()
        Cursor = System.Windows.Forms.Cursors.Default
    End Sub
    Private Sub btnProveedores_Click(sender As Object, e As EventArgs) Handles btnProveedores.Click
        Cursor = System.Windows.Forms.Cursors.WaitCursor
        OcultarForms()
        frmProveedores.MdiParent = frmPrincipal
        frmProveedores.Show()
        frmProveedores.SendToBack()
        Me.BringToFront()
        Cursor = System.Windows.Forms.Cursors.Default
    End Sub
    Private Sub btnPresupuestos_Click(sender As Object, e As EventArgs) Handles btnPresupuestos.Click
        Cursor = System.Windows.Forms.Cursors.WaitCursor
        OcultarForms()
        frmPresupuesto.MdiParent = frmPrincipal
        frmPresupuesto.Show()
        frmPresupuesto.SendToBack()
        Me.BringToFront()
        Cursor = System.Windows.Forms.Cursors.Default
    End Sub
    Private Sub btnColocaciones_Click(sender As Object, e As EventArgs) Handles btnColocaciones.Click
        Cursor = System.Windows.Forms.Cursors.WaitCursor
        OcultarForms()
        frmColocaciones.MdiParent = frmPrincipal
        frmColocaciones.Show()
        frmColocaciones.SendToBack()
        Me.BringToFront()
        Cursor = System.Windows.Forms.Cursors.Default
    End Sub
    Private Sub btnEntregas_Click(sender As Object, e As EventArgs) Handles btnEntregas.Click
        Cursor = System.Windows.Forms.Cursors.WaitCursor
        OcultarForms()
        frmEntregas.MdiParent = frmPrincipal
        frmEntregas.Show()
        frmEntregas.SendToBack()
        Me.BringToFront()
        Cursor = System.Windows.Forms.Cursors.Default
    End Sub
    Private Sub btnVisitas_Click(sender As Object, e As EventArgs) Handles btnVisitas.Click
        Cursor = System.Windows.Forms.Cursors.WaitCursor
        OcultarForms()
        frmVisitas.MdiParent = frmPrincipal
        frmVisitas.Show()
        frmVisitas.SendToBack()
        Me.BringToFront()
        Cursor = System.Windows.Forms.Cursors.Default
    End Sub
    Private Sub btnSalir_Click(sender As Object, e As EventArgs) Handles btnSalir.Click
        frmPrincipal.Close()
    End Sub
    Private Sub btnConfiguracion_Click(sender As Object, e As EventArgs) Handles btnConfiguracion.Click
        Cursor = System.Windows.Forms.Cursors.WaitCursor
        OcultarForms()
        frmConfiguracion.MdiParent = frmPrincipal
        frmConfiguracion.Show()
        frmConfiguracion.SendToBack()
        Me.BringToFront()
        Cursor = System.Windows.Forms.Cursors.Default
    End Sub
    Private Sub btnCobros_Click(sender As Object, e As EventArgs) Handles btnCobros.Click
        Cursor = System.Windows.Forms.Cursors.WaitCursor
        OcultarForms()
        frmCobros.MdiParent = frmPrincipal
        frmCobros.Show()
        frmCobros.SendToBack()
        Me.BringToFront()
        Cursor = System.Windows.Forms.Cursors.Default
    End Sub

    '------ FUNCIONES PARA BOTONES ------
    Private Sub ActivarConfiguracion()
        btnClientes.Hide()
        btnColocaciones.Hide()
        btnEntregas.Hide()
        btnPresupuestos.Hide()
        btnProductos.Hide()
        btnProveedores.Hide()
        btnVisitas.Hide()
        btnSalir.Hide()
        btnConfiguracion.Hide()
    End Sub

    '------ CONFIGURACIONES ------
    Public Sub ConfigurarForm()
        Me.FormBorderStyle = Windows.Forms.FormBorderStyle.None
        Me.BackColor = Color.Black
        btnConfiguracion.BackColor = Color.Black
        btnSalir.Location = New Point(12, frmPrincipal.ClientSize.Height - 40)
    End Sub

    '------ FUNCIONES GENERALES ------
    Private Sub OcultarForms()
        Try
            frmClientes.Hide()
        Catch
        End Try
        Try
            frmColocaciones.Hide()
        Catch
        End Try
        Try
            frmConfiguracion.Hide()
        Catch
        End Try
        Try
            frmCortinas.Hide()
        Catch
        End Try
        Try
            frmEntregas.Hide()
        Catch
        End Try
        Try
            frmFundas.Hide()
        Catch
        End Try
        Try
            frmGenero.Hide()
        Catch
        End Try
        Try
            frmPresupuesto.Hide()
        Catch
        End Try
        Try
            frmProducto.Hide()
        Catch
        End Try
        Try
            frmProveedores.Hide()
        Catch
        End Try
        Try
            frmSillones.Hide()
        Catch
        End Try
        Try
            frmVisitas.Hide()
        Catch
        End Try
        Try
            frmCobros.Hide()
        Catch
        End Try
    End Sub
End Class