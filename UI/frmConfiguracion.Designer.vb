﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmConfiguracion
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnSalir = New DevExpress.XtraEditors.SimpleButton()
        Me.btnGuardar = New DevExpress.XtraEditors.SimpleButton()
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
        Me.btnGuardarCn = New DevExpress.XtraEditors.SimpleButton()
        Me.txtCadenaConex = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cmbServer = New System.Windows.Forms.ComboBox()
        Me.GroupControl2 = New DevExpress.XtraEditors.GroupControl()
        Me.btnGuardarLP = New DevExpress.XtraEditors.SimpleButton()
        Me.btnNuevoLP = New DevExpress.XtraEditors.SimpleButton()
        Me.dtpFechaLP = New System.Windows.Forms.DateTimePicker()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.cmbProvedorLP = New System.Windows.Forms.ComboBox()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl2.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnSalir
        '
        Me.btnSalir.Appearance.Font = New System.Drawing.Font("Cambria", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSalir.Appearance.Options.UseFont = True
        Me.btnSalir.Location = New System.Drawing.Point(1033, 666)
        Me.btnSalir.LookAndFeel.SkinName = "Visual Studio 2013 Dark"
        Me.btnSalir.LookAndFeel.UseDefaultLookAndFeel = False
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(75, 23)
        Me.btnSalir.TabIndex = 0
        Me.btnSalir.Text = "Cerrar"
        '
        'btnGuardar
        '
        Me.btnGuardar.Appearance.Font = New System.Drawing.Font("Cambria", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGuardar.Appearance.Options.UseFont = True
        Me.btnGuardar.Location = New System.Drawing.Point(1033, 637)
        Me.btnGuardar.LookAndFeel.SkinName = "Visual Studio 2013 Dark"
        Me.btnGuardar.LookAndFeel.UseDefaultLookAndFeel = False
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(75, 23)
        Me.btnGuardar.TabIndex = 1
        Me.btnGuardar.Text = "Guardar"
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.btnGuardarCn)
        Me.GroupControl1.Controls.Add(Me.txtCadenaConex)
        Me.GroupControl1.Controls.Add(Me.Label2)
        Me.GroupControl1.Controls.Add(Me.Label1)
        Me.GroupControl1.Controls.Add(Me.cmbServer)
        Me.GroupControl1.Location = New System.Drawing.Point(12, 12)
        Me.GroupControl1.LookAndFeel.SkinName = "Visual Studio 2013 Dark"
        Me.GroupControl1.LookAndFeel.UseDefaultLookAndFeel = False
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(1015, 50)
        Me.GroupControl1.TabIndex = 2
        Me.GroupControl1.Text = "Servidor SQL"
        '
        'btnGuardarCn
        '
        Me.btnGuardarCn.Appearance.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold)
        Me.btnGuardarCn.Appearance.Options.UseFont = True
        Me.btnGuardarCn.Location = New System.Drawing.Point(935, 18)
        Me.btnGuardarCn.LookAndFeel.SkinName = "Visual Studio 2013 Dark"
        Me.btnGuardarCn.LookAndFeel.UseDefaultLookAndFeel = False
        Me.btnGuardarCn.Name = "btnGuardarCn"
        Me.btnGuardarCn.Size = New System.Drawing.Size(75, 27)
        Me.btnGuardarCn.TabIndex = 84
        Me.btnGuardarCn.Text = "Guardar"
        '
        'txtCadenaConex
        '
        Me.txtCadenaConex.Font = New System.Drawing.Font("Calibri", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCadenaConex.Location = New System.Drawing.Point(581, 23)
        Me.txtCadenaConex.Name = "txtCadenaConex"
        Me.txtCadenaConex.ReadOnly = True
        Me.txtCadenaConex.Size = New System.Drawing.Size(348, 22)
        Me.txtCadenaConex.TabIndex = 3
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Cambria", 11.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(423, 24)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(152, 17)
        Me.Label2.TabIndex = 5
        Me.Label2.Text = "Cadena de Conexión:"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Cambria", 11.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(5, 24)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(157, 17)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "Nombre del Servidor:"
        '
        'cmbServer
        '
        Me.cmbServer.FormattingEnabled = True
        Me.cmbServer.Items.AddRange(New Object() {"GONZALO-PC", "GONZALO-NBOOK", "PATRICIA-LOCAL", "PATRICIA-PC", "PATRICIA-NBOOK"})
        Me.cmbServer.Location = New System.Drawing.Point(168, 24)
        Me.cmbServer.Name = "cmbServer"
        Me.cmbServer.Size = New System.Drawing.Size(249, 21)
        Me.cmbServer.TabIndex = 3
        '
        'GroupControl2
        '
        Me.GroupControl2.Controls.Add(Me.btnGuardarLP)
        Me.GroupControl2.Controls.Add(Me.btnNuevoLP)
        Me.GroupControl2.Controls.Add(Me.dtpFechaLP)
        Me.GroupControl2.Controls.Add(Me.Label3)
        Me.GroupControl2.Controls.Add(Me.Label4)
        Me.GroupControl2.Controls.Add(Me.cmbProvedorLP)
        Me.GroupControl2.Location = New System.Drawing.Point(12, 68)
        Me.GroupControl2.LookAndFeel.SkinName = "Visual Studio 2013 Dark"
        Me.GroupControl2.LookAndFeel.UseDefaultLookAndFeel = False
        Me.GroupControl2.Name = "GroupControl2"
        Me.GroupControl2.Size = New System.Drawing.Size(1015, 50)
        Me.GroupControl2.TabIndex = 3
        Me.GroupControl2.Text = "Listas de Precios"
        '
        'btnGuardarLP
        '
        Me.btnGuardarLP.Appearance.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold)
        Me.btnGuardarLP.Appearance.Options.UseFont = True
        Me.btnGuardarLP.Location = New System.Drawing.Point(935, 18)
        Me.btnGuardarLP.LookAndFeel.SkinName = "Visual Studio 2013 Dark"
        Me.btnGuardarLP.LookAndFeel.UseDefaultLookAndFeel = False
        Me.btnGuardarLP.Name = "btnGuardarLP"
        Me.btnGuardarLP.Size = New System.Drawing.Size(75, 27)
        Me.btnGuardarLP.TabIndex = 83
        Me.btnGuardarLP.Text = "Guardar"
        '
        'btnNuevoLP
        '
        Me.btnNuevoLP.Appearance.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold)
        Me.btnNuevoLP.Appearance.Options.UseFont = True
        Me.btnNuevoLP.Location = New System.Drawing.Point(854, 18)
        Me.btnNuevoLP.LookAndFeel.SkinName = "Visual Studio 2013 Dark"
        Me.btnNuevoLP.LookAndFeel.UseDefaultLookAndFeel = False
        Me.btnNuevoLP.Name = "btnNuevoLP"
        Me.btnNuevoLP.Size = New System.Drawing.Size(75, 27)
        Me.btnNuevoLP.TabIndex = 82
        Me.btnNuevoLP.Text = "Nuevo"
        '
        'dtpFechaLP
        '
        Me.dtpFechaLP.CalendarFont = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpFechaLP.Font = New System.Drawing.Font("Calibri", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpFechaLP.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFechaLP.Location = New System.Drawing.Point(597, 21)
        Me.dtpFechaLP.MaxDate = New Date(2030, 12, 31, 0, 0, 0, 0)
        Me.dtpFechaLP.MinDate = New Date(1900, 1, 1, 0, 0, 0, 0)
        Me.dtpFechaLP.Name = "dtpFechaLP"
        Me.dtpFechaLP.Size = New System.Drawing.Size(110, 24)
        Me.dtpFechaLP.TabIndex = 78
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Cambria", 11.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(423, 24)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(168, 17)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "Fecha de Actualización:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Cambria", 11.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(5, 24)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(86, 17)
        Me.Label4.TabIndex = 4
        Me.Label4.Text = "Proveedor:"
        '
        'cmbProvedorLP
        '
        Me.cmbProvedorLP.FormattingEnabled = True
        Me.cmbProvedorLP.Items.AddRange(New Object() {"GONZALO-PC", "GONZALO-NBOOK", "PATRICIA-LOCAL", "PATRICIA-PC", "PATRICIA-NBOOK"})
        Me.cmbProvedorLP.Location = New System.Drawing.Point(97, 24)
        Me.cmbProvedorLP.Name = "cmbProvedorLP"
        Me.cmbProvedorLP.Size = New System.Drawing.Size(320, 21)
        Me.cmbProvedorLP.TabIndex = 3
        '
        'frmConfiguracion
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Black
        Me.ClientSize = New System.Drawing.Size(1120, 701)
        Me.ControlBox = False
        Me.Controls.Add(Me.GroupControl2)
        Me.Controls.Add(Me.GroupControl1)
        Me.Controls.Add(Me.btnGuardar)
        Me.Controls.Add(Me.btnSalir)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmConfiguracion"
        Me.RightToLeftLayout = True
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Configuracion"
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl2.ResumeLayout(False)
        Me.GroupControl2.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnSalir As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnGuardar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cmbServer As System.Windows.Forms.ComboBox
    Friend WithEvents txtCadenaConex As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents GroupControl2 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents cmbProvedorLP As System.Windows.Forms.ComboBox
    Friend WithEvents dtpFechaLP As System.Windows.Forms.DateTimePicker
    Friend WithEvents btnGuardarCn As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnGuardarLP As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnNuevoLP As DevExpress.XtraEditors.SimpleButton
End Class
