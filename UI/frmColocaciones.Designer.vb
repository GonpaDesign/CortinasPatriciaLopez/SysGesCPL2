﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmColocaciones
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.gcCliente = New DevExpress.XtraEditors.GroupControl()
        Me.txtClienteId = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.txtNombre = New System.Windows.Forms.TextBox()
        Me.txtMovil2 = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtCiudad = New System.Windows.Forms.TextBox()
        Me.txtMovil1 = New System.Windows.Forms.TextBox()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.txtTelefono1 = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtDireccion = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.gcColocaciones = New DevExpress.XtraEditors.GroupControl()
        Me.dgvColocaciones = New System.Windows.Forms.DataGridView()
        Me.btnSalir = New DevExpress.XtraEditors.SimpleButton()
        Me.gcFecha = New DevExpress.XtraEditors.GroupControl()
        Me.btnGuardar = New DevExpress.XtraEditors.SimpleButton()
        Me.dtpHora = New System.Windows.Forms.DateTimePicker()
        Me.dtpFecha = New System.Windows.Forms.DateTimePicker()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.gcFiltros = New DevExpress.XtraEditors.GroupControl()
        Me.rbPorFecha = New System.Windows.Forms.RadioButton()
        Me.rbSinProgramar = New System.Windows.Forms.RadioButton()
        Me.btnActualizar = New DevExpress.XtraEditors.SimpleButton()
        Me.dtpFechaSelect = New System.Windows.Forms.DateTimePicker()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.lblHora = New System.Windows.Forms.Label()
        Me.lblFecha = New System.Windows.Forms.Label()
        Me.gcBuscar = New DevExpress.XtraEditors.PanelControl()
        Me.btnRealizado = New DevExpress.XtraEditors.SimpleButton()
        Me.txtPresupuestoId = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txtPendiente = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.txtCobrado = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.txtTotal = New System.Windows.Forms.TextBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.gcPagos = New DevExpress.XtraEditors.GroupControl()
        Me.btnCobros = New DevExpress.XtraEditors.SimpleButton()
        Me.lblTitulo = New System.Windows.Forms.Label()
        CType(Me.gcCliente, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gcCliente.SuspendLayout()
        CType(Me.gcColocaciones, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gcColocaciones.SuspendLayout()
        CType(Me.dgvColocaciones, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gcFecha, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gcFecha.SuspendLayout()
        CType(Me.gcFiltros, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gcFiltros.SuspendLayout()
        CType(Me.gcBuscar, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gcPagos, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gcPagos.SuspendLayout()
        Me.SuspendLayout()
        '
        'gcCliente
        '
        Me.gcCliente.Controls.Add(Me.txtClienteId)
        Me.gcCliente.Controls.Add(Me.Label10)
        Me.gcCliente.Controls.Add(Me.txtNombre)
        Me.gcCliente.Controls.Add(Me.txtMovil2)
        Me.gcCliente.Controls.Add(Me.Label1)
        Me.gcCliente.Controls.Add(Me.txtCiudad)
        Me.gcCliente.Controls.Add(Me.txtMovil1)
        Me.gcCliente.Controls.Add(Me.Label25)
        Me.gcCliente.Controls.Add(Me.txtTelefono1)
        Me.gcCliente.Controls.Add(Me.Label6)
        Me.gcCliente.Controls.Add(Me.Label3)
        Me.gcCliente.Controls.Add(Me.Label2)
        Me.gcCliente.Controls.Add(Me.txtDireccion)
        Me.gcCliente.Controls.Add(Me.Label8)
        Me.gcCliente.Location = New System.Drawing.Point(12, 88)
        Me.gcCliente.LookAndFeel.SkinName = "Visual Studio 2013 Dark"
        Me.gcCliente.LookAndFeel.UseDefaultLookAndFeel = False
        Me.gcCliente.Name = "gcCliente"
        Me.gcCliente.Size = New System.Drawing.Size(907, 122)
        Me.gcCliente.TabIndex = 33
        Me.gcCliente.Text = "Datos del Cliente"
        '
        'txtClienteId
        '
        Me.txtClienteId.Font = New System.Drawing.Font("Calibri", 12.0!)
        Me.txtClienteId.Location = New System.Drawing.Point(821, 24)
        Me.txtClienteId.Name = "txtClienteId"
        Me.txtClienteId.Size = New System.Drawing.Size(78, 27)
        Me.txtClienteId.TabIndex = 81
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.BackColor = System.Drawing.Color.Transparent
        Me.Label10.Font = New System.Drawing.Font("Cambria", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Label10.Location = New System.Drawing.Point(786, 27)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(29, 19)
        Me.Label10.TabIndex = 80
        Me.Label10.Text = "Id:"
        '
        'txtNombre
        '
        Me.txtNombre.Font = New System.Drawing.Font("Calibri", 12.0!)
        Me.txtNombre.Location = New System.Drawing.Point(121, 24)
        Me.txtNombre.Name = "txtNombre"
        Me.txtNombre.Size = New System.Drawing.Size(659, 27)
        Me.txtNombre.TabIndex = 79
        '
        'txtMovil2
        '
        Me.txtMovil2.Font = New System.Drawing.Font("Calibri", 12.0!)
        Me.txtMovil2.Location = New System.Drawing.Point(706, 90)
        Me.txtMovil2.Name = "txtMovil2"
        Me.txtMovil2.Size = New System.Drawing.Size(196, 27)
        Me.txtMovil2.TabIndex = 78
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Cambria", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Label1.Location = New System.Drawing.Point(616, 93)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(84, 19)
        Me.Label1.TabIndex = 77
        Me.Label1.Text = "Tel. Movil:"
        '
        'txtCiudad
        '
        Me.txtCiudad.Font = New System.Drawing.Font("Calibri", 12.0!)
        Me.txtCiudad.Location = New System.Drawing.Point(706, 57)
        Me.txtCiudad.Name = "txtCiudad"
        Me.txtCiudad.Size = New System.Drawing.Size(196, 27)
        Me.txtCiudad.TabIndex = 76
        '
        'txtMovil1
        '
        Me.txtMovil1.Font = New System.Drawing.Font("Calibri", 12.0!)
        Me.txtMovil1.Location = New System.Drawing.Point(414, 90)
        Me.txtMovil1.Name = "txtMovil1"
        Me.txtMovil1.Size = New System.Drawing.Size(196, 27)
        Me.txtMovil1.TabIndex = 75
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.BackColor = System.Drawing.Color.Transparent
        Me.Label25.Font = New System.Drawing.Font("Cambria", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Label25.Location = New System.Drawing.Point(324, 93)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(84, 19)
        Me.Label25.TabIndex = 74
        Me.Label25.Text = "Tel. Movil:"
        '
        'txtTelefono1
        '
        Me.txtTelefono1.Font = New System.Drawing.Font("Calibri", 12.0!)
        Me.txtTelefono1.Location = New System.Drawing.Point(122, 90)
        Me.txtTelefono1.Name = "txtTelefono1"
        Me.txtTelefono1.Size = New System.Drawing.Size(196, 27)
        Me.txtTelefono1.TabIndex = 73
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.BackColor = System.Drawing.Color.Transparent
        Me.Label6.Font = New System.Drawing.Font("Cambria", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Label6.Location = New System.Drawing.Point(8, 93)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(108, 19)
        Me.Label6.TabIndex = 72
        Me.Label6.Text = "Tel. Personal:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.Label3.Font = New System.Drawing.Font("Cambria", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Label3.Location = New System.Drawing.Point(634, 60)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(66, 19)
        Me.Label3.TabIndex = 70
        Me.Label3.Text = "Ciudad:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.Font = New System.Drawing.Font("Cambria", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Label2.Location = New System.Drawing.Point(32, 64)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(84, 19)
        Me.Label2.TabIndex = 69
        Me.Label2.Text = "Dirección:"
        '
        'txtDireccion
        '
        Me.txtDireccion.Font = New System.Drawing.Font("Calibri", 12.0!)
        Me.txtDireccion.Location = New System.Drawing.Point(122, 57)
        Me.txtDireccion.Name = "txtDireccion"
        Me.txtDireccion.Size = New System.Drawing.Size(506, 27)
        Me.txtDireccion.TabIndex = 71
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.BackColor = System.Drawing.Color.Transparent
        Me.Label8.Font = New System.Drawing.Font("Cambria", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Label8.Location = New System.Drawing.Point(45, 27)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(71, 19)
        Me.Label8.TabIndex = 68
        Me.Label8.Text = "Nombre:"
        '
        'gcColocaciones
        '
        Me.gcColocaciones.Controls.Add(Me.dgvColocaciones)
        Me.gcColocaciones.Location = New System.Drawing.Point(12, 218)
        Me.gcColocaciones.LookAndFeel.SkinName = "Visual Studio 2013 Dark"
        Me.gcColocaciones.LookAndFeel.UseDefaultLookAndFeel = False
        Me.gcColocaciones.Name = "gcColocaciones"
        Me.gcColocaciones.Size = New System.Drawing.Size(907, 471)
        Me.gcColocaciones.TabIndex = 34
        Me.gcColocaciones.Text = "Colocaciones"
        '
        'dgvColocaciones
        '
        Me.dgvColocaciones.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(32, Byte), Integer), CType(CType(31, Byte), Integer), CType(CType(53, Byte), Integer))
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvColocaciones.DefaultCellStyle = DataGridViewCellStyle1
        Me.dgvColocaciones.Location = New System.Drawing.Point(121, 27)
        Me.dgvColocaciones.Margin = New System.Windows.Forms.Padding(6)
        Me.dgvColocaciones.Name = "dgvColocaciones"
        Me.dgvColocaciones.Size = New System.Drawing.Size(778, 436)
        Me.dgvColocaciones.TabIndex = 36
        '
        'btnSalir
        '
        Me.btnSalir.Appearance.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold)
        Me.btnSalir.Appearance.Options.UseFont = True
        Me.btnSalir.Location = New System.Drawing.Point(1027, 662)
        Me.btnSalir.LookAndFeel.SkinName = "Visual Studio 2013 Dark"
        Me.btnSalir.LookAndFeel.UseDefaultLookAndFeel = False
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(75, 27)
        Me.btnSalir.TabIndex = 58
        Me.btnSalir.Text = "Cerrar"
        '
        'gcFecha
        '
        Me.gcFecha.Controls.Add(Me.btnGuardar)
        Me.gcFecha.Controls.Add(Me.dtpHora)
        Me.gcFecha.Controls.Add(Me.dtpFecha)
        Me.gcFecha.Controls.Add(Me.Label5)
        Me.gcFecha.Controls.Add(Me.Label4)
        Me.gcFecha.Location = New System.Drawing.Point(925, 88)
        Me.gcFecha.LookAndFeel.SkinName = "Visual Studio 2013 Dark"
        Me.gcFecha.LookAndFeel.UseDefaultLookAndFeel = False
        Me.gcFecha.Name = "gcFecha"
        Me.gcFecha.Size = New System.Drawing.Size(177, 122)
        Me.gcFecha.TabIndex = 59
        Me.gcFecha.Text = "Fecha de Colocacion"
        '
        'btnGuardar
        '
        Me.btnGuardar.Appearance.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold)
        Me.btnGuardar.Appearance.Options.UseFont = True
        Me.btnGuardar.Location = New System.Drawing.Point(51, 90)
        Me.btnGuardar.LookAndFeel.SkinName = "Visual Studio 2013 Dark"
        Me.btnGuardar.LookAndFeel.UseDefaultLookAndFeel = False
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(75, 27)
        Me.btnGuardar.TabIndex = 60
        Me.btnGuardar.Text = "Guardar"
        '
        'dtpHora
        '
        Me.dtpHora.Cursor = System.Windows.Forms.Cursors.Hand
        Me.dtpHora.CustomFormat = "HH:mm"
        Me.dtpHora.Font = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpHora.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpHora.Location = New System.Drawing.Point(62, 60)
        Me.dtpHora.MinDate = New Date(1900, 1, 1, 0, 0, 0, 0)
        Me.dtpHora.Name = "dtpHora"
        Me.dtpHora.ShowUpDown = True
        Me.dtpHora.Size = New System.Drawing.Size(110, 25)
        Me.dtpHora.TabIndex = 60
        '
        'dtpFecha
        '
        Me.dtpFecha.CalendarFont = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpFecha.Font = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpFecha.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFecha.Location = New System.Drawing.Point(62, 29)
        Me.dtpFecha.MaxDate = New Date(2030, 12, 31, 0, 0, 0, 0)
        Me.dtpFecha.MinDate = New Date(1900, 1, 1, 0, 0, 0, 0)
        Me.dtpFecha.Name = "dtpFecha"
        Me.dtpFecha.Size = New System.Drawing.Size(110, 25)
        Me.dtpFecha.TabIndex = 77
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.BackColor = System.Drawing.Color.Transparent
        Me.Label5.Font = New System.Drawing.Font("Cambria", 11.0!, System.Drawing.FontStyle.Bold)
        Me.Label5.Location = New System.Drawing.Point(9, 65)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(47, 17)
        Me.Label5.TabIndex = 76
        Me.Label5.Text = "Hora:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.BackColor = System.Drawing.Color.Transparent
        Me.Label4.Font = New System.Drawing.Font("Cambria", 11.0!, System.Drawing.FontStyle.Bold)
        Me.Label4.Location = New System.Drawing.Point(5, 34)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(51, 17)
        Me.Label4.TabIndex = 75
        Me.Label4.Text = "Fecha:"
        '
        'gcFiltros
        '
        Me.gcFiltros.Controls.Add(Me.rbPorFecha)
        Me.gcFiltros.Controls.Add(Me.rbSinProgramar)
        Me.gcFiltros.Controls.Add(Me.btnActualizar)
        Me.gcFiltros.Controls.Add(Me.dtpFechaSelect)
        Me.gcFiltros.Controls.Add(Me.Label7)
        Me.gcFiltros.Location = New System.Drawing.Point(925, 218)
        Me.gcFiltros.LookAndFeel.SkinName = "Visual Studio 2013 Dark"
        Me.gcFiltros.LookAndFeel.UseDefaultLookAndFeel = False
        Me.gcFiltros.Name = "gcFiltros"
        Me.gcFiltros.Size = New System.Drawing.Size(177, 139)
        Me.gcFiltros.TabIndex = 60
        Me.gcFiltros.Text = "Configuracion de filtros"
        '
        'rbPorFecha
        '
        Me.rbPorFecha.AutoSize = True
        Me.rbPorFecha.Font = New System.Drawing.Font("Cambria", 11.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbPorFecha.Location = New System.Drawing.Point(79, 51)
        Me.rbPorFecha.Name = "rbPorFecha"
        Me.rbPorFecha.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.rbPorFecha.Size = New System.Drawing.Size(93, 21)
        Me.rbPorFecha.TabIndex = 83
        Me.rbPorFecha.TabStop = True
        Me.rbPorFecha.Text = "Por Fecha"
        Me.rbPorFecha.UseVisualStyleBackColor = True
        '
        'rbSinProgramar
        '
        Me.rbSinProgramar.AutoSize = True
        Me.rbSinProgramar.Font = New System.Drawing.Font("Cambria", 11.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbSinProgramar.Location = New System.Drawing.Point(45, 24)
        Me.rbSinProgramar.Name = "rbSinProgramar"
        Me.rbSinProgramar.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.rbSinProgramar.Size = New System.Drawing.Size(127, 21)
        Me.rbSinProgramar.TabIndex = 82
        Me.rbSinProgramar.TabStop = True
        Me.rbSinProgramar.Text = "Sin Programar"
        Me.rbSinProgramar.UseVisualStyleBackColor = True
        '
        'btnActualizar
        '
        Me.btnActualizar.Appearance.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold)
        Me.btnActualizar.Appearance.Options.UseFont = True
        Me.btnActualizar.Location = New System.Drawing.Point(51, 109)
        Me.btnActualizar.LookAndFeel.SkinName = "Visual Studio 2013 Dark"
        Me.btnActualizar.LookAndFeel.UseDefaultLookAndFeel = False
        Me.btnActualizar.Name = "btnActualizar"
        Me.btnActualizar.Size = New System.Drawing.Size(75, 27)
        Me.btnActualizar.TabIndex = 80
        Me.btnActualizar.Text = "Actualizar"
        '
        'dtpFechaSelect
        '
        Me.dtpFechaSelect.CalendarFont = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpFechaSelect.Cursor = System.Windows.Forms.Cursors.Hand
        Me.dtpFechaSelect.Font = New System.Drawing.Font("Calibri", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpFechaSelect.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFechaSelect.Location = New System.Drawing.Point(62, 78)
        Me.dtpFechaSelect.MaxDate = New Date(2030, 12, 31, 0, 0, 0, 0)
        Me.dtpFechaSelect.MinDate = New Date(1900, 1, 1, 0, 0, 0, 0)
        Me.dtpFechaSelect.Name = "dtpFechaSelect"
        Me.dtpFechaSelect.Size = New System.Drawing.Size(110, 25)
        Me.dtpFechaSelect.TabIndex = 79
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.BackColor = System.Drawing.Color.Transparent
        Me.Label7.Font = New System.Drawing.Font("Cambria", 11.0!, System.Drawing.FontStyle.Bold)
        Me.Label7.Location = New System.Drawing.Point(5, 83)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(51, 17)
        Me.Label7.TabIndex = 78
        Me.Label7.Text = "Fecha:"
        '
        'lblHora
        '
        Me.lblHora.AutoSize = True
        Me.lblHora.BackColor = System.Drawing.Color.Transparent
        Me.lblHora.Font = New System.Drawing.Font("Cambria", 12.0!, System.Drawing.FontStyle.Bold)
        Me.lblHora.Location = New System.Drawing.Point(1215, 142)
        Me.lblHora.Name = "lblHora"
        Me.lblHora.Size = New System.Drawing.Size(0, 19)
        Me.lblHora.TabIndex = 77
        '
        'lblFecha
        '
        Me.lblFecha.AutoSize = True
        Me.lblFecha.BackColor = System.Drawing.Color.Transparent
        Me.lblFecha.Font = New System.Drawing.Font("Cambria", 12.0!, System.Drawing.FontStyle.Bold)
        Me.lblFecha.Location = New System.Drawing.Point(1215, 398)
        Me.lblFecha.Name = "lblFecha"
        Me.lblFecha.Size = New System.Drawing.Size(0, 19)
        Me.lblFecha.TabIndex = 79
        '
        'gcBuscar
        '
        Me.gcBuscar.Location = New System.Drawing.Point(12, 45)
        Me.gcBuscar.LookAndFeel.SkinName = "Visual Studio 2013 Dark"
        Me.gcBuscar.LookAndFeel.UseDefaultLookAndFeel = False
        Me.gcBuscar.Name = "gcBuscar"
        Me.gcBuscar.Size = New System.Drawing.Size(1090, 37)
        Me.gcBuscar.TabIndex = 80
        '
        'btnRealizado
        '
        Me.btnRealizado.Appearance.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold)
        Me.btnRealizado.Appearance.Options.UseFont = True
        Me.btnRealizado.Location = New System.Drawing.Point(1027, 629)
        Me.btnRealizado.LookAndFeel.SkinName = "Visual Studio 2013 Dark"
        Me.btnRealizado.LookAndFeel.UseDefaultLookAndFeel = False
        Me.btnRealizado.Name = "btnRealizado"
        Me.btnRealizado.Size = New System.Drawing.Size(75, 27)
        Me.btnRealizado.TabIndex = 81
        Me.btnRealizado.Text = "Realizado"
        '
        'txtPresupuestoId
        '
        Me.txtPresupuestoId.Font = New System.Drawing.Font("Calibri", 12.0!)
        Me.txtPresupuestoId.Location = New System.Drawing.Point(94, 24)
        Me.txtPresupuestoId.Name = "txtPresupuestoId"
        Me.txtPresupuestoId.Size = New System.Drawing.Size(78, 27)
        Me.txtPresupuestoId.TabIndex = 83
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.BackColor = System.Drawing.Color.Transparent
        Me.Label9.Font = New System.Drawing.Font("Cambria", 11.0!, System.Drawing.FontStyle.Bold)
        Me.Label9.Location = New System.Drawing.Point(23, 28)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(65, 17)
        Me.Label9.TabIndex = 82
        Me.Label9.Text = "Pres. N°:"
        '
        'txtPendiente
        '
        Me.txtPendiente.Font = New System.Drawing.Font("Calibri", 12.0!)
        Me.txtPendiente.Location = New System.Drawing.Point(94, 123)
        Me.txtPendiente.Name = "txtPendiente"
        Me.txtPendiente.Size = New System.Drawing.Size(78, 27)
        Me.txtPendiente.TabIndex = 85
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.BackColor = System.Drawing.Color.Transparent
        Me.Label11.Font = New System.Drawing.Font("Cambria", 11.0!, System.Drawing.FontStyle.Bold)
        Me.Label11.Location = New System.Drawing.Point(37, 127)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(51, 17)
        Me.Label11.TabIndex = 84
        Me.Label11.Text = "Saldo:"
        '
        'txtCobrado
        '
        Me.txtCobrado.Font = New System.Drawing.Font("Calibri", 12.0!)
        Me.txtCobrado.Location = New System.Drawing.Point(94, 90)
        Me.txtCobrado.Name = "txtCobrado"
        Me.txtCobrado.Size = New System.Drawing.Size(78, 27)
        Me.txtCobrado.TabIndex = 87
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.BackColor = System.Drawing.Color.Transparent
        Me.Label12.Font = New System.Drawing.Font("Cambria", 11.0!, System.Drawing.FontStyle.Bold)
        Me.Label12.Location = New System.Drawing.Point(15, 94)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(73, 17)
        Me.Label12.TabIndex = 86
        Me.Label12.Text = "A Cuenta:"
        '
        'txtTotal
        '
        Me.txtTotal.Font = New System.Drawing.Font("Calibri", 12.0!)
        Me.txtTotal.Location = New System.Drawing.Point(94, 57)
        Me.txtTotal.Name = "txtTotal"
        Me.txtTotal.Size = New System.Drawing.Size(78, 27)
        Me.txtTotal.TabIndex = 89
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.BackColor = System.Drawing.Color.Transparent
        Me.Label13.Font = New System.Drawing.Font("Cambria", 11.0!, System.Drawing.FontStyle.Bold)
        Me.Label13.Location = New System.Drawing.Point(40, 61)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(48, 17)
        Me.Label13.TabIndex = 88
        Me.Label13.Text = "Total:"
        '
        'gcPagos
        '
        Me.gcPagos.Controls.Add(Me.txtPresupuestoId)
        Me.gcPagos.Controls.Add(Me.Label9)
        Me.gcPagos.Controls.Add(Me.txtTotal)
        Me.gcPagos.Controls.Add(Me.Label13)
        Me.gcPagos.Controls.Add(Me.btnCobros)
        Me.gcPagos.Controls.Add(Me.txtPendiente)
        Me.gcPagos.Controls.Add(Me.txtCobrado)
        Me.gcPagos.Controls.Add(Me.Label12)
        Me.gcPagos.Controls.Add(Me.Label11)
        Me.gcPagos.Location = New System.Drawing.Point(925, 363)
        Me.gcPagos.LookAndFeel.SkinName = "Visual Studio 2013 Dark"
        Me.gcPagos.LookAndFeel.UseDefaultLookAndFeel = False
        Me.gcPagos.Name = "gcPagos"
        Me.gcPagos.Size = New System.Drawing.Size(177, 186)
        Me.gcPagos.TabIndex = 82
        Me.gcPagos.Text = "Pagos"
        '
        'btnCobros
        '
        Me.btnCobros.Appearance.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold)
        Me.btnCobros.Appearance.Options.UseFont = True
        Me.btnCobros.Location = New System.Drawing.Point(51, 156)
        Me.btnCobros.LookAndFeel.SkinName = "Visual Studio 2013 Dark"
        Me.btnCobros.LookAndFeel.UseDefaultLookAndFeel = False
        Me.btnCobros.Name = "btnCobros"
        Me.btnCobros.Size = New System.Drawing.Size(75, 27)
        Me.btnCobros.TabIndex = 80
        Me.btnCobros.Text = "Cobrar"
        '
        'lblTitulo
        '
        Me.lblTitulo.AccessibleRole = System.Windows.Forms.AccessibleRole.TitleBar
        Me.lblTitulo.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.lblTitulo.AutoSize = True
        Me.lblTitulo.BackColor = System.Drawing.Color.Transparent
        Me.lblTitulo.Font = New System.Drawing.Font("Microsoft Sans Serif", 24.0!)
        Me.lblTitulo.ForeColor = System.Drawing.Color.White
        Me.lblTitulo.Location = New System.Drawing.Point(329, 4)
        Me.lblTitulo.Name = "lblTitulo"
        Me.lblTitulo.Size = New System.Drawing.Size(462, 37)
        Me.lblTitulo.TabIndex = 83
        Me.lblTitulo.Text = "Administrador de Colocaciones"
        '
        'frmColocaciones
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoSize = True
        Me.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.BackColor = System.Drawing.Color.Black
        Me.ClientSize = New System.Drawing.Size(1120, 701)
        Me.ControlBox = False
        Me.Controls.Add(Me.lblTitulo)
        Me.Controls.Add(Me.gcPagos)
        Me.Controls.Add(Me.gcFiltros)
        Me.Controls.Add(Me.btnRealizado)
        Me.Controls.Add(Me.gcBuscar)
        Me.Controls.Add(Me.lblFecha)
        Me.Controls.Add(Me.lblHora)
        Me.Controls.Add(Me.gcColocaciones)
        Me.Controls.Add(Me.gcFecha)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(Me.gcCliente)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.MaximumSize = New System.Drawing.Size(1366, 701)
        Me.MinimumSize = New System.Drawing.Size(1120, 701)
        Me.Name = "frmColocaciones"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Colocaciones"
        CType(Me.gcCliente, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gcCliente.ResumeLayout(False)
        Me.gcCliente.PerformLayout()
        CType(Me.gcColocaciones, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gcColocaciones.ResumeLayout(False)
        CType(Me.dgvColocaciones, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gcFecha, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gcFecha.ResumeLayout(False)
        Me.gcFecha.PerformLayout()
        CType(Me.gcFiltros, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gcFiltros.ResumeLayout(False)
        Me.gcFiltros.PerformLayout()
        CType(Me.gcBuscar, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gcPagos, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gcPagos.ResumeLayout(False)
        Me.gcPagos.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents gcCliente As DevExpress.XtraEditors.GroupControl
    Friend WithEvents gcColocaciones As DevExpress.XtraEditors.GroupControl
    Friend WithEvents dgvColocaciones As System.Windows.Forms.DataGridView
    Friend WithEvents txtMovil2 As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtCiudad As System.Windows.Forms.TextBox
    Friend WithEvents txtMovil1 As System.Windows.Forms.TextBox
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents txtTelefono1 As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtDireccion As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents btnSalir As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents gcFecha As DevExpress.XtraEditors.GroupControl
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents dtpFecha As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpHora As System.Windows.Forms.DateTimePicker
    Friend WithEvents btnGuardar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents gcFiltros As DevExpress.XtraEditors.GroupControl
    Friend WithEvents btnActualizar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents dtpFechaSelect As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents lblHora As System.Windows.Forms.Label
    Friend WithEvents txtNombre As System.Windows.Forms.TextBox
    Friend WithEvents lblFecha As System.Windows.Forms.Label
    Friend WithEvents gcBuscar As DevExpress.XtraEditors.PanelControl
    Friend WithEvents btnRealizado As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents txtClienteId As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents txtPresupuestoId As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents txtPendiente As System.Windows.Forms.TextBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents txtCobrado As System.Windows.Forms.TextBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents txtTotal As System.Windows.Forms.TextBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents gcPagos As DevExpress.XtraEditors.GroupControl
    Friend WithEvents btnCobros As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents rbPorFecha As System.Windows.Forms.RadioButton
    Friend WithEvents rbSinProgramar As System.Windows.Forms.RadioButton
    Friend WithEvents lblTitulo As System.Windows.Forms.Label
End Class
