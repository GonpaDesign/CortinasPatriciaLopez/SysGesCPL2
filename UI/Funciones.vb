﻿Imports System.Globalization

Public Class Funciones
    Public Function Precio(Texto As String) As String
        Precio = ""
        Texto = QuitarPrecio(Texto)
        Dim i As Integer = Texto.Length
        Precio = String.Format("$ {0}", Texto)
        Return Precio
    End Function
    Public Function Metros(Texto As String) As String
        Metros = ""
        Texto = QuitarMetros(Texto)
        Dim i As Integer = Texto.Length
        Metros = String.Format("{0} mts.", Texto)
        Return Metros
    End Function
    Public Function DNI(Texto As String) As String
        DNI = ""
        If Texto = "0" Then
            DNI = ""
        Else
            Texto = Texto.Replace(".", "")
            Dim i As Integer = Texto.Length
            If i < 7 Then
            Else
                DNI = String.Format("{0}.{1}.{2}", Mid(Texto, 1, i - 6), Mid(Texto, i - 5, 3), Mid(Texto, i - 2, 3))
            End If
        End If
        Return DNI
    End Function
    Public Function CUIT(Texto As String) As String
        CUIT = ""
        If Texto = "0" Then
            CUIT = ""
        Else
            Texto = Texto.Replace("-", "")
            Dim i As Integer = Texto.Length
            If i < 10 Then
            Else
                CUIT = String.Format("{0}-{1}-{2}", Mid(Texto, 1, 2), Mid(Texto, 3, i - 3), Mid(Texto, i, 1))
            End If
        End If
        Return CUIT
    End Function
    Public Function Telefono(Texto As String) As String
        Telefono = ""
        If Texto = "0" Then
            Telefono = ""
        Else
            Texto = Texto.Replace("-", "").Replace(" ", "").Replace("(", "").Replace(")", "")
            Dim i As Integer = Texto.Length
            Select Case i
                Case 1
                    If Texto = 0 Then
                        Telefono = ""
                    Else
                        Telefono = Texto
                    End If
                Case 8
                    Telefono = String.Format("{0}-{1}", Mid(Texto, 1, 4), Mid(Texto, 5, 4))
                Case 10
                    If Mid(Texto, 1, 2) <> 11 And Mid(Texto, 1, 2) <> 15 Then
                        Telefono = String.Format("({0}) {1}-{2}", Mid(Texto, 1, 3), Mid(Texto, 4, 3), Mid(Texto, 7, 4))
                    Else
                        Telefono = String.Format("{0}-{1}-{2}", Mid(Texto, 1, 2), Mid(Texto, 3, 4), Mid(Texto, 7, 4))
                    End If
                Case 11
                    Telefono = String.Format("({0}) {1}-{2}", Mid(Texto, 1, 3), Mid(Texto, 4, 4), Mid(Texto, 8, 4))
                Case Else
                    Telefono = Texto
            End Select
        End If
        Return Telefono
    End Function
    Public Function SoloNumeros(Texto As String) As Long
        SoloNumeros = 0
        Dim Numero As String = ""
        Dim index As Integer
        For index = 1 To Len(Texto)
            If (Mid$(Texto, index, 1) Like "#") Then
                Numero = Numero & Mid$(Texto, index, 1)
            End If
        Next
        If Len(Numero) = 0 Then
            SoloNumeros = 0
        Else
            SoloNumeros = CLng(Numero)
        End If
        Return SoloNumeros
    End Function
    Public Function QuitarPrecio(Texto As String) As Double
        QuitarPrecio = 0
        If Texto = "" Then
            QuitarPrecio = 0
        Else
            QuitarPrecio = CDbl(Texto.Replace("$ ", ""))
        End If
        Return QuitarPrecio
    End Function
    Public Function QuitarMetros(Texto As String) As Double
        QuitarMetros = 0
        If Texto = "" Then
            QuitarMetros = 0
        Else
            QuitarMetros = CDbl(Texto.Replace(" mts.", ""))
        End If
        Return QuitarMetros
    End Function
    Public Function Costo(CostoUnitario As Double, cantidad As String) As Double
        Costo = 0
        Costo = Math.Round(CostoUnitario * CDbl(cantidad), 2)
        Return Costo
    End Function
    Public Function CostoSup(AnchoI As String, AnchoD As String, LargoI As String, LargoD As String, SupMin As Double, CostoUnitario As Double) As Double
        CostoSup = 0
        If (CDbl(AnchoI) * CDbl(LargoI)) > SupMin And (CDbl(AnchoD) * CDbl(AnchoD)) > SupMin Then
            CostoSup = Math.Round(CDbl(AnchoI) * CDbl(LargoI) * CostoUnitario + CDbl(AnchoD) * CDbl(LargoD) * CostoUnitario, 2)
        Else
            If (CDbl(AnchoI) * CDbl(LargoI)) > SupMin Then
                CostoSup = Math.Round(CDbl(AnchoI) * CDbl(LargoI) * CostoUnitario + SupMin * CostoUnitario, 2)
            Else
                CostoSup = Math.Round(SupMin + (CDbl(AnchoD) * CDbl(LargoD) * CostoUnitario), 2)
            End If
        End If
        Return CostoSup
    End Function
    Public Function CostoLin(Ancho As String, CostoUnitario As Double) As Double
        CostoLin = 0
        If Math.IEEERemainder(CDbl(Ancho), 1.5) > 0 And Math.IEEERemainder(CDbl(Ancho), 1.5) < 1 Then
            CostoLin = Math.Round((CDbl(Ancho) + 0.75) / 1.5 * CostoUnitario, 2)
        Else
            CostoLin = Math.Round((CDbl(Ancho) / 1.5) * CostoUnitario, 2)
        End If
        Return CostoLin
    End Function
    Public Sub CargarItem1DimO(ByVal Nombre As Object, ByVal Precio As Object, ByVal Cantidad As Object, ByVal Obs As Object, Item As Modelo.Item, Valor As Double)
        Dim ArticuloSeleccionado As New Modelo.Articulo
        Dim UnGestorArticulo As New Modelo.GestorArticulo
        ArticuloSeleccionado.IdPresupuesto = Item.IdPresupuesto
        ArticuloSeleccionado.Item = Item.Item + Valor
        ArticuloSeleccionado = UnGestorArticulo.BuscarPorItem(ArticuloSeleccionado)
        Nombre.selectedtext = ArticuloSeleccionado.Nombre
        Precio.Text = ArticuloSeleccionado.Precio
        Cantidad.Text = ArticuloSeleccionado.Dim1
        Obs.Text = ArticuloSeleccionado.Observacion
    End Sub
    Public Sub CargarItem1Dim(ByVal Nombre As Object, ByVal Precio As Object, ByVal Cantidad As Object, Item As Modelo.Item, Valor As Double)
        Dim ArticuloSeleccionado As New Modelo.Articulo
        Dim UnGestorArticulo As New Modelo.GestorArticulo
        ArticuloSeleccionado.IdPresupuesto = Item.IdPresupuesto
        ArticuloSeleccionado.Item = Item.Item + Valor
        ArticuloSeleccionado = UnGestorArticulo.BuscarPorItem(ArticuloSeleccionado)
        Nombre.Text = ArticuloSeleccionado.Nombre
        Precio.Text = ArticuloSeleccionado.Precio
        Cantidad.Text = ArticuloSeleccionado.Dim1
    End Sub
    Public Sub CargarItem2Dim(ByVal Nombre As Object, ByVal Precio As Object, ByVal Ancho As Object, ByVal Largo As Object, Item As Modelo.Item, Valor As Double)
        Dim ArticuloSeleccionado As New Modelo.Articulo
        Dim UnGestorArticulo As New Modelo.GestorArticulo
        ArticuloSeleccionado.IdPresupuesto = Item.IdPresupuesto
        ArticuloSeleccionado.Item = Item.Item + Valor
        ArticuloSeleccionado = UnGestorArticulo.BuscarPorItem(ArticuloSeleccionado)
        Nombre.selectedtext = ArticuloSeleccionado.Nombre
        Precio.Text = ArticuloSeleccionado.Precio
        Ancho.Text = ArticuloSeleccionado.Dim1
        Largo.Text = ArticuloSeleccionado.Dim1
    End Sub
    Public Sub CargarItem2DimCom(ByVal Nombre As Object, ByVal Precio As Object, ByVal Ancho As Object, ByVal Largo As Object, ByVal Comando As Object, Item As Modelo.Item, Valor As Double)
        Dim ArticuloSeleccionado As New Modelo.Articulo
        Dim UnGestorArticulo As New Modelo.GestorArticulo
        ArticuloSeleccionado.IdPresupuesto = Item.IdPresupuesto
        ArticuloSeleccionado.Item = Item.Item + Valor
        ArticuloSeleccionado = UnGestorArticulo.BuscarPorItem(ArticuloSeleccionado)
        Nombre.selectedtext = ArticuloSeleccionado.Nombre
        Precio.Text = ArticuloSeleccionado.Precio
        Ancho.Text = ArticuloSeleccionado.Dim1
        Largo.Text = ArticuloSeleccionado.Dim1
        Comando.checked = ArticuloSeleccionado.Eliminado
    End Sub
    Public Function BuscarTexto(Frase As String, Texto As String) As Integer
        BuscarTexto = 0
        Return BuscarTexto = Frase.IndexOf(Texto)
    End Function
End Class
