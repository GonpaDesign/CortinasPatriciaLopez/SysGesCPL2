﻿Public Class frmPrincipal
    Public Server As String
    Public IVA As Double
    Public IVA10 As Double = 0.105
    Public IIBB As Double = 0.035
    Public Ganancia As Double
    Public ValorAgregado As Double
    Public Main As Integer = 0
    Public Ancho As Integer = Me.ClientSize.Width
    Public Largo As Integer = Me.ClientSize.Height

    Private Sub frmPrincipal_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        CargarVariables()
        frmDockBotones.MdiParent = Me
        frmDockBotones.Dock = DockStyle.None
        frmDockBotones.Location = New Point(0, 0)
        frmDockBotones.Width = 150
        frmDockBotones.Height = Me.ClientSize.Height
        frmDockBotones.TopMost = True
        frmDockBotones.Dock = DockStyle.Left
        frmDockBotones.Show()
    End Sub

    Private Sub ParametrosDeCálculoToolStripMenuItem_Click(sender As Object, e As EventArgs)
        Cursor = System.Windows.Forms.Cursors.WaitCursor
        frmConfiguracion.MdiParent = Me
        frmConfiguracion.Show()
        Cursor = System.Windows.Forms.Cursors.Default
        Main = Main + 1
    End Sub

    Private Sub CargarVariables()
        Dim Funciones As New Funciones
        Ganancia = 0.6
        IVA = 0.21
        ValorAgregado = (1 + Ganancia) * (1 + IVA)
    End Sub

    Private Sub frmPrincipal_ResizeEnd(sender As Object, e As EventArgs) Handles MyBase.Resize
        Try
            frmDockBotones.ConfigurarForm()
        Catch
        End Try
        Try
            frmClientes.ConfigurarForm()
        Catch
        End Try
        Try
            frmProducto.ConfigurarForm()
        Catch
        End Try
        Try
            frmProveedores.ConfigurarForm()
        Catch
        End Try
        Try
            frmPresupuesto.ConfigurarForm()
        Catch
        End Try
        Try
            frmVisitas.ConfigurarForm()
        Catch
        End Try
        Try
            frmEntregas.ConfigurarForm()
        Catch
        End Try
        Try
            frmColocaciones.ConfigurarForm()
        Catch
        End Try
    End Sub

End Class
