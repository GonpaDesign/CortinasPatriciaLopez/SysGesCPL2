﻿Imports System
Imports System.Data
Imports System.Configuration
Imports System.Data.SqlClient
Public Class frmConfiguracion
    Private Modificacion As Boolean = False

    '------ EVENTOS DE FORMULARIOS ------

    Private Sub frmConfiguracion_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        btnGuardar.Enabled = False
    End Sub

    '------ EVENTOS DE BOTONES ------

    Private Sub btnSalir_Click(sender As Object, e As EventArgs) Handles btnSalir.Click
        frmPrincipal.Main = 0
        frmPrincipal.Show()
        Close()
    End Sub
    Private Sub btnGuardar_Click(Sender As Object, e As EventArgs) Handles btnGuardar.Click
        If Modificacion = True Then
            frmPrincipal.Server = cmbServer.Text
            btnGuardar.Enabled = False
        End If
    End Sub

    '------ EVENTOS DE COMBOBOX ------

    Private Sub ComboBox1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbServer.SelectedIndexChanged
        Modificacion = True
        btnGuardar.Enabled = True
    End Sub

    Private Sub btnGuardarLP_Click(sender As Object, e As EventArgs) Handles btnGuardarLP.Click
        Try

        Catch ex As Exception
            MsgBox("La lista de precio no se guardo. Revise el proveedor, recuerde que no puede estar duplicado.", MsgBoxStyle.OkOnly)
        End Try
    End Sub
End Class