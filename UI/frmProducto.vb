﻿Imports System.Xml
Imports DevExpress.Utils

Public Class frmProducto
    Private Funciones As New Funciones
    Private Operacion As Integer = 0 ' 0=Alta, 1=Modificacion
    Private ABM As Boolean = False 'True=Alta, False=Modificacion

    '------ EVENTOS DE FORMULARIOS ------
    Private Sub Productos_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        ConfigurarForm()
        ConfigurarDataGridView()
        ConfigurarTextBox()

        ActualizarCmbBuscar()
        ActualizarCmbCategoria()
        ActualizarCmbProveedor()
        ActualizarCmbBuscarProv()
        ActualizarCmbBucarCat()

        LimpiarCasilleros()

        ModoNormal()
        ModoReadOnly()
        cmbBuscar.Focus()
        cmbBuscar.SelectedIndex = -1
    End Sub
    Private Sub frmProductos_KeyPress(sender As Object, e As KeyPressEventArgs) Handles MyBase.KeyPress, txtCosto.KeyPress, txtUnidad.KeyPress
        If e.KeyChar = "." Then
            e.Handled = True
            SendKeys.Send(",")
        End If
        If e.KeyChar = ChrW(Keys.Enter) Then
            e.Handled = True
            SendKeys.Send("{TAB}")
        End If
    End Sub
    Private Sub Solonumeros(sender As Object, e As KeyPressEventArgs) Handles txtCosto.KeyPress, txtUnidad.KeyPress
        If Not (Char.IsNumber(e.KeyChar) Or Char.IsControl(e.KeyChar) Or Char.IsPunctuation(e.KeyChar)) Then e.Handled = True
    End Sub

    '------ EVENTOS DE FORMULARIOS ------
    Private Sub btnNuevo_Click(sender As System.Object, e As System.EventArgs) Handles btnNuevo.Click
        Operacion = 0
        LimpiarCasilleros()
        ModoAbm()
        ModoEditar()
        txtNombre.Focus()
    End Sub
    Private Sub btnEliminar_Click(sender As Object, e As EventArgs) Handles btnEliminar.Click
        If txtId.Text = "" Then
        Else
            Dim UnProductoAEliminar As New Modelo.Producto
            UnProductoAEliminar.IdProducto = Integer.Parse(txtId.Text)
            If MsgBox(String.Format("¿Desea eliminar el Producto {0}?", txtNombre.Text), vbOKCancel, "Eliminar Producto") = vbOK Then
                Dim UnGestorProducto As New Modelo.GestorProducto
                UnGestorProducto.Eliminar(UnProductoAEliminar)
                LimpiarCasilleros()
                ModoNormal()
                ModoReadOnly()
            End If
        End If
        Operacion = 1
    End Sub
    Private Sub btnSalir_Click(sender As System.Object, e As System.EventArgs) Handles btnSalir.Click
        ModoReadOnly()
        Operacion = 0
        Me.Close()
    End Sub
    Private Sub btnGuardar_Click(sender As System.Object, e As System.EventArgs) Handles btnGuardar.Click
        Dim UnGestorProducto As New Modelo.GestorProducto
        Dim UnProducto As New Modelo.Producto

        UnProducto.Codigo = txtCodigo.Text
        UnProducto.Nombre = txtNombre.Text
        UnProducto.Descripcion = txtDescripcion.Text
        Dim UnaCategoria As New Modelo.Categoria
        UnaCategoria = cmbCategoria.SelectedItem
        UnProducto.IdCategoria = UnaCategoria.IdCategoria
        If txtUnidad.Text = "" Then
            UnProducto.Unidad = 0
        Else
            UnProducto.Unidad = txtUnidad.Text
        End If
        Dim UnProveedor As New Modelo.Proveedor
        UnProveedor = cmbProveedor.SelectedItem
        UnProducto.IdProveedor = UnProveedor.IdProveedor
        UnProducto.Costo = Funciones.QuitarPrecio(txtCosto.Text)
        UnProducto.Fecha = dtpFecha.Text

        Select Case Operacion
            Case 0 ' ALTA
                UnGestorProducto.Nuevo(UnProducto)
            Case 1
                ' MODIFICACION
                UnProducto.IdProducto = Integer.Parse(txtId.Text)
                UnGestorProducto.Modificar(UnProducto)
        End Select
        ModoNormal()
        ModoReadOnly()
        Operacion = 1
    End Sub
    Private Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        Try
            Dim ProductoSeleccionado As New Modelo.Producto
            Dim UnGestorProducto As New Modelo.GestorProducto
            ProductoSeleccionado.IdProducto = txtId.Text
            ProductoSeleccionado = UnGestorProducto.BuscarPorId(ProductoSeleccionado)
            MostrarDetalleDeProducto(ProductoSeleccionado)
        Catch
        End Try
        ModoNormal()
        ModoReadOnly()
        Operacion = 1
    End Sub
    Private Sub btnCargardgv_Click(sender As Object, e As EventArgs) Handles btnCargardgv.Click
        Cargardgv()
        cmbBuscarCat.SelectedIndex = -1
        cmbBuscarProv.SelectedIndex = -1
    End Sub

    '------ CONFIGURACIONES ------
    Public Sub ConfigurarForm()
        Me.Dock = DockStyle.None
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.ShowIcon = False
        Me.MaximumSize = New Size(frmPrincipal.ClientSize.Width - 4, frmPrincipal.ClientSize.Height - 4)
        Me.MinimumSize = Me.MaximumSize
        Me.Dock = DockStyle.Fill
        Dim Separacion As Integer = (frmPrincipal.ClientSize.Width - 1090 + 150) / 2
        gcBuscar.Location = New Point(Separacion, 45)
        gcInfoGral.Location = New Point(Separacion, 88)
        gcProveedor.Location = New Point(Separacion, 214)
        gcBusqueda.Size = New Size(1009, frmPrincipal.ClientSize.Height - (276 + 10))
        gcBusqueda.Location = New Point(Separacion, 276)
        lblTitulo.Location = New Point((frmPrincipal.ClientSize.Width - 415 + 150) / 2, 4)
        Dim Separacionbtn As Integer = (frmPrincipal.ClientSize.Width - Separacion + 75)
        btnCancelar.Location = New Point(Separacionbtn, frmPrincipal.ClientSize.Height - 103)
        btnEliminar.Location = New Point(Separacionbtn, frmPrincipal.ClientSize.Height - 70)
        btnSalir.Location = New Point(Separacionbtn, frmPrincipal.ClientSize.Height - 37)
        btnSalir.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Skin
        btnSalir.LookAndFeel.SkinName = "Visual Studio 2013 Dark"
    End Sub
    Private Sub ConfigurarTextBox()
        'Configuro la cantidad de cararcteres maximos
        txtNombre.MaxLength = 200
        txtCodigo.MaxLength = 50
        txtDescripcion.MaxLength = 200
        txtUnidad.MaxLength = 5
        'Configuro alineacion del texto
        txtId.TextAlign = HorizontalAlignment.Center
        txtPrecio.TextAlign = HorizontalAlignment.Center
        txtCosto.TextAlign = HorizontalAlignment.Center
        txtUnidad.TextAlign = HorizontalAlignment.Center
        'Configuro los que seran siempre solo lectura
        txtId.ReadOnly = True
        txtPrecio.ReadOnly = True
    End Sub
    Private Sub ConfigurarDataGridView()
        dgvProductos.Location = New Point(169, 57)
        dgvProductos.Size = New Size(835, gcBusqueda.ClientSize.Height - 63)
    End Sub

    '------ EVENTOS DE COMBOBOX ------
    Private Sub cmbBuscar_SelectionChangeCommitted(sender As System.Object, e As System.EventArgs) Handles cmbBuscar.SelectionChangeCommitted
        Dim ProductoSeleccionado As New Modelo.Producto
        Dim UnGestorProducto As New Modelo.GestorProducto
        Dim Index As Integer = cmbBuscar.SelectedIndex

        ProductoSeleccionado = UnGestorProducto.BuscarPorId(cmbBuscar.SelectedItem)
        MostrarDetalleDeProducto(ProductoSeleccionado)

        ModoNormal()
        ModoReadOnly()

        dgvProductos.DataSource = Nothing
        dgvProductos.Rows.Clear()

        cmbBuscar.SelectedIndex = Index
        Operacion = 1
    End Sub
    Private Sub ActualizarCmbBuscar()
        Dim UnGestorProducto As New Modelo.GestorProducto
        cmbBuscar.DataSource = UnGestorProducto.ListarTodosNombre
        cmbBuscar.DisplayMember = "Nombre"
    End Sub
    Private Sub ActualizarCmbCategoria()
        Dim UnGestorCategoria As New Modelo.GestorCategoria
        cmbCategoria.DataSource = UnGestorCategoria.ListarTodosNombre
        cmbCategoria.DisplayMember = "Categoria"
    End Sub
    Private Sub ActualizarCmbProveedor()
        Dim UnGestorProveedor As New Modelo.GestorProveedor
        cmbProveedor.DataSource = UnGestorProveedor.ListarTodosNombre
        cmbProveedor.DisplayMember = "Nombre"
    End Sub
    Private Sub ActualizarCmbBucarCat()
        Dim UnGestorCategoria As New Modelo.GestorCategoria
        cmbBuscarCat.DataSource = UnGestorCategoria.ListarTodosNombre
        cmbBuscarCat.DisplayMember = "Categoria"
    End Sub
    Private Sub ActualizarCmbBuscarProv()
        Dim UnGestorProveedor As New Modelo.GestorProveedor
        cmbBuscarProv.DataSource = UnGestorProveedor.ListarTodosNombre
        cmbBuscarProv.DisplayMember = "Nombre"
    End Sub

    '------ EVENTOS DE DATAGRIDVIEW ------
    Private Sub Cargardgv()
        Dim UnProveedor As New Modelo.Proveedor
        UnProveedor = cmbBuscarProv.SelectedItem
        Dim UnaCategoria As New Modelo.Categoria
        UnaCategoria = cmbBuscarCat.SelectedItem
        Dim UnGestorProducto As New Modelo.GestorProducto
        If cmbBuscarCat.SelectedIndex = -1 Then
            Dim UnProducto As New Modelo.Producto
            UnProducto.IdProveedor = UnProveedor.IdProveedor
            dgvProductos.DataSource = UnGestorProducto.ListarProductosPorProveedor(UnProducto)
        Else
            If cmbBuscarProv.SelectedIndex = -1 Then
                Dim UnProducto As New Modelo.Producto
                UnProducto.IdCategoria = UnaCategoria.IdCategoria
                dgvProductos.DataSource = UnGestorProducto.ListarProductosPorCategoria(UnProducto)
            Else
                Dim UnProducto As New Modelo.Producto
                UnProducto.IdProveedor = UnProveedor.IdProveedor
                UnProducto.IdCategoria = UnaCategoria.IdCategoria
                dgvProductos.DataSource = UnGestorProducto.ListarProductosPorCategoriaYProveedor(UnProducto)
            End If
        End If
        Configdgv()
    End Sub
    Private Sub Configdgv()
        'Columnas ocultas
        dgvProductos.Columns(0).Visible = False
        dgvProductos.Columns(1).Visible = False
        dgvProductos.Columns(4).Visible = False
        dgvProductos.Columns(6).Visible = False
        'Ancho Columnas
        dgvProductos.Columns(1).Width = 35
        dgvProductos.Columns(2).Width = 350
        dgvProductos.Columns(3).Width = 200
        dgvProductos.Columns(5).Width = 50
        dgvProductos.Columns(7).Width = 60
        dgvProductos.Columns(8).Width = 70
        'Alineacion columnas
        dgvProductos.Columns(5).DefaultCellStyle.Alignment = HorizontalAlignment.Center
        dgvProductos.Columns(7).DefaultCellStyle.Alignment = HorizontalAlignment.Center
        dgvProductos.Columns(8).DefaultCellStyle.Alignment = HorizontalAlignment.Center
        'Configuracion encabezados
        dgvProductos.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgvProductos.ColumnHeadersDefaultCellStyle.BackColor = Color.Black
        dgvProductos.SelectionMode = DataGridViewSelectionMode.FullRowSelect
    End Sub
    Private Sub dgvProductos_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvProductos.CellDoubleClick
        Cursor = Cursors.WaitCursor
        Dim UnProducto As New Modelo.Producto
        UnProducto.IdProducto = CInt(dgvProductos.CurrentRow.Cells(0).Value)
        Dim ProductoSeleccionado As New Modelo.Producto
        Dim UnGestorProducto As New Modelo.GestorProducto
        ProductoSeleccionado = UnGestorProducto.BuscarPorId(UnProducto)
        MostrarDetalleDeProducto(ProductoSeleccionado)
        ModoNormal()
        ModoReadOnly()
        Cursor = Cursors.Default
    End Sub

    '------ EVENTO DE CARGAR DE OBJETOS ------
    Private Sub MostrarDetalleDeProducto(UnProducto As Modelo.Producto)
        txtId.Text = UnProducto.IdProducto
        txtCodigo.Text = UnProducto.Codigo
        txtNombre.Text = UnProducto.Nombre
        txtDescripcion.Text = UnProducto.Descripcion
        Dim UnaCategoria As New Modelo.Categoria With {.IdCategoria = UnProducto.IdCategoria}
        Dim UnGestorCategoria As New Modelo.GestorCategoria
        cmbCategoria.Text = UnGestorCategoria.BuscarPorId(UnaCategoria).Categoria
        txtUnidad.Text = UnProducto.Unidad
        Dim UnProveedor As New Modelo.Proveedor With {.IdProveedor = UnProducto.IdProveedor}
        Dim UnGestorProveedor As New Modelo.GestorProveedor
        cmbProveedor.Text = UnGestorProveedor.BuscarPorId(UnProveedor).Nombre
        txtCosto.Text = Funciones.Precio(UnProducto.Costo)
        dtpFecha.Text = UnProducto.Fecha
        txtPrecio.Text = Funciones.Precio(Math.Round(CDbl(UnProducto.Costo) * frmPrincipal.ValorAgregado, 2))
    End Sub

    '------ EVENTOS DE TEXTBOX ------
    Private Sub LimpiarCasilleros()
        txtId.Text = ""
        txtNombre.Text = ""
        txtDescripcion.Text = ""
        txtCodigo.Text = ""
        txtCosto.Text = ""
        txtPrecio.Text = ""
        txtUnidad.Text = ""
        cmbBuscarCat.SelectedIndex = -1
        cmbBuscarProv.SelectedIndex = -1
        cmbBuscar.SelectedIndex = -1
        dtpFecha.Text = Date.Today
    End Sub
    Private Sub txtTextChanged(sender As Object, e As EventArgs) Handles txtNombre.Click, txtDescripcion.Click, txtUnidad.Click, txtCodigo.Click, txtCosto.Click, dtpFecha.Click, cmbCategoria.Click, cmbProveedor.Click
        If Operacion = 0 Then
        Else
            Operacion = 1
        End If
        If ABM = False Then
            ModoAbm()
            ModoEditar()
        Else
        End If
    End Sub
    Private Sub CostoTextChanged(sender As Object, e As EventArgs) Handles txtCosto.TextChanged
        Try
            txtPrecio.Text = Funciones.Precio(Funciones.QuitarPrecio((txtCosto.Text)) * frmPrincipal.ValorAgregado)
        Catch
        End Try
    End Sub
    Private Sub txtCosto_Leave(sender As Object, e As EventArgs) Handles txtCosto.Leave
        txtCosto.Text = Funciones.Precio(txtCosto.Text)
    End Sub

    '------ FUNCIONES GENERALES ------
    Public Sub ModoNormal()
        Dim UnGestorProducto As New Modelo.GestorProducto
        ActualizarCmbBuscar()

        If txtId.Text <> "" Then
            Dim UnProducto As New Modelo.Producto
            UnProducto.IdProducto = CInt(txtId.Text)
            Dim ProductoSeleccionado As New Modelo.Producto
            ProductoSeleccionado = UnGestorProducto.BuscarPorId(UnProducto)
            MostrarDetalleDeProducto(ProductoSeleccionado)
        End If

        btnNuevo.Enabled = True
        btnGuardar.Enabled = False
        btnEliminar.Enabled = True
        btnCancelar.Enabled = False
    End Sub
    Public Sub ModoReadOnly()
        cmbBuscar.Enabled = True
        cmbBuscarCat.Enabled = True
        cmbBuscarProv.Enabled = True
        cmbProveedor.Enabled = False
        cmbCategoria.Enabled = False

        dtpFecha.Enabled = False

        txtNombre.ReadOnly = True
        txtDescripcion.ReadOnly = True
        txtUnidad.ReadOnly = True
        txtCosto.ReadOnly = True
        txtCodigo.ReadOnly = True
        cmbCategoria.Enabled = False
        cmbProveedor.Enabled = False

        ABM = False
    End Sub
    Public Sub ModoAbm()
        btnNuevo.Enabled = False
        btnGuardar.Enabled = True
        btnEliminar.Enabled = False
        btnCancelar.Enabled = True
    End Sub
    Public Sub ModoEditar()
        cmbBuscar.Enabled = False
        cmbBuscarCat.Enabled = False
        cmbBuscarProv.Enabled = False
        cmbProveedor.Enabled = True
        cmbCategoria.Enabled = True

        dtpFecha.Enabled = True

        txtNombre.ReadOnly = False
        txtDescripcion.ReadOnly = False
        txtUnidad.ReadOnly = False
        txtCosto.ReadOnly = False
        txtCodigo.ReadOnly = False
        cmbBuscar.Enabled = False
        cmbBuscarCat.Enabled = False
        cmbBuscarProv.Enabled = False

        ABM = True
    End Sub
End Class



