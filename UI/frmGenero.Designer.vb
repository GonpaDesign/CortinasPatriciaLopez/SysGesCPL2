﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmGenero
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.gcGenero = New DevExpress.XtraEditors.GroupControl()
        Me.txtGenero1O = New System.Windows.Forms.TextBox()
        Me.Label51 = New System.Windows.Forms.Label()
        Me.txtGenero1P = New System.Windows.Forms.TextBox()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.txtGeneroQL1 = New System.Windows.Forms.TextBox()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.txtGeneroQA1 = New System.Windows.Forms.TextBox()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.cmbGenero1 = New System.Windows.Forms.ComboBox()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.btnGuardar = New DevExpress.XtraEditors.SimpleButton()
        Me.btnSalir = New DevExpress.XtraEditors.SimpleButton()
        Me.gcTotal = New DevExpress.XtraEditors.GroupControl()
        Me.txtFinal = New System.Windows.Forms.TextBox()
        Me.Label50 = New System.Windows.Forms.Label()
        Me.txtIVA = New System.Windows.Forms.TextBox()
        Me.Label49 = New System.Windows.Forms.Label()
        Me.txtTotalP = New System.Windows.Forms.TextBox()
        Me.Label46 = New System.Windows.Forms.Label()
        Me.gcItem = New DevExpress.XtraEditors.GroupControl()
        Me.Label56 = New System.Windows.Forms.Label()
        Me.SpinEdit1 = New DevExpress.XtraEditors.SpinEdit()
        Me.txtItemDesc = New System.Windows.Forms.TextBox()
        Me.Label55 = New System.Windows.Forms.Label()
        Me.cmbOpcion = New System.Windows.Forms.ComboBox()
        Me.Label54 = New System.Windows.Forms.Label()
        Me.txtObservaciones = New System.Windows.Forms.TextBox()
        Me.Label48 = New System.Windows.Forms.Label()
        Me.txtUbicacion = New System.Windows.Forms.TextBox()
        Me.Label47 = New System.Windows.Forms.Label()
        CType(Me.gcGenero, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gcGenero.SuspendLayout()
        CType(Me.gcTotal, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gcTotal.SuspendLayout()
        CType(Me.gcItem, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gcItem.SuspendLayout()
        CType(Me.SpinEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'gcGenero
        '
        Me.gcGenero.Controls.Add(Me.txtGenero1O)
        Me.gcGenero.Controls.Add(Me.Label51)
        Me.gcGenero.Controls.Add(Me.txtGenero1P)
        Me.gcGenero.Controls.Add(Me.Label26)
        Me.gcGenero.Controls.Add(Me.txtGeneroQL1)
        Me.gcGenero.Controls.Add(Me.Label21)
        Me.gcGenero.Controls.Add(Me.txtGeneroQA1)
        Me.gcGenero.Controls.Add(Me.Label20)
        Me.gcGenero.Controls.Add(Me.cmbGenero1)
        Me.gcGenero.Controls.Add(Me.Label23)
        Me.gcGenero.Location = New System.Drawing.Point(12, 126)
        Me.gcGenero.LookAndFeel.SkinName = "Visual Studio 2013 Dark"
        Me.gcGenero.LookAndFeel.UseDefaultLookAndFeel = False
        Me.gcGenero.Name = "gcGenero"
        Me.gcGenero.Size = New System.Drawing.Size(1090, 80)
        Me.gcGenero.TabIndex = 28
        Me.gcGenero.Text = "Genero"
        '
        'txtGenero1O
        '
        Me.txtGenero1O.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtGenero1O.Location = New System.Drawing.Point(99, 52)
        Me.txtGenero1O.Name = "txtGenero1O"
        Me.txtGenero1O.Size = New System.Drawing.Size(847, 21)
        Me.txtGenero1O.TabIndex = 37
        '
        'Label51
        '
        Me.Label51.AutoSize = True
        Me.Label51.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label51.Location = New System.Drawing.Point(11, 55)
        Me.Label51.Name = "Label51"
        Me.Label51.Size = New System.Drawing.Size(82, 15)
        Me.Label51.TabIndex = 38
        Me.Label51.Text = "Observacion:"
        '
        'txtGenero1P
        '
        Me.txtGenero1P.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtGenero1P.Location = New System.Drawing.Point(1005, 52)
        Me.txtGenero1P.Name = "txtGenero1P"
        Me.txtGenero1P.Size = New System.Drawing.Size(80, 21)
        Me.txtGenero1P.TabIndex = 32
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label26.Location = New System.Drawing.Point(952, 55)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(47, 15)
        Me.Label26.TabIndex = 31
        Me.Label26.Text = "Precio:"
        '
        'txtGeneroQL1
        '
        Me.txtGeneroQL1.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtGeneroQL1.Location = New System.Drawing.Point(1045, 24)
        Me.txtGeneroQL1.MaxLength = 5
        Me.txtGeneroQL1.Name = "txtGeneroQL1"
        Me.txtGeneroQL1.Size = New System.Drawing.Size(40, 21)
        Me.txtGeneroQL1.TabIndex = 29
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label21.Location = New System.Drawing.Point(974, 27)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(65, 15)
        Me.Label21.TabIndex = 30
        Me.Label21.Text = "Largo (m):"
        '
        'txtGeneroQA1
        '
        Me.txtGeneroQA1.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtGeneroQA1.Location = New System.Drawing.Point(928, 25)
        Me.txtGeneroQA1.MaxLength = 5
        Me.txtGeneroQA1.Name = "txtGeneroQA1"
        Me.txtGeneroQA1.Size = New System.Drawing.Size(40, 21)
        Me.txtGeneroQA1.TabIndex = 25
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label20.Location = New System.Drawing.Point(854, 28)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(68, 15)
        Me.Label20.TabIndex = 26
        Me.Label20.Text = "Ancho (m):"
        '
        'cmbGenero1
        '
        Me.cmbGenero1.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbGenero1.FormattingEnabled = True
        Me.cmbGenero1.Location = New System.Drawing.Point(99, 24)
        Me.cmbGenero1.Name = "cmbGenero1"
        Me.cmbGenero1.Size = New System.Drawing.Size(749, 22)
        Me.cmbGenero1.TabIndex = 1
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label23.Location = New System.Drawing.Point(42, 27)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(51, 15)
        Me.Label23.TabIndex = 2
        Me.Label23.Text = "Genero:"
        '
        'btnGuardar
        '
        Me.btnGuardar.Appearance.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold)
        Me.btnGuardar.Appearance.Options.UseFont = True
        Me.btnGuardar.Cursor = System.Windows.Forms.Cursors.Default
        Me.btnGuardar.Location = New System.Drawing.Point(946, 662)
        Me.btnGuardar.LookAndFeel.SkinName = "Visual Studio 2013 Dark"
        Me.btnGuardar.LookAndFeel.UseDefaultLookAndFeel = False
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(75, 27)
        Me.btnGuardar.TabIndex = 62
        Me.btnGuardar.Text = "Guardar"
        '
        'btnSalir
        '
        Me.btnSalir.Appearance.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold)
        Me.btnSalir.Appearance.Options.UseFont = True
        Me.btnSalir.Location = New System.Drawing.Point(1027, 662)
        Me.btnSalir.LookAndFeel.SkinName = "Visual Studio 2013 Dark"
        Me.btnSalir.LookAndFeel.UseDefaultLookAndFeel = False
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(75, 27)
        Me.btnSalir.TabIndex = 61
        Me.btnSalir.Text = "Cerrar"
        '
        'gcTotal
        '
        Me.gcTotal.Controls.Add(Me.txtFinal)
        Me.gcTotal.Controls.Add(Me.Label50)
        Me.gcTotal.Controls.Add(Me.txtIVA)
        Me.gcTotal.Controls.Add(Me.Label49)
        Me.gcTotal.Controls.Add(Me.txtTotalP)
        Me.gcTotal.Controls.Add(Me.Label46)
        Me.gcTotal.Location = New System.Drawing.Point(336, 633)
        Me.gcTotal.LookAndFeel.SkinName = "Visual Studio 2013 Dark"
        Me.gcTotal.LookAndFeel.UseDefaultLookAndFeel = False
        Me.gcTotal.Name = "gcTotal"
        Me.gcTotal.Size = New System.Drawing.Size(448, 56)
        Me.gcTotal.TabIndex = 64
        Me.gcTotal.Text = "Total:"
        '
        'txtFinal
        '
        Me.txtFinal.Location = New System.Drawing.Point(340, 24)
        Me.txtFinal.Name = "txtFinal"
        Me.txtFinal.Size = New System.Drawing.Size(90, 20)
        Me.txtFinal.TabIndex = 32
        '
        'Label50
        '
        Me.Label50.AutoSize = True
        Me.Label50.Font = New System.Drawing.Font("Cambria", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label50.Location = New System.Drawing.Point(292, 24)
        Me.Label50.Name = "Label50"
        Me.Label50.Size = New System.Drawing.Size(43, 16)
        Me.Label50.TabIndex = 31
        Me.Label50.Text = "Final:"
        '
        'txtIVA
        '
        Me.txtIVA.Location = New System.Drawing.Point(196, 24)
        Me.txtIVA.Name = "txtIVA"
        Me.txtIVA.Size = New System.Drawing.Size(90, 20)
        Me.txtIVA.TabIndex = 30
        '
        'Label49
        '
        Me.Label49.AutoSize = True
        Me.Label49.Font = New System.Drawing.Font("Cambria", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label49.Location = New System.Drawing.Point(157, 24)
        Me.Label49.Name = "Label49"
        Me.Label49.Size = New System.Drawing.Size(34, 16)
        Me.Label49.TabIndex = 29
        Me.Label49.Text = "IVA:"
        '
        'txtTotalP
        '
        Me.txtTotalP.Location = New System.Drawing.Point(61, 24)
        Me.txtTotalP.Name = "txtTotalP"
        Me.txtTotalP.Size = New System.Drawing.Size(90, 20)
        Me.txtTotalP.TabIndex = 28
        '
        'Label46
        '
        Me.Label46.AutoSize = True
        Me.Label46.Font = New System.Drawing.Font("Cambria", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label46.Location = New System.Drawing.Point(4, 24)
        Me.Label46.Name = "Label46"
        Me.Label46.Size = New System.Drawing.Size(53, 16)
        Me.Label46.TabIndex = 27
        Me.Label46.Text = "Precio:"
        '
        'gcItem
        '
        Me.gcItem.Controls.Add(Me.Label56)
        Me.gcItem.Controls.Add(Me.SpinEdit1)
        Me.gcItem.Controls.Add(Me.txtItemDesc)
        Me.gcItem.Controls.Add(Me.Label55)
        Me.gcItem.Controls.Add(Me.cmbOpcion)
        Me.gcItem.Controls.Add(Me.Label54)
        Me.gcItem.Controls.Add(Me.txtObservaciones)
        Me.gcItem.Controls.Add(Me.Label48)
        Me.gcItem.Controls.Add(Me.txtUbicacion)
        Me.gcItem.Controls.Add(Me.Label47)
        Me.gcItem.Location = New System.Drawing.Point(12, 12)
        Me.gcItem.LookAndFeel.SkinName = "Visual Studio 2013 Dark"
        Me.gcItem.LookAndFeel.UseDefaultLookAndFeel = False
        Me.gcItem.Name = "gcItem"
        Me.gcItem.Size = New System.Drawing.Size(1090, 108)
        Me.gcItem.TabIndex = 65
        Me.gcItem.Text = "Datos Item:"
        '
        'Label56
        '
        Me.Label56.AutoSize = True
        Me.Label56.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label56.Location = New System.Drawing.Point(955, 85)
        Me.Label56.Name = "Label56"
        Me.Label56.Size = New System.Drawing.Size(60, 15)
        Me.Label56.TabIndex = 64
        Me.Label56.Text = "Cantidad:"
        '
        'SpinEdit1
        '
        Me.SpinEdit1.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.SpinEdit1.Location = New System.Drawing.Point(1021, 81)
        Me.SpinEdit1.Name = "SpinEdit1"
        Me.SpinEdit1.Properties.Appearance.Font = New System.Drawing.Font("SansSerif", 9.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SpinEdit1.Properties.Appearance.Options.UseFont = True
        Me.SpinEdit1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.SpinEdit1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.SpinEdit1.Properties.MaxValue = New Decimal(New Integer() {20, 0, 0, 0})
        Me.SpinEdit1.Size = New System.Drawing.Size(64, 22)
        Me.SpinEdit1.TabIndex = 63
        '
        'txtItemDesc
        '
        Me.txtItemDesc.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtItemDesc.Location = New System.Drawing.Point(99, 24)
        Me.txtItemDesc.Multiline = True
        Me.txtItemDesc.Name = "txtItemDesc"
        Me.txtItemDesc.Size = New System.Drawing.Size(687, 23)
        Me.txtItemDesc.TabIndex = 62
        '
        'Label55
        '
        Me.Label55.AutoSize = True
        Me.Label55.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label55.Location = New System.Drawing.Point(15, 28)
        Me.Label55.Name = "Label55"
        Me.Label55.Size = New System.Drawing.Size(78, 15)
        Me.Label55.TabIndex = 61
        Me.Label55.Text = "Descripción:"
        '
        'cmbOpcion
        '
        Me.cmbOpcion.DisplayMember = "0"
        Me.cmbOpcion.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbOpcion.FormattingEnabled = True
        Me.cmbOpcion.Items.AddRange(New Object() {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9"})
        Me.cmbOpcion.Location = New System.Drawing.Point(1024, 53)
        Me.cmbOpcion.Name = "cmbOpcion"
        Me.cmbOpcion.Size = New System.Drawing.Size(61, 22)
        Me.cmbOpcion.TabIndex = 60
        '
        'Label54
        '
        Me.Label54.AutoSize = True
        Me.Label54.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label54.Location = New System.Drawing.Point(992, 56)
        Me.Label54.Name = "Label54"
        Me.Label54.Size = New System.Drawing.Size(26, 15)
        Me.Label54.TabIndex = 31
        Me.Label54.Text = "Op:"
        '
        'txtObservaciones
        '
        Me.txtObservaciones.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtObservaciones.Location = New System.Drawing.Point(99, 53)
        Me.txtObservaciones.Multiline = True
        Me.txtObservaciones.Name = "txtObservaciones"
        Me.txtObservaciones.Size = New System.Drawing.Size(850, 46)
        Me.txtObservaciones.TabIndex = 30
        '
        'Label48
        '
        Me.Label48.AutoSize = True
        Me.Label48.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label48.Location = New System.Drawing.Point(11, 55)
        Me.Label48.Name = "Label48"
        Me.Label48.Size = New System.Drawing.Size(82, 15)
        Me.Label48.TabIndex = 29
        Me.Label48.Text = "Observación:"
        '
        'txtUbicacion
        '
        Me.txtUbicacion.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtUbicacion.Location = New System.Drawing.Point(864, 24)
        Me.txtUbicacion.Multiline = True
        Me.txtUbicacion.Name = "txtUbicacion"
        Me.txtUbicacion.Size = New System.Drawing.Size(221, 23)
        Me.txtUbicacion.TabIndex = 28
        '
        'Label47
        '
        Me.Label47.AutoSize = True
        Me.Label47.Font = New System.Drawing.Font("SansSerif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label47.Location = New System.Drawing.Point(792, 28)
        Me.Label47.Name = "Label47"
        Me.Label47.Size = New System.Drawing.Size(66, 15)
        Me.Label47.TabIndex = 27
        Me.Label47.Text = "Ubicación:"
        '
        'frmGenero
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoSize = True
        Me.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.BackColor = System.Drawing.Color.Black
        Me.ClientSize = New System.Drawing.Size(1120, 701)
        Me.Controls.Add(Me.gcItem)
        Me.Controls.Add(Me.gcTotal)
        Me.Controls.Add(Me.btnGuardar)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(Me.gcGenero)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.MaximumSize = New System.Drawing.Size(1366, 701)
        Me.MinimumSize = New System.Drawing.Size(1120, 701)
        Me.Name = "frmGenero"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Tag = "Genero"
        Me.Text = "Generos"
        CType(Me.gcGenero, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gcGenero.ResumeLayout(False)
        Me.gcGenero.PerformLayout()
        CType(Me.gcTotal, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gcTotal.ResumeLayout(False)
        Me.gcTotal.PerformLayout()
        CType(Me.gcItem, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gcItem.ResumeLayout(False)
        Me.gcItem.PerformLayout()
        CType(Me.SpinEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gcGenero As DevExpress.XtraEditors.GroupControl
    Friend WithEvents txtGenero1O As System.Windows.Forms.TextBox
    Friend WithEvents Label51 As System.Windows.Forms.Label
    Friend WithEvents txtGenero1P As System.Windows.Forms.TextBox
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents txtGeneroQL1 As System.Windows.Forms.TextBox
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents txtGeneroQA1 As System.Windows.Forms.TextBox
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents cmbGenero1 As System.Windows.Forms.ComboBox
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents btnGuardar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnSalir As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents gcTotal As DevExpress.XtraEditors.GroupControl
    Friend WithEvents txtFinal As System.Windows.Forms.TextBox
    Friend WithEvents Label50 As System.Windows.Forms.Label
    Friend WithEvents txtIVA As System.Windows.Forms.TextBox
    Friend WithEvents Label49 As System.Windows.Forms.Label
    Friend WithEvents txtTotalP As System.Windows.Forms.TextBox
    Friend WithEvents Label46 As System.Windows.Forms.Label
    Friend WithEvents gcItem As DevExpress.XtraEditors.GroupControl
    Friend WithEvents Label56 As System.Windows.Forms.Label
    Friend WithEvents SpinEdit1 As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents txtItemDesc As System.Windows.Forms.TextBox
    Friend WithEvents Label55 As System.Windows.Forms.Label
    Friend WithEvents cmbOpcion As System.Windows.Forms.ComboBox
    Friend WithEvents Label54 As System.Windows.Forms.Label
    Friend WithEvents txtObservaciones As System.Windows.Forms.TextBox
    Friend WithEvents Label48 As System.Windows.Forms.Label
    Friend WithEvents txtUbicacion As System.Windows.Forms.TextBox
    Friend WithEvents Label47 As System.Windows.Forms.Label
End Class
