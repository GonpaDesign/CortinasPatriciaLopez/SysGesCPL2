﻿Imports System.Globalization
Imports iTextSharp
Imports iTextSharp.text
Imports iTextSharp.text.pdf
Imports System.IO

Public Class frmPresupuesto
    Private UnGestorClientes As New Modelo.GestorClientes
    Private UnGestorItem As New Modelo.GestorItem
    Private UnGestorPresupuesto As New Modelo.GestorPresupuesto
    Private Operacion As Integer ' 0=Alta, 1=Modificacion
    Public Item As Integer ' Variable que indicara el numero de Item a generar
    Public Itemselected As Integer
    Public PresupuestoId As Integer ' Variable que indicara el numero de Presupuesto a generar
    Dim Colocacion As Boolean = False

    '----- EVENTOS DE FORMULARIO ------
    Private Sub frmPresupuesto_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.ShowIcon = False

        Dim forceDotCulture As CultureInfo
        forceDotCulture = Application.CurrentCulture.Clone()
        forceDotCulture.NumberFormat.NumberDecimalSeparator = "."
        Application.CurrentCulture = forceDotCulture

        ActualizarCmbcliente()
        cmbCliente.Enabled = False
        ActualizarCmbIdPresupuesto()
        DeshabilitarItems()
        Iniciartxt()
        btnPresExcel.Enabled = False
        btnTallerPDF.Enabled = False
    End Sub
    Private Sub frmPresupuesto_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        frmPrincipal.Main = 0
        frmPrincipal.Show()
        frmPrincipal.MostrarBotones()
    End Sub

    '------ EVENTO DE BOTONES ------
    Private Sub btnSalir_Click(sender As System.Object, e As System.EventArgs) Handles btnSalir.Click
        Close()
        frmPrincipal.Main = 0
        frmPrincipal.Show()
        frmPrincipal.MostrarBotones()
    End Sub
    Private Sub btnEliminar_Click(sender As Object, e As EventArgs) Handles btnEliminar.Click
        Dim UnPresupuestoAEliminar As New Modelo.Presupuesto
        UnPresupuestoAEliminar.IdPresupuesto = Integer.Parse(cmbIdPresupuesto.Text)
        If MsgBox(String.Format("Desea eliminar el presupuesto N° {0} perteneciente al cliente {1} ?", cmbIdPresupuesto.Text, cmbCliente.Text), vbOKCancel, "Eliminar Cliente") = vbOK Then
            UnGestorPresupuesto.Eliminar(UnPresupuestoAEliminar)
        End If
        ActualizarCmbcliente()
        ActualizarCmbIdPresupuesto()
        txtIdCliente.ReadOnly = True
        cmbIdPresupuesto.Text = ""
        cmbCliente.Text = ""
    End Sub
    Private Sub btnAprobar_Click(sender As Object, e As EventArgs) Handles btnAprobar.Click
        Dim UnPresupuesto As New Modelo.Presupuesto
        UnPresupuesto.IdPresupuesto = Integer.Parse(cmbIdPresupuesto.Text)
        If MsgBox(String.Format("Desea dar por aprobado el presupuesto N° {0} perteneciente al cliente {1} ?", cmbIdPresupuesto.Text, cmbCliente.Text), vbOKCancel, "Eliminar Cliente") = vbOK Then
            UnGestorPresupuesto.Aprobar(UnPresupuesto)
        End If
        btnCortina.Enabled = False
        btnFundas.Enabled = False
        btnSillones.Enabled = False
        btnTela.Enabled = False
    End Sub
    Private Sub btnAprobarItem_Click(sender As Object, e As EventArgs) Handles btnAprobarItem.Click
        If CBool(dgvItems.CurrentRow.Cells(7).Value) = False Then
            Dim UnItem As New Modelo.Item
            UnItem.IdItem = CInt(dgvItems.CurrentRow.Cells(0).Value)
            UnGestorItem.Aprobar(UnItem)
        Else
            Dim UnItem As New Modelo.Item
            UnItem.IdItem = CInt(dgvItems.CurrentRow.Cells(0).Value)
            UnGestorItem.Desaprobar(UnItem)
        End If
        Dim UnPresupuesto As New Modelo.Presupuesto
        UnPresupuesto.IdPresupuesto = CInt(dgvItems.CurrentRow.Cells(1).Value)
        Actualizardvg(UnPresupuesto)
    End Sub
    Private Sub btnPresupuestopdf(sender As Object, e As EventArgs) Handles btnPresPdf.Click
        Try
            Dim documentoPDF As New Document(iTextSharp.text.PageSize.A4.Rotate, 50, 50, 50, 35)
            Dim saveFileDialog1 As New SaveFileDialog()

            saveFileDialog1.FileName = String.Format("Presupuesto N° {0}", cmbIdPresupuesto.Text)
            saveFileDialog1.Filter = "PDF Files (*.pdf)|*.pdf"
            saveFileDialog1.FilterIndex = 2
            saveFileDialog1.RestoreDirectory = True
            Dim writer As PdfWriter
            If saveFileDialog1.ShowDialog() = DialogResult.OK Then
                writer = PdfWriter.GetInstance(documentoPDF, New FileStream(saveFileDialog1.FileName, FileMode.Create))
               End If

            documentoPDF.Open()

            'Añadimos los metadatos para el fichero PDF
            documentoPDF.AddAuthor("Cortinas Patricia Lopez")
            'documentoPDF.AddCreator(Usuario)
            Dim Key As String = String.Format("Presupuesto N° {0}", cmbIdPresupuesto.Text)
            'documentoPDF.AddKeywords(Key)
            'documentoPDF.AddSubject(Key)
            documentoPDF.AddTitle(Key)
            documentoPDF.AddCreationDate()

            'Se agrega el PDFTable al documento.
            documentoPDF.Add(Titulo())
            documentoPDF.Add(Encabezado())
            documentoPDF.Add(Datoscliente())
            documentoPDF.Add(ObtenerTablaItems(dgvItems))
            'documentoPDF.Add(Totales())
            documentoPDF.Add(Notasdepago())

            documentoPDF.Close() 'Cerramos el objeto documento, guardamos y creamos el PDF

            'Comprobamos si se ha creado el fichero PDF
            If System.IO.File.Exists(saveFileDialog1.FileName) Then
                If MsgBox("Datos Exportados correctamente " + "¿Desea abrir el archivo?", MsgBoxStyle.Question + MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
                    'Abrimos el fichero PDF con la aplicación asociada
                    System.Diagnostics.Process.Start(saveFileDialog1.FileName)
                End If
            Else
                MsgBox("El fichero PDF no se ha generado, " + "compruebe que tiene permisos en la carpeta de destino.", MsgBoxStyle.Exclamation + MsgBoxStyle.OkOnly)
            End If
        Catch ex As Exception
            MsgBox("Se ha producido un error al intentar convertir el presupuesto a PDF: " + ex.Message, MsgBoxStyle.Critical + MsgBoxStyle.OkOnly)
        End Try
    End Sub
    Private Sub btnNuevo_Click(sender As System.Object, e As System.EventArgs) Handles btnNuevo.Click
        Operacion = 0

        txtFechaGenerado.Text = Date.Today
        txtFechaEntrega.Text = Date.Today.AddDays(15)
        txtIdCliente.Text = ""
        txtNotas.Text = ""
        txtVigencia.Text = "7"
        cmbCliente.Text = ""
        cmbIdPresupuesto.Enabled = False
        chkColocacion.Checked = False
        HabilitarItems()
        Limpiardgv()
    End Sub
    Private Sub btnGuardar_Click(sender As System.Object, e As System.EventArgs) Handles btnGuardar.Click
        Dim UnPresupuesto As New Modelo.Presupuesto
        UnPresupuesto.IdCliente = txtIdCliente.Text
        UnPresupuesto.FechaGenerado = txtFechaGenerado.Text
        UnPresupuesto.Colocacion = chkColocacion.Checked
        UnPresupuesto.Vigencia = txtVigencia.Text
        UnPresupuesto.FechaEntrega = txtFechaEntrega.Text
        UnPresupuesto.Notas = txtNotas.Text

        Select Case Operacion
            Case 0 ' ALTA
                UnGestorPresupuesto.Nuevo(UnPresupuesto)
            Case 1
                ' MODIFICACION
                UnPresupuesto.IdPresupuesto = Integer.Parse(cmbIdPresupuesto.Text)
                UnGestorPresupuesto.Modificar(UnPresupuesto)
        End Select
        DeshabilitarItems()
        cmbIdPresupuesto.Enabled = True
    End Sub
    Private Sub btnCortina_Click(sender As Object, e As EventArgs) Handles btnCortina.Click
        Cursor = System.Windows.Forms.Cursors.WaitCursor
        AgregarItem()
        frmCortinas.MdiParent = frmPrincipal
        frmCortinas.Dock = DockStyle.Fill
        frmCortinas.Show()
        Cursor = System.Windows.Forms.Cursors.Default
    End Sub
    Private Sub btnTela_Click(sender As Object, e As EventArgs) Handles btnTela.Click
        Cursor = System.Windows.Forms.Cursors.WaitCursor
        AgregarItem()
        frmGenero.MdiParent = frmPrincipal
        frmGenero.Dock = DockStyle.Fill
        frmGenero.Show()
        Cursor = System.Windows.Forms.Cursors.Default
    End Sub
    Private Sub btnFundas_Click(sender As Object, e As EventArgs) Handles btnFundas.Click
        Cursor = System.Windows.Forms.Cursors.WaitCursor
        AgregarItem()
        frmFundas.MdiParent = frmPrincipal
        frmFundas.Dock = DockStyle.Fill
        frmFundas.Show()
        Cursor = System.Windows.Forms.Cursors.Default
    End Sub
    Private Sub btnSillones_Click(sender As Object, e As EventArgs) Handles btnSillones.Click
        Cursor = System.Windows.Forms.Cursors.WaitCursor
        AgregarItem()
        frmSillones.MdiParent = frmPrincipal
        frmSillones.Dock = DockStyle.Fill
        frmSillones.Show()
        Cursor = System.Windows.Forms.Cursors.Default
    End Sub
    Private Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        Operacion = 0
        DeshabilitarItems()
        cmbIdPresupuesto.Enabled = True
    End Sub
    Private Sub btnEnviar_Click(sender As Object, e As EventArgs) Handles btnEnviar.Click
        Dim UnPresupuestoAEnviar As New Modelo.Presupuesto
        UnPresupuestoAEnviar.IdPresupuesto = Integer.Parse(cmbIdPresupuesto.Text)
        If MsgBox("Desea dar por enviado el presupuesto N° " & cmbIdPresupuesto.Text & " perteneciente al cliente " & cmbCliente.Text & " ?", vbOKCancel, "Eliminar Cliente") = vbOK Then
            UnGestorPresupuesto.Enviar(UnPresupuestoAEnviar)
        End If
    End Sub

    '------ FUNCIONES PARA EXPORTAR A PDF -----
    Private Function ObtenerTablaItems(dg As DataGridView)
        'Genero tabla con items del presupuesto
        Dim DataTable As New PdfPTable(6) 'Se crea un objeto PDFTable con el numero de columnas del DataGridView.

        Dim fntHeader As iTextSharp.text.Font = FontFactory.GetFont(Font.Name = "Cambria", 10, iTextSharp.text.Font.BOLD, BaseColor.WHITE)
        Dim fntBody As iTextSharp.text.Font = FontFactory.GetFont(Font.Name = "Calibri", 9, iTextSharp.text.Font.NORMAL, BaseColor.BLACK)

        'Se asignan algunas propiedades para el diseño del pdf
        DataTable.DefaultCell.Padding = 3

        DataTable.WidthPercentage = 100
        Dim TblWidths(5) As Single
        TblWidths(0) = 45
        TblWidths(1) = 115
        TblWidths(2) = 500
        TblWidths(3) = 200
        TblWidths(4) = 70
        TblWidths(5) = 60
        DataTable.SetWidths(TblWidths)

        DataTable.DefaultCell.BorderWidth = 2
        DataTable.DefaultCell.HorizontalAlignment = Element.ALIGN_CENTER


        DataTable.WidthPercentage = 100

        'Se capturan los nombres de las columnas del DataGridView.

        Dim Cell1 As New PdfPCell(New Phrase(dg.Columns(2).HeaderText, fntHeader))
        DataTable.AddCell(Cell1).GrayFill = True
        Dim Cell2 As New PdfPCell(New Phrase(dg.Columns(9).HeaderText, fntHeader))
        DataTable.AddCell(Cell2).GrayFill = True
        Dim Cell3 As New PdfPCell(New Phrase(dg.Columns(3).HeaderText, fntHeader))
        DataTable.AddCell(Cell3).GrayFill = True
        Dim Cell4 As New PdfPCell(New Phrase(dg.Columns(4).HeaderText, fntHeader))
        DataTable.AddCell(Cell4).GrayFill = True
        Dim Cell5 As New PdfPCell(New Phrase(dg.Columns(10).HeaderText, fntHeader))
        DataTable.AddCell(Cell5).GrayFill = True
        Dim Cell6 As New PdfPCell(New Phrase(dg.Columns(5).HeaderText, fntHeader))
        DataTable.AddCell(Cell6).GrayFill = True
        DataTable.HeaderRows = 1
        DataTable.DefaultCell.BorderWidth = 1
        'Se generan las columnas del DataGridView. 
        For i As Integer = 0 To dg.RowCount - 1
            Dim Cell01 As New PdfPCell(New Phrase(CStr(i + 1), fntBody))
            DataTable.AddCell(Cell01).HorizontalAlignment = Element.ALIGN_CENTER
            Dim Cell02 As New PdfPCell(New Phrase(dg(9, i).Value.ToString(), fntBody))
            DataTable.AddCell(Cell02).HorizontalAlignment = Element.ALIGN_CENTER
            Dim Cell03 As New PdfPCell(New Phrase(dg(3, i).Value.ToString(), fntBody))
            DataTable.AddCell(Cell03)
            Dim Cell04 As New PdfPCell(New Phrase(dg(4, i).Value.ToString(), fntBody))
            DataTable.AddCell(Cell04).HorizontalAlignment = Element.ALIGN_CENTER
            Dim Cell05 As New PdfPCell(New Phrase(dg(10, i).Value.ToString(), fntBody))
            DataTable.AddCell(Cell05).HorizontalAlignment = Element.ALIGN_CENTER
            Dim Cell06 As New PdfPCell(New Phrase(String.Format("$ {0}", dg(5, i).Value.ToString()), fntBody))
            DataTable.AddCell(Cell06).HorizontalAlignment = Element.ALIGN_CENTER
            DataTable.CompleteRow()
        Next
        Return DataTable
    End Function
    Private Function Titulo() As Paragraph
        Dim fntHeader As iTextSharp.text.Font = FontFactory.GetFont(Font.Name = "Cambria", 20, iTextSharp.text.Font.BOLD, BaseColor.BLACK)
        Dim fntBody As iTextSharp.text.Font = FontFactory.GetFont(Font.Name = "Calibri", 9, iTextSharp.text.Font.NORMAL, BaseColor.BLACK)

        Dim P As New Paragraph
        Dim C0 As New Chunk(vbNewLine)
        Dim C1 As New Chunk(String.Format("Cortinas Patricia López", cmbIdPresupuesto.Text), fntHeader)
        Dim Linea As New Chunk(New draw.LineSeparator())

        P.Alignment = HorizontalAlignment.Right
        P.Add(C1)
        P.Add(C0)
        P.Add(Linea)

        Titulo = P
        Return Titulo
    End Function
    Private Function Encabezado() As Paragraph
        Dim fntHeader As iTextSharp.text.Font = FontFactory.GetFont(Font.Name = "Cambria", 10, iTextSharp.text.Font.BOLD, BaseColor.BLACK)
        Dim fntBody As iTextSharp.text.Font = FontFactory.GetFont(Font.Name = "Calibri", 9, iTextSharp.text.Font.NORMAL, BaseColor.BLACK)

        Dim P As New Paragraph
        Dim C0 As New Chunk(vbNewLine)
        Dim C1 As New Chunk("Cuit: 27-14818451-3", fntBody)
        Dim C2 As New Chunk("IIBB: C.M. 902-649712-0", fntBody)
        Dim C3 As New Chunk("Visitenos en: Uruguay 1000, Beccar (1643)", fntBody)
        Dim C4 As New Chunk("Telefono: 4742-3205", fntBody)
        Dim glue As New Chunk(New draw.VerticalPositionMark())
        Dim Linea As New Chunk(New draw.LineSeparator())

        P.Alignment = HorizontalAlignment.Center
        P.Add(C3)
        P.Add(glue)
        P.Add(C1)
        P.Add(C0)
        P.Add(C4)
        P.Add(glue)
        P.Add(C2)
        P.Add(C0)
        P.Add(Linea)

        Encabezado = P
        Return Encabezado
    End Function
    Private Function Datoscliente() As Paragraph
        Dim fntHeader As iTextSharp.text.Font = FontFactory.GetFont(Font.Name = "Cambria", 10, iTextSharp.text.Font.BOLD, BaseColor.BLACK)
        Dim fntBody As iTextSharp.text.Font = FontFactory.GetFont(Font.Name = "Calibri", 9, iTextSharp.text.Font.NORMAL, BaseColor.BLACK)

        Dim P As New Paragraph
        Dim C0 As New Chunk(vbNewLine)
        Dim C1 As New Chunk("Cliente: ", fntHeader)
        Dim C2 As New Chunk(cmbCliente.Text, fntBody)
        Dim C3 As New Chunk("Direccion: ", fntHeader)
        'Dim C4 As New Chunk(txt, fntBody)
        Dim C5 As New Chunk("Mail: ", fntHeader)
        'Dim C6 As New Chunk(txt, fntBody)
        Dim C7 As New Chunk("Telefono: ", fntHeader)
        'Dim C8 As New Chunk(txt, fntBody)
        Dim C9 As New Chunk(String.Format("Presupuesto N°: "), fntHeader)
        Dim C10 As New Chunk(String.Format("Fecha: "), fntHeader)
        Dim C11 As New Chunk(String.Format("{0}", cmbIdPresupuesto.Text), fntBody)
        Dim C12 As New Chunk(String.Format("{0}", Date.Today.ToShortDateString), fntBody)
        Dim glue As New Chunk(New draw.VerticalPositionMark())

        P.Add(C0)
        P.Add(C1)
        P.Add(C2)
        P.Add(glue)
        P.Add(C9)
        P.Add(C11)
        P.Add(C0)
        P.Add(C3)
        'P.Add(C4)
        P.Add(glue)
        P.Add(C10)
        P.Add(C12)
        P.Add(C0)
        P.Add(C5)
        'P.Add(C6)
        P.Add(C0)
        P.Add(C7)
        'P.Add(C8)
        P.Add(C0)
        P.Add(C0)

        Datoscliente = P
        Return Datoscliente
    End Function
    Private Function Notasdepago() As Paragraph
        Dim fntHeader As iTextSharp.text.Font = FontFactory.GetFont(Font.Name = "Cambria", 10, iTextSharp.text.Font.BOLD, BaseColor.BLACK)
        Dim fntBody As iTextSharp.text.Font = FontFactory.GetFont(Font.Name = "Calibri", 9, iTextSharp.text.Font.NORMAL, BaseColor.BLACK)

        Dim P As New Paragraph
        Dim C0 As New Chunk(vbNewLine)
        Dim C1 As New Chunk("Observaciones: ", fntHeader)
        Dim C2 As New Chunk("Los precios son en efectivo.", fntBody)
        Dim C3 As New Chunk("Forma de Pago: ", fntHeader)
        Dim C4 As New Chunk("Se abona 50% por adelantado, saldo contra entrega.", fntBody)
        Dim C5 As New Chunk("Tarjetas: ", fntHeader)
        Dim C6 As New Chunk("Consultar", fntBody)
        Dim C7 As New Chunk(String.Format("El presupuesto tiene valides por {0} días", txtVigencia.Text), fntHeader)

        P.Add(C0)
        P.Add(C1)
        P.Add(C2)
        P.Add(C0)
        P.Add(C3)
        P.Add(C4)
        P.Add(C0)
        P.Add(C5)
        P.Add(C6)
        P.Add(C0)
        P.Add(C7)

        Notasdepago = P
        Return Notasdepago
    End Function

    '------ EVENTOS DE COMBOBOX ------
    Private Sub ActualizarCmbcliente()
        cmbCliente.DataSource = UnGestorClientes.ListarTodosNombre
        cmbCliente.DisplayMember = "Nombre"
    End Sub
    Private Sub ActualizarCmbIdPresupuesto()
        cmbIdPresupuesto.DataSource = UnGestorPresupuesto.ListarTodosNombre
        cmbIdPresupuesto.DisplayMember = "IdPresupuesto"
    End Sub
    Private Sub cmbCliente_SelectionChangeCommitted(sender As System.Object, e As System.EventArgs) Handles cmbCliente.SelectionChangeCommitted
        Dim ClienteSeleccionado As New Modelo.Cliente
        ClienteSeleccionado = UnGestorClientes.BuscarPorId(cmbCliente.SelectedItem)
        MostrarDetalleDeCliente(ClienteSeleccionado)
    End Sub
    Private Sub cmbIdPresupuesto_SelectionChangeCommitted(sender As System.Object, e As System.EventArgs) Handles cmbIdPresupuesto.SelectionChangeCommitted
        Operacion = 1

        Dim PresupuestoSeleccionado As New Modelo.Presupuesto
        PresupuestoSeleccionado = UnGestorPresupuesto.BuscarPorId(cmbIdPresupuesto.SelectedItem)
        MostrarDetalleDePresupuesto(PresupuestoSeleccionado)
        HabilitarItems()
    End Sub

    '------ FUNCIONES PARA CARGAR OBJETOS ------
    Private Sub MostrarDetalleDeCliente(UnCliente As Modelo.Cliente)
        txtIdCliente.Text = UnCliente.IdCliente
    End Sub
    Private Sub MostrarDetalleDePresupuesto(UnPresupuesto As Modelo.Presupuesto)
        Operacion = 1

        cmbIdPresupuesto.Text = UnPresupuesto.IdPresupuesto
        txtFechaGenerado.Text = UnPresupuesto.FechaGenerado
        txtIdCliente.Text = UnPresupuesto.IdCliente
        chkColocacion.Checked = UnPresupuesto.Colocacion
        txtVigencia.Text = UnPresupuesto.Vigencia
        txtFechaEntrega.Text = UnPresupuesto.FechaEntrega
        txtNotas.Text = UnPresupuesto.Notas
        PresupuestoId = CInt(cmbIdPresupuesto.Text)
        cmbCliente.Text = UnGestorClientes.BuscarPorIdP(UnPresupuesto).Nombre
        Actualizardvg(UnPresupuesto)

    End Sub
    Public Sub CargarPresupuesto(UnPresupuesto As Modelo.Presupuesto)
        Cursor = System.Windows.Forms.Cursors.WaitCursor
        Try
            Dim PresupuestoSeleccionado As New Modelo.Presupuesto
            PresupuestoSeleccionado = UnGestorPresupuesto.BuscarPorId(UnPresupuesto)
            MostrarDetalleDePresupuesto(PresupuestoSeleccionado)
            HabilitarItems()
            cmbIdPresupuesto.Enabled = False
        Catch
        End Try
        Cursor = System.Windows.Forms.Cursors.Default
    End Sub

    '------ FUNCIONES GENERALES
    Private Sub AgregarItem()
        Item = Item + 1
    End Sub
    Public Sub EliminarItem()
        Item = Item - 1
    End Sub
    Private Sub HabilitarItems()
        btnCortina.Enabled = True
        btnFundas.Enabled = True
        btnSillones.Enabled = True
        btnTela.Enabled = True
        btnGuardar.Enabled = Enabled
        btnEliminar.Show()
        btnAprobarItem.Enabled = True
        btnAprobar.Enabled = True
        btnPresPdf.Enabled = True
        btnEnviar.Enabled = True
        cmbCliente.Enabled = True
    End Sub
    Private Sub DeshabilitarItems()
        btnCortina.Enabled = False
        btnFundas.Enabled = False
        btnSillones.Enabled = False
        btnTela.Enabled = False
        btnGuardar.Enabled = False
        btnAprobarItem.Enabled = False
        btnAprobar.Enabled = False
        btnPresPdf.Enabled = False
        btnEnviar.Enabled = False
        btnEliminar.Hide()
        cmbCliente.Enabled = False
    End Sub
    Private Sub Iniciartxt()
        txtIdCliente.ReadOnly = True
        txtSubGen.ReadOnly = True
        txtSubOP1.ReadOnly = True
        txtSubOP2.ReadOnly = True
        txtTotal.ReadOnly = True
        txtTotalOP1.ReadOnly = True
        txtTotalOP2.ReadOnly = True
        cmbIdPresupuesto.Text = ""
        cmbCliente.Text = ""
    End Sub

    '------ EVENTOS DE DATAGRIDVIEW ------
    Public Sub Actualizardvg(Unpresupuesto As Modelo.Presupuesto)
        Dim unAccesoItem As New Modelo.DbItem
        dgvItems.DataSource = unAccesoItem.SelectItemPorPresupuesto(cmbIdPresupuesto.SelectedItem)

        Item = UnGestorItem.ObtenerItemMax(Unpresupuesto)
        txtTotal.Text = UnGestorItem.CalcularTotal(Unpresupuesto.IdPresupuesto)
        txtSubOP1.Text = UnGestorItem.CalcularPrecioOpcion(Unpresupuesto.IdPresupuesto, 1)
        txtSubOP2.Text = UnGestorItem.CalcularPrecioOpcion(Unpresupuesto.IdPresupuesto, 2)
        txtSubGen.Text = UnGestorItem.CalcularPrecioOpcion(Unpresupuesto.IdPresupuesto, 0)
        txtTotalOP1.Text = CDbl(txtSubGen.Text) + CDbl(txtSubOP1.Text)
        txtTotalOP2.Text = CDbl(txtSubGen.Text) + CDbl(txtSubOP2.Text)
        Configdvg()
    End Sub
    Private Sub dgvItems_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvItems.CellDoubleClick
        Cursor = System.Windows.Forms.Cursors.WaitCursor
        Dim UnItem As New Modelo.Item
        UnItem.Presupuesto = CInt(dgvItems.CurrentRow.Cells(1).Value)
        UnItem.Item = CInt(dgvItems.CurrentRow.Cells(2).Value)
        Dim M As Integer = CInt(dgvItems.CurrentRow.Cells(8).Value)
        Itemselected = UnItem.Item
        Select Case M
            Case 1
                frmCortinas.Operacion = 1
                frmCortinas.MdiParent = frmPrincipal
                frmCortinas.Dock = DockStyle.Fill
                frmCortinas.Show()
                frmCortinas.CargarArticulo(UnItem)
            Case 2
                frmGenero.Operacion = 1
                frmGenero.MdiParent = frmPrincipal
                frmGenero.Dock = DockStyle.Fill
                frmGenero.Show()
                frmGenero.CargarArticulo(UnItem)
            Case 3
                frmFundas.Operacion = 1
                frmFundas.MdiParent = frmPrincipal
                frmFundas.Dock = DockStyle.Fill
                frmFundas.Show()
                'frmFundas.CargarArticulo(UnItem)
            Case 4
                frmSillones.Operacion = 1
                frmSillones.MdiParent = frmPrincipal
                frmSillones.Dock = DockStyle.Fill
                frmSillones.Show()
                'frmSillones.CargarArticulo(UnItem)
        End Select
        Me.Hide()
        Cursor = System.Windows.Forms.Cursors.Default
    End Sub
    Private Sub dgvItems_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvItems.CellClick
        If CBool(dgvItems.CurrentRow.Cells(7).Value) = False Then
            btnAprobarItem.Text = "Aprobar Item"
        Else
            btnAprobarItem.Text = "Desaprobar Item"
        End If
    End Sub
    Private Sub Configdvg()
        dgvItems.Columns("IdItem").Visible = False
        dgvItems.Columns("Presupuesto").Visible = False
        dgvItems.Columns("Eliminado").Visible = False
        dgvItems.Columns("Categoria").Visible = False
        dgvItems.Columns("Item").DisplayIndex = 1
        dgvItems.Columns("Item").Width = 35
        dgvItems.Columns("Item").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgvItems.Columns("Ubicacion").DisplayIndex = 2
        dgvItems.Columns("Ubicacion").Width = 115
        dgvItems.Columns("Ubicacion").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgvItems.Columns("Descripcion").DisplayIndex = 3
        dgvItems.Columns("Descripcion").Width = 500
        dgvItems.Columns("Observaciones").DisplayIndex = 4
        dgvItems.Columns("Observaciones").Width = 200
        dgvItems.Columns("Observaciones").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgvItems.Columns("Precio").DisplayIndex = 5
        dgvItems.Columns("Precio").Width = 60
        dgvItems.Columns("Precio").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgvItems.Columns("Opcion").DisplayIndex = 6
        dgvItems.Columns("Opcion").Width = 60
        dgvItems.Columns("Opcion").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgvItems.Columns("Aprobado").DisplayIndex = 7
        dgvItems.Columns("Aprobado").Width = 60
        dgvItems.SelectionMode = DataGridViewSelectionMode.FullRowSelect
    End Sub
    Public Sub ActualizarListaItems()
        Dim UnGestorPresupuesto As New Modelo.GestorPresupuesto
        Dim PresupuestoSeleccionado As New Modelo.Presupuesto
        PresupuestoSeleccionado = UnGestorPresupuesto.BuscarPorId(cmbIdPresupuesto.SelectedItem)
        Actualizardvg(PresupuestoSeleccionado)
    End Sub
    Public Sub Limpiardgv()
        dgvItems.Dispose()
    End Sub
End Class